-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jan 31, 2021 at 12:45 AM
-- Server version: 10.3.27-MariaDB
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `smartsamv_smartsam`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_biro`
--

CREATE TABLE `tbl_biro` (
  `biro_no` int(11) NOT NULL,
  `biro_uuid` varchar(32) NOT NULL,
  `biro_username` varchar(20) NOT NULL,
  `biro_nama` varchar(30) NOT NULL,
  `biro_posisi` varchar(40) NOT NULL,
  `biro_time` timestamp NOT NULL DEFAULT current_timestamp(),
  `biro_email` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_biro`
--

INSERT INTO `tbl_biro` (`biro_no`, `biro_uuid`, `biro_username`, `biro_nama`, `biro_posisi`, `biro_time`, `biro_email`) VALUES
(2, '2', 'biro_akpk', 'Fahmi Rizal, S. H., M. M.', 'Biro AKPK', '2020-12-10 04:44:36', 'ilhamr6000@gmail.com'),
(1, '1', 'biro_umum', 'Ir. Adnan, M. M.', 'Biro Umum & Keuangan', '2020-12-10 04:44:36', 'adnanchmd48@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_distribusi_surat`
--

CREATE TABLE `tbl_distribusi_surat` (
  `dist_no` int(11) NOT NULL,
  `dist_uuid` varchar(32) NOT NULL,
  `dist_pengirim` varchar(30) NOT NULL,
  `dist_nomorsurat` varchar(30) NOT NULL,
  `dist_lampiran` varchar(20) NOT NULL,
  `dist_perihal` varchar(40) NOT NULL,
  `dist_penerima` varchar(40) NOT NULL,
  `dist_disposisi` varchar(40) NOT NULL COMMENT 'Current Disposisi',
  `dist_dokumen` varchar(255) NOT NULL,
  `dist_status` varchar(30) NOT NULL,
  `dist_updated_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `dist_tgl_terima` date NOT NULL,
  `dist_tgl_surat` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_distribusi_surat`
--

INSERT INTO `tbl_distribusi_surat` (`dist_no`, `dist_uuid`, `dist_pengirim`, `dist_nomorsurat`, `dist_lampiran`, `dist_perihal`, `dist_penerima`, `dist_disposisi`, `dist_dokumen`, `dist_status`, `dist_updated_at`, `dist_tgl_terima`, `dist_tgl_surat`) VALUES
(44, '0BE421945B9B11EB9F7D20677CDFA8E8', 'FKIP', '140/UN54.4/KP/2021', '1', 'Permohonan Operator Komputer Bagian Akad', '', 'rektor', 'FKIP-0BE421945B9B11EB9F7D20677CDFA8E8.pdf', 'Process', '2021-01-21 03:45:04', '2021-01-21', '0000-00-00'),
(45, '577318145B9B11EB9F7D20677CDFA8E8', 'Biro Umum dan Keuangan', '258/UN54/TU/2021', '1', 'Usulan Kegiatan PBJ 2021', '', 'kabag_perenc', 'Biro_Umum_dan_Keuangan-577318145B9B11EB9F7D20677CDFA8E8.pdf', 'Process', '2021-01-21 03:47:11', '2021-01-21', '0000-00-00'),
(46, 'AA44F39D5B9F11EB9F7D20677CDFA8E8', 'Fakultas Teknik', '192/UN54.5/KP/2021', '1', 'PERMOHONAN CUTI PNS a.n DONY SUHARYONO', '', 'kasubbag_peg', 'Fakultas_Teknik-AA44F39D5B9F11EB9F7D20677CDFA8E8.pdf', 'Process', '2021-01-21 04:18:07', '2021-01-21', '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_draft_surat`
--

CREATE TABLE `tbl_draft_surat` (
  `draft_no` int(11) NOT NULL,
  `draft_uuid` varchar(32) NOT NULL,
  `draft_asal` varchar(30) NOT NULL,
  `draft_penerima` varchar(32) NOT NULL,
  `draft_perihal` varchar(40) NOT NULL,
  `draft_created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `draft_status` varchar(30) NOT NULL,
  `draft_ket` varchar(255) NOT NULL,
  `draft_read` varchar(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_draft_surat`
--

INSERT INTO `tbl_draft_surat` (`draft_no`, `draft_uuid`, `draft_asal`, `draft_penerima`, `draft_perihal`, `draft_created_at`, `draft_status`, `draft_ket`, `draft_read`) VALUES
(7, '02531C7D2BA511EBBEB92CFDA1251793', 'pelaksana_tik1', '1324234', 'TES SURAT KELUAR UPT TIK', '2020-11-21 02:55:27', 'Done', '', '0'),
(5, '1194A66A1E0911EBB5512CFDA1251793', 'pelaksana9', '3', 'Perihal Pelaksana 9', '2020-11-03 19:16:26', 'Process', '', ''),
(6, '1C1918CE21E611EB8E842CFDA1251793', 'pelaksana10', '2234234', 'Sosialisasi Program Aplikasi SmartSam', '2020-11-08 17:16:02', 'Process', '', '0'),
(1, '1DFD19AD1D8D11EBB39D2CFDA1251793', 'pelaksana10', '3', 'TES', '2020-11-03 04:29:09', 'Process', '', ''),
(4, '48AB009E1DDB11EBB5512CFDA1251793', 'pelaksana10', '2234234', 'Kunjungan kerja', '2020-11-03 13:48:41', 'Done', '', ''),
(9, '5312EF742BDC11EBBEB92CFDA1251793', 'pelaksana10', '1', 'Sosialisasi Program Aplikasi SmartSam', '2020-11-21 09:31:16', 'Process', '', '0'),
(12, '557D5B763AC711EBA5442CFDA1251793', 'pelaksana1021', '11', 'tes', '2020-12-10 09:08:52', 'Process', '', '0'),
(14, '592FC1A1404211EB97F42CFDA1251793', 'pelaksana10', 'BBAD8CA0404111EB97F42CFDA12517', 'TES SURAT KELUAR', '2020-12-17 08:32:07', 'Process', '', '0'),
(2, '607B44EA1D8E11EBB39D2CFDA1251793', 'pelaksana10', '2', 'TES COBA', '2020-11-03 04:38:10', 'Process', '', ''),
(10, '75EB85152CDF11EBBEB92CFDA1251793', 'teknisilabbahasa', '1324234', 'Kunjungan kerja (LAB BAHASA)', '2020-11-22 16:26:14', 'Done', '', '0'),
(11, '7D306A492E2F11EBB1F82CFDA1251793', 'anggota_sp', '5', 'TES SURAT KELUAR SP', '2020-11-24 08:31:37', 'Process', '', '0'),
(13, '99BB8625403F11EB97F42CFDA1251793', 'pelaksana10', '2', 'Permohonan Studi Banding dari UNIMAL', '2020-12-17 08:12:27', 'Process', '', '0'),
(3, '9B6206141DA411EBB39D2CFDA1251793', 'pelaksana10', '2', 'TES', '2020-11-03 07:17:18', 'Process', '', ''),
(8, 'FEF9F97F2BC411EBBEB92CFDA1251793', 'teknisi_labor', '1324234', 'Tes Ajukan Surat Keluar By Lab Dasar', '2020-11-21 06:44:21', 'Done', '', '0');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_kabag`
--

CREATE TABLE `tbl_kabag` (
  `kabag_no` int(11) NOT NULL,
  `kabag_uuid` varchar(32) NOT NULL,
  `kabag_username` varchar(20) NOT NULL,
  `kabag_nama` varchar(30) NOT NULL,
  `kabag_posisi` varchar(40) NOT NULL,
  `kabag_uuid_biro` varchar(32) NOT NULL,
  `kabag_time` timestamp NOT NULL DEFAULT current_timestamp(),
  `kabag_kategori` varchar(20) NOT NULL,
  `kabag_email` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_kabag`
--

INSERT INTO `tbl_kabag` (`kabag_no`, `kabag_uuid`, `kabag_username`, `kabag_nama`, `kabag_posisi`, `kabag_uuid_biro`, `kabag_time`, `kabag_kategori`, `kabag_email`) VALUES
(29, '1E1180F65A8C11EB9CA820677CDFA8E8', 'kabag_akad', 'Abdul Hamid, M. T.', 'Kepala Bagian Akademik dan Kemahasiswaan', '2', '2021-01-19 19:27:11', 'biro', ''),
(31, '8A8162905A8C11EB9CA820677CDFA8E8', 'kabag_keu', 'Anwar A., S. P., M. M.', 'Kepala Bagian Keuangan', '1', '2021-01-19 19:30:13', 'biro', ''),
(30, '5F9F917E5A8C11EB9CA820677CDFA8E8', 'kabag_perenc', 'Munira, S. T', 'Kepala Bagian Perencanaan dan Kerja sama', '2', '2021-01-19 19:29:01', 'biro', ''),
(32, 'C07810945A8C11EB9CA820677CDFA8E8', 'kabag_umum', 'KABAG UMUM', 'Kepala Bagian Umum', '1', '2021-01-19 19:31:44', 'biro', ''),
(33, '13376A205B2411EB9CA820677CDFA8E8', 'ka_sp', 'Drs. Muhammad Yakop, M. Pd.', 'Ka SP', '0', '2021-01-20 13:34:57', 'spi', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_kasubbag`
--

CREATE TABLE `tbl_kasubbag` (
  `kasubbag_no` int(11) NOT NULL,
  `kasubbag_uuid` varchar(32) NOT NULL,
  `kasubbag_username` varchar(16) NOT NULL,
  `kasubbag_nama` varchar(40) NOT NULL,
  `kasubbag_posisi` varchar(40) NOT NULL,
  `kasubbag_uuid_kabag` varchar(32) NOT NULL,
  `kasubbag_time` timestamp NOT NULL DEFAULT current_timestamp(),
  `kasubbag_kategori` varchar(20) NOT NULL,
  `kasubbag_email` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_kasubbag`
--

INSERT INTO `tbl_kasubbag` (`kasubbag_no`, `kasubbag_uuid`, `kasubbag_username`, `kasubbag_nama`, `kasubbag_posisi`, `kasubbag_uuid_kabag`, `kasubbag_time`, `kasubbag_kategori`, `kasubbag_email`) VALUES
(38, '0FA798305A8F11EB9CA820677CDFA8E8', 'kasubbag_akuntan', 'Belum Ditentukan', 'Kasubbag Akuntansi & Pelaporan', '8A8162905A8C11EB9CA820677CDFA8E8', '2021-01-19 19:48:16', 'biro', ''),
(36, 'B3DDA0155A8D11EB9CA820677CDFA8E8', 'kasubbag_bend', 'Junaidi, S. Pd', 'Kasubbag Perbendaharaan', '8A8162905A8C11EB9CA820677CDFA8E8', '2021-01-19 19:38:32', 'biro', ''),
(37, 'E9D7965F5A8D11EB9CA820677CDFA8E8', 'kasubbag_humas', 'Muammar Khalis, S. Sos', 'Kasubbag Kerjasama & Humas', '5F9F917E5A8C11EB9CA820677CDFA8E8', '2021-01-19 19:40:03', 'biro', ''),
(44, 'F0679EF65A9011EB9CA820677CDFA8E8', 'kasubbag_mhs', 'Azizah, S. Pd., M. Pd.', 'Kasubbag Kemahasiswaan', '1E1180F65A8C11EB9CA820677CDFA8E8', '2021-01-19 20:01:42', 'biro', ''),
(41, '1C51B6B65A9011EB9CA820677CDFA8E8', 'kasubbag_peg', 'Tarmizi, S. E., M. S. M.', 'Kasubbag Kepegawaian', 'C07810945A8C11EB9CA820677CDFA8E8', '2021-01-19 19:55:46', 'biro', ''),
(42, '54C87EE15A9011EB9CA820677CDFA8E8', 'kasubbag_pend', 'Budiansyah Daulay, S. E.', 'Kasubbag Pendidikan & Evaluasi', '1E1180F65A8C11EB9CA820677CDFA8E8', '2021-01-19 19:57:21', 'biro', ''),
(45, '1F8688125A9111EB9CA820677CDFA8E8', 'kasubbag_perenc', 'Dianawati, S. P., M. P.', 'Kasubbag Perencanaan', '5F9F917E5A8C11EB9CA820677CDFA8E8', '2021-01-19 20:03:01', 'biro', ''),
(43, 'B910FB775A9011EB9CA820677CDFA8E8', 'kasubbag_regstat', 'Chairul Fuad, S. P', 'Kasubbag Registrasi & Statistik', '1E1180F65A8C11EB9CA820677CDFA8E8', '2021-01-19 20:00:09', 'biro', ''),
(40, 'E72462D15A8F11EB9CA820677CDFA8E8', 'kasubbag_rtbmn', 'Muhammad Nur, S. P.', 'Kasubbag RT & BM', 'C07810945A8C11EB9CA820677CDFA8E8', '2021-01-19 19:54:17', 'biro', ''),
(39, '7497C6215A8F11EB9CA820677CDFA8E8', 'kasubbag_tu', 'Hendra Rahayu, S. Hut.', 'Kasubbag TU & Ketatalaksanaan', 'C07810945A8C11EB9CA820677CDFA8E8', '2021-01-19 19:51:05', 'biro', ''),
(47, 'D7CBDCED5B2511EB9CA820677CDFA8E8', 'kaupt_tik', 'Munawir, S. ST., M. T.', 'Ka UPT TIK', '0', '2021-01-20 13:47:36', 'upt tik', ''),
(49, '7735EAC15B2811EB9CA820677CDFA8E8', 'ka_labbahasa', 'Makhroji, M. Pd.', 'Ka Lab Bahasa', '0', '2021-01-20 14:06:22', 'upt lab bahasa', ''),
(48, '5972486E5B2711EB9CA820677CDFA8E8', 'ka_labdasar', 'Teuku Hasan Basri, S. Pd., M. Pd.', 'Ka Lab Dasar', '0', '2021-01-20 13:58:23', 'upt lab dasar', ''),
(46, '5571CA2B5B2411EB9CA820677CDFA8E8', 'sekretaris_sp', 'Cut Elidar, S. H., M. H.', 'Sekretaris SP', '13376A205B2411EB9CA820677CDFA8E8', '2021-01-20 13:36:48', 'spi', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_kode_hal`
--

CREATE TABLE `tbl_kode_hal` (
  `hal_no` int(11) NOT NULL,
  `hal_nama` varchar(30) NOT NULL,
  `hal_kode` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_kode_hal`
--

INSERT INTO `tbl_kode_hal` (`hal_no`, `hal_nama`, `hal_kode`) VALUES
(1, 'Akreditasi', 'AK'),
(2, 'Banttuan Pendidikan', 'BP');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_kode_instansi`
--

CREATE TABLE `tbl_kode_instansi` (
  `instansi_no` int(11) NOT NULL,
  `instansi_nama` varchar(30) NOT NULL,
  `instansi_kode` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_kode_instansi`
--

INSERT INTO `tbl_kode_instansi` (`instansi_no`, `instansi_nama`, `instansi_kode`) VALUES
(1, 'Biro', 'UN45'),
(2, 'UPTK TIK', 'UN45.8');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_konsep_draft`
--

CREATE TABLE `tbl_konsep_draft` (
  `konsep_no` int(11) NOT NULL,
  `konsep_uuid` varchar(32) NOT NULL,
  `konsep_uuid_draft` varchar(32) NOT NULL,
  `konsep_asal` varchar(30) NOT NULL,
  `konsep_tujuan` varchar(32) NOT NULL,
  `konsep_isi` text DEFAULT NULL,
  `konsep_updated_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `konsep_author` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_konsep_draft`
--

INSERT INTO `tbl_konsep_draft` (`konsep_no`, `konsep_uuid`, `konsep_uuid_draft`, `konsep_asal`, `konsep_tujuan`, `konsep_isi`, `konsep_updated_at`, `konsep_author`) VALUES
(30, '0253408C2BA511EBBEB92CFDA1251793', '02531C7D2BA511EBBEB92CFDA1251793', 'pelaksana_tik1', '11', 'TES\r\n', '2020-11-21 02:55:27', 'pelaksana_tik1'),
(21, '04805B161E1011EBB5512CFDA1251793', '48AB009E1DDB11EBB5512CFDA1251793', 'pelaksana10', '1', '', '2020-11-03 20:06:10', 'biro_umum'),
(48, '0EC8AB052CD711EBBEB92CFDA1251793', 'FEF9F97F2BC411EBBEB92CFDA1251793', 'teknisi_labor', '0', 'KA LAB DAsar', '2020-11-22 15:26:05', 'kalabdas'),
(19, '119518F71E0911EBB5512CFDA1251793', '1194A66A1E0911EBB5512CFDA1251793', 'pelaksana9', '3', 'TES ISI SURAT', '2020-11-03 19:16:26', 'pelaksana9'),
(26, '1C194FCB21E611EB8E842CFDA1251793', '1C1918CE21E611EB8E842CFDA1251793', 'pelaksana10', '3', 'TES ISI', '2020-11-08 17:16:02', 'pelaksana10'),
(33, '20457B822BA711EBBEB92CFDA1251793', '02531C7D2BA511EBBEB92CFDA1251793', 'pelaksana_tik1', '2', 'tes', '2020-11-21 03:10:36', 'kaupttik'),
(37, '21150E122BB011EBBEB92CFDA1251793', '02531C7D2BA511EBBEB92CFDA1251793', 'pelaksana_tik1', '2', NULL, '2020-11-21 04:15:03', 'kaupttik'),
(9, '337D643D1DE211EBB5512CFDA1251793', '48AB009E1DDB11EBB5512CFDA1251793', 'pelaksana10', '3', 'Perihal di ganti saja menjadi\r\n\r\nPerihal : Kunjungan kerja bersama Dinkes\r\n\r\nBody:\r\nSehubungan dengan surat permohonan yang bapak ibu sampaikan. maka kami akan dengan senang hati untuk menyambut kedatangan bapak ibu dalam rangka kunjunga kerja.\r\n\r\nDemikian, Assalam\r\n', '2020-11-03 14:38:12', 'pelaksana10'),
(58, '373C2E64404011EB97F42CFDA1251793', '99BB8625403F11EB97F42CFDA1251793', 'pelaksana10', '2', 'Tambahan:\r\nTerkait permohonan studi banding saudara, maka saudara diberikan izin dengan nomor xxxxx\r\n', '2020-12-17 08:16:51', 'kasubbag3'),
(38, '385891122BB011EBBEB92CFDA1251793', '02531C7D2BA511EBBEB92CFDA1251793', 'pelaksana_tik1', '-', 'TES TERSUKAN KE REKTOR 1', '2020-11-21 04:15:42', 'kaupttik'),
(27, '391EE4B521E611EB8E842CFDA1251793', '1C1918CE21E611EB8E842CFDA1251793', 'pelaksana10', '2', 'Isinya harus gunakan EYD', '2020-11-08 17:16:51', 'kasubbag3'),
(22, '429DAE371E1011EBB5512CFDA1251793', '48AB009E1DDB11EBB5512CFDA1251793', 'pelaksana10', '2', '', '2020-11-03 20:07:55', 'biro_umum'),
(3, '48AB32051DDB11EBB5512CFDA1251793', '48AB009E1DDB11EBB5512CFDA1251793', 'pelaksana10', '3', 'Kunjungan kerja', '2020-11-03 13:48:41', 'pelaksana10'),
(44, '531352322BDC11EBBEB92CFDA1251793', '5312EF742BDC11EBBEB92CFDA1251793', 'pelaksana10', '3', 'Sehubungan dengan blablablablablablablabla\r\nblablablablabla', '2020-11-21 09:31:16', 'pelaksana10'),
(56, '557DB3113AC711EBA5442CFDA1251793', '557D5B763AC711EBA5442CFDA1251793', 'pelaksana1021', '11', 'tes\r\n', '2020-12-10 09:08:52', 'pelaksana1021'),
(28, '5879275821E611EB8E842CFDA1251793', '1C1918CE21E611EB8E842CFDA1251793', 'pelaksana10', '1', 'udah oke', '2020-11-08 17:17:44', 'kabag2'),
(59, '592FE6E2404211EB97F42CFDA1251793', '592FC1A1404211EB97F42CFDA1251793', 'pelaksana10', '3', 'TES', '2020-12-17 08:32:07', 'pelaksana10'),
(1, '607B68741D8E11EBB39D2CFDA1251793', '607B44EA1D8E11EBB39D2CFDA1251793', 'pelaksana10', '3', 'TES COBA', '2020-11-03 04:38:10', 'pelaksana10'),
(31, '668CF84A2BA611EBBEB92CFDA1251793', '02531C7D2BA511EBBEB92CFDA1251793', 'pelaksana_tik1', '0', 'Lanjut ke Biro Umum', '2020-11-21 03:05:24', 'kaupttik'),
(16, '6E830D951DF111EBB5512CFDA1251793', '9B6206141DA411EBB39D2CFDA1251793', 'pelaksana10', '2', 'COBA DRAFT 3', '2020-11-03 16:27:14', 'kasubbag3'),
(23, '6FF02B321E1011EBB5512CFDA1251793', '48AB009E1DDB11EBB5512CFDA1251793', 'pelaksana10', '1', '', '2020-11-03 20:09:11', 'biro_umum'),
(17, '6FF986601DF211EBB5512CFDA1251793', '607B44EA1D8E11EBB39D2CFDA1251793', 'pelaksana10', '2', 'Merah', '2020-11-03 16:34:26', 'kasubbag3'),
(4, '7438A4461DE011EBB5512CFDA1251793', '48AB009E1DDB11EBB5512CFDA1251793', 'pelaksana10', '3', '', '2020-11-03 14:25:42', 'pelaksana10'),
(42, '75A444062BC111EBBEB92CFDA1251793', '02531C7D2BA511EBBEB92CFDA1251793', 'pelaksana_tik1', '-', 't', '2020-11-21 06:19:02', 'kaupttik'),
(51, '75EC02492CDF11EBBEB92CFDA1251793', '75EB85152CDF11EBBEB92CFDA1251793', 'teknisilabbahasa', '13', 'TES\r\n', '2020-11-22 16:26:14', 'teknisilabbahasa'),
(29, '7A3D101721E611EB8E842CFDA1251793', '1C1918CE21E611EB8E842CFDA1251793', 'pelaksana10', '2234234', 'Lanjutkan', '2020-11-08 17:18:40', 'biro_umum'),
(60, '7B8BEF12404211EB97F42CFDA1251793', '592FC1A1404211EB97F42CFDA1251793', 'pelaksana10', 'BBAD8CA0404111EB97F42CFDA12517', 'TES LAGI', '2020-12-17 08:33:04', 'kasubbag3'),
(54, '7D3095492E2F11EBB1F82CFDA1251793', '7D306A492E2F11EBB1F82CFDA1251793', 'anggota_sp', '14', 'TES', '2020-11-24 08:31:37', 'anggota_sp'),
(39, '861B5F362BB011EBBEB92CFDA1251793', '02531C7D2BA511EBBEB92CFDA1251793', 'pelaksana_tik1', '-', 'TERUSKAN KE REKTOR 11111', '2020-11-21 04:17:52', 'kaupttik'),
(49, '9367269F2CD711EBBEB92CFDA1251793', 'FEF9F97F2BC411EBBEB92CFDA1251793', 'teknisi_labor', '1324234', 'TES KE REKTOR dr Ka lAB DASAR', '2020-11-22 15:29:47', 'kalabdas'),
(25, '9814A51C1E1611EBB5512CFDA1251793', '48AB009E1DDB11EBB5512CFDA1251793', 'pelaksana10', '-', 'BY REKTOR', '2020-11-03 20:53:15', 'rektor3'),
(57, '99BBABD0403F11EB97F42CFDA1251793', '99BB8625403F11EB97F42CFDA1251793', 'pelaksana10', '3', 'Terkait permohonan studi banding saudara, maka ......... dst.', '2020-12-17 08:12:27', 'pelaksana10'),
(2, '9B623CB71DA411EBB39D2CFDA1251793', '9B6206141DA411EBB39D2CFDA1251793', 'pelaksana10', '3', 'TES', '2020-11-03 07:17:18', 'pelaksana10'),
(45, 'A12007502BDC11EBBEB92CFDA1251793', '5312EF742BDC11EBBEB92CFDA1251793', 'pelaksana10', '2', NULL, '2020-11-21 09:33:27', 'kasubbag3'),
(50, 'A32F35902CD711EBBEB92CFDA1251793', 'FEF9F97F2BC411EBBEB92CFDA1251793', 'teknisi_labor', '-', 'OKE ', '2020-11-22 15:30:14', 'kalabdas'),
(24, 'A335B8681E1011EBB5512CFDA1251793', '48AB009E1DDB11EBB5512CFDA1251793', 'pelaksana10', '2234234', '', '2020-11-03 20:10:37', 'biro_umum'),
(52, 'A7D2206C2CDF11EBBEB92CFDA1251793', '75EB85152CDF11EBBEB92CFDA1251793', 'teknisilabbahasa', '1324234', 'teruskan ke rektor', '2020-11-22 16:27:38', 'kalabahasa'),
(40, 'AD8AEF3B2BB011EBBEB92CFDA1251793', '02531C7D2BA511EBBEB92CFDA1251793', 'pelaksana_tik1', '1324234', 'LAGI', '2020-11-21 04:18:59', 'kaupttik'),
(34, 'AF68144A2BAE11EBBEB92CFDA1251793', '02531C7D2BA511EBBEB92CFDA1251793', 'pelaksana_tik1', '-', 'TES DONE', '2020-11-21 04:04:43', 'kaupttik'),
(32, 'B3908E372BA611EBBEB92CFDA1251793', '02531C7D2BA511EBBEB92CFDA1251793', 'pelaksana_tik1', '0', 'lanjut ke biro\r\n', '2020-11-21 03:07:34', 'kaupttik'),
(5, 'B5F33BE81DE011EBB5512CFDA1251793', '48AB009E1DDB11EBB5512CFDA1251793', 'pelaksana10', '3', 'TESSSS', '2020-11-03 14:27:32', 'pelaksana10'),
(10, 'B7004DC01DEF11EBB5512CFDA1251793', '48AB009E1DDB11EBB5512CFDA1251793', 'pelaksana10', '2', 'Kasubbag Tes', '2020-11-03 16:14:56', 'pelaksana10'),
(46, 'B7725E912BDC11EBBEB92CFDA1251793', '5312EF742BDC11EBBEB92CFDA1251793', 'pelaksana10', '2', NULL, '2020-11-21 09:34:04', 'kasubbag3'),
(14, 'BAD743EB1DF011EBB5512CFDA1251793', '48AB009E1DDB11EBB5512CFDA1251793', 'pelaksana10', '2', 'TESS', '2020-11-03 16:22:12', 'kasubbag3'),
(53, 'BADF88F72CDF11EBBEB92CFDA1251793', '75EB85152CDF11EBBEB92CFDA1251793', 'teknisilabbahasa', '-', 'Done\r\n', '2020-11-22 16:28:10', 'kalabahasa'),
(11, 'BAF3F59C1DEF11EBB5512CFDA1251793', '48AB009E1DDB11EBB5512CFDA1251793', 'pelaksana10', '2', 'tes', '2020-11-03 16:15:03', 'pelaksana10'),
(12, 'C3FF55BA1DEF11EBB5512CFDA1251793', '48AB009E1DDB11EBB5512CFDA1251793', 'pelaksana10', '2', 'Kasubbag', '2020-11-03 16:15:18', 'pelaksana10'),
(15, 'C5562D3C1DF011EBB5512CFDA1251793', '48AB009E1DDB11EBB5512CFDA1251793', 'pelaksana10', '2', 'TESS', '2020-11-03 16:22:30', 'kasubbag3'),
(20, 'CE71E29C1E0F11EBB5512CFDA1251793', '48AB009E1DDB11EBB5512CFDA1251793', 'pelaksana10', '1', '', '2020-11-03 20:04:40', 'biro_umum'),
(41, 'D3E3B08B2BB011EBBEB92CFDA1251793', '02531C7D2BA511EBBEB92CFDA1251793', 'pelaksana_tik1', '-', 'SELESAI AKSI', '2020-11-21 04:20:03', 'rektor'),
(18, 'D7B10F651E0811EBB5512CFDA1251793', '48AB009E1DDB11EBB5512CFDA1251793', 'pelaksana10', '1', 'OKE by Kabag', '2020-11-03 19:14:49', 'kabag2'),
(36, 'DA34C0C32BAF11EBBEB92CFDA1251793', '02531C7D2BA511EBBEB92CFDA1251793', 'pelaksana_tik1', '2', NULL, '2020-11-21 04:13:04', 'kaupttik'),
(13, 'E1E915671DEF11EBB5512CFDA1251793', '48AB009E1DDB11EBB5512CFDA1251793', 'pelaksana10', '2', 'tesssss afaaa', '2020-11-03 16:16:08', 'kasubbag3'),
(35, 'E2CCB1DC2BAE11EBBEB92CFDA1251793', '02531C7D2BA511EBBEB92CFDA1251793', 'pelaksana_tik1', '-', 'SELESAI TIK\r\n', '2020-11-21 04:06:09', 'kaupttik'),
(47, 'E7018A212BDC11EBBEB92CFDA1251793', '5312EF742BDC11EBBEB92CFDA1251793', 'pelaksana10', '1', 'Revisi:\r\n\r\nSehubungan dengan akan diterapkannya aplikasi smartsam , maka diharapkan semua mengikuti sosialisasi tsb', '2020-11-21 09:35:24', 'kabag2'),
(6, 'E7438DEF1DE011EBB5512CFDA1251793', '48AB009E1DDB11EBB5512CFDA1251793', 'pelaksana10', '3', 'teess aaa', '2020-11-03 14:28:55', 'pelaksana10'),
(55, 'E9E59BF82F8E11EB93972CFDA1251793', '7D306A492E2F11EBB1F82CFDA1251793', 'anggota_sp', '5', 'LANJUT KE KABAG SP BY SEKRETARIS SP', '2020-11-26 02:27:14', 'sek_sp'),
(7, 'F407FF051DE011EBB5512CFDA1251793', '48AB009E1DDB11EBB5512CFDA1251793', 'pelaksana10', '3', 'teess aaa bbbb', '2020-11-03 14:29:16', 'pelaksana10'),
(8, 'FAB7C2771DE011EBB5512CFDA1251793', '48AB009E1DDB11EBB5512CFDA1251793', 'pelaksana10', '3', ' awd awdawdawda  awd awdawdawda  awd awdawdawda  awd awdawdawda  awd awdawdawda  awd awdawdawda  awd awdawdawda  awd awdawdawda  awd awdawdawda  awd awdawdawda  awd awdawdawda  awd awdawdawda  awd awdawdawda  awd awdawdawda  awd awdawdawda  awd awdawdawda  awd awdawdawda  awd awdawdawda  awd awdawdawda  awd awdawdawda  awd awdawdawda  awd awdawdawda  awd awdawdawda  awd awdawdawda  awd awdawdawda  awd awdawdawda  awd awdawdawda  awd awdawdawda  awd awdawdawda  awd awdawdawda  awd awdawdawda  awd awdawdawda  awd awdawdawda  awd awdawdawda  awd awdawdawda  awd awdawdawda  awd awdawdawda  awd awdawdawda ', '2020-11-03 14:29:28', 'pelaksana10'),
(43, 'FEFA3AE02BC411EBBEB92CFDA1251793', 'FEF9F97F2BC411EBBEB92CFDA1251793', 'teknisi_labor', '12', 'tes', '2020-11-21 06:44:21', 'teknisi_labor');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_login`
--

CREATE TABLE `tbl_login` (
  `username` varchar(16) NOT NULL,
  `stts` varchar(20) NOT NULL,
  `password` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_login`
--

INSERT INTO `tbl_login` (`username`, `stts`, `password`) VALUES
('1111111111', 'kabag', 'e11170b8cbd2d74102651cb967fa28e5'),
('1234', 'pelaksana', '81dc9bdb52d04dc20036dbd8313ed055'),
('2222', 'rektor', '934b535800b1cba8f96a5d72f72f1611'),
('23323', 'kabag', 'b35b31a24acc2da3bd9e3feb30fc7e79'),
('244', 'biro', '9188905e74c28e489b44e954ec0b9bca'),
('adm_surat_labbhs', 'admin surat', '9014ce68f4881396c22bb972e3435d96'),
('adm_surat_labdas', 'admin surat', 'cd81aeda7802b4c8deabfed8c49bd699'),
('agt_sp1', 'pelaksana', '7c61c8368e81cd88f58f443f3771ead5'),
('agt_sp2', 'pelaksana', 'deadda39273d8d47f4c881e6387be6b3'),
('agt_sp3', 'pelaksana', 'e77fba8f642a9bd0bd2ae8cdb171cbde'),
('biro_akpk', 'biro', '0959631689d0a2a92a8c3ee6722e02f6'),
('biro_umum', 'biro', '28abd1f04d287eeb1321dd6332b0ee11'),
('kabag1211', 'kabag', 'adab0f8eabbb1120177a2e8cce2b1876'),
('kabag2', 'kabag', '81d868eb7a14df728173e1ee147ea5fd'),
('kabag222', 'kabag', '40c71b93c63e33b9a429874bb9458ff5'),
('kabag2222', 'kabag', 'e28ffeb34b5290fc7f89fc0c06004d1c'),
('kabag_akad', 'kabag', '92fe5ffb9f1968604ecc7d176c974c6a'),
('kabag_keu', 'kabag', '67b7c4ae0fbebd4c47a40d41ac5ef774'),
('kabag_perenc', 'kabag', '5e071cdbfb320ea482cf3d769a390dbd'),
('kabag_sp', 'kabag', '26624757725d7021f33ea11fd9e024cb'),
('kabag_umum', 'kabag', '8544b76161b03fa9fc71cbcbf0f02bc8'),
('kasp2323', 'kabag', '10540eeaace1fa83dae47f75c3a63a20'),
('kasubbag1', 'kasubbag', '1a27f515cd7cd58d5b5248ab66a295e8'),
('kasubbag10', 'kasubbag', 'f583ea4ad4a11eaebcb2d9531bd8b92d'),
('kasubbag2', 'kasubbag', '4480b2610a01e277f947c6da1ea6588d'),
('kasubbag211', 'kasubbag', '3a8f2c4921cee0e214a35ce05cc11a75'),
('kasubbag3', 'kasubbag', '5b18490a44d521fb9b12de44cd2decff'),
('kasubbag4', 'kasubbag', '326da7a9f89af02866f6206a7c03d61b'),
('kasubbag5', 'kasubbag', '809e9fa9442bf6eb359a51121d3b1b13'),
('kasubbag6', 'kasubbag', '813b5fbcac1a39f730bf9f1549712760'),
('kasubbag7', 'kasubbag', 'ee2d2c1e6badd2c269260b582f5458c1'),
('kasubbag8', 'kasubbag', '5bd668424efba8a862d36b31aa6a9d70'),
('kasubbag9', 'kasubbag', '9ed6283fe6df769b0e356bd1138fd680'),
('kasubbagtes22', 'kabag', 'c0c32b768d16861ea9a0688fffc52bf4'),
('kasubbagtes222', 'kabag', '2834537a0e38ea2bd7daa3e382cc8519'),
('kasubbagtes333', 'kasubbag', '02c97b671ea7bb70aaf0c89e47737c7d'),
('kasubbag_akuntan', 'kasubbag', '7ff3b33d5232b3c568aa67070c449a0a'),
('kasubbag_bend', 'kasubbag', '809661ce281785a06fba78bace4a0284'),
('kasubbag_humas', 'kasubbag', '77393daa25231ddfcff59aab104cc51c'),
('kasubbag_mhs', 'kasubbag', '620186d12b8552f46f8e13e39c369f60'),
('kasubbag_peg', 'kasubbag', '11d7b750b3b2f3f1327847ce2f56d1fa'),
('kasubbag_pend', 'kasubbag', 'f89dfc80b6644f8697ec1a85b049733a'),
('kasubbag_perenc', 'kasubbag', 'b25059c17b12b55f179f65e4ef068de7'),
('kasubbag_regstat', 'kasubbag', 'c08c87d9215f3e792377fbdaeccd34b0'),
('kasubbag_rtbmn', 'kasubbag', '73040e1b1f51427d777e9d79e8d83bae'),
('kasubbag_tu', 'kasubbag', '9d53860577b01c47b99491e50d377de1'),
('kaupt_tik', 'kasubbag', 'e0443d1fc107a9902fe69b20118fbdb3'),
('ka_labbahasa', 'kasubbag', '9debf5c4971f3a7bf76ba9c8a4bfe7cd'),
('ka_labdasar', 'kasubbag', 'f7f2e5cc4e0f4d590d0e4aa0fa655e93'),
('ka_sp', 'kabag', 'eb13f2cc4797a66845c72c1036b94819'),
('labbahasa', 'admin surat', 'cff8beaf555b96ca5ea3e604a281d112'),
('labdas', 'admin surat', 'b99e60c3dfaa8ed812502bc49504d572'),
('pelaklabbhs222', 'pelaksana', 'bb9f39446659b0f7e123bb1024155f9d'),
('pelaksana', 'pelaksana', '6875ccc2c267a8c215afb1f25f81d7a0'),
('pelaksana1', 'pelaksana', 'e32daea4f91d2d6f69efa2da385e3725'),
('pelaksana10', 'pelaksana', '5223d74665420a8d4f0288ba67fc098e'),
('pelaksana1022', 'pelaksana', 'c1543531879e04c63dc9e51524c90586'),
('pelaksana11', 'pelaksana', '58e8c2b298a4af3ddcd9471c647e149e'),
('pelaksana12', 'pelaksana', '82c21ef79bf0d351e9bfc1cc32e84494'),
('pelaksana13', 'pelaksana', '6c67361dc3924d4a558adbc47080422b'),
('pelaksana14', 'pelaksana', '3f6a48dc7ad496d7fcde260daca8bd50'),
('pelaksana15', 'pelaksana', 'bc8998908292bd2508f43e808ce3a772'),
('pelaksana16', 'pelaksana', '9ae9646ecb7588ca8820296d40aa2463'),
('pelaksana17', 'pelaksana', 'b5395bcfbced23528b410b02192e034b'),
('pelaksana18', 'pelaksana', '80df783e19e4d1ffc69840f69edaa099'),
('pelaksana19', 'pelaksana', 'bcdb21d4ecd652f48b40926e0ee55377'),
('pelaksana2', 'pelaksana', 'd4f803080f4699bddecf5f19c73ca333'),
('pelaksana20', 'pelaksana', 'b5f32940f6b88ec1986219b5ddb63e7e'),
('pelaksana21', 'pelaksana', '287f47c5b6cafead3b0cd47c41edb934'),
('pelaksana22', 'pelaksana', 'c7c0348ab82418ec1f349bfdff8868da'),
('pelaksana23', 'pelaksana', 'dde7b0feae3e725cdb8208eb420b1cae'),
('pelaksana24', 'pelaksana', '725d7020d50dffe0de99b983b518bde5'),
('pelaksana3', 'pelaksana', '1c97c30aaaab877a76c231d3d5326bf9'),
('pelaksana4', 'pelaksana', 'a7c1e10f9d32aa50fbd755cf1606f8be'),
('pelaksana5', 'pelaksana', '5072a44b03756a10b424e9287801ebfd'),
('pelaksana6', 'pelaksana', 'e3a3b65cde0283ab4a57082bfe10ea9d'),
('pelaksana7', 'pelaksana', '372428373e31bd9bb44789209bbed802'),
('pelaksana8', 'pelaksana', 'e547e873a416571bde449a192d2ebe3d'),
('pelaksana9', 'pelaksana', '6f146bed8fe5eb3dd4e7e4df7e3420dc'),
('pelaksanalabdas2', 'pelaksana', 'c96dfd6f06698ab2a6ec53ffc3064971'),
('pelaksana_tik1', 'pelaksana', '18448d59db2c7d636239f08605e65b7f'),
('pelaksana_tik2', 'pelaksana', '4a976f6c9fd532d1567725c3bec10b1b'),
('pelaksana_tik3', 'pelaksana', '2c2f4b60313632270d8d6d2b0e85db98'),
('pelaksana_tik4', 'pelaksana', 'c88cf1ee3b1c5fec2b7f063773c7c9d1'),
('pelaksana_tik5', 'pelaksana', '2c4c977dd1d135617cbfc387e922ad0b'),
('pelak_labbhs1', 'pelaksana', 'b6c225cefedc9168c68c5e4bda8348f1'),
('pelak_labbhs2', 'pelaksana', '621a3cc99173453736489cc2e64779d8'),
('pelak_labbhs3', 'pelaksana', '2e015e3ac1d98c65c05baebb214d496b'),
('pelak_labbhs4', 'pelaksana', '6382b8c908d6cee7766d072c9673c138'),
('pelak_labdas1', 'pelaksana', 'b39f356d72389eece601851904179aa9'),
('pelak_labdas2', 'pelaksana', '181b4da7a376b57fab6798bcd12e57ce'),
('pelak_labdas3', 'pelaksana', '8268aa72e794029769dde95b547e43db'),
('pelak_surat_biro', 'admin surat', '4b70489d7465a256b75cde75108a10f4'),
('pelak_surat_sp', 'admin surat', 'c3653f441b14176ddcc0b6ddaafadd74'),
('pelak_surat_tik', 'admin surat', '2926c67e2932fd22459168611322dbda'),
('rektor', 'rektor', '7802f18483b28d50a62e65c27e9e12a9'),
('sekretaris_sp', 'kasubbag', '1c3ed57de950fca38d744fd5f25fe5e5'),
('seksp111', 'kasubbag', '3180bfa82e147e86f1db9b39f14f04a2'),
('sek_sp', 'kasubbag', '642425dae2e905cb1c5aaabc77015f99'),
('sp', 'admin surat', '1952a01898073d1e561b9b4f2e42cbd7'),
('superadmin', 'super admin', 'e9335e177b288c7af4af8f1225c3f938'),
('teknisilabbahasa', 'pelaksana', 'fde93bfefc29d85e501dfdf042522598'),
('tes', 'biro', '28b662d883b6d76fd96e4ddc5e9ba780'),
('tes008', 'rektor', '204f39398c77c95231dd5b2db6a578ef'),
('tes111', 'pelaksana', 'bc7e7c618a9bb0beb5a4092ca281acd1'),
('tes11222', 'kasubbag', '748405dd6329089058fe29e820418d53'),
('tes21222', 'kasubbag', '03673a18f2b8a240f9297fbf910c61bf'),
('tes22', 'kabag', 'f9da714e5247d9642aeb4d088b4c1f64'),
('tes666', 'pelaksana', '3b38c046dc084a57a296f6d0e1cb48f2'),
('teskabag', 'kabag', 'cde2404bf7789529e653ea9281a5351d'),
('teskabag1', 'kabag', 'f50d239b8ed8c8cce4e07481709c07d5'),
('tesseksp', 'kasubbag', '62aa5cd19cf1fd3f03c27c554e238781'),
('uptik', 'kabag', 'f251a2c273f38d8c0181ce97cff39bb7'),
('warek1', 'rektor', 'e456c7dd9c9a0fa491abf69384e10fd6'),
('warek2', 'rektor', 'db32c02c3b6c6dcca58954fd13180638'),
('warek3', 'rektor', '5823e678e29df2707b6d57206f350548'),
('warek_akademik', 'rektor', '101acdbb08961bcf15a7dccdfe029b06');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_rektor`
--

CREATE TABLE `tbl_rektor` (
  `rektor_no` int(11) NOT NULL,
  `rektor_uuid` varchar(32) NOT NULL,
  `rektor_username` varchar(10) NOT NULL,
  `rektor_nama` varchar(30) NOT NULL,
  `rektor_posisi` varchar(40) NOT NULL,
  `rektor_time` timestamp NOT NULL DEFAULT current_timestamp(),
  `rektor_email` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_rektor`
--

INSERT INTO `tbl_rektor` (`rektor_no`, `rektor_uuid`, `rektor_username`, `rektor_nama`, `rektor_posisi`, `rektor_time`, `rektor_email`) VALUES
(1, '1324234', 'rektor', 'Dr. Bachtiar Akob, M.Pd.', 'Rektor', '2020-12-12 14:34:24', 'ilhamr6000@gmail.com'),
(10, '14083DE55A8A11EB9CA820677CDFA8E8', 'warek1', 'Dr. Saiman, M. Pd.', 'Wakil Rektor Bidang Akademik', '2021-01-19 19:12:35', ''),
(11, '2B74635C5A8A11EB9CA820677CDFA8E8', 'warek2', 'Dr. Ir. Hamdani, M. T.', 'Wakil Rektor Bidang Umum dan Keuangan', '2021-01-19 19:13:15', ''),
(12, '42E72DC45A8A11EB9CA820677CDFA8E8', 'warek3', 'Bukhari, S. Pd., M. Pd.', 'Wakil Rektor Bidang Kemahasiswaan', '2021-01-19 19:13:54', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_riwayat_disposisi`
--

CREATE TABLE `tbl_riwayat_disposisi` (
  `riw_no` int(11) NOT NULL,
  `riw_uuid` varchar(32) NOT NULL,
  `riw_dist_uuid` varchar(32) NOT NULL,
  `riw_asal` varchar(30) NOT NULL,
  `riw_disposisi` varchar(30) NOT NULL,
  `riw_update_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `riw_memo` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_riwayat_disposisi`
--

INSERT INTO `tbl_riwayat_disposisi` (`riw_no`, `riw_uuid`, `riw_dist_uuid`, `riw_asal`, `riw_disposisi`, `riw_update_at`, `riw_memo`) VALUES
(547, '094D130A5BA411EB9F7D20677CDFA8E8', 'AA44F39D5B9F11EB9F7D20677CDFA8E8', 'kabag_umum', 'kasubbag_peg', '2021-01-21 04:49:25', ''),
(548, '094D28795BA411EB9F7D20677CDFA8E8', 'AA44F39D5B9F11EB9F7D20677CDFA8E8', 'kabag_umum', 'kasubbag_peg', '2021-01-21 04:49:25', 'Proses Sesuai Ketentuan/Peraturan yang Berlaku'),
(543, '0995B8565BA011EB9F7D20677CDFA8E8', 'AA44F39D5B9F11EB9F7D20677CDFA8E8', 'kabag_umum', 'biro_umum', '2021-01-21 04:20:47', ''),
(544, '0995CDE15BA011EB9F7D20677CDFA8E8', 'AA44F39D5B9F11EB9F7D20677CDFA8E8', 'kabag_umum', 'biro_umum', '2021-01-21 04:20:47', 'Teruskan'),
(549, '09F834BC5BA411EB9F7D20677CDFA8E8', 'AA44F39D5B9F11EB9F7D20677CDFA8E8', 'kabag_umum', 'kasubbag_peg', '2021-01-21 04:49:26', ''),
(550, '09F84B645BA411EB9F7D20677CDFA8E8', 'AA44F39D5B9F11EB9F7D20677CDFA8E8', 'kabag_umum', 'kasubbag_peg', '2021-01-21 04:49:26', 'Proses Sesuai Ketentuan/Peraturan yang Berlaku'),
(532, '0BE437975B9B11EB9F7D20677CDFA8E8', '0BE421945B9B11EB9F7D20677CDFA8E8', 'pelak_surat_biro', 'kabag_umum', '2021-01-21 03:45:04', 'Permohonan Operator'),
(536, '2B64CC7C5B9D11EB9F7D20677CDFA8E8', '577318145B9B11EB9F7D20677CDFA8E8', 'kabag_umum', 'biro_akpk', '2021-01-21 04:00:16', ''),
(537, '2B64E2345B9D11EB9F7D20677CDFA8E8', '577318145B9B11EB9F7D20677CDFA8E8', 'kabag_umum', 'biro_akpk', '2021-01-21 04:00:16', 'Teruskan'),
(546, '47F62ECA5BA311EB9F7D20677CDFA8E8', 'AA44F39D5B9F11EB9F7D20677CDFA8E8', 'biro_umum', 'kabag_umum', '2021-01-21 04:44:01', ''),
(545, '47F643985BA311EB9F7D20677CDFA8E8', 'AA44F39D5B9F11EB9F7D20677CDFA8E8', 'biro_umum', 'kabag_umum', '2021-01-21 04:44:01', 'Proses Sesuai Ketentuan/Peraturan yang Berlaku'),
(538, '4E9B40EB5B9D11EB9F7D20677CDFA8E8', '0BE421945B9B11EB9F7D20677CDFA8E8', 'kabag_umum', 'rektor', '2021-01-21 04:01:15', ''),
(539, '4E9B56785B9D11EB9F7D20677CDFA8E8', '0BE421945B9B11EB9F7D20677CDFA8E8', 'kabag_umum', 'rektor', '2021-01-21 04:01:15', 'Teruskan'),
(533, '57732E1D5B9B11EB9F7D20677CDFA8E8', '577318145B9B11EB9F7D20677CDFA8E8', 'pelak_surat_biro', 'kabag_umum', '2021-01-21 03:47:11', 'Usulan Kegiatan PBJ 2021'),
(541, '8F9F1A245B9D11EB9F7D20677CDFA8E8', '577318145B9B11EB9F7D20677CDFA8E8', 'biro_akpk', 'kabag_perenc', '2021-01-21 04:03:04', ''),
(540, '8F9F2FA65B9D11EB9F7D20677CDFA8E8', '577318145B9B11EB9F7D20677CDFA8E8', 'biro_akpk', 'kabag_perenc', '2021-01-21 04:03:04', 'Proses Sesuai Ketentuan/Peraturan yang Berlaku'),
(542, 'AA4508FC5B9F11EB9F7D20677CDFA8E8', 'AA44F39D5B9F11EB9F7D20677CDFA8E8', 'pelak_surat_biro', 'kabag_umum', '2021-01-21 04:18:07', 'PERMOHONAN CUTI PNS'),
(534, 'EDBB71705B9C11EB9F7D20677CDFA8E8', '577318145B9B11EB9F7D20677CDFA8E8', 'kabag_umum', '', '2021-01-21 03:58:32', ''),
(535, 'EDBB85E95B9C11EB9F7D20677CDFA8E8', '577318145B9B11EB9F7D20677CDFA8E8', 'kabag_umum', '', '2021-01-21 03:58:32', 'Teruskan');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_staff`
--

CREATE TABLE `tbl_staff` (
  `staff_no` int(11) NOT NULL,
  `staff_uuid` varchar(32) NOT NULL,
  `staff_username` varchar(20) NOT NULL,
  `staff_nama` varchar(30) NOT NULL,
  `staff_posisi` varchar(55) NOT NULL,
  `staff_uuid_kasubbag` varchar(32) NOT NULL,
  `staff_time` timestamp NOT NULL DEFAULT current_timestamp(),
  `staff_kategori` varchar(20) NOT NULL,
  `staff_email` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_staff`
--

INSERT INTO `tbl_staff` (`staff_no`, `staff_uuid`, `staff_username`, `staff_nama`, `staff_posisi`, `staff_uuid_kasubbag`, `staff_time`, `staff_kategori`, `staff_email`) VALUES
(47, '00A6535D5B2511EB9CA820677CDFA8E8', 'agt_sp1', 'Tuti Meutia, S. E., M. Si.', 'Anggota SP', '5571CA2B5B2411EB9CA820677CDFA8E8', '2021-01-20 13:41:35', 'spi', ''),
(48, '234BBAA05B2511EB9CA820677CDFA8E8', 'agt_sp2', 'Muhammad Thaid Hasan, ST., MT.', 'Anggota SP', '5571CA2B5B2411EB9CA820677CDFA8E8', '2021-01-20 13:42:33', 'spi', ''),
(49, '541EFBBE5B2511EB9CA820677CDFA8E8', 'agt_sp3', 'Irwansyah, S. T., M. T', 'Anggota SP', '5571CA2B5B2411EB9CA820677CDFA8E8', '2021-01-20 13:43:55', 'spi', ''),
(40, 'D01A962F3B8F11EBA5442CFDA1251793', 'pelaklabbhs222', 'Ilham Ramadha, S.Tr.Kom A', 'Pelaksana Lab Bahasa', '0EBCAC743B9011EBA5442CFDA1251793', '2020-12-11 09:03:54', 'upt lab bahasa', 'ilhamr6000@gmail.com'),
(1, '1', 'pelaksana1', 'Nurhayati, S.Pd., M.Pd.', 'Bendahara Penerimaan', '1', '2020-12-10 05:01:42', 'biro', 'ilhamr6000@gmail.com'),
(10, '10', 'pelaksana10', 'Alfadhil, S.H.', 'Pengolah Data', '3', '2020-12-10 05:01:42', 'biro', 'ilhamr6000@gmail.com'),
(11, '11', 'pelaksana11', 'Rahmad Fadela Syahputra, S.E.', 'Pengelola Barang Milik Negara', '4', '2020-12-10 05:01:42', 'biro', 'ilhamr6000@gmail.com'),
(12, '12', 'pelaksana12', 'T. Muhammad Juanda, S.Pd.', 'Pengelola Data BMN', '4', '2020-12-10 05:01:42', 'biro', 'ilhamr6000@gmail.com'),
(13, '13', 'pelaksana13', 'Zulkhalik, S.Pd.', 'Pengelola Kepegawaian', '5', '2020-12-10 05:01:42', 'biro', 'ilhamr6000@gmail.com'),
(14, '14', 'pelaksana14', 'Hari Suryono, S.E.', 'Pengadministrasi Kepegawaian', '5', '2020-12-10 05:01:42', 'biro', 'ilhamr6000@gmail.com'),
(15, '15', 'pelaksana15', 'Hidayati, S.E.', 'Pengolah Data Akademik', '6', '2020-12-10 05:01:42', 'biro', 'ilhamr6000@gmail.com'),
(16, '16', 'pelaksana16', 'Mardiana Susanti, S.E.', 'Pengelola Data Sarana Pendidikan', '6', '2020-12-10 05:01:42', 'biro', 'ilhamr6000@gmail.com'),
(17, '17', 'pelaksana17', 'R. Aja Dwi Syukrillah, S.Kom.', 'Pengelola Informasi Akademik', '7', '2020-12-10 05:01:42', 'biro', 'ilhamr6000@gmail.com'),
(18, '18', 'pelaksana18', 'Suryanto, S.H.', 'Pengelola Data Registrasi', '7', '2020-12-10 05:01:42', 'biro', 'ilhamr6000@gmail.com'),
(19, '19', 'pelaksana19', 'Nurhayati, S.E.', 'Pengelola Program Minat Bakat & Penalaran Mahasiswa', '8', '2020-12-10 05:01:42', 'biro', 'ilhamr6000@gmail.com'),
(2, '2', 'pelaksana2', 'Wardiana, S.Hut.', 'Bendahara Pengeluaran', '1', '2020-12-10 05:01:42', 'biro', 'ilhamr6000@gmail.com'),
(20, '20', 'pelaksana20', 'Aza Mariani Ulpa, S.Pd.', 'Pengadministrasian Minat Bakat & Penalaran Mahasiswa', '8', '2020-12-10 05:01:42', 'biro', 'ilhamr6000@gmail.com'),
(21, '21', 'pelaksana21', 'Adi Musfadry, S.P.', 'Pengelola Data Pelaksanaan Program dan Anggaran', '9', '2020-12-10 05:01:42', 'biro', 'ilhamr6000@gmail.com'),
(22, '22', 'pelaksana22', 'Sasi Eka Putra, S.H.', 'Pengadministrasi Umum', '9', '2020-12-10 05:01:42', 'biro', 'ilhamr6000@gmail.com'),
(23, '23', 'pelaksana23', 'Badriah, S.P.', 'Pengelola Data Kerjasama', '10', '2020-12-10 05:01:42', 'biro', 'ilhamr6000@gmail.com'),
(24, '24', 'pelaksana24', 'Dailamy, S.H.I.', 'Pengadministrasi Umum', '10', '2020-12-10 05:01:42', 'biro', 'ilhamr6000@gmail.com'),
(3, '3', 'pelaksana3', 'Meilin', 'Pengelola Gaji', '1', '2020-12-10 05:01:42', 'biro', 'ilhamr6000@gmail.com'),
(4, '4', 'pelaksana4', 'Desi Maulida, S.Pd.', 'Pengolah Data Keuangan', '1', '2020-12-10 05:01:42', 'biro', 'ilhamr6000@gmail.com'),
(5, '5', 'pelaksana5', 'Muhammad Asri, S.T.', 'Pengelola Surat Perintah Membayar', '1', '2020-12-10 05:01:42', 'biro', 'ilhamr6000@gmail.com'),
(6, '6', 'pelaksana6', 'Munar, S.E.', 'Penata Dokumen Keuangan', '1', '2020-12-10 05:01:42', 'biro', 'ilhamr6000@gmail.com'),
(7, '7', 'pelaksana7', '-', 'Penyusun Laporan Keuangan', '2', '2020-12-10 05:01:42', 'biro', 'ilhamr6000@gmail.com'),
(8, '8', 'pelaksana8', 'Nuraini, S.Pd.', 'Pengelola Keuangan', '2', '2020-12-10 05:01:42', 'biro', 'ilhamr6000@gmail.com'),
(9, '9', 'pelaksana9', 'Hendra, S.H.', 'Pengadministrasi Data & Peraturan UU', '3', '2020-12-10 05:01:42', 'biro', 'ilhamr6000@gmail.com'),
(51, '54B6883B5B2611EB9CA820677CDFA8E8', 'pelaksana_tik1', 'Fathuroyan, S. T.', 'Petugas Teknologi Informasi Komputer', 'D7CBDCED5B2511EB9CA820677CDFA8E8', '2021-01-20 13:51:05', 'upt tik', ''),
(52, 'AD99E1E15B2611EB9CA820677CDFA8E8', 'pelaksana_tik2', 'Muslihadi, S. Kom.', 'Pengelola Sistem dan Jaringan', 'D7CBDCED5B2511EB9CA820677CDFA8E8', '2021-01-20 13:53:35', 'upt tik', ''),
(53, 'D8B09FB85B2611EB9CA820677CDFA8E8', 'pelaksana_tik3', 'Zulfan, S. Kom.', 'Teknisi Komputer', 'D7CBDCED5B2511EB9CA820677CDFA8E8', '2021-01-20 13:54:47', 'upt tik', ''),
(54, 'FC7BC9AB5B2611EB9CA820677CDFA8E8', 'pelaksana_tik4', 'Dewi Rosita, S. H.', 'Pengadministrasi Umum', 'D7CBDCED5B2511EB9CA820677CDFA8E8', '2021-01-20 13:55:47', 'upt tik', ''),
(55, '13BB0F125B2711EB9CA820677CDFA8E8', 'pelaksana_tik5', 'Faria Efendi', 'Pengelola Laman', 'D7CBDCED5B2511EB9CA820677CDFA8E8', '2021-01-20 13:56:26', 'upt tik', ''),
(59, 'D3111F445B2811EB9CA820677CDFA8E8', 'pelak_labbhs1', 'Mulyana, S. E.', 'Pengadministrasi Keuangan', '7735EAC15B2811EB9CA820677CDFA8E8', '2021-01-20 14:08:56', 'upt lab bahasa', ''),
(60, 'E79821605B2811EB9CA820677CDFA8E8', 'pelak_labbhs2', 'Nazarnita, S. E.', 'Pengadministrasi Keuangan', '7735EAC15B2811EB9CA820677CDFA8E8', '2021-01-20 14:09:31', 'upt lab bahasa', ''),
(61, '09571BE05B2911EB9CA820677CDFA8E8', 'pelak_labbhs3', 'Bayu Nur Cahyo, A. Md.', 'Teknisi Laboratorium', '7735EAC15B2811EB9CA820677CDFA8E8', '2021-01-20 14:10:27', 'upt lab bahasa', ''),
(62, '2199147E5B2911EB9CA820677CDFA8E8', 'pelak_labbhs4', 'Istiandari', 'Teknisi Laboratorium Bahasa', '7735EAC15B2811EB9CA820677CDFA8E8', '2021-01-20 14:11:08', 'upt lab bahasa', ''),
(56, 'BC6BFFC65B2711EB9CA820677CDFA8E8', 'pelak_labdas1', 'Santi, A. M. AK.', 'Teknisi Laboratorium', '5972486E5B2711EB9CA820677CDFA8E8', '2021-01-20 14:01:09', 'upt lab dasar', ''),
(57, 'D81EBB955B2711EB9CA820677CDFA8E8', 'pelak_labdas2', 'Ryandini Putri, S. Farm.', 'Teknisi Laboratorium', '5972486E5B2711EB9CA820677CDFA8E8', '2021-01-20 14:01:55', 'upt lab dasar', ''),
(58, 'F9CD2C4D5B2711EB9CA820677CDFA8E8', 'pelak_labdas3', 'Yuliana, S. E.', 'Penata Dokumen Keuangan', '5972486E5B2711EB9CA820677CDFA8E8', '2021-01-20 14:02:52', 'upt lab dasar', ''),
(27, '27', 'teknisilabbahasa', 'Istiandari', 'Teknisi Laboratorium Bahasa', '13', '2020-12-10 05:01:42', 'upt lab bahasa', 'ilhamr6000@gmail.com'),
(34, '12BAC39B3AFE11EBA5442CFDA1251793', 'tes111', 'Ilham Ramadha, S.Tr.Kom', 'Anggota SP', 'CC1AC29F392D11EBB57E2CFDA1251793', '2020-12-10 15:40:42', '', 'ilhamr6000@gmail.com'),
(36, 'BDB6AB2D3B0711EBA5442CFDA1251793', 'tes666', 'Ilham Ramadha, S.Tr.Kom', 'Pengelola Barang Milik Negara', '2', '2020-12-10 16:49:55', '', 'ilhamr6000@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_super_admin`
--

CREATE TABLE `tbl_super_admin` (
  `username` varchar(10) NOT NULL,
  `nama_lengkap` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_super_admin`
--

INSERT INTO `tbl_super_admin` (`username`, `nama_lengkap`) VALUES
('superadmin', 'Super Admin');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_surat_keluar`
--

CREATE TABLE `tbl_surat_keluar` (
  `out_no` int(11) NOT NULL,
  `out_uuid` varchar(32) NOT NULL,
  `out_urut` int(11) NOT NULL,
  `out_kode_hal` varchar(5) NOT NULL,
  `out_kode_instansi` varchar(30) NOT NULL,
  `out_tujuan` varchar(30) NOT NULL,
  `out_lampiran` varchar(20) NOT NULL,
  `out_perihal` varchar(40) NOT NULL,
  `out_penerbit` varchar(40) NOT NULL,
  `out_dokumen` varchar(255) NOT NULL,
  `out_status` varchar(30) NOT NULL,
  `out_created_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_surat_keluar`
--

INSERT INTO `tbl_surat_keluar` (`out_no`, `out_uuid`, `out_urut`, `out_kode_hal`, `out_kode_instansi`, `out_tujuan`, `out_lampiran`, `out_perihal`, `out_penerbit`, `out_dokumen`, `out_status`, `out_created_at`) VALUES
(14, '0B00E2661ED711EBB5512CFDA1251793', 1000, 'BP', 'UN45.10', 'Ristekdikti', '1', 'Kunjungan kerja', 'pelaksana10', '', 'Menunggu Upload', '2020-11-04 19:50:46'),
(15, '1616200B1ED711EBB5512CFDA1251793', 1000, 'BP', 'UN45', 'Ristekdikti', '1', 'Kunjungan kerja', 'pelaksana10', '', 'Menunggu Upload', '2020-11-04 19:51:05'),
(16, '444AC42C1ED711EBB5512CFDA1251793', 1000, 'AK', 'UN45', 'Ristekdikti', '1', 'Kunjungan kerja', 'pelaksana10', '', 'Menunggu Upload', '2020-11-04 19:52:22'),
(17, '523583911ED711EBB5512CFDA1251793', 1001, 'AK', 'UN45', 'Ristekdikti', '1', 'Kunjungan kerja', 'pelaksana10', '523583911ED711EBB5512CFDA1251793.pdf', 'Done', '2020-11-04 19:52:45'),
(9, '83FF6C7A1ED611EBB5512CFDA1251793', 1, 'AK', 'UN45', 'Ristekdikti', '1', 'Kunjungan kerja', 'pelaksana10', '', 'Menunggu Upload', '2020-11-04 19:46:59'),
(10, '8A9F29F41ED611EBB5512CFDA1251793', 2, 'AK', 'UN45.10', 'Ristekdikti', '1', 'Kunjungan kerja', 'pelaksana10', '', 'Menunggu Upload', '2020-11-04 19:47:11'),
(11, '9610589B1ED611EBB5512CFDA1251793', 3, 'AK', 'UN45', 'Ristekdikti', '1', 'TES COBA', 'pelaksana10', '', 'Menunggu Upload', '2020-11-04 19:47:30'),
(12, 'F1E0A0441ED611EBB5512CFDA1251793', 4, 'AK', 'UN45', 'Ristekdikti', '1', 'TES', 'pelaksana10', '', 'Menunggu Upload', '2020-11-04 19:50:04'),
(13, 'F907B1081ED611EBB5512CFDA1251793', 999, 'AK', 'UN45', 'Ristekdikti', '1', 'TES', 'pelaksana10', 'F907B1081ED611EBB5512CFDA1251793.pdf', 'Done', '2020-11-04 19:50:16');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_unit`
--

CREATE TABLE `tbl_unit` (
  `ka_upt_no` int(11) NOT NULL,
  `ka_upt_uuid` varchar(32) NOT NULL,
  `ka_upt_username` varchar(10) NOT NULL,
  `ka_upt_nama` varchar(30) NOT NULL,
  `ka_upt_posisi` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_unit`
--

INSERT INTO `tbl_unit` (`ka_upt_no`, `ka_upt_uuid`, `ka_upt_username`, `ka_upt_nama`, `ka_upt_posisi`) VALUES
(1, '1', 'ka_upt_tik', 'Bapak Ka UPT TIK', 'Ka. UPT TIK');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE `tbl_users` (
  `username` varchar(16) NOT NULL,
  `nama_lengkap` varchar(30) NOT NULL,
  `jabatan` varchar(30) NOT NULL,
  `status` varchar(20) NOT NULL,
  `admin_time` timestamp NOT NULL DEFAULT current_timestamp(),
  `uuid_atasan` varchar(32) NOT NULL,
  `users_email` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`username`, `nama_lengkap`, `jabatan`, `status`, `admin_time`, `uuid_atasan`, `users_email`) VALUES
('adm_surat_labbhs', 'Belum Ditentukan', 'Admin Surat Lab. Bahasa', 'admin surat', '2021-01-20 14:07:58', '7735EAC15B2811EB9CA820677CDFA8E8', ''),
('adm_surat_labdas', 'Belum Ditentukan', 'Admin Surat Lab. Dasar', 'admin surat', '2021-01-20 13:59:44', '5972486E5B2711EB9CA820677CDFA8E8', ''),
('labbahasa', 'Ilham Ramadhan', 'Admin Surat Lab. Bahasa', 'admin surat', '2020-12-15 02:59:06', '', 'ilhamr6000@gmail.com'),
('labdas', 'Ilham Ramadhan', 'Admin Surat Lab. Dasar', 'admin surat', '2020-12-15 02:59:06', '', 'ilhamr6000@gmail.com'),
('pelak_surat_biro', 'Catrin Novrista Harni, S. E.', 'Admin Surat Subbag. TU', 'admin surat', '2021-01-20 13:45:10', '7497C6215A8F11EB9CA820677CDFA8E8', ''),
('pelak_surat_sp', 'Belum Ditentukan', 'Admin Surat SP', 'admin surat', '2021-01-20 13:38:14', '5571CA2B5B2411EB9CA820677CDFA8E8', ''),
('pelak_surat_tik', 'Belum Ditentukan', 'Admin Surat UPT TIK', 'admin surat', '2021-01-20 13:48:34', 'D7CBDCED5B2511EB9CA820677CDFA8E8', ''),
('sp', 'Ilham Ramadhan', 'Admin Surat SP', 'admin surat', '2020-12-15 02:59:06', '', 'ilhamr6000@gmail.com');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_biro`
--
ALTER TABLE `tbl_biro`
  ADD PRIMARY KEY (`biro_username`),
  ADD UNIQUE KEY `biro_no` (`biro_no`),
  ADD UNIQUE KEY `biro_uuid` (`biro_uuid`);

--
-- Indexes for table `tbl_distribusi_surat`
--
ALTER TABLE `tbl_distribusi_surat`
  ADD PRIMARY KEY (`dist_uuid`),
  ADD UNIQUE KEY `dist_no` (`dist_no`);

--
-- Indexes for table `tbl_draft_surat`
--
ALTER TABLE `tbl_draft_surat`
  ADD PRIMARY KEY (`draft_uuid`),
  ADD UNIQUE KEY `draft_no` (`draft_no`);

--
-- Indexes for table `tbl_kabag`
--
ALTER TABLE `tbl_kabag`
  ADD PRIMARY KEY (`kabag_username`),
  ADD UNIQUE KEY `kabag_no` (`kabag_no`),
  ADD UNIQUE KEY `kabag_uuid` (`kabag_uuid`);

--
-- Indexes for table `tbl_kasubbag`
--
ALTER TABLE `tbl_kasubbag`
  ADD PRIMARY KEY (`kasubbag_username`),
  ADD UNIQUE KEY `kasubbag_no` (`kasubbag_no`),
  ADD UNIQUE KEY `kasubbag_uuid` (`kasubbag_uuid`);

--
-- Indexes for table `tbl_kode_hal`
--
ALTER TABLE `tbl_kode_hal`
  ADD PRIMARY KEY (`hal_no`);

--
-- Indexes for table `tbl_kode_instansi`
--
ALTER TABLE `tbl_kode_instansi`
  ADD PRIMARY KEY (`instansi_no`);

--
-- Indexes for table `tbl_konsep_draft`
--
ALTER TABLE `tbl_konsep_draft`
  ADD PRIMARY KEY (`konsep_uuid`),
  ADD UNIQUE KEY `konsep_no` (`konsep_no`);

--
-- Indexes for table `tbl_login`
--
ALTER TABLE `tbl_login`
  ADD PRIMARY KEY (`username`);

--
-- Indexes for table `tbl_rektor`
--
ALTER TABLE `tbl_rektor`
  ADD PRIMARY KEY (`rektor_no`),
  ADD UNIQUE KEY `rektor_username` (`rektor_username`);

--
-- Indexes for table `tbl_riwayat_disposisi`
--
ALTER TABLE `tbl_riwayat_disposisi`
  ADD PRIMARY KEY (`riw_uuid`),
  ADD UNIQUE KEY `riw_no` (`riw_no`),
  ADD KEY `riw_dist_uuid` (`riw_dist_uuid`),
  ADD KEY `riw_asal` (`riw_asal`);

--
-- Indexes for table `tbl_staff`
--
ALTER TABLE `tbl_staff`
  ADD PRIMARY KEY (`staff_username`),
  ADD UNIQUE KEY `staff_no` (`staff_no`),
  ADD UNIQUE KEY `staff_uuid` (`staff_uuid`);

--
-- Indexes for table `tbl_super_admin`
--
ALTER TABLE `tbl_super_admin`
  ADD PRIMARY KEY (`username`);

--
-- Indexes for table `tbl_surat_keluar`
--
ALTER TABLE `tbl_surat_keluar`
  ADD PRIMARY KEY (`out_uuid`),
  ADD UNIQUE KEY `out_no` (`out_no`);

--
-- Indexes for table `tbl_unit`
--
ALTER TABLE `tbl_unit`
  ADD PRIMARY KEY (`ka_upt_username`),
  ADD UNIQUE KEY `ka_upt_no` (`ka_upt_no`),
  ADD UNIQUE KEY `ka_upt_uuid` (`ka_upt_uuid`);

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_biro`
--
ALTER TABLE `tbl_biro`
  MODIFY `biro_no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `tbl_distribusi_surat`
--
ALTER TABLE `tbl_distribusi_surat`
  MODIFY `dist_no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- AUTO_INCREMENT for table `tbl_draft_surat`
--
ALTER TABLE `tbl_draft_surat`
  MODIFY `draft_no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `tbl_kabag`
--
ALTER TABLE `tbl_kabag`
  MODIFY `kabag_no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `tbl_kasubbag`
--
ALTER TABLE `tbl_kasubbag`
  MODIFY `kasubbag_no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;

--
-- AUTO_INCREMENT for table `tbl_kode_hal`
--
ALTER TABLE `tbl_kode_hal`
  MODIFY `hal_no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_kode_instansi`
--
ALTER TABLE `tbl_kode_instansi`
  MODIFY `instansi_no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_konsep_draft`
--
ALTER TABLE `tbl_konsep_draft`
  MODIFY `konsep_no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=61;

--
-- AUTO_INCREMENT for table `tbl_rektor`
--
ALTER TABLE `tbl_rektor`
  MODIFY `rektor_no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `tbl_riwayat_disposisi`
--
ALTER TABLE `tbl_riwayat_disposisi`
  MODIFY `riw_no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=551;

--
-- AUTO_INCREMENT for table `tbl_staff`
--
ALTER TABLE `tbl_staff`
  MODIFY `staff_no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;

--
-- AUTO_INCREMENT for table `tbl_surat_keluar`
--
ALTER TABLE `tbl_surat_keluar`
  MODIFY `out_no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `tbl_unit`
--
ALTER TABLE `tbl_unit`
  MODIFY `ka_upt_no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
