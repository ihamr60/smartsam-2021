<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Web_App_Model extends CI_Model 
{	

	//=====================
	// Created by : Ilham Ramadhan, S.Tr.Kom
	// 05/09/3030
	//====================

	// db2 digunakan untuk mengakses database ke-2
	// private $db2;

	// public function __construct()
	// {
	//  parent::__construct();
	//         $this->db2 = $this->load->database('db2', TRUE);
	// }

	public function getLoginData($usr, $psw) 
	{
		$u = $usr; 
		$p = md5($psw);
		
		$q_cek_login = $this->db->get_where('tbl_login', array('username' => $u, 'password' => $p)); 
		if(count($q_cek_login->result())>0)
		{
			foreach ($q_cek_login->result() as $qck)
			{	
				if($qck->stts=='admin surat')
				{
					$q_ambil_data = $this->db->get_where('tbl_users', array('username' => $u));
					foreach($q_ambil_data -> result() as $qad)
					{
						$sess_data['logged_in']  	 = 'yes';
						$sess_data['username'] 		 = $qad->username;
						$sess_data['nama'] 		 	 = $qad->nama_lengkap;
						$sess_data['posisi'] 		 = $qad->jabatan;
						$sess_data['stts'] 			 = 'admin surat';
						$sess_data['id_atasan'] 	 = $qad->uuid_atasan;
					    $this->session->set_userdata($sess_data);  

					    $modal_info_update   = $this->load->view('modal_info_update','',true);
					}	
					header('location:'.base_url().'index.php/panel/bg_home?home=1');
					$this->session->set_flashdata('info',''.$modal_info_update.'');
				}
				else if($qck->stts=='rektor')
				{
					$q_ambil_data = $this->db->get_where('tbl_rektor', array('rektor_username' => $u));
					foreach($q_ambil_data -> result() as $qad)
					{
						$sess_data['logged_in']  	 = 'yes';
						$sess_data['username'] 		 = $qad->rektor_username;
						$sess_data['nama'] 		 	 = $qad->rektor_nama;
						$sess_data['posisi'] 		 = $qad->rektor_posisi;
						$sess_data['uuid'] 		 	 = $qad->rektor_uuid;
						$sess_data['stts'] 			 = 'rektor';
					    $this->session->set_userdata($sess_data);  

					    $modal_info_update   = $this->load->view('modal_info_update','',true);
					}	
					header('location:'.base_url().'index.php/rektor/bg_home?home=1');
					$this->session->set_flashdata('info',''.$modal_info_update.'');
				}
				else if($qck->stts=='biro')
				{
					$q_ambil_data = $this->db->get_where('tbl_biro', array('biro_username' => $u));
					foreach($q_ambil_data -> result() as $qad)
					{
						$sess_data['logged_in']  	 = 'yes';
						$sess_data['username'] 		 = $qad->biro_username;
						$sess_data['nama'] 		 	 = $qad->biro_nama;
						$sess_data['posisi'] 	 	 = $qad->biro_posisi;
						$sess_data['uuid'] 		 	 = $qad->biro_uuid;
						$sess_data['kategori'] 		 = $qad->biro_kategori;
						$sess_data['stts'] 			 = 'biro';
					    $this->session->set_userdata($sess_data);  

					    $modal_info_update   = $this->load->view('modal_info_update','',true);
					}	
					header('location:'.base_url().'index.php/biro/bg_home?home=1');
					$this->session->set_flashdata('info',''.$modal_info_update.'');
				}
				else if($qck->stts=='kabag')
				{
					$q_ambil_data = $this->db->get_where('tbl_kabag', array('kabag_username' => $u));
					foreach($q_ambil_data -> result() as $qad)
					{
						$sess_data['logged_in']  	 = 'yes';
						$sess_data['username'] 		 = $qad->kabag_username;
						$sess_data['nama'] 		 	 = $qad->kabag_nama;
						$sess_data['posisi'] 		 = $qad->kabag_posisi;
						$sess_data['uuid'] 		 	 = $qad->kabag_uuid;
						$sess_data['kabag_kategori'] = $qad->kabag_kategori;
						$sess_data['stts'] 			 = 'kabag';
						$sess_data['id_atasan'] 	 = $qad->kabag_uuid_biro;
					    $this->session->set_userdata($sess_data);  

					    $modal_info_update   = $this->load->view('modal_info_update','',true);
					}	
					header('location:'.base_url().'index.php/kabag/bg_home?home=1');
					$this->session->set_flashdata('info',''.$modal_info_update.'');
				}
				else if($qck->stts=='kasubbag')
				{
					$q_ambil_data = $this->db->get_where('tbl_kasubbag', array('kasubbag_username' => $u));
					foreach($q_ambil_data -> result() as $qad)
					{
						$sess_data['logged_in']  	 = 'yes';
						$sess_data['username'] 		 = $qad->kasubbag_username;
						$sess_data['nama'] 		 	 = $qad->kasubbag_nama;
						$sess_data['posisi'] 		 = $qad->kasubbag_posisi;
						$sess_data['uuid'] 		 	 = $qad->kasubbag_uuid;
						$sess_data['stts'] 			 = 'kasubbag';
						$sess_data['id_atasan'] 	 = $qad->kasubbag_uuid_kabag;
					    $this->session->set_userdata($sess_data);  

					    $modal_info_update   = $this->load->view('modal_info_update','',true);
					}	
					header('location:'.base_url().'index.php/kasubbag/bg_home?home=1');
					$this->session->set_flashdata('info',''.$modal_info_update.'');
				}
				else if($qck->stts=='pelaksana')
				{
					$q_ambil_data = $this->db->get_where('tbl_staff', array('staff_username' => $u));
					foreach($q_ambil_data -> result() as $qad)
					{
						$sess_data['logged_in']  	 = 'yes';
						$sess_data['username'] 		 = $qad->staff_username;
						$sess_data['nama'] 		 	 = $qad->staff_nama;
						$sess_data['posisi'] 		 = $qad->staff_posisi;
						$sess_data['uuid'] 		 	 = $qad->staff_uuid;
						$sess_data['stts'] 			 = 'pelaksana';
						$sess_data['id_atasan'] 	 = $qad->staff_uuid_kasubbag;
					    $this->session->set_userdata($sess_data);  

					    $modal_info_update   = $this->load->view('modal_info_update','',true);
					}	
					header('location:'.base_url().'index.php/pelaksana/bg_home?home=1');
					$this->session->set_flashdata('info',''.$modal_info_update.'');
				}
				else if($qck->stts=='super admin')
				{
					$q_ambil_data = $this->db->get_where('tbl_super_admin', array('username' => $u));
					foreach($q_ambil_data -> result() as $qad)
					{
						$sess_data['logged_in']  	 = 'yes';
						$sess_data['username'] 		 = $qad->username;
						$sess_data['nama'] 		 	 = $qad->nama_lengkap;
						$sess_data['stts'] 			 = 'super admin';
						$sess_data['posisi'] 		 = 'Hak Akses Tertinggi (Super Admin)';
					    $this->session->set_userdata($sess_data);  

					    $modal_info_update   = $this->load->view('modal_info_update','',true);
					}	
					header('location:'.base_url().'index.php/super_admin/bg_home?home=1');
					$this->session->set_flashdata('info',''.$modal_info_update.'');
				}
			}
		}
		else
			{
				header('location:'.base_url().'index.php/web');
				$this->session->set_flashdata('info','<div class="alert alert-danger">
					                                    <button data-dismiss="alert" class="close">
					                                        &times;
					                                    </button>
					                                    <i class="fa fa-times-circle"></i>
					                                    <strong>Ups, Maaf!</strong> Username atau password anda salah.
					                                </div>');

				$this->session->set_flashdata("info2","<script type='text/javascript'>
											     Swal.fire({
													  icon: 'error',
													  title: 'Oops...',
													  text: 'Something went wrong!',
													  //footer: '<a href>Why do I have this issue?</a>'
													})
											    </script>
											    ");
			}
	}

	public function insertData($data,$table)
	{
		$this->db->insert($table,$data);
	}

	public function getWhereOneItem($where,$field,$table)
	{
		$data = array();
  		$options = array($field => $where);
  		$Q = $this->db->get_where($table,$options);
    		if ($Q->num_rows() > 0)
				{
      				$data = $Q->row_array();
    			}
  		$Q->free_result();
  		return $data;
	}

	public function getWhereTwoItem($where,$field,$where2,$field2,$table)
	{
		$data = array();
  		$options = array($field => $where, $field2 => $where2);
  		$Q = $this->db->get_where($table,$options);
    		if ($Q->num_rows() > 0)
				{
      				$data = $Q->row_array();
    			}
  		$Q->free_result();
  		return $data;
	}

	public function getAllData($table)
	{
		return $this->db->get($table);
		//return $this->db->get($table)->result();
	}

	public function get_traceSuratMasuk($nomor_surat,$perihal,$pengirim)
	{
		return $this->db->query("SELECT * FROM tbl_distribusi_surat WHERE dist_perihal LIKE '%".$perihal."%' OR dist_perihal LIKE '%".$perihal."' OR dist_perihal LIKE '".$perihal."%' OR dist_perihal LIKE '".$perihal."_' OR dist_perihal LIKE '_".$perihal."' AND dist_pengirim LIKE '%".$pengirim."%' OR dist_pengirim LIKE '".$pengirim."%' OR dist_pengirim LIKE '%".$pengirim."' OR dist_pengirim LIKE '_".$pengirim."' OR dist_pengirim LIKE '".$pengirim."_' AND dist_nomorsurat LIKE '%".$nomor_surat."%'");
	}

	public function get_dekan()
	{
		return $this->db->query("SELECT * FROM `tbl_biro` WHERE biro_kategori = 'dekan fakultas'");
	}

	public function get_kasubbag_dekan($fakultas_kateg)
	{
		return $this->db->query("SELECT * FROM `tbl_kasubbag` INNER JOIN tbl_kabag ON tbl_kasubbag.kasubbag_uuid_kabag = tbl_kabag.kabag_uuid WHERE kasubbag_kategori = '".$fakultas_kateg."'");
	}

	public function get_admsurat_dekan($fakultas_kateg1, $fakultas_kateg2, $fakultas_kateg3, $fakultas_kateg4, $fakultas_kateg5)
	{
		return $this->db->query("SELECT * FROM `tbl_users` 
				WHERE jabatan = '".$fakultas_kateg1."' 
				OR jabatan = '".$fakultas_kateg2."' 
				OR jabatan = '".$fakultas_kateg3."' 
				OR jabatan = '".$fakultas_kateg4."' 
				OR jabatan = '".$fakultas_kateg5."'");
	}

	public function get_pelaksana_dekan($fakultas_kateg)
	{
		return $this->db->query("SELECT * FROM `tbl_staff` WHERE 
		staff_kategori = '".$fakultas_kateg."'");
	}

	public function get_kabag_prodi_dekan($fakultas_kateg)
	{
		return $this->db->query("SELECT * FROM `tbl_kabag` WHERE 
		kabag_kategori = '".$fakultas_kateg."'");
	}

	public function updateDataWhere($where,$data,$table){
		$this->db->where($where);
		$this->db->update($table,$data);
	}

	public function getWhereAllItem($where,$field,$table)
	{
		$this->db->where($field,$where);
		return $this->db->get($table);
	}

	public function get2WhereAllItem($where,$field,$where2,$field2,$table)
	{
		$this->db->where($field,$where);
		$this->db->where($field2,$where2);
		return $this->db->get($table);
	}

	public function getWhereAllItem_between($where,$field,$table,$date_field,$tgl_awal,$tgl_akhir)
	{
		$this->db->where($field,$where);
		$this->db->where(''.$date_field.' >=',$tgl_awal);
		$this->db->where(''.$date_field.' <=',$tgl_akhir);
		return $this->db->get($table);
	}

	public function getAllData_between($table,$date_field,$tgl_awal,$tgl_akhir)
	{
		$this->db->where(''.$date_field.' >=',$tgl_awal);
		$this->db->where(''.$date_field.' <=',$tgl_akhir);
		return $this->db->get($table);
		//return $this->db->get($table)->result();
	}	

	public function getCount($table,$field,$where)
	{
		$data = array();
  		$Q = $this->db->query("SELECT COUNT(*) AS TOTAL FROM ".$table." WHERE ".$field."='".$where."'");
    		if ($Q->num_rows() > 0)
				{
      				$data = $Q->row_array();
    			}
  		$Q->free_result();
  		return $data;
	}

	public function getCount2Where($table,$field1,$where1,$field2,$where2)
	{
		$data = array();
  		$Q = $this->db->query("SELECT COUNT(*) AS TOTAL FROM ".$table." WHERE ".$field1."='".$where1."' AND ".$field2."='".$where2."'");
    		if ($Q->num_rows() > 0)
				{
      				$data = $Q->row_array();
    			}
  		$Q->free_result();
  		return $data;
	}

	public function getCount3Where($table,$field1,$where1,$field2,$where2,$field3,$where3)
	{
		$data = array();
  		$Q = $this->db->query("SELECT COUNT(*) AS TOTAL FROM ".$table." WHERE ".$field1."='".$where1."' AND ".$field2."='".$where2."' AND ".$field3."='".$where3."'");
    		if ($Q->num_rows() > 0)
				{
      				$data = $Q->row_array();
    			}
  		$Q->free_result();
  		return $data;
	}

	public function getCount1Where($table,$field1,$where1)
	{
		$data = array();
  		$Q = $this->db->query("SELECT COUNT(*) AS TOTAL FROM ".$table." WHERE ".$field1."='".$where1."'");
    		if ($Q->num_rows() > 0)
				{
      				$data = $Q->row_array();
    			}
  		$Q->free_result();
  		return $data;
	}

	public function getJoinOneWhere($idTabel1,$idTabel2,$table1,$table2,$field,$where)
	{
		$data = array();
		$this->db->where($field,$where);
		$this->db->join($table2, ''.$table1.'.'.$idTabel1.' = '.$table2.'.'.$idTabel2.'');
		$this->db->from($table1);
		$Q = $this->db->get();
		if ($Q->num_rows() > 0)
				{
      				$data = $Q->row_array();
    			}
  		$Q->free_result();
  		return $data;
	}

	public function get2JoinOneWhere($idTabel1,$idTabel2,$idTabel3,$idTabel4,$table1,$table2,$table3,$table4,$field,$where)
	{
		$data = array();
		$this->db->where($field,$where);
		$this->db->join($table2, ''.$table1.'.'.$idTabel1.' = '.$table2.'.'.$idTabel2.'');
		$this->db->join($table3, ''.$table4.'.'.$idTabel4.' = '.$table3.'.'.$idTabel3.'');
		$this->db->from($table1);
		$Q = $this->db->get();
		if ($Q->num_rows() > 0)
				{
      				$data = $Q->row_array();
    			}
  		$Q->free_result();
  		return $data;
	}

	public function get3JoinOneWhere($idTabel1,$idTabel2,$idTabel3,$idTabel4,$idTabel5,$idTabel6,$table1,$table2,$table3,$table4,$table5,$table6,$field,$where)
	{
		$data = array();
		$this->db->where($field,$where);
		$this->db->join($table2, ''.$table1.'.'.$idTabel1.' = '.$table2.'.'.$idTabel2.'');
		$this->db->join($table3, ''.$table4.'.'.$idTabel4.' = '.$table3.'.'.$idTabel3.'');
		$this->db->join($table5, ''.$table6.'.'.$idTabel6.' = '.$table5.'.'.$idTabel5.'');
		$this->db->from($table1);
		$Q = $this->db->get();
		if ($Q->num_rows() > 0)
				{
      				$data = $Q->row_array();
    			}
  		$Q->free_result();
  		return $data;
	}

	public function deleteData($table,$data)
	{
		$this->db->delete($table, $data);
	}

	public function get2JoinAllWhere($idTabel1,$idTabel2,$idTabel3,$idTabel4,$table1,$table2,$table3,$table4,$field,$where)
	{
		$this->db->where($field,$where);
		$this->db->join($table2, ''.$table1.'.'.$idTabel1.' = '.$table2.'.'.$idTabel2.'');
		$this->db->join($table3, ''.$table4.'.'.$idTabel4.' = '.$table3.'.'.$idTabel3.'');
		$this->db->from($table1);
		$query = $this->db->get();
		return $query;
	}

	public function get2JoinAllWhereGroup($idTabel1,$idTabel2,$idTabel3,$idTabel4,$table1,$table2,$table3,$table4,$field,$where,$group)
	{
		$this->db->where($field,$where);
		$this->db->join($table2, ''.$table1.'.'.$idTabel1.' = '.$table2.'.'.$idTabel2.'');
		$this->db->join($table3, ''.$table4.'.'.$idTabel4.' = '.$table3.'.'.$idTabel3.'');
		$this->db->group_by($group); 
		$this->db->from($table1);
		$query = $this->db->get();
		return $query;
	}

	public function get2JoinAll2WhereGroup($idTabel1,$idTabel2,$idTabel3,$idTabel4,$table1,$table2,$table3,$table4,$field,$where,$field2,$where2,$group)
	{
		$this->db->where($field,$where);
		$this->db->where($field2,$where2);
		$this->db->join($table2, ''.$table1.'.'.$idTabel1.' = '.$table2.'.'.$idTabel2.'');
		$this->db->join($table3, ''.$table4.'.'.$idTabel4.' = '.$table3.'.'.$idTabel3.'');
		$this->db->group_by($group); 
		$this->db->from($table1);
		$query = $this->db->get();
		return $query;
	}

	public function getJoinAllWhere($idTabel1,$idTabel2,$table1,$table2,$field,$where)
	{
		$this->db->where($field,$where);
		$this->db->join($table2, ''.$table1.'.'.$idTabel1.' = '.$table2.'.'.$idTabel2.'');
		$this->db->from($table1);
		$query = $this->db->get();
		return $query;
	}

	public function getJoinAllWhereOrderBy($idTabel1,$idTabel2,$table1,$table2,$field,$where,$order,$by)
	{

		
		$this->db->where($field,$where);
		$this->db->join($table2, ''.$table1.'.'.$idTabel1.' = '.$table2.'.'.$idTabel2.'');
		$this->db->order_by($order,$by);
		$this->db->from($table1);
		$query = $this->db->get();
		return $query;
	}

	public function getJoinAll2Where($idTabel1,$idTabel2,$table1,$table2,$field,$where,$field1,$where1)
	{
		$this->db->where($field,$where);
		$this->db->where($field1,$where1);
		$this->db->join($table2, ''.$table1.'.'.$idTabel1.' = '.$table2.'.'.$idTabel2.'');
		$this->db->from($table1);
		$query = $this->db->get();
		return $query;
	}

	public function getAll2Join($idTabel1,$idTabel2,$table1,$table2)
	{
		$this->db->join($table2, ''.$table1.'.'.$idTabel1.' = '.$table2.'.'.$idTabel2.'');
		$this->db->from($table1);
		$query = $this->db->get();
		return $query;
	}

	public function getAll3Join($idTabel1,$idTabel12,$idTabel2,$idTabel3,$table1,$table2,$table3)
	{
		 $this->db->select('*');
		 $this->db->from($table1);
		 $this->db->join($table2,''.$table2.'.'.$idTabel2.'='.$table1.'.'.$idTabel1.'');
		 $this->db->join($table3,''.$table3.'.'.$idTabel3.'='.$table1.'.'.$idTabel12.'');
		 $query = $this->db->get();
		 return $query;
	}

	public function getAll4Join($idTabel1,$idTabel12,$idTabel2,$idTabel3,$idTabel4,$table1,$table2,$table3,$table4)
	{
		 $this->db->select('*');
		 $this->db->from($table1);
		 $this->db->join($table2,''.$table2.'.'.$idTabel2.'='.$table1.'.'.$idTabel1.'');
		 $this->db->join($table3,''.$table3.'.'.$idTabel3.'='.$table1.'.'.$idTabel1.'');
		 $this->db->join($table4,''.$table4.'.'.$idTabel4.'='.$table1.'.'.$idTabel12.'');
		 $query = $this->db->get();
		 return $query;
	}
}
