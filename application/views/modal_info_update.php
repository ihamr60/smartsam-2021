 <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h2 class="modal-title">INFORMASI PENGEMBANGAN</h2>
                </div>
                <div class="modal-body">
                    <h4>Halo, <?php

date_default_timezone_set("Asia/Jakarta");

$b = time();
$jam = date("G",$b);

if ($jam>=0 && $jam<=11)
{
echo "Selamat Pagi ";
}
else if ($jam >=12 && $jam<=14)
{
echo "Selamat Siang";
}
else if ($jam >=15 && $jam<=17)
{
echo "Selamat Sore";
}
else if ($jam >=17 && $jam<=18)
{
echo "Selamat Petang";
}
else if ($jam >=19 && $jam<=23)
{
echo "Selamat Malam";
}

?> <b><?php echo $this->session->userdata('nama'); ?></b>, Update terbaru SmartSAM V1.19<br>
                    
                    
                    <h4>Changelog Update V1.20<br><br>
                        <pre>
Tanggal Update: Kamis, 23 Sept 2021 
<b>Apa saja yang baru di V1.20?</b>

- Update penambahan fitur Layanan Surat Masuk di Lingkungan Fakultas
- Update Penambahan Fitur Layanan Pengajuan Draft Surat Keluar di Lingkungan Fakultas

                        </pre>
                    </h4>

                    <h4>Changelog Update V1.19<br><br>
                        <pre>
Tanggal Update: Rabu, 03 Mei 2021 
<b>Apa saja yang baru di V1.19?</b>

- Surat Keluar Kasubbag 
- — Fungsi Input Surat Keluar (DONE)

- Surat Keluar Kabag
- — Fungsi Input Surat Keluar (DONE)

- Surat Keluar Biro
- — Fungsi Input Surat Keluar (DONE)


                        </pre>
                    </h4>

                    <h4>Changelog Update V1.18<br><br>
                        <pre>
Tanggal Update: Rabu, 28 April 2021 
<b>Apa saja yang baru di V1.18?</b>

- Menu create draft surat kasubbag. (SUDAH TERUJI)
    — Membuat menu Draft Keluar (DONE)
    — Membuat Halaman Draft DIterima (DONE)
    — Membuat Halaman Draft Pending (DONE)
    — Membuat Halaman Draft Acc (DONE)
    — Membuat Halaman Draft Usulan (DONE)
    — Membuat Aksi Tandai Selesai pada Draft ACC (DONE) 
    — Membuat Batas Akses “Sdh diteruskan” pada Draft Pending (DONE)
    — Membuat Batas Akses “Done” pada draft pending selesai (DONE)

- Membuat Pembatasan Akses “Sdh diteruskan” pada Draf Pending Pelaksana (DONE)



- Menu create draft surat kabag (SUDAH TERUJI)
-         — Membuat menu Draft Keluar (DONE)
-   — Membuat Halaman Draft DIterima (DONE)
-   — Membuat Halaman Draft Pending (DONE)
-   — Membuat Halaman Draft Acc (DONE)
-   — Membuat Halaman Draft Usulan (DONE)
-   — Membuat Aksi Tandai Selesai pada Draft ACC (DONE) 
-   — Membuat Batas Akses “Sdh diteruskan” pada Draft Pending (DONE)
-   — Membuat Batas Akses “Done” pada draft pending selesai (DONE)
-   — Membuat Batas AKses Done Pada Draft Pending Ka_SP (DONE)
-   — Membuat Batas Akses Done pada Surat Masuk (DONE)



- Menu create draft surat biro (SUDAH TERUJI)
- — Membuat Menu Draft Keluar (DONE)
- — Membuat Halaman Draft Diterima (DONE)
- — Membuat Halaman Draft Pending (DONE)
- — Membuat Halaman Draft Acc (DONE)
- — Membuat Halaman Draft Usulan (DONE)
- — Membuat Aksi Tandai Selesai pada Draft ACC 
- — Membuat Batas Akses “Sdh diteruskan” pada Draft Pending (DONE)
- — Membuat Batas Akses “Done” pada draft pending selesai (DONE)


REKTOR
- Membuat batas akses “”sudah diteruskan” pada draft pending (DONE)
- Membuat batas akses “DOne” pada draft pending selesai (DONE)
- Tidak masuk muncul nama penerima pada notif Tele (DONE)
- Notif Email Gammasuk ke Tele (Warek t rektor) (DONE)


NOte: masalah pada var email tujuan yg berlebihan.
- Email gak masuk saat Acc DRAFT SURAT by Rektor (DONE)




Buat fungsi Pembatas pada Rektor draft yg Sudah Diteruskan (DONE)


                        </pre>
                    </h4>

                    <h4>Changelog Update V1.17<br><br>
                        <pre>
Tanggal Update: Selasa, 23 Maret 2021 
<b>Apa saja yang baru di V1.17?</b>

- Revisi nama Kolom DIsposisi pada Surat Masuk Level Biro (DONE)
- Revisi Kolom DIsposisi pada Surat masuk Level Admin Surat (DONE)

- Membatasi Akses TInjuk Surat Masuk Pada Level Ka UPT (DONE)
- Membatasi Akses TInjut Surat Masuk pada Level Sekretaris Ka UPT (DONE)
- Membatasi Akses TInjut Surat Masuk pada Level Pelaksana UPT(DONE)

- Membatasi Admin SUrat Tinjut Selesai Surat Masuk yg blm clear (DONE)

- Fungsi wajib isi tujuan disposisi surat masuk pada level Kasubbag (DONE)
- Fungsi wajib isi tujuan disposisi surat masuk pada level kabag (DONE)
- Fungsi wajib isi tujuan disposisi surat masuk pada level biro (DONE)
- Fungsi wajib isi tujuan disposisi surat masuk pada level rektor (DONE)
- Fungsi wajib isi tujuan disposisi surat masuk pada level admin surat (DONE)

                        </pre>
                    </h4>

                    <h4>Changelog Update V1.16<br><br>
                        <pre>
Tanggal Update: Sabtu, 20 Maret 2021 
<b>Apa saja yang baru di V1.16?</b>

- Revisi Footer  (DONE)
- Pilihan disposisi pada ka_biro bisa ke semua staff pada bironya masing2 (DONE)
- Tambah menu Kode Hal & Bagian ke Admin Persuratan (DONE)
- 
- Author nya di ganti menjadi Nama User pda Pelaksana Menu Surat Masuk (DONE)
- Author nya di ganti menjadi Nama User pda Pelaksana Menu Draft Pending (DONE)
- Author nya di ganti menjadi Nama User pda Pelaksana Menu Draft Acc (DONE)
- Author nya di ganti menjadi Nama User pda Pelaksana Menu Surat Keluar (DONE)

- Author nya di ganti menjadi Nama User pda Kasubbag Menu Surat Masuk (DONE)
- Author nya di ganti menjadi Nama User pda Kasubbag Menu Draft Keluar (DONE)
- Author nya di ganti menjadi Nama User pda Kasubbag Menu Surat Keluar (DONE)

- Author nya di ganti menjadi Nama User pda Kabag Menu Surat Masuk (DONE)
- Author nya di ganti menjadi Nama User pda Kabag Menu Draft Keluar (DONE)
- Author nya di ganti menjadi Nama User pda Kabag Menu Surat Keluar (DONE)

- Author nya di ganti menjadi Nama User pda Biro Menu Surat Masuk (DONE)
- Author nya di ganti menjadi Nama User pda Biro Menu Draft Keluar (DONE)
- Author nya di ganti menjadi Nama User pda Biro Menu Surat Keluar (DONE)

- Author nya di ganti menjadi Nama User pda Rektor Menu Surat Masuk (DONE)
- Author nya di ganti menjadi Nama User pda Rektor Menu Draft Keluar (DONE)
- Author nya di ganti menjadi Nama User pda Rektor Menu Surat Keluar (DONE)
- 
- Author nya di ganti menjadi Nama User pda Admin Surat Menu Surat Masuk (DONE)
- Author nya di ganti menjadi Nama User pda Admin Surat Menu Draft Pending (DONE)
- Author nya di ganti menjadi Nama User pda Admin Surat Menu Draft Acc (DONE)
- Author nya di ganti menjadi Nama User pda Admin Surat Menu Surat Keluar (DONE)

                        </pre>
                    </h4>

                    <h4>Changelog Update V1.15<br><br>
                        <pre>
Tanggal Update: Selasa, 16 Maret 2021 
<b>Apa saja yang baru di V1.15?</b>

- Membuat Aksi Teruskan Draft ke Rektor atau Disetujui langsung pada level Ka Perpus (DONE)
- Tambah Menu Kode Bagian pada Super Admin (DONE)
- Tambah Menu Kode Hal pada Super Admin (DONE)

- Membatasi akses Tinjut Surat Masuk pada Level Pelaksana (DONE)
- Membatasi aksesi TInjut Surat Masuk pada Level Kasubbag (DONE)
- Membatasi AKses TInjut Surat Masuk pada Level Kabag (DONE)
- Membatasi AKses TInjur surat Masuk pada Level Biro (DONE)
- Membatasi AKses TInjut SUrat Masuk pda Level Rektor (DONE)

                        </pre>
                    </h4>

                    <h4>Changelog Update V1.14<br><br>
                        <pre>
Tanggal Update: Sabtu, 13 Maret 2021 
<b>Apa saja yang baru di V1.14?</b>

- FUngsi Edit data Ka perpus pd superadmin (DONE)
- Fungsi edit data Pelaksana pd superadmin (DONE)
- Fungsi edit data Admin Surat pd superadmin (DONE)

- Fungsi hapus data ka perpus pd superadmin (DONE)
- Fungsi hapus data pelaksana pd superadmin (DONE)
- Fungsi hapus data admin surat pd superadmin (DONE)

- Login Ka Perpus (DONE)
- Login Admin Surat Perpus (DONE)
- Login Pelaksana Perpus (DONE)

                        </pre>
                    </h4>

                    <h4>Changelog Update V1.13<br><br>
                        <pre>
Tanggal Update: Kamis, 11 Maret 2021 
<b>Apa saja yang baru di V1.13?</b>

- Fungsi tambah data user Admin Surat UPT Perpus pada level SuperAdmin (DONE)
- Fungsi tambah data user Ka Perpus pada level SUperAdmin (DONE)
- Fungsi tambah data user Pelaksana perpus pada level SuperAdmin (DONE)

- Fungsi Read Data Ka Perpus pd Super Admin (DONE)
- FUngsi Read Data Pelaksana pd Superadmin Admin (DONE)
- Fungsi Read Data Admin Surat Pada Super Admin (DONE)

                        </pre>
                    </h4>

                    <h4>Changelog Update V1.12<br><br>
                        <pre>
Tanggal Update: Kamis, 04 Maret 2021 
<b>Apa saja yang baru di V1.12?</b>

- Halaman data user UPT Perpus pada level SuperAdmin (DONE)
- Fungsi Read Data User UPT Perpus pada SuperAdmin (DONE)

                        </pre>
                    </h4>

                    <h4>Changelog Update V1.11<br><br>
                        <pre>
Tanggal Update: Minggu, 28 Februari 2021 
<b>Apa saja yang baru di V1.11?</b>

- Menu Kode Instansi pada Level Pelaksana (DONE)
    — Laman Kode Bagian (DONE)
    — Aksi tambah & hapus kd bagian (DONE)

- Menu Kode Hal pada Level Pelaksana (DONE)
    — Laman Kode Hal (DONE)
    — Aksi tambah & hapus kd hal (DONE)

- Menu Kode Instansi pada Level Admin Surat (DONE)
    — Laman kode Bagian (DONE)
    — Aksi Tambah & Hapus kd bagian (DONE)

- Menu Kode Hal pada Level Admin Surat (DONE)
    — Laman Kode Hal (DONE)
    — Aksi tambah & hapus kd hal (DONE)

- Form tambah surat keluar pada Admin Surat (DONE)
    — Laman Form tambah surat Keluar (DONE)
    — Aksi Tambah Surat Keluar (DONE)
    — Akasi Upload surat Keluar (DONE)

-  Tambah Menu Surat Keluar pada level Admin Surat (DONE)
    — Form Tambah Draft Surat pd admin surat (DONE)
    — Aksi tambah Draft Surat pd admin surat (DONE)
    — Notif Email dan Tele pada Kontroller Admin Surat (DONE)
    — bg-draft_pending, halaman direct after sukses input draft (DONE)
    — Halaman Detail Draft pada Admin Surat (DONE)
    — Aksi forward konsep draft pada Admin Surat (DONE)

                        </pre>
                    </h4>

                    <h4>Changelog Update V1.10<br><br>
                        <pre>
Tanggal Update: Minggu, 21 Februari 2021 
<b>Apa saja yang baru di V1.10?</b>
- Penyempurnaan tabel surat keluar pada level Pelaksana (DONE)
- Penyempurnaan tabel Surat Keluar pada level Kasubbag (DONE)
- Penyempurnaan tabel surat keluar pada level Kabag ( DONE)
- Penyempurnaan tabel surat  keluar pada level Biro (DOnE)
- PEnyempurnaan tabel surat keluar pada lebel Rektor (DONE)
- Penyempurnaan tabel surat keluar pada lavel Admin Surat (DONE)

                        </pre>
                    </h4>

                    <h4>Changelog Update V1.9<br><br>
                        <pre>
Tanggal Update: Sabtu, 20 Februari 2021 
<b>Apa saja yang baru di V1.9?</b>
- Tambah Notif Email pada Kontroller Pelaksana (DONE)
- Tambah Notif Email pada Kontroller Kasubbag (DONE)
- Perbaiki tabel Draft Keluar & Notif EMail pada Ka UPT TIK x Kasubbag Acc (DONE)
- Perbaiki tabel Draft Keluar & Notif EMail pada Ka UPT TIK x Kasubbag Forward Rektorr (DONE)
- Perbaiki tabel Draft Keluar & Notif EMail pada Ka SP x Kabag Acc (DONE)
- Perbaiki tabel Draft Keluar & Notif EMail pada Ka SP x Kabag Forward (DONE)
- Tambah Notif Email Pada Kontroller Kabag (DONE)
- Tambah Notif EMail Rektor ACC UPT TIK x Kasubbag (DONE)

- Tambah Notif Email pada Kontroller Biro (DONE)
- Tambah Notif EMail pada kontroller rektor (DONE)


                        </pre>
                    </h4>

                    <h4>Changelog Update V1.8<br><br>
                        <pre>
Tanggal Update: Kamis, 18 Februari 2021 
<b>Apa saja yang baru di V1.8?</b>
- Perbaikan tabel Draft Keluar pada Rektor (DONE)
- Tata Letak Kolom pada Daftar Draft Surat pada Kasubbag Terbalik (DONE) 
- Tambah Notif Menu Draft Keluar Baru pd rektor (DONE)
- Penyempurnaan tabel riwayat koreksi draft keluar  pd Rektor (DONE)
- Error Rektor_uuid pada Menu Settings Rektor (DONE)
- FITUR "DRAFT KELUAR" PADA MENU SUDAH DAPAT DIGUNAKAN



                        </pre>
                    </h4>

                    <h4>Changelog Update V1.7<br><br>
                        <pre>
Tanggal Update: Senin, 15 Februari 2021 
<b>Apa saja yang baru di V1.7?</b>
- Penyempurnaan halaman Draf Keluar pd Level Kasubbag (DONE)
- Penambahan Notif Draf Baru pada Kasubbag (DONE)
- Penyempurnaan tampilan Tabel RIwayat Koreksi Draft pada Kasubbag (DONE)

- Penyempurnaan tampilan Draft Keluar pada level Kabag (DONE)
- Penambahan Notif menu Draft Baru pada Kabag (DONE)

- Penyempurnaan tabel riwayat koreksi draft Kabag (DONE)
- Perbaikan Form Konsep Draft Hilang pada User Kabag_umum (DONE)
- Yg Di atas Berhubungan dengan User Ka SP juga bermasalah (DONE)

- Masalah pada Kabag Ka SP Blank saat dimasukin Modal Form (DONE)
- Penyempurnaan tabel Draft Keluar pada Biro (DONE)
- Penyempurnaan tabel riwayat koreksi draft biro (DONE)
- Penambahan Notif Draft menu pada Biro (DONE)


                        </pre>
                    </h4>

                    <h4>Changelog Update V1.6<br><br>
                        <pre>
Tanggal Update: Jumat, 12 Februari 2021 
<b>Apa saja yang baru di V1.6?</b>
- Penambahan Kolom Database Draft_tujuan_surat pada tabel Draft_Surat (Varchar,40) (DONE)
- Tambah Comment pada draft_penerima (tbl_draft_surat) (Penerima Disposisi) (DONE)
- Tambah Comment pada draft_tujuan_surat (tbl_draft_surat) (Tujuan statis) (DONE)
- Tambah COmment pada konsep_tujuan (tbl_konsep_draft) (Tujuan DIsposisi (Dinamis)) (DONE)
- Penambahan Form Tujuan Surat pada Pelaksana (DONE)
- Edit Tabel bg_draft_pending pada Pelaksana tambah Kolom Tujuan(DONE)
- Update Draft_perihal Value Ganti dr 40 jd 100 pd tbl_draft_surat (DONE)
- Perbaiki Tampilan ACC pada pelaksana (DONE)
- Tambah Aksi Tandai pada Draft ACC Pelaksana (DONE)


                        </pre>
                    </h4>

                    <h4>Changelog Update V1.5<br><br>
                        <pre>
Changelog: Selasa, 09 Februari 2021 
<b>Apa saja yang baru di V1.5?</b>
- Menyembunyikan status posisi terkini pada pencarian surat (Req. P'Adnan)
- Perbaikan tampilan tabel pada bg_detail_draft [Riwayat Memo]


                        </pre>
                    </h4>

                    <h4>Changelog Update V1.4<br><br>
                        <pre>
Changelog: Minggu, 07 Februari 2021 
<b>Apa saja yang baru di V1.4?</b>
- Update Penambahan fitur Trace / Lacak Surat Masuk pada halaman login (Req. P'Adnan)


                        </pre>
                    </h4>

                    <h4>Changelog Update V1.3<br><br>
                        <pre>
Changelog: Sabtu, 30 Januari 2021 
<b>Apa saja yang baru di V1.3?</b>
- Perbaikan Form Pengajuan Draft Surat pada Pelaksana
- Mengarahkan Halaman Sukses Isi Draf ke Page Draf Pending pd Pelaksana
- Perbaikan Database tbl_konsep_draf kolom konsep_tujuan lengt 30 ke 32
- Perbaikan Database tbl_draft_surat Kolom draft_penerima lengt 30 ke 32
- Fitur Notifikasi Pada Draff ACC Pelaksana
- Perbaikan Tampilan tabel Draft Surat Pending pd Pelaksana
- Perbaikan Tindakan tidak berfungsi pada Ka UPT / tbl Kasubbag 


                        </pre>
                    </h4>

                    <h4>Changelog Update V1.2<br><br>
                        <pre>
Changelog: Rabu, 27 Januari 2021 
<b>Apa saja yang baru di V1.2?</b>
- Memperbaiki fungsi & tampilan tabel surat keluar Admin Surat Users
- Memperbaiki fungsi & tampilan tabel surat keluar pada Pelaksana
- Membatasi fungsi akses Upload surat utk user yang bukan bersangkutan
- Perbaikan fungsi Umpan Balik saat gagal upload surat keluar
- Memperbaiki fungsi & tampilan tabel surat keluar pada kasubbag
- Memperbaiki fungsi & tampilan tabel surat keluar pada kabag
- Memperbaiki fungsi & Tampilan Tabel Surat Keluar pada Biro
- Memperbaiki fungsi & Tampilan pada Surat Keluar Rektor

                        </pre>
                    </h4>

                    <h4>Changelog Update V1.1<br><br>
                        <pre>
Changelog: Sabtu, 23 Januari 2021 
<b>Apa saja yang baru di V1.1?</b>
- Perbaikan text pada halaman login (request by pak fadhil)
- Form input tanggal surat pada admin surat
                        </pre>
                    </h4>

                    <h4>Changelog Update V1.0<br><br>
                        <pre>
Changelog: Rabu, 20 Januari 2021 
<b>Apa saja yang baru di V1.0?</b>
- Rilis pertama
                        </pre>
                    </h4>


                    
                    

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>





<!-- NOTE: Letakan script dibawah ini ke halaman tempat dimana modal ditampilkan -->
    <script>
        $('#myModal').modal('show');
    </script>