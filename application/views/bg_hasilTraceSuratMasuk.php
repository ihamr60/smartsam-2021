<!DOCTYPE html>
<!--[if IE 8]><html class="ie8 no-js" lang="en"><![endif]-->
<!--[if IE 9]><html class="ie9 no-js" lang="en"><![endif]-->
<!--[if !IE]><!-->
<html class="no-js">
<!--<![endif]-->

<head>
    <title>Tracer Surat Masuk</title>
    <link rel="shortcut icon" href="favicon.ico" />
    <!-- start: META -->
    <meta charset="utf-8" />
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content="IE=edge,IE=9,IE=8,chrome=1" /><![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta content="Responsive Admin Template build with Twitter Bootstrap and jQuery" name="description" />
    <meta content="ClipTheme" name="author" />
    <!-- end: META -->
    <!-- start: MAIN CSS -->
    <link type="text/css" rel="stylesheet" href="//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700|Raleway:400,100,200,300,500,600,700,800,900/" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/bower_components/bootstrap/dist/css/bootstrap.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/bower_components/font-awesome/css/font-awesome.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/assets/fonts/clip-font.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/bower_components/iCheck/skins/all.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/bower_components/perfect-scrollbar/css/perfect-scrollbar.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/bower_components/sweetalert/dist/sweetalert.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/assets/css/main.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/assets/css/main-responsive.min.css" />
    <link type="text/css" rel="stylesheet" media="print" href="<?php echo base_url(); ?>vendor/assets/css/print.min.css" />
    <link type="text/css" rel="stylesheet" id="skin_color" href="<?php echo base_url(); ?>vendor/assets/css/theme/light.min.css" />
    <!-- end: MAIN CSS -->
    <!-- start: CSS REQUIRED FOR THIS PAGE ONLY -->
    <link href="<?php echo base_url();?>vendor/bower_components/select2/dist/css/select2.min.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>vendor/bower_components/datatables/media/css/dataTables.bootstrap.min.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>vendor/bower_components/datatables/media/css/jquery.dataTables.min.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>vendor/bower_components/datatables.net-buttons-dt/css/buttons.dataTables.min.css" rel="stylesheet" />
    
    <link href="<?php echo base_url();?>vendor/bower_components/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>vendor/bower_components/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" />

    <!--  BUTTON -->
    <link href="<?php echo base_url();?>vendor/bower_components/ladda-bootstrap/dist/ladda-themeless.min.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>vendor/bower_components/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.min.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>vendor/bower_components/bootstrap-social/bootstrap-social.css" rel="stylesheet" />
    <!-- end: CSS REQUIRED FOR THIS PAGE ONLY -->

</head>

<body class="page-full-width">

    <!-- start: HEADER -->
    <div class="navbar navbar-inverse navbar-fixed-top">
        <!-- start: TOP NAVIGATION CONTAINER -->
        <div class="container">
            <div class="navbar-header">
                <!-- start: RESPONSIVE MENU TOGGLER -->
                <button data-target=".navbar-collapse" data-toggle="collapse" class="navbar-toggle" type="button">
                <span class="clip-list-2"></span>
            </button>
                <!-- end: RESPONSIVE MENU TOGGLER -->
                <!-- start: LOGO -->
                <a class="navbar-brand" href="#">
               <b> >> TRACE SURAT MASUK </b>
            </a>
                <!-- end: LOGO -->
            </div>
            <!-- start: HORIZONTAL MENU -->
            <div class="horizontal-menu navbar-collapse collapse">
                <ul class="nav navbar-nav">
                    <li>
                        <a href="<?php echo base_url() ?>index.php">
                        LOGIN
                    </a>
                </ul>
            </div>
            <!-- end: HORIZONTAL MENU -->
        </div>
        <!-- end: TOP NAVIGATION CONTAINER -->
    </div>
    <!-- end: HEADER -->
    <!-- start: MAIN CONTAINER -->
    <div class="main-container">
        <!-- start: PAGE -->
        <div class="main-content">
            
            <!-- end: SPANEL CONFIGURATION MODAL FORM -->
            <div class="container">
                <!-- start: PAGE HEADER -->
                <div class="row">
                    <div class="col-sm-12">
                        <!-- start: PAGE TITLE & BREADCRUMB -->
                        <!--<ol class="breadcrumb">
                            <li>
                                <i class="clip-home-3"></i>
                                <a href="#">
                                    Home
                                </a>
                            </li>
                            <li class="active">
                                Dashboard
                            </li>
                            <li class="search-box">
                                <form class="sidebar-search">
                                    <div class="form-group">
                                        <input type="text" placeholder="Start Searching...">
                                        <button class="submit">
                                            <i class="clip-search-3"></i>
                                        </button>
                                    </div>
                                </form>
                            </li>
                        </ol> -->
                        <div class="page-header">
                            
                        </div>
                        <!-- end: PAGE TITLE & BREADCRUMB -->
                    </div>
                </div>
                <!-- end: PAGE HEADER -->
                <!-- start: PAGE CONTENT -->
                <div class="row">
                    <!-- BATAS -->
                    <div class="col-md-12">

                                <?php echo $this->session->flashdata('info_trace'); ?>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <i class="fa fa-external-link-square"></i><b style="text-transform: uppercase;">FORM PELACAKAN SURAT MASUK</b>
                                <div class="panel-tools">
                                    <a class="btn btn-xs btn-link panel-collapse collapses" href="#"> </a>
                                    <a data-original-title="RESIZE FULL" data-content="Klik icon tersebut untuk tampilan full table" data-placement="top" data-trigger="hover" id="test" class="btn btn-xs btn-link panel-expand popovers" href="#"> <i class="fa fa-laptop"></i> </a>
                                    <a class="btn btn-xs btn-link panel-close" href="#"> <i class="fa fa-times"></i> </a>
                                </div>
                            </div>
                            <div class="panel-body">
                                <div class='alert alert-block alert-warning'>
                                    <button type='button' class='close' data-dismiss='alert'>
                                        <i class='icon-remove'></i>
                                    </button>

                                    <p>
                                        <strong>
                                            <i class='icon-ok'></i>
                                            Warning! - 
                                        </strong>
                                        Masukan kata kunci (Keyword) tertentu untuk mencari surat masuk pada sistem
                                    </p>
                                </div>
                                <div class="row">
                                    <form method="post" action="<?php echo base_url();?>index.php/web/aksi_traceSuratMasuk">
                                        <div class="col-md-4">
                                            <label>Nomor Surat:</label>
                                            <p>
                                                <input
                                                    type="text"
                                                    name="nomor_surat"
                                                    class="form-control"
                                                    value="<?php echo $nomor_surat ?>"
                                                    placeholder="Ex : 1002/UN45/IX/2021 (Optional)"
                                                    >
                                            </p>
                                        </div> 
                                        <div class="col-md-4">
                                            <label>Pengirim (Keyword):</label>
                                            <p>
                                                <input
                                                    type="text"
                                                    name="pengirim"
                                                    class="form-control"
                                                    value="<?php echo $pengirim ?>"
                                                    placeholder="Ex : Teknik (Optional)"
                                                    >
                                            </p>
                                        </div>  
                                        <div class="col-md-4">
                                            <label>Perihal (Keyword):</label>
                                            <p>
                                                <input
                                                    type="text"
                                                    name="perihal"
                                                    class="form-control"
                                                    value="<?php echo $perihal ?>"
                                                    required>
                                            </p>
                                        </div> 
                                        
                                        <div class="col-md-12"> 
                                            <button type="submit" class="btn btn-purple btn-block" data-placement="top" data-original-title="Hapus" onclick="return confirm('Anda yakin akan kata kunci pencarian sudah benar?')"><i class="fa fa-search"></i> TRACE SURAT</button> 
                                            <br/>       
                                        </div>      
                                    </form>
                                </div>
                                <div class="table-responsive">
                                    <table class="table table-striped table-hover" id="sample-table-2">
                                        <thead>
                                            <tr>
                                                <th width="1" class="col-to-export center">#</th>
                                                <th width="" class="col-to-export">NO SURAT</th>
                                                <th width="" class="col-to-export">INSTANSI PENGIRIM</th>
                                                <th width="" class="col-to-export">PERIHAL</th>
                                                <th width="1" class="col-to-export center">STATUS</th>
                                               <!-- <th width="" class="col-to-export center">POSISI TERKINI</th> -->
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php 
                                                foreach ($traceSuratMasuk->result_array() as $d)
                                                {
                                            ?>
                                            <tr>
                                                <td align="center">
                                                    <b>
                                                        <?php echo $d['dist_no'] ?>
                                                    </b>
                                                </td>
                                                <td><b><?php echo $d['dist_nomorsurat'] ?></b></td>
                                                <td style="text-transform: uppercase;">
                                                    <b>
                                                        <?php echo $d['dist_pengirim'] ?>
                                                    </b>
                                                </td>
                                                <td style="text-transform: uppercase;"><b><?php echo $d['dist_perihal'] ?></b></td>
                                                <td class="center" style="text-transform: uppercase;">
                                                    <?php 
                                                        if($d['dist_status']=="Done")
                                                        {
                                                            echo 
                                                            '<span class="label label-green">S E L E S A I</span>';
                                                        }
                                                        else if($d['dist_status']=="Process")
                                                        {
                                                            echo 
                                                            '<span class="label label-purple">ON PROCESS</span>';
                                                        }
                                                    ?>
                                                </td>
                                               <!-- <td align="center">
                                                    <b>
                        <?php 
                        $data_rektor   = $this->web_app_model->getWhereOneItem($d['dist_disposisi'],'rektor_username','tbl_rektor');
                        $data_biro     = $this->web_app_model->getWhereOneItem($d['dist_disposisi'],'biro_username','tbl_biro');    
                        $data_kabag    = $this->web_app_model->getWhereOneItem($d['dist_disposisi'],'kabag_username','tbl_kabag');
                        $data_kasubbag = $this->web_app_model->getWhereOneItem($d['dist_disposisi'],'kasubbag_username','tbl_kasubbag');
                        $data_staff    = $this->web_app_model->getWhereOneItem($d['dist_disposisi'],'staff_username','tbl_staff'); 
                        $data_user_adm = $this->web_app_model->getWhereOneItem($d['dist_disposisi'],'username','tbl_users'); 

                        if(!empty($data_rektor))
                        {
                            echo $data_rektor['rektor_nama'].' - ('.$data_rektor['rektor_posisi'].')';
                        }
                        else if(!empty($data_biro))
                        {
                            echo $data_biro['biro_nama'].' - ('.$data_biro['biro_posisi'].')';
                        }
                        else if(!empty($data_kabag))
                        {
                            echo $data_kabag['kabag_nama'].' - ('.$data_kabag['kabag_posisi'].')';
                        }
                        else if(!empty($data_kasubbag))
                        {
                            echo $data_kasubbag['kasubbag_nama'].' - ('.$data_kasubbag['kasubbag_posisi'].')';
                        }
                        else if(!empty($data_staff))
                        {
                            echo $data_staff['staff_nama'].' - ('.$data_staff['staff_posisi'].')';
                        }
                        else if(!empty($data_user_adm))
                        {
                            echo $data_user_adm['nama_lengkap'].' - ('.$data_user_adm['jabatan'].')';
                        }
                        ?>
                                                    </b>
                                                </td> -->
                                                
                                            </tr>
                                        <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <?php //echo $modalTambahSurat ?>
                    </div>
                    <!-- BATAS -->
                </div>
                <!-- end: PAGE CONTENT-->
            </div>
        </div>
        <!-- end: PAGE -->
    </div>
    <!-- end: MAIN CONTAINER -->
    <!-- start: FOOTER -->
    <div class="footer clearfix">
        <div class="footer-inner">
            <script>
                document.write(new Date().getFullYear())
            </script> &copy; <?php $this->load->view('versi'); ?>
        </div>
        <div class="footer-items">
            <span class="go-top"><i class="clip-chevron-up"></i></span>
        </div>
    </div>
    <!-- end: FOOTER -->
    <!-- start: RIGHT SIDEBAR -->
    
    <!-- end: RIGHT SIDEBAR -->
    <!-- start: MAIN JAVASCRIPTS -->
    <!--[if lt IE 9]>
            <script src="<?php echo base_url(); ?>vendor/bower_components/respond/dest/respond.min.js"></script>
            <script src="<?php echo base_url(); ?>vendor/bower_components/Flot/excanvas.min.js"></script>
            <script src="<?php echo base_url(); ?>vendor/bower_components/jquery-1.x/dist/jquery.min.js"></script>
            <![endif]-->
    <!--[if gte IE 9]><!-->
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/jquery/dist/jquery.min.js"></script>
    <!--<![endif]-->
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/jquery-ui/jquery-ui.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/blockUI/jquery.blockUI.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/iCheck/icheck.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/perfect-scrollbar/js/min/perfect-scrollbar.jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/jquery.cookie/jquery.cookie.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/sweetalert/dist/sweetalert.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/assets/js/min/main.min.js"></script>
    <!-- end: MAIN JAVASCRIPTS -->
    <!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
    <script src="<?php echo base_url();?>vendor/bower_components/bootstrap-modal/js/bootstrap-modal.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/bootstrap-modal/js/bootstrap-modalmanager.js"></script>
    <script src="<?php echo base_url();?>vendor/assets/js/min/ui-modals.min.js"></script>

    <script src="<?php echo base_url();?>vendor/bower_components/select2/dist/js/select2.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/jszip/dist/jszip.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/pdfmake/build/pdfmake.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/pdfmake/build/vfs_fonts.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?php echo base_url();?>vendor/assets/js/min/table-export.min.js"></script>
    <!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
    <?php echo $this->session->flashdata('info2'); ?>
    <script>
        jQuery(document).ready(function() {
            Main.init();
            TableExport.init();
        });
    </script>

</body>

</html>