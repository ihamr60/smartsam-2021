<!DOCTYPE html>
<!--[if IE 8]><html class="ie8 no-js" lang="en"><![endif]-->
<!--[if IE 9]><html class="ie9 no-js" lang="en"><![endif]-->
<!--[if !IE]><!-->
<html class="no-js">
<!--<![endif]-->

<head>
    <title>SmartSAM Home</title>
    <link rel="shortcut icon" href="<?php echo base_url() ?>vendor/assets/images/web/unsam.ico" />
    <!-- start: META -->
    <meta charset="utf-8" />
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content="IE=edge,IE=9,IE=8,chrome=1" /><![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta content="Responsive Admin Template build with Twitter Bootstrap and jQuery" name="description" />
    <meta content="ClipTheme" name="author" />
    <!-- end: META -->
    <!-- start: MAIN CSS -->
    <link type="text/css" rel="stylesheet" href="//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700|Raleway:400,100,200,300,500,600,700,800,900/" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/bower_components/bootstrap/dist/css/bootstrap.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/bower_components/font-awesome/css/font-awesome.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/assets/fonts/clip-font.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/bower_components/iCheck/skins/all.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/bower_components/perfect-scrollbar/css/perfect-scrollbar.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/bower_components/sweetalert/dist/sweetalert.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/assets/css/main.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/assets/css/main-responsive.min.css" />
    <link type="text/css" rel="stylesheet" media="print" href="<?php echo base_url(); ?>vendor/assets/css/print.min.css" />
    <link type="text/css" rel="stylesheet" id="skin_color" href="<?php echo base_url(); ?>vendor/assets/css/theme/light.min.css" />

    <!-- end: MAIN CSS -->
    <!-- start: CSS REQUIRED FOR THIS PAGE ONLY -->
    <link href="<?php echo base_url();?>vendor/bower_components/select2/dist/css/select2.min.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>vendor/bower_components/datatables/media/css/dataTables.bootstrap.min.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>vendor/bower_components/datatables/media/css/jquery.dataTables.min.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>vendor/bower_components/datatables.net-buttons-dt/css/buttons.dataTables.min.css" rel="stylesheet" />
    
    <link href="<?php echo base_url();?>vendor/bower_components/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>vendor/bower_components/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" />

    <!--  BUTTON -->
    <link href="<?php echo base_url();?>vendor/bower_components/ladda-bootstrap/dist/ladda-themeless.min.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>vendor/bower_components/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.min.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>vendor/bower_components/bootstrap-social/bootstrap-social.css" rel="stylesheet" />
    <!-- end: CSS REQUIRED FOR THIS PAGE ONLY -->

    <link
    rel="stylesheet"
    href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"
  />
</head>

<body>

    <!-- start: HEADER -->
    <div class="navbar navbar-inverse navbar-fixed-top">
        <!-- start: TOP NAVIGATION CONTAINER -->
        	<?php echo $atas; ?>
        <!-- end: TOP NAVIGATION CONTAINER -->
    </div>
    <!-- end: HEADER -->
        <br>
    <!-- start: MAIN CONTAINER -->
    <div class="main-container">
        <div class="navbar-content">
            <!-- start: SIDEBAR -->
            <div class="main-navigation navbar-collapse collapse">
                <!-- start: MAIN MENU TOGGLER BUTTON -->
                <div class="navigation-toggler">
                    <i class="clip-chevron-left"></i>
                    <i class="clip-chevron-right"></i>
                </div>
                <!-- end: MAIN MENU TOGGLER BUTTON -->
                <!-- start: MAIN NAVIGATION MENU -->
                <?php echo $menu; ?>
                <!-- end: MAIN NAVIGATION MENU -->
            </div>
            <!-- end: SIDEBAR -->
        </div>

        <!-- start: PAGE -->
        <div class="main-content">
           
            <div class="container">
                <!-- start: PAGE HEADER -->
                <div class="row">
                    <div class="col-sm-12">
                        
                        <!-- start: PAGE TITLE & BREADCRUMB -->
                        <ol class="breadcrumb">
                            <li class="active">
                                <i class="clip-home-3"></i>
                                <a href="">
                                    Home
                                </a>
                            </li>
                            <li class="active">
                                    Surat Masuk 
                            </li>
                           
                            <li class="search-box">
                                <form class="sidebar-search">
                                    <div class="form-group">
                                        <input type="text" placeholder="Start Searching...">
                                        <button class="submit">
                                            <i class="clip-search-3"></i>
                                        </button>
                                    </div>
                                </form>
                            </li>
                        </ol>
                        <div class="page-header">
                            <?php echo $bio; ?>
                        </div>
                        <!-- end: PAGE TITLE & BREADCRUMB -->
                    </div>
                </div>
                <!-- end: PAGE HEADER -->
                <!-- start: PAGE CONTENT -->
                <!--<marquee><span class='label label-info'><font color="black">SELAMAT DATANG DI SISTEM INFORMASI E-PDAM TIRTA KEUMUENENG KOTA LANGSA</font></span></marquee> -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="tabbable">
                            <ul class="nav nav-tabs tab-padding tab-space-3 tab-blue" id="myTab4">
                                <li class="active">
                                    <a data-toggle="tab" href="#panel_overview">
                                        Detail Section
                                    </a>
                                </li>
                                <li>
                            </ul>
                            <div class="tab-content">
                                <div id="panel_overview" class="tab-pane in active">
                                    <div class="row">
                                        <div class="col-sm-5 col-md-4">
                                            <div class="user-left">
                                                <div class="center">
                                                    <h4>Document Attachment</h4>
                                                    <div class="fileupload fileupload-new" data-provides="fileupload">
                                                        <div class="user-image">
                                                            <div id="kv-avatar-errors" class="center-block" style="display:none"></div>
                                                            <div class="kv-avatar ">
                                                                <a target="_blank" href="<?php echo base_url() ?>upload/suratMasuk/<?php echo $data_suratmasuk['dist_dokumen'] ?>">
                                                                    <img src="<?php echo base_url()?>vendor/assets/images/web/file.png" width="100px">
                                                                </a>
                                                            </div><br>
                                                            <a target="_blank" href="<?php echo base_url() ?>upload/suratMasuk/<?php echo $data_suratmasuk['dist_dokumen'] ?>" class="btn btn-danger btn-block" type="submit">
                                                                Lihat Dokumen <i class="fa fa-arrow-circle-right"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    
                                                    <hr>
                                                </div>
                                                <table class="table table-condensed table-hover">
                                                    <thead>
                                                        <tr>
                                                            <th colspan="3">General Information</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td>Nomor Surat</td>
                                                            <td>
                                                                <a href="#">
                                                                    <b><?php echo $data_suratmasuk['dist_nomorsurat'] ?></b>
                                                                </a>
                                                            </td>
                                                            <td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Tanggal Surat</td>
                                                            <td>
                                                                <a href="#">
                                                                    <?php echo $data_suratmasuk['dist_tgl_surat'] ?>
                                                                </a>
                                                            </td>
                                                            <td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Tanggal Terima</td>
                                                            <td>
                                                                <a href="#">
                                                                    <?php echo $data_suratmasuk['dist_tgl_terima'] ?>
                                                                </a>
                                                            </td>
                                                            <td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Asal Surat</td>
                                                            <td>
                                                                <a href="">
                                                                    <b><?php echo $data_suratmasuk['dist_pengirim'] ?></b>
                                                                </a>
                                                            </td>
                                                            <td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Perihal Surat</td>
                                                            <td><?php echo $data_suratmasuk['dist_perihal'] ?></td>
                                                            <td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Lampiran</td>
                                                            <td>
                                                                <a href="">
                                                                    <?php echo $data_suratmasuk['dist_lampiran'] ?>
                                                                </a>
                                                            </td>
                                                            <td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                <!--<table class="table table-condensed table-hover">
                                                    <thead>
                                                        <tr>
                                                            <th colspan="3">Dispositioner Information</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td>Username</td>
                                                            <td><?php echo $data_suratmasuk['riw_asal'] ?></td>
                                                            <td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Full Name</td>
                                                            <td><?php echo $data_suratmasuk['staff_nama'] ?></td>
                                                            <td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Position</td>
                                                            <td><?php echo $data_suratmasuk['staff_posisi'] ?></td>
                                                            <td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
                                                        </tr>
                                                    </tbody>
                                                </table> -->
                                                <?php 
                                                if($data_suratmasuk['dist_status'] == "Done" )
                                                {
                                                    echo '<button disabled type="submit" class="btn btn-danger">
                                                                CLOSED / SELESAI
                                                            </button>';
                                                }
                                                else
                                                {
                                                ?>
                                                    <a href="<?php echo base_url();?>index.php/pelaksana/proses_surat/<?php echo $data_suratmasuk['dist_uuid'] ?>/<?php echo $username ?>" class="btn btn-blue btn-block" data-placement="top" data-original-title="Hapus" onclick="return confirm('Anda yakin akan menindak lanjuti surat ini?')"><i class="fa fa-check"></i> Tindak Lanjuti / Tandai sudah diproses</a>
                                            <?php } ?>
                                            </div>
                                        </div>
                                        <div class="col-sm-7 col-md-8">
                                            <div class="panel panel-white">
                                                <!-- BATAS -->
                                                <div class="col-md-12">
                                                    <?php echo $this->session->flashdata('info'); ?>
                                                    <div class="panel panel-default">
                                                        <div class="panel-heading">
                                                            <i class="fa fa-external-link-square"></i><b style="text-transform: uppercase;">RIWAYAT MEMO SURAT MASUK</b>
                                                            <div class="panel-tools">
                                                                <a class="btn btn-xs btn-link panel-collapse collapses" href="#"> </a>
                                                                <a data-original-title="RESIZE FULL" data-content="Klik icon tersebut untuk tampilan full table" data-placement="top" data-trigger="hover" id="test" class="btn btn-xs btn-link panel-expand popovers" href="#"> <i class="fa fa-laptop"></i> </a>
                                                                <a class="btn btn-xs btn-link panel-close" href="#"> <i class="fa fa-times"></i> </a>
                                                            </div>
                                                        </div>
                                                        <div class="panel-body">
                                                            <div class="row">
                                                                <!--<div class="col-md-12 space20">
                                                                    <a data-toggle="modal" href="#modalTambahSurat" class="btn btn-teal">
                                                                        <i class="fa fa-plus"></i> SURAT MASUK 
                                                                    </a>
                                                                </div> -->
                                                            </div>
                                                            <div class="table-responsive">
                                                                <table class="table table-striped table-hover" id="sample-table-2">
                                                                    <thead>
                                                                        <tr>
                                                                            <th width="100" class="col-to-export center">UPDATED AT</th>
                                                                            <th></th>
                                                                            <th width="" class="col-to-export">ISI MEMO</th>
                                                                            <th width="" class="col-to-export center">TIMES</th>
                                                                            <th width="" class="col-to-export">AUTHOR</th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                        <?php 
                                                                            foreach($riwayat_memo->result_array() as $d)
                                                                            {
                                                                        ?>
                                                                        <tr>
                                                                            <td align="center">
                                                                                <b><?php echo $d['riw_update_at'] ?></b>
                                                                            </td>
                                                                            <td><img src="<?php echo base_url() ?>vendor/assets/images/web/memo.png" width="30px"></td>
                                                                            <td>
                                                                                <b><?php echo $d['riw_memo'] ?></b>
                                                                            </td>
                                                                            <td class="center">
                                                                                <?php 
                                                                                    $waktu_awal   = strtotime($d['riw_update_at']);
                                                                                    $waktu_akhir  = strtotime(date('Y-m-d H:i:s')); 

                                                                                    $waktu_1      = new DateTime($d['riw_update_at']);
                                                                                    $waktu_2      = new DateTime(date('Y-m-d H:i:s'));

                                                                                    $diff    =$waktu_akhir - $waktu_awal;
                                                                                    $jam    =floor($diff / (60 * 60));
                                                                                    $menit    =$diff - $jam * (60 * 60);
                                                                                    $hari       = $waktu_2->diff($waktu_1)->days + 1;
                                                                                    
                                                                                    if($jam == 0)
                                                                                    {
                                                                                        echo '
                                                                                        <span class="label label-warning">
                                                                                            <font color="black">' . floor( $menit / 60 ) . ' min ago</font>
                                                                                        </span>';
                                                                                    }
                                                                                    else if($jam > 24)
                                                                                    {
                                                                                        echo '
                                                                                        <span class="label label-purple">
                                                                                            <font color="white">'.$hari.' day ago</font>
                                                                                        </span>
                                                                                        ';
                                                                                    }
                                                                                    else
                                                                                    {
                                                                                        echo '
                                                                                        <span class="label label-orange">
                                                                                            <font color="white">' . $jam .  ' hour ago</font>
                                                                                        </span>
                                                                                        ';
                                                                                    }
                                                                                ?>
                                                                            </td>
                                                                            <td align="">
                                                                                <span class="label label-info">
                                                                                    <font color="black"><?php echo $d['riw_asal'] ?></font>
                                                                                </span>    
                                                                            </td>
                                                                            <!--<td align="center">
                                                                                <div class="btn-group">
                                                                                    <a class="btn btn-primary dropdown-toggle btn-xs" data-toggle="dropdown" href="#">
                                                                                        <i class="fa fa-cog"></i> <span class="caret"></span>
                                                                                    </a>
                                                                                    <ul role="menu" class="dropdown-menu pull-right">
                                                                                        <li role="presentation">
                                                                                            <a role="menuitem" tabindex="-1" href="<?php echo base_url() ?>index.php/rektor/bg_detail_surat/<?php //echo $d['dist_uuid'] ?>/<?php //echo $rektor_uuid ?>?tab1=1">
                                                                                                <i class="clip-file-2"></i> Detail Surat
                                                                                            </a>
                                                                                        </li>
                                                                                        <li role="presentation">
                                                                                            <a role="menuitem" tabindex="-1" href="<?php echo base_url() ?>index.php/admin/bg_editPegawai/<?php //echo $d['nik_ktp'] ?>">
                                                                                                <i class="fa fa-pencil"></i> Edit Data Umum
                                                                                            </a>
                                                                                        </li>
                                                                                        <li role="presentation">
                                                                                            <a role="menuitem" tabindex="-1" onclick="return confirm('Anda yakin akan menghapus data <?php //echo $d['nama_pegawai'] ?>.??')" href="<?php echo base_url() ?>index.php/crud_pegawai/hapusPegawai/<?php //echo $d['nik_ktp'] ?>/<?php echo $this->uri->segment(3);?>">
                                                                                                <i class="fa fa-times"></i> Hapus
                                                                                            </a>
                                                                                        </li>
                                                                                    </ul>
                                                                                </div>    
                                                                            </td>
                                                                            -->
                                                                        </tr>
                                                                    <?php } ?>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <?php //echo $modalTambahSurat ?>
                                                </div>
                                                <!-- BATAS -->
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                               
                                
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end: PAGE CONTENT-->
            </div>
        </div>
        <!-- end: PAGE -->
    </div>
    <!-- end: MAIN CONTAINER -->
    <!-- start: FOOTER -->
    <div class="footer clearfix">
        <div class="footer-inner">
            <script>
                document.write(new Date().getFullYear())
            </script> &copy; <?php $this->load->view('versi'); ?>
        </div>
        <div class="footer-items">
            <span class="go-top"><i class="clip-chevron-up"></i></span>
        </div>
    </div>
    <!-- end: FOOTER -->
    <!-- start: RIGHT SIDEBAR -->
    	<!-- ISI RIGHT SIDE BAR -->
    <!-- end: RIGHT SIDEBAR -->
 
    <!-- start: MAIN JAVASCRIPTS -->
    <!--[if lt IE 9]>
        <script src="<?php echo base_url(); ?>vendor/bower_components/respond/dest/respond.min.js"></script>
        <script src="<?php echo base_url(); ?>vendor/bower_components/Flot/excanvas.min.js"></script>
        <script src="<?php echo base_url(); ?>vendor/bower_components/jquery-1.x/dist/jquery.min.js"></script>
        <![endif]-->
    <!--[if gte IE 9]><!-->
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/jquery/dist/jquery.min.js"></script>
    <!--<![endif]-->

    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/jquery-ui/jquery-ui.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/blockUI/jquery.blockUI.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/iCheck/icheck.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/perfect-scrollbar/js/min/perfect-scrollbar.jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/jquery.cookie/jquery.cookie.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/sweetalert/dist/sweetalert.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/assets/js/min/main.min.js"></script>
    <!-- end: MAIN JAVASCRIPTS -->
    <!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
    
    <script src="<?php echo base_url();?>vendor/bower_components/bootstrap-modal/js/bootstrap-modal.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/bootstrap-modal/js/bootstrap-modalmanager.js"></script>
    <script src="<?php echo base_url();?>vendor/assets/js/min/ui-modals.min.js"></script>

    <script src="<?php echo base_url();?>vendor/bower_components/select2/dist/js/select2.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/jszip/dist/jszip.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/pdfmake/build/pdfmake.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/pdfmake/build/vfs_fonts.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?php echo base_url();?>vendor/assets/js/min/table-export.min.js"></script>
    <!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->

    <?php echo $this->session->flashdata('info2'); ?>
    
     <script>
        jQuery(document).ready(function() {
            Main.init();
            TableExport.init();
            UIModals.init();
        });
    </script>

    <script type="text/javascript">
      $('select').select2();
    </script>
    
</body>

</html>