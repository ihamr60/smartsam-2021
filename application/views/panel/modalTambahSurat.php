
<div id="modalTambahSurat" class="modal fade" data-width="760" style="display: none;">
    <form role="form" action="<?php echo base_url() ?>index.php/panel/tambahSurat" method="post" enctype="multipart/form-data" >
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                &times;
            </button>
            <h4 class="modal-title">DIGITALISASI / SCAN SURAT MASUK</h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-6">
                    <label>Pengirim:</label>
                    <p>
                        <input
                            type="text"
                            name="pengirim"
                            class="form-control"
                            placeholder="Ex: Personal, BNN Langsa, CV Multi Datanesia, etc."
                            required>
                    </p>
                </div>
                <div class="col-md-6">
                    <label>Nomor Surat:</label>
                    <p>
                        <input
                            type="text"
                            name="nomorSurat"
                            class="form-control"
                            placeholder="Ex: 034/UN45/XI/2020"
                            required>
                    </p>
                </div>
                <div class="col-md-6">
                    <label>Lampiran:</label>
                    <p>
                        <input
                            type="number"
                            name="lampiran"
                            class="form-control"
                            placeholder="Ex: 3 (Just Number)"
                            required>
                    </p>
                </div>
                <div class="col-md-6">
                    <label>Perihal:</label>
                    <p>
                        <input
                            type="text"
                            name="perihal"
                            class="form-control"
                            placeholder="Ex: Permintaan Data Penelitian"
                            required>
                    </p>
                </div>
                <div class="col-md-6">
                    <label>Disposisi Ke:</label>
                    <p>
                        <select
                            id="select2insidemodal"
                            class="form-control"
                            name="disposisi"
                            required>
                            <option value="">Pilih Disposisi</option>

                            <?php 
                                foreach($data_biro->result_array() as $d)
                                {
                                    echo "<option value='".$d['biro_username']."'>".$d['biro_nama']." (".$d['biro_posisi'].")</option>";
                                }
                            ?>
                            <?php
                            if($posisi == "Admin Surat Subbag. TU" || $posisi == "Admin Surat SP" || $posisi == "Admin Surat Fak. Hukum")
                            { 
                                foreach($data_kabag->result_array() as $d){
                                    if($posisi == "Admin Surat Subbag. TU" && $d['kabag_posisi'] == "Kepala Bagian Umum")
                                    {
                                        echo "<option value='".$d['kabag_username']."'>".$d['kabag_nama']." (".$d['kabag_posisi'].")</option>";

                                        foreach($data_admin_surat->result_array() as $e)
                                        {
                                            echo "<option value='".$e['username']."'>".$e['nama_lengkap']." (".$e['jabatan'].")</option>";
                                        }
                                    }
                                    else if($posisi == "Admin Surat SP" && $d['kabag_posisi'] == "Ka SP")
                                    {
                                        echo "<option value='".$d['kabag_username']."'>".$d['kabag_nama']." (".$d['kabag_posisi'].")</option>";

                                        foreach($data_admin_surat->result_array() as $e)
                                        {
                                            echo "<option value='".$e['username']."'>".$e['nama_lengkap']." (".$e['jabatan'].")</option>";
                                        }
                                    }
                                }
                            }
                            else
                            { 
                                foreach($data_ka_upt->result_array() as $d){

                                    if($d['kasubbag_posisi']=="Ka UPT TIK" && $posisi == "Admin Surat UPT TIK")
                                    {
                                        echo '<option value="'.$d['kasubbag_username'].'">'.$d['kasubbag_nama'].' ('.$d['kasubbag_posisi'].')</option>"'; 

                                        foreach($data_admin_surat->result_array() as $e)
                                        {
                                            echo "<option value='".$e['username']."'>".$e['nama_lengkap']." (".$e['jabatan'].")</option>";
                                        }   
                                    }

                                    if($d['kasubbag_posisi']=="Ka Lab Dasar"  && $posisi == "Admin Surat Lab. Dasar")
                                    {
                                        echo '<option value="'.$d['kasubbag_username'].'">'.$d['kasubbag_nama'].' ('.$d['kasubbag_posisi'].')</option>"';  

                                        foreach($data_admin_surat->result_array() as $e)
                                        {
                                            echo "<option value='".$e['username']."'>".$e['nama_lengkap']." (".$e['jabatan'].")</option>";
                                        }  
                                    }

                                    if($d['kasubbag_posisi']=="Ka Lab Bahasa"  && $posisi == "Admin Surat Lab. Bahasa")
                                    {
                                        echo '<option value="'.$d['kasubbag_username'].'">'.$d['kasubbag_nama'].' ('.$d['kasubbag_posisi'].')</option>"';  

                                        foreach($data_admin_surat->result_array() as $e)
                                        {
                                            echo "<option value='".$e['username']."'>".$e['nama_lengkap']." (".$e['jabatan'].")</option>";
                                        }  
                                    }

                                    if($d['kasubbag_posisi']=="Ka UPT Perpustakaan"  && $posisi == "Admin Surat Perpustakaan")
                                    {
                                        echo '<option value="'.$d['kasubbag_username'].'">'.$d['kasubbag_nama'].' ('.$d['kasubbag_posisi'].')</option>"';  

                                        foreach($data_admin_surat->result_array() as $e)
                                        {
                                            echo "<option value='".$e['username']."'>".$e['nama_lengkap']." (".$e['jabatan'].")</option>";
                                        }  
                                    }



                                    // BATAS 
                                    
                                    if($d['kasubbag_posisi']=="Kasubbag Umum & Keu Fak. Ekonomi"  && $posisi == "Admin Surat Fak. Ekonomi")
                                    {
                                        echo "<option value='".$d['kasubbag_username']."'>".$d['kasubbag_nama']." (".$d['kasubbag_posisi'].")</option>";

                                        foreach($data_admin_surat->result_array() as $e)
                                        {
                                            echo "<option value='".$e['username']."'>".$e['nama_lengkap']." (".$e['jabatan'].")</option>";
                                        }
                                        foreach($data_kabag->result_array() as $f)
                                        {
                                            echo "<option value='".$f['kabag_username']."'>".$f['kabag_nama']." (".$f['kabag_posisi'].")</option>";
                                        }
                                    }
                                    if($d['kasubbag_posisi']=="Kasubbag Umum & Keu Fak. Pertanian"  && $posisi == "Admin Surat Fak. Pertanian")
                                    {
                                        echo "<option value='".$d['kasubbag_username']."'>".$d['kasubbag_nama']." (".$d['kasubbag_posisi'].")</option>";

                                        foreach($data_admin_surat->result_array() as $e)
                                        {
                                            echo "<option value='".$e['username']."'>".$e['nama_lengkap']." (".$e['jabatan'].")</option>";
                                        }
                                        foreach($data_kabag->result_array() as $f)
                                        {
                                            echo "<option value='".$f['kabag_username']."'>".$f['kabag_nama']." (".$f['kabag_posisi'].")</option>";
                                        }
                                    }
                                    if($d['kasubbag_posisi']=="Kasubbag Umum & Keu Fak. Keguruan"  && $posisi == "Admin Surat Fak. Keguruan")
                                    {
                                        echo "<option value='".$d['kasubbag_username']."'>".$d['kasubbag_nama']." (".$d['kasubbag_posisi'].")</option>";

                                        foreach($data_admin_surat->result_array() as $e)
                                        {
                                            echo "<option value='".$e['username']."'>".$e['nama_lengkap']." (".$e['jabatan'].")</option>";
                                        }
                                        foreach($data_kabag->result_array() as $f)
                                        {
                                            echo "<option value='".$f['kabag_username']."'>".$f['kabag_nama']." (".$f['kabag_posisi'].")</option>";
                                        }
                                    }
                                    if($d['kasubbag_posisi']=="Kasubbag Umum & Keu Fak. Teknik"  && $posisi == "Admin Surat Fak. Teknik")
                                    {
                                        echo "<option value='".$d['kasubbag_username']."'>".$d['kasubbag_nama']." (".$d['kasubbag_posisi'].")</option>";

                                        foreach($data_admin_surat->result_array() as $e)
                                        {
                                            echo "<option value='".$e['username']."'>".$e['nama_lengkap']." (".$e['jabatan'].")</option>";
                                        }
                                        foreach($data_kabag->result_array() as $f)
                                        {
                                            echo "<option value='".$f['kabag_username']."'>".$f['kabag_nama']." (".$f['kabag_posisi'].")</option>";
                                        }
                                    }
                                    if($d['kasubbag_posisi']=="Kasubbag Umum & Keu Fak. HUKUM"  && $posisi == "Admin Surat Fak. HUKUM")
                                    {
                                        echo "<option value='".$d['kasubbag_username']."'>".$d['kasubbag_nama']." (".$d['kasubbag_posisi'].")</option>";

                                        foreach($data_admin_surat->result_array() as $e)
                                        {
                                            echo "<option value='".$e['username']."'>".$e['nama_lengkap']." (".$e['jabatan'].")</option>";
                                        }
                                        foreach($data_kabag->result_array() as $f)
                                        {
                                            echo "<option value='".$f['kabag_username']."'>".$f['kabag_nama']." (".$f['kabag_posisi'].")</option>";
                                        }
                                    }

                                    // BATAS LPPPM
                                    if($posisi == "Admin Surat LPPPM & PM" && $d['kasubbag_posisi'] == "Kasubbag TU LPPPM & PM")
                                    {
                                        echo "<option value='".$d['kasubbag_username']."'>".$d['kasubbag_nama']." (".$d['kasubbag_posisi'].")</option>";
                                        foreach($data_admin_surat->result_array() as $e)
                                        {
                                            echo "<option value='".$e['username']."'>".$e['nama_lengkap']." (".$e['jabatan'].")</option>";
                                        }
                                    }
                                    
                                }
                            }
                            ?>
                        </select>
                    </p>
                </div>
                <div class="col-md-3">
                    <label>Upload Document:</label>
                    <p>
                        <input
                            type="file"
                            name="dokumen"
                            class="form-control"
                            required>
                    </p>
                </div>
                <div class="col-md-3">
                    <label>Tanggal Surat:</label>
                    <p>
                        <input
                            type="date"
                            name="tgl_surat"
                            class="form-control"
                            placeholder="yyyy-mm-dd"
                            required>
                    </p>
                </div>
                <div class="col-md-12">
                    <label>Memo:</label>
                    <p>
                        <textarea
                            type="memo"
                            name="memo"
                            class="form-control"
                            required></textarea>
                    </p>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-light-grey">
                Cancel
            </button>
            <button type="submit" class="btn btn-blue">
                Tambahkan
            </button>
        </div>
    </form>
</div>
