
<div id="modalForwardRektor" class="modal fade" tabindex="-1" data-width="760" style="display: none;">
    <form role="form" action="<?php echo base_url() ?>index.php/kasubbag/teruskan_konsep_ke_rektor" method="post" enctype="multipart/form-data" >
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                &times;
            </button>
            <h4 class="modal-title">MASUKAN PESAN / MEMO / KOREKSI UNTUK MELAKUKAN FORWARD: </h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <label>Pesan / Memo:</label>
                    <p>
                        <textarea name="konsep" cols="30" rows="3" class="form-control"></textarea>
                        <input type="hidden" name="draft_uuid" value="<?php echo $data_draft['draft_uuid'] ?>">
                        <input type="hidden" name="draft_asal" value="<?php echo $data_draft['draft_asal'] ?>">
                    </p>
                </div>

                <div class="col-md-6">
                    <label>Teruskan Ke:</label>
                    <p>
                        <select
                            class="form-control"
                            name="teruskan"
                            required>
                            <option>Pilih Terusan</option>
                            <?php
                                foreach($data_rektor->result_array() as $d){
                                    echo "<option value='".$d['rektor_uuid']."'>".$d['rektor_nama']." (".$d['rektor_posisi'].")</option>";
                                }
                                foreach($data_biro->result_array() as $d){
                                    echo "<option value='".$d['biro_uuid']."'>".$d['biro_nama']." (".$d['biro_posisi'].")</option>";
                                }
                            ?>
                        </select>
                    </p>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-light-grey">
                Cancel
            </button>
            <button type="submit" class="btn btn-blue">
                Submit
            </button>
        </div>
    </form>
</div>
