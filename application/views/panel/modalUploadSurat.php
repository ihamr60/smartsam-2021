<?php 
    foreach($suratKeluar->result_array() as $d)
    {
        $thn_surat = substr($d['out_created_at'],0,-15);
?>
<div id="modalUploadSurat<?php echo $d['out_uuid'] ?>" class="modal fade" tabindex="-1" data-width="560" style="display: none;">
    <form role="form" action="<?php echo base_url();?>index.php/panel/uploadSurat" method="post" enctype="multipart/form-data">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                &times;
            </button>
            <h4 class="modal-title">Upload Surat (<?php echo "".$d['out_urut']."/".$d['out_kode_instansi']."/".$d['out_kode_hal']."/".$thn_surat."" ?>)</h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <label>Tujuan:</label>
                    <p>
                        <input
                            type="text"
                            class="form-control"
                            readonly
                            value="<?php echo $d['out_tujuan'] ?>"
                            required>
                            <input type="hidden" name="out_uuid" value="<?php echo $d['out_uuid'] ?>">
                    </p>
                   
                </div>
                <div class="col-md-12">
                    <label>Perihal:</label>
                    <p>
                        <input
                            type="text"
                            class="form-control"
                            readonly
                            value="<?php echo $d['out_perihal'] ?>"
                            required>
                    </p>
                   
                </div>
                <div class="col-md-12">
                    <label>Upload Surat:</label>
                    <p>
                        <input
                            type="file"
                            name="surat"
                            class="form-control"
                            required>
                    </p>
                   
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-light-grey">
                Cancel
            </button>
            <button type="submit" class="btn btn-blue">
                Tambahkan
            </button>
        </div>
    </form>
</div>

<?php } ?>