<ul class="main-navigation-menu">
    <?php
        $color = "#0c5174";
    ?>
    <li>
        <a 
            <?php if (isset($_GET['home']) && $_GET['home']==1){echo 'style="background: '.$color.'; color: white;"';}?> 
            href="<?php echo base_url();?>index.php/biro/bg_home?home=1">
            <i class="clip-home-3" <?php if (isset($_GET['home']) && $_GET['home']==1){echo 'style="background: '.$color.'; color: white;"';}?>></i>
            <span class="title"><b>DASHBOARD</b></span>
        </a>
    </li>
    <li>
        <?php 
            $need_act   = $this->web_app_model->getCount1Where('tbl_distribusi_surat','dist_disposisi',$username);

        ?>
        <a <?php if (isset($_GET['suratMasuk']) && $_GET['suratMasuk']==1){echo 'style="background: '.$color.'; color: white;"';}?> 
        href="<?php echo base_url();?>index.php/biro/bg_suratMasuk?suratMasuk=1">
            <i class="clip-file-2" <?php if (isset($_GET['suratMasuk']) && $_GET['suratMasuk']==1){echo 'style="background: '.$color.'; color: white;"';}?>></i>
            <span class="title"><b>SURAT MASUK</b>
                <?php
                    if($need_act['TOTAL'] != 0)
                    {
                ?>
                    <span class="animate__animated animate__infinite animate__fast animate__flash label label-warning">
                        <font color="black"><?php echo $need_act['TOTAL'] ?></font>
                    </span>
            <?php   } ?>
            </span>
        </a>
    </li>

    
    <li>
        <?php 
            $need_act3   = $this->web_app_model->getCount3Where('tbl_draft_surat','draft_penerima',$biro_uuid,'draft_status','Process','draft_read','0');
        ?>
        <?php 
            $need_act2   = $this->web_app_model->getCount3Where('tbl_draft_surat','draft_asal',$username,'draft_status','Done','draft_read','0');
        ?>
        <a <?php if (isset($_GET['draft']) && $_GET['draft']==1){echo 'style="background: '.$color.'; color: white;"';}?> 
        href="javascript:void(0)">
            <i class="clip-file-2" <?php if (isset($_GET['draft']) && $_GET['draft']==1){echo 'style="background: '.$color.'; color: white;"';}?>></i>
            <span class="title"><b>DRAFT SURAT</b>

               <?php
                    if($need_act2['TOTAL'] != 0 || $need_act3['TOTAL'] != 0)
                    {
                ?>
                    <span class="animate__animated animate__infinite animate__fast animate__flash label label-warning">
                        <font color="black"><?php echo $need_act2['TOTAL']+$need_act3['TOTAL'] ?></font>
                    </span>
            <?php   } ?>
            </span>
            <i <?php if (isset($_GET['draft']) && $_GET['draft']==1){echo 'style="background: '.$color.'; color: white;"';}?> class="icon-arrow"></i>
        </a>
        <ul class="sub-menu">
            <li>
                <a href="<?php echo base_url();?>index.php/biro/bg_tulis_draft_surat?draft=1">
                    <i class="clip-pencil-2"></i>
                    <span class="title">Ajukan Surat
                        <!--<span class="label label-danger">
                            Soon
                        </span>-->
                    </span>
                </a>
                <a href="<?php echo base_url();?>index.php/biro/bg_draft_pending?draft=1">
                    <i class="clip-file-2"></i>
                    <span class="title">Pending
                        <!--<span class="label label-danger">
                            Soon
                        </span>-->
                    </span>
                </a>
                <a href="<?php echo base_url();?>index.php/biro/bg_draft_acc?draft=1">
                    <i class="clip-file-2"></i>
                    <span class="title">Acc 
                        <?php
                                if($need_act2['TOTAL'] != 0)
                                {
                            ?>
                                <span class="animate__animated animate__infinite animate__fast animate__flash label label-warning">
                                    <font color="black"><?php echo $need_act2['TOTAL'] ?></font>
                                </span>
                        <?php   } ?>
                    </span>
                </a>
                <a href="<?php echo base_url();?>index.php/biro/bg_draft_surat?draft=1">
                    <i class="clip-file-2"></i>
                    <span class="title">Diterima / Masuk
                        <?php
                            if($need_act3['TOTAL'] != 0)
                            {
                        ?>
                            <span class="animate__animated animate__infinite animate__fast animate__flash label label-warning">
                                <font color="black"><?php echo $need_act3['TOTAL'] ?></font>
                            </span>
                    <?php   } ?>
                    </span>
                </a>
            </li>
        </ul>
    </li>







    <li>
        <a <?php if (isset($_GET['suratKeluar']) && $_GET['suratKeluar']==1){echo 'style="background: '.$color.'; color: white;"';}?> 
        href="<?php echo base_url();?>index.php/biro/bg_suratKeluar?suratKeluar=1">
            <i class="clip-file-2" <?php if (isset($_GET['suratKeluar']) && $_GET['suratKeluar']==1){echo 'style="background: '.$color.'; color: white;"';}?>></i>
            <span class="title"><b>ARSIP SURAT KELUAR</b>
            </span>
        </a>
    </li>
    <li>
        <a <?php if (isset($_GET['settings']) && $_GET['settings']==1){echo 'style="background: '.$color.'; color: white;"';}?> 
        href="javascript:void(0)">
            <i class="clip-file-2" <?php if (isset($_GET['settings']) && $_GET['settings']==1){echo 'style="background: '.$color.'; color: white;"';}?>></i>
            <span class="title"><b>SETTINGS</b></span>
            <i <?php if (isset($_GET['settings']) && $_GET['settings']==1){echo 'style="background: #346da4; color: white;"';}?> class="icon-arrow"></i>
        </a>
        <ul class="sub-menu">
            <li>
                <!--active open-->
                <a href="<?php echo base_url();?>index.php/biro/bg_mail_config/?settings=1">
                    <i class="clip-user-2"></i>
                    <span class="title"> My Profile </span>
                </a>
            </li>
            <li>
                <!--active open-->
                <a href="<?php echo base_url();?>index.php/biro/bg_ganti_password?settings=1">
                    <i class="clip-user-2"></i>
                    <span class="title"> Change Password </span>
                </a>
            </li>
        </ul>
    </li>
</ul>