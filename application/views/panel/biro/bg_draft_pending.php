<!DOCTYPE html>
<!--[if IE 8]><html class="ie8 no-js" lang="en"><![endif]-->
<!--[if IE 9]><html class="ie9 no-js" lang="en"><![endif]-->
<!--[if !IE]><!-->
<html class="no-js">
<!--<![endif]-->

<head>
    <title>SmartSAM Home</title>
    <link rel="shortcut icon" href="<?php echo base_url() ?>vendor/assets/images/web/unsam.ico" />
    <!-- start: META -->
    <meta charset="utf-8" />
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content="IE=edge,IE=9,IE=8,chrome=1" /><![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta content="Responsive Admin Template build with Twitter Bootstrap and jQuery" name="description" />
    <meta content="ClipTheme" name="author" />
    <!-- end: META -->
    <!-- start: MAIN CSS -->
    <link type="text/css" rel="stylesheet" href="//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700|Raleway:400,100,200,300,500,600,700,800,900/" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/bower_components/bootstrap/dist/css/bootstrap.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/bower_components/font-awesome/css/font-awesome.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/assets/fonts/clip-font.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/bower_components/iCheck/skins/all.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/bower_components/perfect-scrollbar/css/perfect-scrollbar.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/bower_components/sweetalert/dist/sweetalert.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/assets/css/main.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/assets/css/main-responsive.min.css" />
    <link type="text/css" rel="stylesheet" media="print" href="<?php echo base_url(); ?>vendor/assets/css/print.min.css" />
    <link type="text/css" rel="stylesheet" id="skin_color" href="<?php echo base_url(); ?>vendor/assets/css/theme/light.min.css" />

    <!-- end: MAIN CSS -->
    <!-- start: CSS REQUIRED FOR THIS PAGE ONLY -->
    <link href="<?php echo base_url();?>vendor/bower_components/select2/dist/css/select2.min.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>vendor/bower_components/datatables/media/css/dataTables.bootstrap.min.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>vendor/bower_components/datatables/media/css/jquery.dataTables.min.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>vendor/bower_components/datatables.net-buttons-dt/css/buttons.dataTables.min.css" rel="stylesheet" />
    
    <link href="<?php echo base_url();?>vendor/bower_components/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>vendor/bower_components/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" />

    <!--  BUTTON -->
    <link href="<?php echo base_url();?>vendor/bower_components/ladda-bootstrap/dist/ladda-themeless.min.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>vendor/bower_components/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.min.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>vendor/bower_components/bootstrap-social/bootstrap-social.css" rel="stylesheet" />
    <!-- end: CSS REQUIRED FOR THIS PAGE ONLY -->

    <link
    rel="stylesheet"
    href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"
  />
</head>

<body>

    <!-- start: HEADER -->
    <div class="navbar navbar-inverse navbar-fixed-top">
        <!-- start: TOP NAVIGATION CONTAINER -->
        	<?php echo $atas; ?>
        <!-- end: TOP NAVIGATION CONTAINER -->
    </div>
    <!-- end: HEADER -->
        <br>
    <!-- start: MAIN CONTAINER -->
    <div class="main-container">
        <div class="navbar-content">
            <!-- start: SIDEBAR -->
            <div class="main-navigation navbar-collapse collapse">
                <!-- start: MAIN MENU TOGGLER BUTTON -->
                <div class="navigation-toggler">
                    <i class="clip-chevron-left"></i>
                    <i class="clip-chevron-right"></i>
                </div>
                <!-- end: MAIN MENU TOGGLER BUTTON -->
                <!-- start: MAIN NAVIGATION MENU -->
                <?php echo $menu; ?>
                <!-- end: MAIN NAVIGATION MENU -->
            </div>
            <!-- end: SIDEBAR -->
        </div>

        <!-- start: PAGE -->
        <div class="main-content">
           
            <div class="container">
                <!-- start: PAGE HEADER -->
                <div class="row">
                    <div class="col-sm-12">
                        
                        <!-- start: PAGE TITLE & BREADCRUMB -->
                        <ol class="breadcrumb">
                            <li class="active">
                                <i class="clip-home-3"></i>
                                <a href="">
                                    Home
                                </a>
                            </li>
                            <li class="active">
                                    Draft Surat Keluar (Pending)
                            </li>
                           
                            <li class="search-box">
                                <form class="sidebar-search">
                                    <div class="form-group">
                                        <input type="text" placeholder="Start Searching...">
                                        <button class="submit">
                                            <i class="clip-search-3"></i>
                                        </button>
                                    </div>
                                </form>
                            </li>
                        </ol>
                        <div class="page-header">
                            <?php echo $bio; ?>
                        </div>
                        <!-- end: PAGE TITLE & BREADCRUMB -->
                    </div>
                </div>
                <!-- end: PAGE HEADER -->
                <!-- start: PAGE CONTENT -->
                <!--<marquee><span class='label label-info'><font color="black">SELAMAT DATANG DI SISTEM INFORMASI E-PDAM TIRTA KEUMUENENG KOTA LANGSA</font></span></marquee> -->
                <div class="row">
                    <!-- BATAS -->
                    <div class="col-md-12">
                        <?php echo $this->session->flashdata('info'); ?>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <i class="fa fa-external-link-square"></i><b style="text-transform: uppercase;">DATA DRAFT SURAT KELUAR</b>
                                <div class="panel-tools">
                                    <a class="btn btn-xs btn-link panel-collapse collapses" href="#"> </a>
                                    <a data-original-title="RESIZE FULL" data-content="Klik icon tersebut untuk tampilan full table" data-placement="top" data-trigger="hover" id="test" class="btn btn-xs btn-link panel-expand popovers" href="#"> <i class="fa fa-laptop"></i> </a>
                                    <a class="btn btn-xs btn-link panel-close" href="#"> <i class="fa fa-times"></i> </a>
                                </div>
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <!--<div class="col-md-12 space20">
                                        <a data-toggle="modal" href="#modalTambahSurat" class="btn btn-teal">
                                            <i class="fa fa-plus"></i> SURAT MASUK 
                                        </a>
                                    </div> -->
                                </div>
                                <div class="table-responsive">
                                    <table class="table table-striped table-hover" id="sample-table-2">
                                        <thead>
                                            <tr>
                                                <th width="1" class="col-to-export center">#</th>
                                                <th width="" class="col-to-export">PERIHAL</th>
                                                <th width="" class="col-to-export">TUJUAN SURAT</th>
                                                <th width="" class="col-to-export">AUTHOR</th>
                                                <th width="1" class="col-to-export center">STATUS</th>
                                                <th width="1" class="col-to-export center">CREATED AT</th>
                                                <th width="1" class="center">#</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php 
                                                foreach ($data_draft->result_array() as $d)
                                               {
                                                    if($d['draft_status']=="Done")
                                                    {
                                                        echo '
                                                        <tr style="background-color: #2bd947;">';
                                                    }
                                                    else if($d['draft_status']=="Process")
                                                    {
                                                        echo '
                                                        <tr style="background-color: #acdff8;">';
                                                    }

                                                    else
                                                    {
                                                        echo '
                                                        <tr style="background-color: #f6cdcd;">';
                                                    } 
                                            ?>
                                                <td align="center"><b><?php echo $d['draft_no'] ?></b></td>
                                                <td style="text-transform: uppercase;"><b><?php echo $d['draft_perihal'] ?></b></td>
                                                <td style="text-transform: uppercase;"><b><?php echo $d['draft_tujuan_surat'] ?></b></td>
                                                <td class="">
                                                    <b>
                                                        <?php 
                                                            $nama_biro      = $this->web_app_model->getJoinOneWhere('draft_asal','biro_username','tbl_draft_surat','tbl_biro','biro_username',$d['draft_asal']);
                                                            $nama_kabag     = $this->web_app_model->getJoinOneWhere('draft_asal','kabag_username','tbl_draft_surat','tbl_kabag','kabag_username',$d['draft_asal']);
                                                            $nama_kasubbag  = $this->web_app_model->getJoinOneWhere('draft_asal','kasubbag_username','tbl_draft_surat','tbl_kasubbag','kasubbag_username',$d['draft_asal']);
                                                            $nama_rektor    = $this->web_app_model->getJoinOneWhere('draft_asal','rektor_username','tbl_draft_surat','tbl_rektor','rektor_username',$d['draft_asal']);
                                                            $nama_staff    = $this->web_app_model->getJoinOneWhere('draft_asal','staff_username','tbl_draft_surat','tbl_staff','staff_username',$d['draft_asal']);
                                                            $nama_users    = $this->web_app_model->getJoinOneWhere('draft_asal','username','tbl_draft_surat','tbl_users','username',$d['draft_asal']);
                                                            
                                                            if(!empty($nama_biro))
                                                            {
                                                                echo $nama_biro['biro_nama'];
                                                            }
                                                            else if(!empty($nama_kabag))
                                                            {
                                                                echo $nama_kabag['kabag_nama'];
                                                            }
                                                            else if(!empty($nama_kasubbag))
                                                            {
                                                                echo $nama_kasubbag['kasubbag_nama'];
                                                            }
                                                            else if(!empty($nama_rektor))
                                                            {
                                                                echo $nama_rektor['rektor_nama'];
                                                            }
                                                            else if(!empty($nama_staff))
                                                            {
                                                                echo $nama_staff['staff_nama'];
                                                            }
                                                            else if(!empty($nama_users))
                                                            {
                                                                echo $nama_users['nama_lengkap'];
                                                            }

                                                        echo "<br>(".$d['draft_asal'].")" ?>
                                                        
                                                    </b>
                                                </td>
                                                <td class="center" style="text-transform: uppercase;"><span class="label label-purple">
                                                    <?php 
                                                      echo $d['draft_status']
                                                    ?>
                                                </span>
                                                </td>
                                                <td class="center"><b><?php echo $d['draft_created_at'] ?> WIB</b></td>
                                                <td align="center">
                                                    <div class="btn-group">
                                                        <a class="btn btn-primary dropdown-toggle btn-xs" data-toggle="dropdown" href="#">
                                                            <i class="fa fa-cog"></i> <span class="caret"></span>
                                                        </a>
                                                        <ul role="menu" class="dropdown-menu pull-right">
                                                            <li role="presentation">
                                                                <a role="menuitem" tabindex="-1" href="<?php echo base_url() ?>index.php/biro/bg_detail_draft/<?php echo $d['draft_uuid'] ?>/<?php //echo $kasubbag_uuid ?>?tab1=1">
                                                                    <i class="clip-file-2"></i> Detail Draft
                                                                </a>
                                                            </li>
                                                            <!--<li role="presentation">
                                                                <a role="menuitem" tabindex="-1" href="<?php echo base_url() ?>index.php/admin/bg_editPegawai/<?php //echo $d['nik_ktp'] ?>">
                                                                    <i class="fa fa-pencil"></i> Edit Data Umum
                                                                </a>
                                                            </li>
                                                            <li role="presentation">
                                                                <a role="menuitem" tabindex="-1" onclick="return confirm('Anda yakin akan menghapus data <?php //echo $d['nama_pegawai'] ?>.??')" href="<?php echo base_url() ?>index.php/crud_pegawai/hapusPegawai/<?php //echo $d['nik_ktp'] ?>/<?php echo $this->uri->segment(3);?>">
                                                                    <i class="fa fa-times"></i> Hapus
                                                                </a>
                                                            </li> -->
                                                        </ul>
                                                    </div>    
                                                </td>
                                                
                                            </tr>
                                        <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <?php //echo $modalTambahSurat ?>
                    </div>
                    <!-- BATAS -->
                </div>
                <!-- end: PAGE CONTENT-->
            </div>
        </div>
        <!-- end: PAGE -->
    </div>
    <!-- end: MAIN CONTAINER -->
    <!-- start: FOOTER -->
    <div class="footer clearfix">
        <div class="footer-inner">
            <script>
                document.write(new Date().getFullYear())
            </script> &copy; <?php $this->load->view('versi'); ?>
        </div>
        <div class="footer-items">
            <span class="go-top"><i class="clip-chevron-up"></i></span>
        </div>
    </div>
    <!-- end: FOOTER -->
    <!-- start: RIGHT SIDEBAR -->
    	<!-- ISI RIGHT SIDE BAR -->
    <!-- end: RIGHT SIDEBAR -->
 
    <!-- start: MAIN JAVASCRIPTS -->
    <!--[if lt IE 9]>
        <script src="<?php echo base_url(); ?>vendor/bower_components/respond/dest/respond.min.js"></script>
        <script src="<?php echo base_url(); ?>vendor/bower_components/Flot/excanvas.min.js"></script>
        <script src="<?php echo base_url(); ?>vendor/bower_components/jquery-1.x/dist/jquery.min.js"></script>
        <![endif]-->
    <!--[if gte IE 9]><!-->
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/jquery/dist/jquery.min.js"></script>
    <!--<![endif]-->

    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/jquery-ui/jquery-ui.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/blockUI/jquery.blockUI.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/iCheck/icheck.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/perfect-scrollbar/js/min/perfect-scrollbar.jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/jquery.cookie/jquery.cookie.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/sweetalert/dist/sweetalert.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/assets/js/min/main.min.js"></script>
    <!-- end: MAIN JAVASCRIPTS -->
    <!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
    
    <script src="<?php echo base_url();?>vendor/bower_components/bootstrap-modal/js/bootstrap-modal.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/bootstrap-modal/js/bootstrap-modalmanager.js"></script>
    <script src="<?php echo base_url();?>vendor/assets/js/min/ui-modals.min.js"></script>

    <script src="<?php echo base_url();?>vendor/bower_components/select2/dist/js/select2.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/jszip/dist/jszip.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/pdfmake/build/pdfmake.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/pdfmake/build/vfs_fonts.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?php echo base_url();?>vendor/assets/js/min/table-export.min.js"></script>
    <!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->

    <?php echo $this->session->flashdata('info2'); ?>
    
     <script>
        jQuery(document).ready(function() {
            Main.init();
            TableExport.init();
            UIModals.init();
        });
    </script>

</body>

</html>