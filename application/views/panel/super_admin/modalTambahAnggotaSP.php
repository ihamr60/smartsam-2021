
<div id="modalTambahAnggotaSP" class="modal fade" tabindex="-1" data-width="760" style="display: none;">
    <form role="form" action="<?php echo base_url() ?>index.php/super_admin/tambahAnggotaSP" method="post" enctype="multipart/form-data" >
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                &times;
            </button>
            <h4 class="modal-title">TAMBAH DATA ANGGOTA SATUAN PENGAWASAN (SP)</h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <label>Username:</label>
                    <p>
                        <input
                            type="text"
                            name="username"
                            class="form-control"
                            placeholder="Ex: spi3897"
                            required>
                    </p>
                </div>
                <div class="col-md-12">
                    <label>Nama Lengkap:</label>
                    <p>
                        <input
                            type="text"
                            name="nama_lengkap"
                            class="form-control"
                            placeholder="Ex: TUTI MEUTIA, S.E., M.SI."
                            required>
                    </p>
                </div>
                <div class="col-md-12">
                    <label>Posisi / Jabatan Satuan Pengawasan:</label>
                    <p>
                        <input
                            type="text"
                            readonly
                            name="posisi"
                            class="form-control"
                            value="Anggota SP"
                            required>
                    </p>
                </div>
                <div class="col-md-12">
                    <label>Atasan Anggota SP:</label>
                    <p>
                        <select
                            required
                            name="atasan"
                            class="form-control">
                            <option value="">Pilih Atasan</option>
                            <?php 
                            foreach($data_seksp->result_array() as $d)
                            {
                            ?>
                            <option style='text-transform: uppercase;' value="<?php echo $d['kasubbag_uuid'] ?>"><?php echo $d['kasubbag_nama'] ?> / (<?php echo $d['kasubbag_posisi'] ?>)</option>
                        <?php } ?>
                        </select>
                    </p>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-light-grey">
                Cancel
            </button>
            <button type="submit" class="btn btn-blue">
                Tambahkan
            </button>
        </div>
    </form>
</div>
