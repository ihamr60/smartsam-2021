<?php 
    foreach($data_fakultas->result_array() as $d){
?>
<div id="modalEditFakultas<?php echo $d['fak_fakultas_no'] ?>" class="modal fade" tabindex="-1" data-width="760" style="display: none;">
    <form role="form" action="<?php echo base_url() ?>index.php/super_admin/editFakultas" method="post" enctype="multipart/form-data" >
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                &times;
            </button>
            <h4 class="modal-title">EDIT DATA FAKULTAS</h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <label>FAKULTAS:</label>
                    <p>
                        <input name="fak_fakultas_no" type="hidden" value="<?php echo $d['fak_fakultas_no'] ?>">
                        <input
                            type="text"
                            name="fak_fakultas_nama"
                            class="form-control"
                            value="<?php echo $d['fak_fakultas_nama'] ?>"
                            required
                            >
                    </p>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-light-grey">
                Cancel
            </button>
            <button type="submit" class="btn btn-blue">
                Tambahkan
            </button>
        </div>
    </form>
</div>
<?php } ?>
