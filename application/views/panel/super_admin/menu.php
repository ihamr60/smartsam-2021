<ul class="main-navigation-menu">
    <?php
        $color = "#0c5174";
    ?>
    <li>
        <a 
            <?php if (isset($_GET['home']) && $_GET['home']==1){echo 'style="background: '.$color.'; color: white;"';}?> 
            href="<?php echo base_url();?>index.php/panel/bg_home?home=1">
            <i class="clip-home-3" <?php if (isset($_GET['home']) && $_GET['home']==1){echo 'style="background: '.$color.'; color: white;"';}?>></i>
            <span class="title"><b>DASHBOARD</b></span>
        </a>
    </li>
    <li>
        <a <?php if (isset($_GET['data_users']) && $_GET['data_users']==1){echo 'style="background: '.$color.'; color: white;"';}?> 
        href="javascript:void(0)">
            <i class="clip-users-2" <?php if (isset($_GET['data_users']) && $_GET['data_users']==1){echo 'style="background: '.$color.'; color: white;"';}?>></i>
            <span class="title"><b>DATA USERS</b></span>
            <span class="animate__animated animate__infinite animate__fast animate__flash label label-warning"><font color="black">NEW</font>
                    </span>
            <i <?php if (isset($_GET['settings']) && $_GET['settings']==1){echo 'style="background: #346da4; color: white;"';}?> class="icon-arrow"></i>
        </a>
        <ul class="sub-menu">
            <li>
                <a href="<?php echo base_url();?>index.php/super_admin/bg_rektor?data_users=1">
                    <i class="clip-user-2"></i>
                    <span class="title">Rektor, Warek</span>
                </a>
                <a href="<?php echo base_url();?>index.php/super_admin/bg_biro?data_users=1">
                    <i class="clip-user-2"></i>
                    <span class="title">Biro</span>
                </a>
                <a href="<?php echo base_url();?>index.php/super_admin/bg_sp?data_users=1">
                    <i class="clip-user-2"></i>
                    <span class="title">Satuan Pengawasan</span>
                </a>
                <a href="<?php echo base_url();?>index.php/super_admin/bg_upt_tik?data_users=1">
                    <i class="clip-user-2"></i>
                    <span class="title">UPT TIK</span>
                </a>
                <a href="<?php echo base_url();?>index.php/super_admin/bg_upt_labdas?data_users=1">
                    <i class="clip-user-2"></i>
                    <span class="title">UPT Lab Dasar</span>
                </a>
                <a href="<?php echo base_url();?>index.php/super_admin/bg_upt_lab_bhs?data_users=1">
                    <i class="clip-user-2"></i>
                    <span class="title">UPT Lab Bahasa</span>
                </a>
                <a href="<?php echo base_url();?>index.php/super_admin/bg_upt_perpus?data_users=1">
                    <i class="clip-user-2"></i>
                    <span class="title">UPT Perpustakaan</span>
                </a>
                <a href="<?php echo base_url();?>index.php/super_admin/bg_dekan?data_users=1">
                    <i class="clip-user-2"></i>
                    <span class="title">Dekan Fakultas</span> 
                    <span class="animate__animated animate__infinite animate__fast animate__flash label label-warning"><font color="black">NEW</font>
                    </span>
                </a>
                <a href="<?php echo base_url();?>index.php/super_admin/bg_lpppm?data_users=1">
                    <i class="clip-user-2"></i>
                    <span class="title">LPPPM & PM</span> 
                    <span class="animate__animated animate__infinite animate__fast animate__flash label label-warning"><font color="black">NEW</font>
                    </span>
                </a>
                <!--<a href="<?php echo base_url();?>index.php/admin/bg_pegawai?dt_peg=1">
                    <i class="clip-user-2"></i>
                    <span class="title">Pegawai
                        <span class="label label-danger">
                            Soon
                        </span>
                    </span>
                </a>-->
            </li>
        </ul>
    </li>
    <li>
        <a <?php if (isset($_GET['kd_bagian']) && $_GET['kd_bagian']==1){echo 'style="background: '.$color.'; color: white;"';}?> 
        href="<?php echo base_url();?>index.php/super_admin/bg_kode_bagian?kd_bagian=1">
            <i class="clip-file-2" <?php if (isset($_GET['kd_bagian']) && $_GET['kd_bagian']==1){echo 'style="background: '.$color.'; color: white;"';}?>></i>
            <span class="title"><b>KODE BAGIAN</b>
            </span>
        </a>
    </li>
    <li>
        <a <?php if (isset($_GET['kd_hal']) && $_GET['kd_hal']==1){echo 'style="background: '.$color.'; color: white;"';}?> 
        href="<?php echo base_url();?>index.php/super_admin/bg_kode_hal?kd_hal=1">
            <i class="clip-file-2" <?php if (isset($_GET['kd_hal']) && $_GET['kd_hal']==1){echo 'style="background: '.$color.'; color: white;"';}?>></i>
            <span class="title"><b>KODE HAL</b>
            </span>
        </a>
    </li>
    <!--<li>
        <a <?php if (isset($_GET['kd_fakultas']) && $_GET['kd_fakultas']==1){echo 'style="background: '.$color.'; color: white;"';}?> 
        href="<?php echo base_url();?>index.php/super_admin/bg_fakultas?kd_fakultas=1">
            <i class="clip-file-2" <?php if (isset($_GET['kd_fakultas']) && $_GET['kd_fakultas']==1){echo 'style="background: '.$color.'; color: white;"';}?>></i>
            <span class="title"><b>DATA FAKULTAS</b>
            </span>
        </a>
    </li>-->
   <li>
        <a <?php if (isset($_GET['settings']) && $_GET['settings']==1){echo 'style="background: '.$color.'; color: white;"';}?> 
        href="javascript:void(0)">
            <i class="clip-file-2" <?php if (isset($_GET['settings']) && $_GET['settings']==1){echo 'style="background: '.$color.'; color: white;"';}?>></i>
            <span class="title"><b>SETTINGS</b></span>
            <i <?php if (isset($_GET['settings']) && $_GET['settings']==1){echo 'style="background: #346da4; color: white;"';}?> class="icon-arrow"></i>
        </a>
        <ul class="sub-menu">
            <li>
                <!--active open-->
                <a href="<?php echo base_url();?>index.php/super_admin/bg_ganti_password?settings=1">
                    <i class="clip-user-2"></i>
                    <span class="title"> Change Password </span>
                </a>
            </li>
        </ul>
    </li>
</ul>