<?php 
    foreach($data_biro->result_array() as $d){
?>
<div id="modalEditBiro<?php echo $d['biro_uuid'] ?>" class="modal fade" tabindex="-1" data-width="760" style="display: none;">
    <form role="form" action="<?php echo base_url() ?>index.php/super_admin/editBiro" method="post" enctype="multipart/form-data" >
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                &times;
            </button>
            <h4 class="modal-title">EDIT DATA AKUN BIRO</h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <label>Username:</label>
                    <p>
                        <input name="biro_uuid" type="hidden" value="<?php echo $d['biro_uuid'] ?>">
                        <input
                            type="text"
                            name="username"
                            class="form-control"
                            value="<?php echo $d['biro_username'] ?>"
                            required
                            readonly>
                    </p>
                </div>
                <div class="col-md-12">
                    <label>Nama Lengkap:</label>
                    <p>
                        <input
                            type="text"
                            name="nama_lengkap"
                            class="form-control"
                            value="<?php echo $d['biro_nama'] ?>"
                            required>
                    </p>
                </div>
                <div class="col-md-12">
                    <label>Posisi / Jabatan:</label>
                    <p>
                        <select
                            name="posisi"
                            class="form-control">
                            <?php 
                                if($d['biro_posisi'] == "Biro Umum & Keuangan")
                                {
                                    echo '
                                    <option selected value="Biro Umum & Keuangan">Biro Umum & Keuangan</option>
                                    <option value="Biro AKPK">Biro AKPK</option>';
                                }
                                else if($d['biro_posisi'] == "Biro AKPK")
                                {
                                    echo '
                                    <option value="Biro Umum & Keuangan">Biro Umum & Keuangan</option>
                                    <option selected value="Biro AKPK">Biro AKPK</option>';
                                }
                            ?>
                        </select>
                    </p>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-light-grey">
                Cancel
            </button>
            <button type="submit" class="btn btn-blue">
                Tambahkan
            </button>
        </div>
    </form>
</div>
<?php } ?>
