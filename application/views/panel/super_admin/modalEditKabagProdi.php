<?php 
    foreach($data_kabag->result_array() as $d){
?>
<div id="modalEditKabagProdi<?php echo $d['kabag_uuid'] ?>" class="modal fade" tabindex="-1" data-width="760" style="display: none;">
    <form role="form" action="<?php echo base_url() ?>index.php/super_admin/editKabagProdi" method="post" enctype="multipart/form-data" >
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                &times;
            </button>
            <h4 class="modal-title">EDIT DATA KABAG / KAPRODI</h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-6">
                    <label>USERNAME:</label>
                    <p>
                        <input name="kabag_uuid" type="hidden" value="<?php echo $d['kabag_uuid'] ?>">
                        <input
                            type="text"
                            name="kabag_username"
                            class="form-control"
                            value="<?php echo $d['kabag_username'] ?>"
                            required
                            readonly>
                    </p>
                </div>
                <div class="col-md-6">
                    <label>NAMA LENGKAP:</label>
                    <p>
                        <input
                            type="text"
                            name="kabag_nama"
                            class="form-control"
                            value="<?php echo $d['kabag_nama'] ?>"
                            required>
                    </p>
                </div>
                
                <div class="col-md-6">
                    <label>EMAIL:</label>
                    <p>
                        <input
                            type="text"
                            name="kabag_email"
                            class="form-control"
                            value="<?php echo $d['kabag_email'] ?>"
                            required>
                    </p>
                </div>
                <div class="col-md-6">
                    <label>JABATAN/POSISI:</label>
                    <p>
                        <select
                            name="kabag_posisi"
                            class="form-control">
                            <option value="<?php echo $d['kabag_posisi'] ?>"><?php echo $d['kabag_posisi'] ?></option>
                            <?php 
                                if($d['kabag_posisi'] == 'Kabag TU Fak. Hukum')
                                {
                                    echo '<option selected value="Kabag TU Fak. Hukum">Kabag TU Fak. Hukum</option>
                                        <option value="Kabag TU Fak. Pertanian">Kabag TU Fak. Pertanian</option>
                                        <option value="Kabag TU Fak. Ekonomi">Kabag TU Fak. Ekonomi</option>
                                        <option value="Kabag TU Fak. Keguruan">Kabag TU Fak. Keguruan</option>
                                        <option value="Kabag TU Fak. Teknik">Kabag TU Fak. Teknik</option>';
                                }
                                else if($d['kabag_posisi'] == 'Kabag TU Fak. Pertanian')
                                {
                                    echo '<option value="Kabag TU Fak. Hukum">Kabag TU Fak. Hukum</option>
                                        <option selected value="Kabag TU Fak. Pertanian">Kabag TU Fak. Pertanian</option>
                                        <option value="Kabag TU Fak. Ekonomi">Kabag TU Fak. Ekonomi</option>
                                        <option value="Kabag TU Fak. Keguruan">Kabag TU Fak. Keguruan</option>
                                        <option value="Kabag TU Fak. Teknik">Kabag TU Fak. Teknik</option>';
                                }
                                else if($d['kabag_posisi'] == 'Kabag TU Fak. Ekonomi')
                                {
                                    echo '<option value="Kabag TU Fak. Hukum">Kabag TU Fak. Hukum</option>
                                        <option value="Kabag TU Fak. Pertanian">Kabag TU Fak. Pertanian</option>
                                        <option selected value="Kabag TU Fak. Ekonomi">Kabag TU Fak. Ekonomi</option>
                                        <option value="Kabag TU Fak. Keguruan">Kabag TU Fak. Keguruan</option>
                                        <option value="Kabag TU Fak. Teknik">Kabag TU Fak. Teknik</option>';
                                }
                                else if($d['kabag_posisi'] == 'Kabag TU Fak. Keguruan')
                                {
                                    echo '<option value="Kabag TU Fak. Hukum">Kabag TU Fak. Hukum</option>
                                        <option value="Kabag TU Fak. Pertanian">Kabag TU Fak. Pertanian</option>
                                        <option value="Kabag TU Fak. Ekonomi">Kabag TU Fak. Ekonomi</option>
                                        <option selected value="Kabag TU Fak. Keguruan">Kabag TU Fak. Keguruan</option>
                                        <option value="Kabag TU Fak. Teknik">Kabag TU Fak. Teknik</option>';
                                }
                                else if($d['kabag_posisi'] == 'Kabag TU Fak. Teknik')
                                {
                                    echo '<option value="Kabag TU Fak. Hukum">Kabag TU Fak. Hukum</option>
                                        <option value="Kabag TU Fak. Pertanian">Kabag TU Fak. Pertanian</option>
                                        <option value="Kabag TU Fak. Ekonomi">Kabag TU Fak. Ekonomi</option>
                                        <option value="Kabag TU Fak. Keguruan">Kabag TU Fak. Keguruan</option>
                                        <option selected value="Kabag TU Fak. Teknik">Kabag TU Fak. Teknik</option>';
                                }
                            ?>
                            
                            <?php
                                foreach($data_prodi->result_array() as $d)
                                {
                                    echo '<option value="'.$d['prodi_nama'].'">'.$d['prodi_nama'].'</option>';
                                }
                            ?>
                        </select>
                    </p>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-light-grey">
                Cancel
            </button>
            <button type="submit" class="btn btn-blue">
                Update
            </button>
        </div>
    </form>
</div>
<?php } ?>
