
<div id="modalTambahKaUptTik" class="modal fade" tabindex="-1" data-width="760" style="display: none;">
    <form role="form" action="<?php echo base_url() ?>index.php/super_admin/tambahKaUptTik" method="post" enctype="multipart/form-data" >
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                &times;
            </button>
            <h4 class="modal-title">TAMBAH DATA KEPALA UPT TIK</h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <label>Username:</label>
                    <p>
                        <input
                            type="text"
                            name="username"
                            class="form-control"
                            placeholder="Ex: kaupt_tik21"
                            required>
                    </p>
                </div>
                <div class="col-md-12">
                    <label>Nama Lengkap:</label>
                    <p>
                        <input
                            type="text"
                            name="nama_lengkap"
                            class="form-control"
                            placeholder="Ex: MUNAWIR, S.ST., M.T."
                            required>
                    </p>
                </div>
                <div class="col-md-12">
                    <label>Jabatan / Posisi:</label>
                    <p>
                        <input
                            type="text"
                            name="posisi"
                            class="form-control"
                            value="Ka UPT TIK"
                            required
                            readonly>
                    </p>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-light-grey">
                Cancel
            </button>
            <button type="submit" class="btn btn-blue">
                Tambahkan
            </button>
        </div>
    </form>
</div>
