<?php 
    foreach($data_pelaksanaLabdas->result_array() as $d)
    {
?>
<div id="modalEditPelaksanaLabdas<?php echo $d['staff_uuid'] ?>" class="modal fade" tabindex="-1" data-width="760" style="display: none;">
    <form role="form" action="<?php echo base_url() ?>index.php/super_admin/editPelaksanaLabdas" method="post" enctype="multipart/form-data" >
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                &times;
            </button>
            <h4 class="modal-title">EDIT DATA AKUN PELAKSANA UPT LAB DASAR</h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <label>Username:</label>
                    <p>
                        <input name="staff_uuid" type="hidden" value="<?php echo $d['staff_uuid'] ?>">
                        <input
                            type="text"
                            name="username"
                            class="form-control"
                            value="<?php echo $d['staff_username'] ?>"
                            required
                            readonly>
                    </p>
                </div>
                <div class="col-md-12">
                    <label>Nama Lengkap:</label>
                    <p>
                        <input
                            type="text"
                            name="nama_lengkap"
                            class="form-control"
                            value="<?php echo $d['staff_nama'] ?>"
                            required>
                    </p>
                </div>
                <div class="col-md-12">
                    <label>Posisi / Jabatan:</label>
                    <p>
                        <input
                            type="text"
                            name="posisi"
                            class="form-control"
                            value="<?php echo $d['staff_posisi'] ?>"
                            required
                            >
                    </p>
                </div>
                <div class="col-md-12">
                    <label>Atasan Pelaksana UPT Lab Dasar:</label>
                    <p>
                        <select
                            required
                            name="atasan"
                            class="form-control">
                            <?php 
                            foreach($data_kaLabdas->result_array() as $e)
                            {
                                if($d['staff_uuid_kasubbag'] == $e['kasubbag_uuid'])
                                {
                            ?>
                                <option selected style='text-transform: uppercase;' value="<?php echo $e['kasubbag_uuid'] ?>"><?php echo $e['kasubbag_nama'] ?> / (<?php echo $e['kasubbag_posisi'] ?>)</option>
                        <?php   }
                            else 
                                {
                        ?>
                                <option style='text-transform: uppercase;' value="<?php echo $e['kasubbag_uuid'] ?>"><?php echo $e['kasubbag_nama'] ?> / (<?php echo $e['kasubbag_posisi'] ?>)</option>
                        <?php   }    
                            } ?>
                        </select>
                    </p>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-light-grey">
                Cancel
            </button>
            <button type="submit" class="btn btn-blue">
                Tambahkan
            </button>
        </div>
    </form>
</div>
<?php } ?>
