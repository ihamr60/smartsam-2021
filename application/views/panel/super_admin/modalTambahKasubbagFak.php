
<div id="modalTambahKasubbagFak" class="modal fade" tabindex="-1" data-width="760" style="display: none;">
    <form role="form" action="<?php echo base_url() ?>index.php/super_admin/tambahKasubbagFak" method="post" enctype="multipart/form-data" >
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                &times;
            </button>
            <h4 class="modal-title">TAMBAH DATA KASUBBAG LINGKUNGAN FAKULTAS</h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-6">
                    <label>USERNAME:</label>
                    <p>
                        <input
                            type="text"
                            name="kasubbag_username"
                            class="form-control"
                            placeholder="Ex: ismail232"
                            required>
                    </p>
                </div>
                <div class="col-md-6">
                    <label>NAMA KASUBBAG (FAKULTAS):</label>
                    <p>
                        <input
                            type="text"
                            name="kasubbag_nama"
                            class="form-control"
                            placeholder="Ex: Ismail, M.T"
                            required>
                    </p>
                </div>
                <div class="col-md-6">
                    <label>JABATAN/POSISI:</label>
                    <p>
                        <select
                            name="kasubbag_posisi"
                            class="form-control">
                            <option value=""> Please Select </option>
                            <option value="Kasubbag Akademik & Mhs Fak. Hukum">Kasubbag Akademik Fak. Hukum</option>
                            <option value="Kasubbag Akademik & Mhs Fak. Pertanian">Kasubbag Akademik Fak. Pertanian</option>
                            <option value="Kasubbag Akademik & Mhs Fak. Ekonomi">Kasubbag Akademik Fak. Ekonomi</option>
                            <option value="Kasubbag Akademik & Mhs Fak. Keguruan">Kasubbag Akademik Fak. Keguruan</option>
                            <option value="Kasubbag Akademik & Mhs Fak. Teknik">Kasubbag Akademik Fak. Teknik</option>

                            <option value="Kasubbag Umum & Keu Fak. HUKUM">Kasubbag Umum & Keu Fak. Hukum</option>
                            <option value="Kasubbag Umum & Keu Fak. Pertanian">Kasubbag Umum & Keu Fak. Pertanian</option>
                            <option value="Kasubbag Umum & Keu Fak. Ekonomi">Kasubbag Umum & Keu Fak. Ekonomi</option>
                            <option value="Kasubbag Umum & Keu Fak. Keguruan">Kasubbag Umum & Keu Fak. Keguruan</option>
                            <option value="Kasubbag Umum & Keu Fak. Teknik">Kasubbag Umum & Keu Fak. Teknik</option>
                        </select>
                    </p>
                </div>
                <div class="col-md-6">
                    <label>PILIH ATASAN::</label>
                    <p>
                        <select
                            name="kasubbag_uuid_kabag"
                            class="form-control">
                            <option value="">Please Select</option>
                            <?php
                                foreach($data_kabag->result_array() as $d)
                                {
                                    echo '<option value="'.$d['kabag_uuid'].'">'.$d['kabag_nama'].'</option>';
                                }
                            ?>
                        </select>
                    </p>
                </div>
                <div class="col-md-6">
                    <label>EMAIL :</label>
                    <p>
                        <input
                            type="text"
                            name="kasubbag_email"
                            class="form-control"
                            placeholder="Ex: ilhamr6000@gmail.com"
                            required>
                    </p>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-light-grey">
                Cancel
            </button>
            <button type="submit" class="btn btn-blue">
                Tambahkan
            </button>
        </div>
    </form>
</div>
