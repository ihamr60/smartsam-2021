<?php 
    foreach($data_kasubbag->result_array() as $d){
?>
<div id="modalEditKasubbagFak<?php echo $d['kasubbag_uuid'] ?>" class="modal fade" tabindex="-1" data-width="760" style="display: none;">
    <form role="form" action="<?php echo base_url() ?>index.php/super_admin/editKasubbagFak" method="post" enctype="multipart/form-data" >
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                &times;
            </button>
            <h4 class="modal-title">EDIT DATA KASUBBAG </h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-6">
                    <label>USERNAME:</label>
                    <p>
                        <input name="kasubbag_uuid" type="hidden" value="<?php echo $d['kasubbag_uuid'] ?>">
                        <input name="kasubbag_posisi_before" type="hidden" value="<?php echo $d['kasubbag_posisi'] ?>">
                        <input
                            type="text"
                            name="kasubbag_username"
                            class="form-control"
                            value="<?php echo $d['kasubbag_username'] ?>"
                            required
                            readonly>
                    </p>
                </div>
                <div class="col-md-6">
                    <label>NAMA LENGKAP:</label>
                    <p>
                        <input
                            type="text"
                            name="kasubbag_nama"
                            class="form-control"
                            value="<?php echo $d['kasubbag_nama'] ?>"
                            required>
                    </p>
                </div>

                <div class="col-md-6">
                    <label>JABATAN/POSISI:</label>
                    <p>
                        <select
                            name="kasubbag_posisi"
                            class="form-control">
                            
                            <?php 
                                if($d['kasubbag_posisi'] == 'Kasubbag Akademik & Mhs Fak. Hukum')
                                {
                                    echo '<option selected value="Kasubbag Akademik & Mhs Fak. Hukum">Kasubbag Akademik Fak. Hukum</option>
                                        <option value="Kasubbag Akademik & Mhs Fak. Pertanian">Kasubbag Akademik Fak. Pertanian</option>
                                        <option value="Kasubbag Akademik & Mhs Fak. Ekonomi">Kasubbag Akademik Fak. Ekonomi</option>
                                        <option value="Kasubbag Akademik & Mhs Fak. Keguruan">Kasubbag Akademik Fak. Keguruan</option>
                                        <option value="Kasubbag Akademik & Mhs Fak. Teknik">Kasubbag Akademik Fak. Teknik</option>

                                        <option value="Kasubbag Umum & Keu Fak. HUKUM">Kasubbag Umum & Keu Fak. Hukum</option>
                                        <option value="Kasubbag Umum & Keu Fak. Pertanian">Kasubbag Umum & Keu Fak. Pertanian</option>
                                        <option value="Kasubbag Umum & Keu Fak. Ekonomi">Kasubbag Umum & Keu Fak. Ekonomi</option>
                                        <option value="Kasubbag Umum & Keu Fak. Keguruan">Kasubbag Umum & Keu Fak. Keguruan</option>
                                        <option value="Kasubbag Umum & Keu Fak. Teknik">Kasubbag Umum & Keu Fak. Teknik</option>';
                                }
                                else if($d['kasubbag_posisi'] == 'Kasubbag Akademik & Mhs Fak. Pertanian')
                                {
                                    echo '<option value="Kasubbag Akademik & Mhs Fak. Hukum">Kasubbag Akademik Fak. Hukum</option>
                                        <option selected value="Kasubbag Akademik & Mhs Fak. Pertanian">Kasubbag Akademik Fak. Pertanian</option>
                                        <option value="Kasubbag Akademik & Mhs Fak. Ekonomi">Kasubbag Akademik Fak. Ekonomi</option>
                                        <option value="Kasubbag Akademik & Mhs Fak. Keguruan">Kasubbag Akademik Fak. Keguruan</option>
                                        <option value="Kasubbag Akademik & Mhs Fak. Teknik">Kasubbag Akademik Fak. Teknik</option>

                                        <option value="Kasubbag Umum & Keu Fak. HUKUM">Kasubbag Umum & Keu Fak. Hukum</option>
                                        <option value="Kasubbag Umum & Keu Fak. Pertanian">Kasubbag Umum & Keu Fak. Pertanian</option>
                                        <option value="Kasubbag Umum & Keu Fak. Ekonomi">Kasubbag Umum & Keu Fak. Ekonomi</option>
                                        <option value="Kasubbag Umum & Keu Fak. Keguruan">Kasubbag Umum & Keu Fak. Keguruan</option>
                                        <option value="Kasubbag Umum & Keu Fak. Teknik">Kasubbag Umum & Keu Fak. Teknik</option>';
                                }
                                else if($d['kasubbag_posisi'] == 'Kasubbag Akademik & Mhs Fak. Ekonomi')
                                {
                                    echo '<option value="Kasubbag Akademik & Mhs Fak. Hukum">Kasubbag Akademik Fak. Hukum</option>
                                        <option value="Kasubbag Akademik & Mhs Fak. Pertanian">Kasubbag Akademik Fak. Pertanian</option>
                                        <option selected value="Kasubbag Akademik & Mhs Fak. Ekonomi">Kasubbag Akademik Fak. Ekonomi</option>
                                        <option value="Kasubbag Akademik & Mhs Fak. Keguruan">Kasubbag Akademik Fak. Keguruan</option>
                                        <option value="Kasubbag Akademik & Mhs Fak. Teknik">Kasubbag Akademik Fak. Teknik</option>

                                        <option value="Kasubbag Umum & Keu Fak. HUKUM">Kasubbag Umum & Keu Fak. Hukum</option>
                                        <option value="Kasubbag Umum & Keu Fak. Pertanian">Kasubbag Umum & Keu Fak. Pertanian</option>
                                        <option value="Kasubbag Umum & Keu Fak. Ekonomi">Kasubbag Umum & Keu Fak. Ekonomi</option>
                                        <option value="Kasubbag Umum & Keu Fak. Keguruan">Kasubbag Umum & Keu Fak. Keguruan</option>
                                        <option value="Kasubbag Umum & Keu Fak. Teknik">Kasubbag Umum & Keu Fak. Teknik</option>';
                                }
                                else if($d['kasubbag_posisi'] == 'Kasubbag Akademik & Mhs Fak. Keguruan')
                                {
                                    echo '<option value="Kasubbag Akademik & Mhs Fak. Hukum">Kasubbag Akademik Fak. Hukum</option>
                                        <option value="Kasubbag Akademik & Mhs Fak. Pertanian">Kasubbag Akademik Fak. Pertanian</option>
                                        <option value="Kasubbag Akademik & Mhs Fak. Ekonomi">Kasubbag Akademik Fak. Ekonomi</option>
                                        <option selected value="Kasubbag Akademik & Mhs Fak. Keguruan">Kasubbag Akademik Fak. Keguruan</option>
                                        <option value="Kasubbag Akademik & Mhs Fak. Teknik">Kasubbag Akademik Fak. Teknik</option>

                                        <option value="Kasubbag Umum & Keu Fak. HUKUM">Kasubbag Umum & Keu Fak. Hukum</option>
                                        <option value="Kasubbag Umum & Keu Fak. Pertanian">Kasubbag Umum & Keu Fak. Pertanian</option>
                                        <option value="Kasubbag Umum & Keu Fak. Ekonomi">Kasubbag Umum & Keu Fak. Ekonomi</option>
                                        <option value="Kasubbag Umum & Keu Fak. Keguruan">Kasubbag Umum & Keu Fak. Keguruan</option>
                                        <option value="Kasubbag Umum & Keu Fak. Teknik">Kasubbag Umum & Keu Fak. Teknik</option>';
                                }
                                else if($d['kasubbag_posisi'] == 'Kasubbag Akademik & Mhs Fak. Teknik')
                                {
                                    echo '<option value="Kasubbag Akademik & Mhs Fak. Hukum">Kasubbag Akademik Fak. Hukum</option>
                                        <option value="Kasubbag Akademik & Mhs Fak. Pertanian">Kasubbag Akademik Fak. Pertanian</option>
                                        <option value="Kasubbag Akademik & Mhs Fak. Ekonomi">Kasubbag Akademik Fak. Ekonomi</option>
                                        <option value="Kasubbag Akademik & Mhs Fak. Keguruan">Kasubbag Akademik Fak. Keguruan</option>
                                        <option selected value="Kasubbag Akademik & Mhs Fak. Teknik">Kasubbag Akademik Fak. Teknik</option>

                                        <option value="Kasubbag Umum & Keu Fak. HUKUM">Kasubbag Umum & Keu Fak. Hukum</option>
                                        <option value="Kasubbag Umum & Keu Fak. Pertanian">Kasubbag Umum & Keu Fak. Pertanian</option>
                                        <option value="Kasubbag Umum & Keu Fak. Ekonomi">Kasubbag Umum & Keu Fak. Ekonomi</option>
                                        <option value="Kasubbag Umum & Keu Fak. Keguruan">Kasubbag Umum & Keu Fak. Keguruan</option>
                                        <option value="Kasubbag Umum & Keu Fak. Teknik">Kasubbag Umum & Keu Fak. Teknik</option>';
                                }
                                else if($d['kasubbag_posisi'] == 'Kasubbag Umum & Keu Fak. HUKUM')
                                {
                                    echo '<option value="Kasubbag Akademik & Mhs Fak. Hukum">Kasubbag Akademik Fak. Hukum</option>
                                        <option value="Kasubbag Akademik & Mhs Fak. Pertanian">Kasubbag Akademik Fak. Pertanian</option>
                                        <option value="Kasubbag Akademik & Mhs Fak. Ekonomi">Kasubbag Akademik Fak. Ekonomi</option>
                                        <option value="Kasubbag Akademik & Mhs Fak. Keguruan">Kasubbag Akademik Fak. Keguruan</option>
                                        <option value="Kasubbag Akademik & Mhs Fak. Teknik">Kasubbag Akademik Fak. Teknik</option>

                                        <option selected value="Kasubbag Umum & Keu Fak. HUKUM">Kasubbag Umum & Keu Fak. Hukum</option>
                                        <option value="Kasubbag Umum & Keu Fak. Pertanian">Kasubbag Umum & Keu Fak. Pertanian</option>
                                        <option value="Kasubbag Umum & Keu Fak. Ekonomi">Kasubbag Umum & Keu Fak. Ekonomi</option>
                                        <option value="Kasubbag Umum & Keu Fak. Keguruan">Kasubbag Umum & Keu Fak. Keguruan</option>
                                        <option value="Kasubbag Umum & Keu Fak. Teknik">Kasubbag Umum & Keu Fak. Teknik</option>';
                                }
                                else if($d['kasubbag_posisi'] == 'Kasubbag Umum & Keu Fak. Pertanian')
                                {
                                    echo '<option value="Kasubbag Akademik & Mhs Fak. Hukum">Kasubbag Akademik Fak. Hukum</option>
                                        <option value="Kasubbag Akademik & Mhs Fak. Pertanian">Kasubbag Akademik Fak. Pertanian</option>
                                        <option value="Kasubbag Akademik & Mhs Fak. Ekonomi">Kasubbag Akademik Fak. Ekonomi</option>
                                        <option value="Kasubbag Akademik & Mhs Fak. Keguruan">Kasubbag Akademik Fak. Keguruan</option>
                                        <option value="Kasubbag Akademik & Mhs Fak. Teknik">Kasubbag Akademik Fak. Teknik</option>

                                        <option value="Kasubbag Umum & Keu Fak. HUKUM">Kasubbag Umum & Keu Fak. Hukum</option>
                                        <option selected value="Kasubbag Umum & Keu Fak. Pertanian">Kasubbag Umum & Keu Fak. Pertanian</option>
                                        <option value="Kasubbag Umum & Keu Fak. Ekonomi">Kasubbag Umum & Keu Fak. Ekonomi</option>
                                        <option value="Kasubbag Umum & Keu Fak. Keguruan">Kasubbag Umum & Keu Fak. Keguruan</option>
                                        <option value="Kasubbag Umum & Keu Fak. Teknik">Kasubbag Umum & Keu Fak. Teknik</option>';
                                }
                                else if($d['kasubbag_posisi'] == 'Kasubbag Umum & Keu Fak. Ekonomi')
                                {
                                    echo '<option value="Kasubbag Akademik & Mhs Fak. Hukum">Kasubbag Akademik Fak. Hukum</option>
                                        <option value="Kasubbag Akademik & Mhs Fak. Pertanian">Kasubbag Akademik Fak. Pertanian</option>
                                        <option value="Kasubbag Akademik & Mhs Fak. Ekonomi">Kasubbag Akademik Fak. Ekonomi</option>
                                        <option value="Kasubbag Akademik & Mhs Fak. Keguruan">Kasubbag Akademik Fak. Keguruan</option>
                                        <option value="Kasubbag Akademik & Mhs Fak. Teknik">Kasubbag Akademik Fak. Teknik</option>

                                        <option value="Kasubbag Umum & Keu Fak. HUKUM">Kasubbag Umum & Keu Fak. Hukum</option>
                                        <option value="Kasubbag Umum & Keu Fak. Pertanian">Kasubbag Umum & Keu Fak. Pertanian</option>
                                        <option selected value="Kasubbag Umum & Keu Fak. Ekonomi">Kasubbag Umum & Keu Fak. Ekonomi</option>
                                        <option value="Kasubbag Umum & Keu Fak. Keguruan">Kasubbag Umum & Keu Fak. Keguruan</option>
                                        <option value="Kasubbag Umum & Keu Fak. Teknik">Kasubbag Umum & Keu Fak. Teknik</option>';
                                }
                                else if($d['kasubbag_posisi'] == 'Kasubbag Umum & Keu Fak. Keguruan')
                                {
                                    echo '<option value="Kasubbag Akademik & Mhs Fak. Hukum">Kasubbag Akademik Fak. Hukum</option>
                                        <option value="Kasubbag Akademik & Mhs Fak. Pertanian">Kasubbag Akademik Fak. Pertanian</option>
                                        <option value="Kasubbag Akademik & Mhs Fak. Ekonomi">Kasubbag Akademik Fak. Ekonomi</option>
                                        <option value="Kasubbag Akademik & Mhs Fak. Keguruan">Kasubbag Akademik Fak. Keguruan</option>
                                        <option value="Kasubbag Akademik & Mhs Fak. Teknik">Kasubbag Akademik Fak. Teknik</option>

                                        <option value="Kasubbag Umum & Keu Fak. HUKUM">Kasubbag Umum & Keu Fak. Hukum</option>
                                        <option value="Kasubbag Umum & Keu Fak. Pertanian">Kasubbag Umum & Keu Fak. Pertanian</option>
                                        <option value="Kasubbag Umum & Keu Fak. Ekonomi">Kasubbag Umum & Keu Fak. Ekonomi</option>
                                        <option selected value="Kasubbag Umum & Keu Fak. Keguruan">Kasubbag Umum & Keu Fak. Keguruan</option>
                                        <option value="Kasubbag Umum & Keu Fak. Teknik">Kasubbag Umum & Keu Fak. Teknik</option>';
                                }
                                else if($d['kasubbag_posisi'] == 'Kasubbag Umum & Keu Fak. Teknik')
                                {
                                    echo '<option value="Kasubbag Akademik & Mhs Fak. Hukum">Kasubbag Akademik Fak. Hukum</option>
                                        <option value="Kasubbag Akademik & Mhs Fak. Pertanian">Kasubbag Akademik Fak. Pertanian</option>
                                        <option value="Kasubbag Akademik & Mhs Fak. Ekonomi">Kasubbag Akademik Fak. Ekonomi</option>
                                        <option value="Kasubbag Akademik & Mhs Fak. Keguruan">Kasubbag Akademik Fak. Keguruan</option>
                                        <option value="Kasubbag Akademik & Mhs Fak. Teknik">Kasubbag Akademik Fak. Teknik</option>

                                        <option value="Kasubbag Umum & Keu Fak. HUKUM">Kasubbag Umum & Keu Fak. Hukum</option>
                                        <option value="Kasubbag Umum & Keu Fak. Pertanian">Kasubbag Umum & Keu Fak. Pertanian</option>
                                        <option value="Kasubbag Umum & Keu Fak. Ekonomi">Kasubbag Umum & Keu Fak. Ekonomi</option>
                                        <option value="Kasubbag Umum & Keu Fak. Keguruan">Kasubbag Umum & Keu Fak. Keguruan</option>
                                        <option selected value="Kasubbag Umum & Keu Fak. Teknik">Kasubbag Umum & Keu Fak. Teknik</option>';
                                }
                            ?>
                        </select>
                    </p>
                </div>
                <div class="col-md-6">
                    <label>EMAIL:</label>
                    <p>
                        <input
                            type="text"
                            name="kasubbag_email"
                            class="form-control"
                            value="<?php echo $d['kasubbag_email'] ?>"
                            required>
                    </p>
                </div>
                <div class="col-md-6">
                    <label>PILIH ATASAN::</label>
                    <p>
                        <select
                            name="kasubbag_uuid_kabag"
                            class="form-control">
                            <option selected value="<?php echo $d['kabag_uuid'] ?>"><?php echo $d['kabag_nama'] ?></option>
                            <?php
                                foreach($data_kabag->result_array() as $d)
                                {
                                    echo '<option value="'.$d['kabag_uuid'].'">'.$d['kabag_nama'].'</option>';
                                }
                            ?>
                        </select>
                    </p>
                </div>

            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-light-grey">
                Cancel
            </button>
            <button type="submit" class="btn btn-blue">
                Update
            </button>
        </div>
    </form>
</div>
<?php } ?>
