<?php 
    foreach($data_admin_surat->result_array() as $d){
?>
<div id="modalEditAdminSuratLabdas<?php echo $d['username'] ?>" class="modal fade" tabindex="-1" data-width="760" style="display: none;">
    <form role="form" action="<?php echo base_url() ?>index.php/super_admin/editAdminSuratLabdas" method="post" enctype="multipart/form-data" >
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                &times;
            </button>
            <h4 class="modal-title">EDIT DATA AKUN ADMIN PERSURATAN BAGIAN LAB DASAR</h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <label>Username:</label>
                    <p>
                        <input
                            type="text"
                            name="username"
                            class="form-control"
                            value="<?php echo $d['username'] ?>"
                            readonly>
                    </p>
                </div>
                <div class="col-md-12">
                    <label>Nama Lengkap:</label>
                    <p>
                        <input
                            type="text"
                            name="nama_lengkap"
                            class="form-control"
                            value="<?php echo $d['nama_lengkap'] ?>"
                            required>
                    </p>
                </div>
                <div class="col-md-12">
                    <label>Posisi / Jabatan:</label>
                    <p>
                        <input
                            type="text"
                            name="posisi"
                            class="form-control"
                            value="<?php echo $d['jabatan'] ?>"
                            required
                            readonly>
                    </p>
                </div>
                <div class="col-md-12">
                    <label>Atasan:</label>
                    <p>
                        <select
                            name="atasan"
                            class="form-control"
                            required>
                            <?php 
                                foreach($data_kasubbag->result_array() as $e)
                                {
                                    if($e['kasubbag_posisi']=="Ka Lab Dasar")
                                    {
                                        if($e['kasubbag_uuid']==$d['uuid_atasan'])
                                        {
                                            echo "<option selected value='".$e['kasubbag_uuid']."'>".$e['kasubbag_nama']." (".$e['kasubbag_posisi'].")</option>";
                                        }
                                        else
                                        {
                                            echo "<option value='".$e['kasubbag_uuid']."'>".$e['kasubbag_nama']." (".$e['kasubbag_posisi'].")</option>";
                                        }
                                    }
                                }
                            ?>
                        </select>
                    </p>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-light-grey">
                Cancel
            </button>
            <button type="submit" class="btn btn-blue">
                Update
            </button>
        </div>
    </form>
</div>
<?php } ?>