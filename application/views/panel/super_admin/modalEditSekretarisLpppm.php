<?php 
    foreach($data_sekretaris->result_array() as $d){
?>
<div id="modalEditSekretarisLpppm<?php echo $d['kabag_uuid'] ?>" class="modal fade" tabindex="-1" data-width="760" style="display: none;">
    <form role="form" action="<?php echo base_url() ?>index.php/super_admin/editSekretarisLpppm" method="post" enctype="multipart/form-data" >
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                &times;
            </button>
            <h4 class="modal-title">EDIT DATA SEKRETARIS LPPPM & PM</h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-6">
                    <label>USERNAME:</label>
                    <p>
                        <input name="kabag_uuid" type="hidden" value="<?php echo $d['kabag_uuid'] ?>">
                        <input
                            type="text"
                            name="kabag_username"
                            class="form-control"
                            value="<?php echo $d['kabag_username'] ?>"
                            required
                            readonly>
                    </p>
                </div>
                <div class="col-md-6">
                    <label>NAMA LENGKAP:</label>
                    <p>
                        <input
                            type="text"
                            name="kabag_nama"
                            class="form-control"
                            value="<?php echo $d['kabag_nama'] ?>"
                            required>
                    </p>
                </div>
                
                <div class="col-md-6">
                    <label>EMAIL:</label>
                    <p>
                        <input
                            type="text"
                            name="kabag_email"
                            class="form-control"
                            value="<?php echo $d['kabag_email'] ?>"
                            required>
                    </p>
                </div>
                <div class="col-md-6">
                    <label>JABATAN/POSISI:</label>
                    <p>
                        <input
                            type="text"
                            name="kabag_posisi"
                            class="form-control"
                            value="<?php echo $d['kabag_posisi'] ?>"
                            required
                            readonly>
                    </p>
                </div>
                <div class="col-md-6">
                    <label>PILIH ATASAN::</label>
                    <p>
                        <select
                            name="kabag_uuid_biro"
                            class="form-control"
                            required>
                            <option value="">Please Select</option>
                            <?php
                                foreach($data_kepala->result_array() as $e)
                                {
                                    if($e['biro_uuid'] == $d['kabag_uuid_biro'] )
                                    {
                                        echo '<option selected value="'.$e['biro_uuid'].'">'.$e['biro_nama'].'</option>';
                                    }
                                    else
                                    {
                                        echo '<option value="'.$e['biro_uuid'].'">'.$e['biro_nama'].'</option>';
                                    }
                                }
                            ?>
                        </select>
                    </p>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-light-grey">
                Cancel
            </button>
            <button type="submit" class="btn btn-blue">
                Update
            </button>
        </div>
    </form>
</div>
<?php } ?>
