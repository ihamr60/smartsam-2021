
<div id="modalTambahPelaksanaBiro" class="modal fade" tabindex="-1" data-width="760" style="display: none;">
    <form role="form" action="<?php echo base_url() ?>index.php/super_admin/tambahPelaksanaBiro" method="post" enctype="multipart/form-data" >
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                &times;
            </button>
            <h4 class="modal-title">TAMBAH DATA PELAKSANA</h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <label>Username:</label>
                    <p>
                        <input
                            type="text"
                            name="username"
                            class="form-control"
                            placeholder="Ex: pelaksana221"
                            required>
                    </p>
                </div>
                <div class="col-md-12">
                    <label>Nama Lengkap:</label>
                    <p>
                        <input
                            type="text"
                            name="nama_lengkap"
                            class="form-control"
                            placeholder="Ex: Ilham Ramadhan, S.Tr.Kom"
                            required>
                    </p>
                </div>
                <div class="col-md-12">
                    <label>Jabatan / Posisi:</label>
                    <p>
                        <input
                            type="text"
                            name="posisi"
                            class="form-control"
                            placeholder="Ex: Pengolah Barang Milik Negara"
                            required>
                    </p>
                </div>

                <div class="col-md-12">
                    <label>Atasan Pelaksana:</label>
                    <p>
                        <select
                            name="atasan"
                            class="form-control">
                            <option value="">Pilih Atasan</option>
                            <?php 
                            foreach($data_kasubbag->result_array() as $d)
                            {
                                echo "<option style='text-transform: uppercase;' value='".$d['kasubbag_uuid']."'>".$d['kasubbag_nama']." / (".$d['kasubbag_posisi'].")</option>";
                            }
                            ?>
                        </select>
                    </p>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-light-grey">
                Cancel
            </button>
            <button type="submit" class="btn btn-blue">
                Tambahkan
            </button>
        </div>
    </form>
</div>
