<?php
foreach($data_admin_surat->result_array() as $d)
{
?>
<div id="modalEditAdminSuratFak<?php echo $d['username'] ?>" class="modal fade" tabindex="-1" data-width="760" style="display: none;">
    <form role="form" action="<?php echo base_url() ?>index.php/super_admin/editAdminSuratFak" method="post" enctype="multipart/form-data" >
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                &times;
            </button>
            <h4 class="modal-title">EDIT DATA AKUN ADMIN PERSURATAN FAKULTAS</h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <label>Username:</label>
                    <p>
                        <input
                            type="text"
                            name="username"
                            class="form-control"
                            value="<?php echo $d['username'] ?>"
                            maxlength="16"
                            required
                            readonly>
                    </p>
                </div>
                <div class="col-md-12">
                    <label>Nama Lengkap:</label>
                    <p>
                        <input
                            type="text"
                            name="nama_lengkap"
                            class="form-control"
                            value="<?php echo $d['nama_lengkap'] ?>"
                            required>
                    </p>
                </div>
                <div class="col-md-12">
                    <label>Posisi / Jabatan:</label>
                    <p>
                        
                        <select
                            name="posisi"
                            class="form-control"
                            required>
                            <?php 
                                if($d['jabatan'] == 'Admin Surat Fak. HUKUM')
                                {
                                    echo '<option selected value="Admin Surat Fak. HUKUM">Admin Surat Fak. Hukum</option>
                                        <option value="Admin Surat Fak. Pertanian">Admin Surat Fak. Pertanian</option>
                                        <option value="Admin Surat Fak. Ekonomi">Admin Surat Fak. Ekonomi</option>
                                        <option value="Admin Surat Fak. Keguruan">Admin Surat Fak. Keguruan</option>
                                        <option value="Admin Surat Fak. Teknik">Admin Surat Fak. Teknik</option>';
                                }
                                else if($d['jabatan'] == 'Admin Surat Fak. Pertanian')
                                {
                                    echo '<option value="Admin Surat Fak. HUKUM">Admin Surat Fak. Hukum</option>
                                        <option selected value="Admin Surat Fak. Pertanian">Admin Surat Fak. Pertanian</option>
                                        <option value="Admin Surat Fak. Ekonomi">Admin Surat Fak. Ekonomi</option>
                                        <option value="Admin Surat Fak. Keguruan">Admin Surat Fak. Keguruan</option>
                                        <option value="Admin Surat Fak. Teknik">Admin Surat Fak. Teknik</option>';
                                }
                                else if($d['jabatan'] == 'Admin Surat Fak. Ekonomi')
                                {
                                    echo '<option value="Admin Surat Fak. HUKUM">Admin Surat Fak. Hukum</option>
                                        <option value="Admin Surat Fak. Pertanian">Admin Surat Fak. Pertanian</option>
                                        <option selected value="Admin Surat Fak. Ekonomi">Admin Surat Fak. Ekonomi</option>
                                        <option value="Admin Surat Fak. Keguruan">Admin Surat Fak. Keguruan</option>
                                        <option value="Admin Surat Fak. Teknik">Admin Surat Fak. Teknik</option>';
                                }
                                else if($d['jabatan'] == 'Admin Surat Fak. Keguruan')
                                {
                                    echo '<option value="Admin Surat Fak. HUKUM">Admin Surat Fak. Hukum</option>
                                        <option value="Admin Surat Fak. Pertanian">Admin Surat Fak. Pertanian</option>
                                        <option value="Admin Surat Fak. Ekonomi">Admin Surat Fak. Ekonomi</option>
                                        <option selected value="Admin Surat Fak. Keguruan">Admin Surat Fak. Keguruan</option>
                                        <option value="Admin Surat Fak. Teknik">Admin Surat Fak. Teknik</option>';
                                }
                                else if($d['jabatan'] == 'Admin Surat Fak. Teknik')
                                {
                                    echo '<option value="Admin Surat Fak. HUKUM">Admin Surat Fak. Hukum</option>
                                        <option value="Admin Surat Fak. Pertanian">Admin Surat Fak. Pertanian</option>
                                        <option value="Admin Surat Fak. Ekonomi">Admin Surat Fak. Ekonomi</option>
                                        <option value="Admin Surat Fak. Keguruan">Admin Surat Fak. Keguruan</option>
                                        <option selected value="Admin Surat Fak. Teknik">Admin Surat Fak. Teknik</option>';
                                }
                            ?>
                        </select>
                    </p>
                </div>
                <div class="col-md-12">
                    <label>Atasan:</label>
                    <p>
                        <select
                            name="atasan"
                            class="form-control"
                            required>
                            <?php 
                                foreach($data_kasubbag->result_array() as $e)
                                {
                                    if($d['uuid_atasan'] == $e['kasubbag_uuid'])
                                    {
                                        echo "<option selected value='".$e['kasubbag_uuid']."'>".$e['kasubbag_nama']." (".$e['kasubbag_posisi'].")</option>";
                                    }  
                                    else{
                                        echo "<option value='".$e['kasubbag_uuid']."'>".$e['kasubbag_nama']." (".$e['kasubbag_posisi'].")</option>";
                                    }
                                    
                                }
                            ?>
                        </select>
                    </p>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-light-grey">
                Cancel
            </button>
            <button type="submit" class="btn btn-blue">
                Update
            </button>
        </div>
    </form>
</div>
<?php } ?>