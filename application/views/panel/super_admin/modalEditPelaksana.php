<?php 
    foreach($data_pelaksana->result_array() as $d){
?>
<div id="modalEditPelaksana<?php echo $d['staff_uuid'] ?>" class="modal fade" tabindex="-1" data-width="760" style="display: none;">
    <form role="form" action="<?php echo base_url() ?>index.php/super_admin/editPelaksana" method="post" enctype="multipart/form-data" >
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                &times;
            </button>
            <h4 class="modal-title">EDIT DATA AKUN PELAKSANA</h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <label>Username:</label>
                    <p>
                        <input name="staff_uuid" type="hidden" value="<?php echo $d['staff_uuid'] ?>">
                        <input
                            type="text"
                            name="username"
                            class="form-control"
                            value="<?php echo $d['staff_username'] ?>"
                            required
                            readonly>
                    </p>
                </div>
                <div class="col-md-12">
                    <label>Nama Lengkap:</label>
                    <p>
                        <input
                            type="text"
                            name="nama_lengkap"
                            class="form-control"
                            value="<?php echo $d['staff_nama'] ?>"
                            required>
                    </p>
                </div>
                <div class="col-md-12">
                    <label>Jabatan / Posisi:</label>
                    <p>
                        <input
                            type="text"
                            name="posisi"
                            class="form-control"
                            value="<?php echo $d['staff_posisi'] ?>"
                            required>
                    </p>
                </div>
                <div class="col-md-12">
                    <label>Atasan Pelaksana:</label>
                    <p>
                        <select
                            name="atasan"
                            class="form-control">
                            <?php 
                                foreach($data_kasubbag->result_array() as $e)
                                {
                                    if($d['staff_uuid_kasubbag'] == $e['kasubbag_uuid'])
                                    {
                                        echo "<option style='text-transform: uppercase;' selected value=".$e['kasubbag_uuid'].">".$e['kasubbag_nama']." / (".$e['kasubbag_posisi'].")</option>";
                                    }
                                    else
                                    {
                                        echo "<option style='text-transform: uppercase;' value=".$e['kasubbag_uuid'].">".$e['kasubbag_nama']." / (".$e['kasubbag_posisi'].")</option>";
                                    }
                                }
                            ?>
                        </select>
                    </p>
                </div>

            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-light-grey">
                Cancel
            </button>
            <button type="submit" class="btn btn-blue">
                Tambahkan
            </button>
        </div>
    </form>
</div>
<?php } ?>
