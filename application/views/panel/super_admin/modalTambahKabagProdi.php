
<div id="modalTambahKabagProdi" class="modal fade" tabindex="-1" data-width="760" style="display: none;">
    <form role="form" action="<?php echo base_url() ?>index.php/super_admin/tambahKabagProdi" method="post" enctype="multipart/form-data" >
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                &times;
            </button>
            <h4 class="modal-title">TAMBAH DATA KABAG / KAPRODI</h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-6">
                    <label>USERNAME:</label>
                    <p>
                        <input
                            type="text"
                            name="kabag_username"
                            class="form-control"
                            placeholder="Ex: ismail232"
                            required>
                    </p>
                </div>
                <div class="col-md-6">
                    <label>NAMA KABAG / KAPRODI:</label>
                    <p>
                        <input
                            type="text"
                            name="kabag_nama"
                            class="form-control"
                            placeholder="Ex: Ismail, M.T"
                            required>
                    </p>
                </div>
                <div class="col-md-6">
                    <label>JABATAN/POSISI:</label>
                    <p>
                        <select
                            name="kabag_posisi"
                            class="form-control">
                            <option value=""> Please Select </option>
                            <option value="Kabag TU Fak. Hukum">Kabag TU Fak. Hukum</option>
                            <option value="Kabag TU Fak. Pertanian">Kabag TU Fak. Pertanian</option>
                            <option value="Kabag TU Fak. Ekonomi">Kabag TU Fak. Ekonomi</option>
                            <option value="Kabag TU Fak. Keguruan">Kabag TU Fak. Keguruan</option>
                            <option value="Kabag TU Fak. Teknik">Kabag TU Fak. Teknik</option>
                            <?php
                                foreach($data_prodi->result_array() as $d)
                                {
                                    echo '<option value="'.$d['prodi_nama'].'">'.$d['prodi_nama'].'</option>';
                                }
                            ?>
                        </select>
                    </p>
                </div>
                <!--<div class="col-md-6">
                    <label>PILIH ATASAN::</label>
                    <p>
                        <select
                            name="kabag_uuid_biro"
                            class="form-control">
                            <?php
                                foreach($data_dekan->result_array() as $d)
                                {
                                    echo '<option value="'.$d['biro_uuid'].'">'.$d['biro_nama'].'</option>';
                                }
                            ?>
                        </select>
                    </p>
                </div>-->
                <div class="col-md-6">
                    <label>EMAIL :</label>
                    <p>
                        <input
                            type="text"
                            name="kabag_email"
                            class="form-control"
                            placeholder="Ex: ilhamr6000@gmail.com"
                            required>
                    </p>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-light-grey">
                Cancel
            </button>
            <button type="submit" class="btn btn-blue">
                Tambahkan
            </button>
        </div>
    </form>
</div>
