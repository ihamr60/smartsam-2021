
<div id="modalTambahKasubbag" class="modal fade" tabindex="-1" data-width="760" style="display: none;">
    <form role="form" action="<?php echo base_url() ?>index.php/super_admin/tambahKasubbag" method="post" enctype="multipart/form-data" >
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                &times;
            </button>
            <h4 class="modal-title">TAMBAH DATA KASUBBAG</h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <label>Username:</label>
                    <p>
                        <input
                            type="text"
                            name="username"
                            class="form-control"
                            placeholder="Ex: kasubbag223"
                            required>
                    </p>
                </div>
                <div class="col-md-12">
                    <label>Nama Lengkap:</label>
                    <p>
                        <input
                            type="text"
                            name="nama_lengkap"
                            class="form-control"
                            placeholder="Ex: Ernawaty, S.H."
                            required>
                    </p>
                </div>
                <div class="col-md-12">
                    <label>Posisi / Jabatan:</label>
                    <p>
                        <select
                            name="posisi"
                            class="form-control">
                            <option value="Kasubbag Perbendaharaan">Kasubbag Perbendaharaan</option>
                            <option value="Kasubbag Kerjasama & Humas">Kasubbag Kerjasama & Humas</option>
                            <option value="Kasubbag Akuntansi & Pelaporan">Kasubbag Akuntansi & Pelaporan</option>
                            <option value="Kasubbag TU & Ketatalaksanaan">Kasubbag TU & Ketatalaksanaan</option>
                            <option value="Kasubbag RT & BM">Kasubbag RT & BM</option>
                            <option value="Kasubbag Kepegawaian">Kasubbag Kepegawaian</option>
                            <option value="Kasubbag Pendidikan & Evaluasi">Kasubbag Pendidikan & Evaluasi</option>
                            <option value="Kasubbag Registrasi & Statistik">Kasubbag Registrasi & Statistik</option>
                            <option value="Kasubbag Kemahasiswaan">Kasubbag Kemahasiswaan</option>
                            <option value="Kasubbag Perencanaan">Kasubbag Perencanaan</option>
                        </select>
                    </p>
                </div>

                <div class="col-md-12">
                    <label>Atasan Kasubbag:</label>
                    <p>
                        <select
                            name="atasan"
                            class="form-control">
                            <option value="">Pilih Atasan</option>
                            <?php 
                            foreach($data_kabag->result_array() as $d)
                            {
                                echo "<option value='".$d['kabag_uuid']."'>".$d['kabag_nama']." / (".$d['kabag_posisi'].")</option>";
                            }
                            ?>
                        </select>
                    </p>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-light-grey">
                Cancel
            </button>
            <button type="submit" class="btn btn-blue">
                Tambahkan
            </button>
        </div>
    </form>
</div>
