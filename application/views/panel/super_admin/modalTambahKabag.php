
<div id="modalTambahKabag" class="modal fade" tabindex="-1" data-width="760" style="display: none;">
    <form role="form" action="<?php echo base_url() ?>index.php/super_admin/tambahKabag" method="post" enctype="multipart/form-data" >
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                &times;
            </button>
            <h4 class="modal-title">TAMBAH DATA KABAG</h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <label>Username:</label>
                    <p>
                        <input
                            type="text"
                            name="username"
                            class="form-control"
                            placeholder="Ex: kabag363"
                            required>
                    </p>
                </div>
                <div class="col-md-12">
                    <label>Nama Lengkap:</label>
                    <p>
                        <input
                            type="text"
                            name="nama_lengkap"
                            class="form-control"
                            placeholder="Ex: Zulfan, S.H., M.Si."
                            required>
                    </p>
                </div>
                <div class="col-md-12">
                    <label>Posisi / Jabatan Kabag:</label>
                    <p>
                        <select
                            name="posisi"
                            class="form-control">
                            <option value="Kepala Bagian Akademik dan Kemahasiswaan">Kepala Bagian Akademik dan Kemahasiswaan</option>
                            <option value="Kepala Bagian Perencanaan dan Kerja sama">Kepala Bagian Perencanaan dan Kerja sama</option>
                            <option value="Kepala Bagian Keuangan">Kepala Bagian Keuangan</option>
                            <option value="Kepala Bagian Umum">Kepala Bagian Umum</option>
                        </select>
                    </p>
                </div>
                <div class="col-md-12">
                    <label>Atasan Kabag:</label>
                    <p>
                        <select
                            name="atasan"
                            class="form-control">
                            <option value="">Pilih Atasan</option>
                            <?php 
                                foreach($data_biro->result_array() as $d)
                                {
                                    echo "<option value='".$d['biro_uuid']."'>".$d['biro_nama']." / (".$d['biro_posisi'].")</option>";
                                }
                            ?>
                        </select>
                    </p>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-light-grey">
                Cancel
            </button>
            <button type="submit" class="btn btn-blue">
                Tambahkan
            </button>
        </div>
    </form>
</div>
