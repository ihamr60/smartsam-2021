<?php 
    foreach($data_dekan->result_array() as $d){
?>
<div id="modalEditDekan<?php echo $d['biro_uuid'] ?>" class="modal fade" tabindex="-1" data-width="760" style="display: none;">
    <form role="form" action="<?php echo base_url() ?>index.php/super_admin/editDekan" method="post" enctype="multipart/form-data" >
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                &times;
            </button>
            <h4 class="modal-title">EDIT DEKAN FAKULTAS</h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-6">
                    <label>USERNAME:</label>
                    <p>
                        <input type="hidden" value="<?php echo $d['biro_uuid'] ?>" name="biro_uuid">
                        <input
                            type="text"
                            name="biro_username"
                            class="form-control"
                            value="<?php echo $d['biro_username'] ?>"
                            required
                            readonly>
                    </p>
                </div>
                <div class="col-md-6">
                    <label>NAMA DEKAN:</label>
                    <p>
                        <input
                            type="text"
                            name="biro_nama"
                            class="form-control"
                            value="<?php echo $d['biro_nama'] ?>"
                            required>
                    </p>
                </div>
                <div class="col-md-6">
                    <label>JABATAN/POSISI:</label>
                    <p>
                        <select
                            name="biro_posisi"
                            class="form-control">
                            <?php 
                                if($d['biro_posisi'] == 'Dekan Fak. Hukum')
                                {
                                    echo '<option selected value="Dekan Fak. Hukum"> Dekan Fakultas Hukum</option>
                                            <option value="Dekan Fak. Ekonomi"> Dekan Fakultas Ekonomi</option>
                                            <option value="Dekan Fak. Pertanian"> Dekan Fakultas Pertanian</option>
                                            <option value="Dekan Fak. Keguruan"> Dekan Fakultas Keguruan</option>
                                            <option value="Dekan Fak. Teknik"> Dekan Fakultas Teknik</option>
                                            <option value="Wakil Dekan 1 Fak. Hukum"> Wakil Dekan 1 Fakultas Hukum </option>
                                            <option value="Wakil Dekan 1 Fak. Ekonomi"> Wakil Dekan 1 Fakultas Ekonomi </option>
                                            <option value="Wakil Dekan 1 Fak. Pertanian"> Wakil Dekan 1 Fakultas Pertanian </option>
                                            <option value="Wakil Dekan 1 Fak. Keguruan"> Wakil Dekan 1 Fakultas Keguruan </option>
                                            <option value="Wakil Dekan 1 Fak. Teknik"> Wakil Dekan 1 Fakultas Teknik </option>
                                            <option value="Wakil Dekan 2 Fak. Hukum"> Wakil Dekan 2 Fakultas Hukum </option>
                                            <option value="Wakil Dekan 2 Fak. Ekonomi"> Wakil Dekan 2 Fakultas Ekonomi </option>
                                            <option value="Wakil Dekan 2 Fak. Pertanian"> Wakil Dekan 2 Fakultas Pertanian </option>
                                            <option value="Wakil Dekan 2 Fak. Keguruan"> Wakil Dekan 2 Fakultas Keguruan </option>
                                            <option value="Wakil Dekan 2 Fak. Teknik"> Wakil Dekan 2 Fakultas Teknik </option>';
                                }
                                else if($d['biro_posisi'] == 'Dekan Fak. Ekonomi')
                                {
                                    echo '<option value="Dekan Fak. Hukum"> Dekan Fakultas Hukum</option>
                                            <option selected value="Dekan Fak. Ekonomi"> Dekan Fakultas Ekonomi</option>
                                            <option value="Dekan Fak. Pertanian"> Dekan Fakultas Pertanian</option>
                                            <option value="Dekan Fak. Keguruan"> Dekan Fakultas Keguruan</option>
                                            <option value="Dekan Fak. Teknik"> Dekan Fakultas Teknik</option>
                                            <option value="Wakil Dekan 1 Fak. Hukum"> Wakil Dekan 1 Fakultas Hukum </option>
                                            <option value="Wakil Dekan 1 Fak. Ekonomi"> Wakil Dekan 1 Fakultas Ekonomi </option>
                                            <option value="Wakil Dekan 1 Fak. Pertanian"> Wakil Dekan 1 Fakultas Pertanian </option>
                                            <option value="Wakil Dekan 1 Fak. Keguruan"> Wakil Dekan 1 Fakultas Keguruan </option>
                                            <option value="Wakil Dekan 1 Fak. Teknik"> Wakil Dekan 1 Fakultas Teknik </option>
                                            <option value="Wakil Dekan 2 Fak. Hukum"> Wakil Dekan 2 Fakultas Hukum </option>
                                            <option value="Wakil Dekan 2 Fak. Ekonomi"> Wakil Dekan 2 Fakultas Ekonomi </option>
                                            <option value="Wakil Dekan 2 Fak. Pertanian"> Wakil Dekan 2 Fakultas Pertanian </option>
                                            <option value="Wakil Dekan 2 Fak. Keguruan"> Wakil Dekan 2 Fakultas Keguruan </option>
                                            <option value="Wakil Dekan 2 Fak. Teknik"> Wakil Dekan 2 Fakultas Teknik </option>';
                                }
                                else if($d['biro_posisi'] == 'Dekan Fak. Pertanian')
                                {
                                    echo '<option value="Dekan Fak. Hukum"> Dekan Fakultas Hukum</option>
                                            <option value="Dekan Fak. Ekonomi"> Dekan Fakultas Ekonomi</option>
                                            <option selected value="Dekan Fak. Pertanian"> Dekan Fakultas Pertanian</option>
                                            <option value="Dekan Fak. Keguruan"> Dekan Fakultas Keguruan</option>
                                            <option value="Dekan Fak. Teknik"> Dekan Fakultas Teknik</option>
                                            <option value="Wakil Dekan 1 Fak. Hukum"> Wakil Dekan 1 Fakultas Hukum </option>
                                            <option value="Wakil Dekan 1 Fak. Ekonomi"> Wakil Dekan 1 Fakultas Ekonomi </option>
                                            <option value="Wakil Dekan 1 Fak. Pertanian"> Wakil Dekan 1 Fakultas Pertanian </option>
                                            <option value="Wakil Dekan 1 Fak. Keguruan"> Wakil Dekan 1 Fakultas Keguruan </option>
                                            <option value="Wakil Dekan 1 Fak. Teknik"> Wakil Dekan 1 Fakultas Teknik </option>
                                            <option value="Wakil Dekan 2 Fak. Hukum"> Wakil Dekan 2 Fakultas Hukum </option>
                                            <option value="Wakil Dekan 2 Fak. Ekonomi"> Wakil Dekan 2 Fakultas Ekonomi </option>
                                            <option value="Wakil Dekan 2 Fak. Pertanian"> Wakil Dekan 2 Fakultas Pertanian </option>
                                            <option value="Wakil Dekan 2 Fak. Keguruan"> Wakil Dekan 2 Fakultas Keguruan </option>
                                            <option value="Wakil Dekan 2 Fak. Teknik"> Wakil Dekan 2 Fakultas Teknik </option>';
                                }

                                else if($d['biro_posisi'] == 'Dekan Fak. Keguruan')
                                {
                                    echo '<option value="Dekan Fak. Hukum"> Dekan Fakultas Hukum</option>
                                            <option value="Dekan Fak. Ekonomi"> Dekan Fakultas Ekonomi</option>
                                            <option value="Dekan Fak. Pertanian"> Dekan Fakultas Pertanian</option>
                                            <option selected value="Dekan Fak. Keguruan"> Dekan Fakultas Keguruan</option>
                                            <option value="Dekan Fak. Teknik"> Dekan Fakultas Teknik</option>
                                            <option value="Wakil Dekan 1 Fak. Hukum"> Wakil Dekan 1 Fakultas Hukum </option>
                                            <option value="Wakil Dekan 1 Fak. Ekonomi"> Wakil Dekan 1 Fakultas Ekonomi </option>
                                            <option value="Wakil Dekan 1 Fak. Pertanian"> Wakil Dekan 1 Fakultas Pertanian </option>
                                            <option value="Wakil Dekan 1 Fak. Keguruan"> Wakil Dekan 1 Fakultas Keguruan </option>
                                            <option value="Wakil Dekan 1 Fak. Teknik"> Wakil Dekan 1 Fakultas Teknik </option>
                                            <option value="Wakil Dekan 2 Fak. Hukum"> Wakil Dekan 2 Fakultas Hukum </option>
                                            <option value="Wakil Dekan 2 Fak. Ekonomi"> Wakil Dekan 2 Fakultas Ekonomi </option>
                                            <option value="Wakil Dekan 2 Fak. Pertanian"> Wakil Dekan 2 Fakultas Pertanian </option>
                                            <option value="Wakil Dekan 2 Fak. Keguruan"> Wakil Dekan 2 Fakultas Keguruan </option>
                                            <option value="Wakil Dekan 2 Fak. Teknik"> Wakil Dekan 2 Fakultas Teknik </option>';
                                }

                                else if($d['biro_posisi'] == 'Dekan Fak. Teknik')
                                {
                                    echo '<option value="Dekan Fak. Hukum"> Dekan Fakultas Hukum</option>
                                            <option value="Dekan Fak. Ekonomi"> Dekan Fakultas Ekonomi</option>
                                            <option value="Dekan Fak. Pertanian"> Dekan Fakultas Pertanian</option>
                                            <option value="Dekan Fak. Keguruan"> Dekan Fakultas Keguruan</option>
                                            <option selected value="Dekan Fak. Teknik"> Dekan Fakultas Teknik</option>
                                            <option value="Wakil Dekan 1 Fak. Hukum"> Wakil Dekan 1 Fakultas Hukum </option>
                                            <option value="Wakil Dekan 1 Fak. Ekonomi"> Wakil Dekan 1 Fakultas Ekonomi </option>
                                            <option value="Wakil Dekan 1 Fak. Pertanian"> Wakil Dekan 1 Fakultas Pertanian </option>
                                            <option value="Wakil Dekan 1 Fak. Keguruan"> Wakil Dekan 1 Fakultas Keguruan </option>
                                            <option value="Wakil Dekan 1 Fak. Teknik"> Wakil Dekan 1 Fakultas Teknik </option>
                                            <option value="Wakil Dekan 2 Fak. Hukum"> Wakil Dekan 2 Fakultas Hukum </option>
                                            <option value="Wakil Dekan 2 Fak. Ekonomi"> Wakil Dekan 2 Fakultas Ekonomi </option>
                                            <option value="Wakil Dekan 2 Fak. Pertanian"> Wakil Dekan 2 Fakultas Pertanian </option>
                                            <option value="Wakil Dekan 2 Fak. Keguruan"> Wakil Dekan 2 Fakultas Keguruan </option>
                                            <option value="Wakil Dekan 2 Fak. Teknik"> Wakil Dekan 2 Fakultas Teknik </option>';
                                }
                                else if($d['biro_posisi'] == 'Wakil Dekan 1 Fak. Hukum')
                                {
                                    echo '<option value="Dekan Fak. Hukum"> Dekan Fakultas Hukum</option>
                                            <option value="Dekan Fak. Ekonomi"> Dekan Fakultas Ekonomi</option>
                                            <option value="Dekan Fak. Pertanian"> Dekan Fakultas Pertanian</option>
                                            <option value="Dekan Fak. Keguruan"> Dekan Fakultas Keguruan</option>
                                            <option value="Dekan Fak. Teknik"> Dekan Fakultas Teknik</option>
                                            <option selected value="Wakil Dekan 1 Fak. Hukum"> Wakil Dekan 1 Fakultas Hukum </option>
                                            <option value="Wakil Dekan 1 Fak. Ekonomi"> Wakil Dekan 1 Fakultas Ekonomi </option>
                                            <option value="Wakil Dekan 1 Fak. Pertanian"> Wakil Dekan 1 Fakultas Pertanian </option>
                                            <option value="Wakil Dekan 1 Fak. Keguruan"> Wakil Dekan 1 Fakultas Keguruan </option>
                                            <option value="Wakil Dekan 1 Fak. Teknik"> Wakil Dekan 1 Fakultas Teknik </option>
                                            <option value="Wakil Dekan 2 Fak. Hukum"> Wakil Dekan 2 Fakultas Hukum </option>
                                            <option value="Wakil Dekan 2 Fak. Ekonomi"> Wakil Dekan 2 Fakultas Ekonomi </option>
                                            <option value="Wakil Dekan 2 Fak. Pertanian"> Wakil Dekan 2 Fakultas Pertanian </option>
                                            <option value="Wakil Dekan 2 Fak. Keguruan"> Wakil Dekan 2 Fakultas Keguruan </option>
                                            <option value="Wakil Dekan 2 Fak. Teknik"> Wakil Dekan 2 Fakultas Teknik </option>';
                                }
                                else if($d['biro_posisi'] == 'Wakil Dekan 1 Fak. Ekonomi')
                                {
                                    echo '<option value="Dekan Fak. Hukum"> Dekan Fakultas Hukum</option>
                                            <option value="Dekan Fak. Ekonomi"> Dekan Fakultas Ekonomi</option>
                                            <option value="Dekan Fak. Pertanian"> Dekan Fakultas Pertanian</option>
                                            <option value="Dekan Fak. Keguruan"> Dekan Fakultas Keguruan</option>
                                            <option value="Dekan Fak. Teknik"> Dekan Fakultas Teknik</option>
                                            <option value="Wakil Dekan 1 Fak. Hukum"> Wakil Dekan 1 Fakultas Hukum </option>
                                            <option selected value="Wakil Dekan 1 Fak. Ekonomi"> Wakil Dekan 1 Fakultas Ekonomi </option>
                                            <option value="Wakil Dekan 1 Fak. Pertanian"> Wakil Dekan 1 Fakultas Pertanian </option>
                                            <option value="Wakil Dekan 1 Fak. Keguruan"> Wakil Dekan 1 Fakultas Keguruan </option>
                                            <option value="Wakil Dekan 1 Fak. Teknik"> Wakil Dekan 1 Fakultas Teknik </option>
                                            <option value="Wakil Dekan 2 Fak. Hukum"> Wakil Dekan 2 Fakultas Hukum </option>
                                            <option value="Wakil Dekan 2 Fak. Ekonomi"> Wakil Dekan 2 Fakultas Ekonomi </option>
                                            <option value="Wakil Dekan 2 Fak. Pertanian"> Wakil Dekan 2 Fakultas Pertanian </option>
                                            <option value="Wakil Dekan 2 Fak. Keguruan"> Wakil Dekan 2 Fakultas Keguruan </option>
                                            <option value="Wakil Dekan 2 Fak. Teknik"> Wakil Dekan 2 Fakultas Teknik </option>';
                                }
                                else if($d['biro_posisi'] == 'Wakil Dekan 1 Fak. Pertanian')
                                {
                                    echo '<option value="Dekan Fak. Hukum"> Dekan Fakultas Hukum</option>
                                            <option value="Dekan Fak. Ekonomi"> Dekan Fakultas Ekonomi</option>
                                            <option value="Dekan Fak. Pertanian"> Dekan Fakultas Pertanian</option>
                                            <option value="Dekan Fak. Keguruan"> Dekan Fakultas Keguruan</option>
                                            <option value="Dekan Fak. Teknik"> Dekan Fakultas Teknik</option>
                                            <option value="Wakil Dekan 1 Fak. Hukum"> Wakil Dekan 1 Fakultas Hukum </option>
                                            <option value="Wakil Dekan 1 Fak. Ekonomi"> Wakil Dekan 1 Fakultas Ekonomi </option>
                                            <option selected value="Wakil Dekan 1 Fak. Pertanian"> Wakil Dekan 1 Fakultas Pertanian </option>
                                            <option value="Wakil Dekan 1 Fak. Keguruan"> Wakil Dekan 1 Fakultas Keguruan </option>
                                            <option value="Wakil Dekan 1 Fak. Teknik"> Wakil Dekan 1 Fakultas Teknik </option>
                                            <option value="Wakil Dekan 2 Fak. Hukum"> Wakil Dekan 2 Fakultas Hukum </option>
                                            <option value="Wakil Dekan 2 Fak. Ekonomi"> Wakil Dekan 2 Fakultas Ekonomi </option>
                                            <option value="Wakil Dekan 2 Fak. Pertanian"> Wakil Dekan 2 Fakultas Pertanian </option>
                                            <option value="Wakil Dekan 2 Fak. Keguruan"> Wakil Dekan 2 Fakultas Keguruan </option>
                                            <option value="Wakil Dekan 2 Fak. Teknik"> Wakil Dekan 2 Fakultas Teknik </option>';
                                }
                                else if($d['biro_posisi'] == 'Wakil Dekan 1 Fak. Keguruan')
                                {
                                    echo '<option value="Dekan Fak. Hukum"> Dekan Fakultas Hukum</option>
                                            <option value="Dekan Fak. Ekonomi"> Dekan Fakultas Ekonomi</option>
                                            <option value="Dekan Fak. Pertanian"> Dekan Fakultas Pertanian</option>
                                            <option value="Dekan Fak. Keguruan"> Dekan Fakultas Keguruan</option>
                                            <option value="Dekan Fak. Teknik"> Dekan Fakultas Teknik</option>
                                            <option value="Wakil Dekan 1 Fak. Hukum"> Wakil Dekan 1 Fakultas Hukum </option>
                                            <option value="Wakil Dekan 1 Fak. Ekonomi"> Wakil Dekan 1 Fakultas Ekonomi </option>
                                            <option value="Wakil Dekan 1 Fak. Pertanian"> Wakil Dekan 1 Fakultas Pertanian </option>
                                            <option selected value="Wakil Dekan 1 Fak. Keguruan"> Wakil Dekan 1 Fakultas Keguruan </option>
                                            <option value="Wakil Dekan 1 Fak. Teknik"> Wakil Dekan 1 Fakultas Teknik </option>
                                            <option value="Wakil Dekan 2 Fak. Hukum"> Wakil Dekan 2 Fakultas Hukum </option>
                                            <option value="Wakil Dekan 2 Fak. Ekonomi"> Wakil Dekan 2 Fakultas Ekonomi </option>
                                            <option value="Wakil Dekan 2 Fak. Pertanian"> Wakil Dekan 2 Fakultas Pertanian </option>
                                            <option value="Wakil Dekan 2 Fak. Keguruan"> Wakil Dekan 2 Fakultas Keguruan </option>
                                            <option value="Wakil Dekan 2 Fak. Teknik"> Wakil Dekan 2 Fakultas Teknik </option>';
                                }
                                else if($d['biro_posisi'] == 'Wakil Dekan 1 Fak. Teknik')
                                {
                                    echo '<option value="Dekan Fak. Hukum"> Dekan Fakultas Hukum</option>
                                            <option value="Dekan Fak. Ekonomi"> Dekan Fakultas Ekonomi</option>
                                            <option value="Dekan Fak. Pertanian"> Dekan Fakultas Pertanian</option>
                                            <option value="Dekan Fak. Keguruan"> Dekan Fakultas Keguruan</option>
                                            <option value="Dekan Fak. Teknik"> Dekan Fakultas Teknik</option>
                                            <option value="Wakil Dekan 1 Fak. Hukum"> Wakil Dekan 1 Fakultas Hukum </option>
                                            <option value="Wakil Dekan 1 Fak. Ekonomi"> Wakil Dekan 1 Fakultas Ekonomi </option>
                                            <option value="Wakil Dekan 1 Fak. Pertanian"> Wakil Dekan 1 Fakultas Pertanian </option>
                                            <option value="Wakil Dekan 1 Fak. Keguruan"> Wakil Dekan 1 Fakultas Keguruan </option>
                                            <option selected value="Wakil Dekan 1 Fak. Teknik"> Wakil Dekan 1 Fakultas Teknik </option>
                                            <option value="Wakil Dekan 2 Fak. Hukum"> Wakil Dekan 2 Fakultas Hukum </option>
                                            <option value="Wakil Dekan 2 Fak. Ekonomi"> Wakil Dekan 2 Fakultas Ekonomi </option>
                                            <option value="Wakil Dekan 2 Fak. Pertanian"> Wakil Dekan 2 Fakultas Pertanian </option>
                                            <option value="Wakil Dekan 2 Fak. Keguruan"> Wakil Dekan 2 Fakultas Keguruan </option>
                                            <option value="Wakil Dekan 2 Fak. Teknik"> Wakil Dekan 2 Fakultas Teknik </option>';
                                }
                                else if($d['biro_posisi'] == 'Wakil Dekan 2 Fak. Hukum')
                                {
                                    echo '<option value="Dekan Fak. Hukum"> Dekan Fakultas Hukum</option>
                                            <option value="Dekan Fak. Ekonomi"> Dekan Fakultas Ekonomi</option>
                                            <option value="Dekan Fak. Pertanian"> Dekan Fakultas Pertanian</option>
                                            <option value="Dekan Fak. Keguruan"> Dekan Fakultas Keguruan</option>
                                            <option value="Dekan Fak. Teknik"> Dekan Fakultas Teknik</option>
                                            <option value="Wakil Dekan 1 Fak. Hukum"> Wakil Dekan 1 Fakultas Hukum </option>
                                            <option value="Wakil Dekan 1 Fak. Ekonomi"> Wakil Dekan 1 Fakultas Ekonomi </option>
                                            <option value="Wakil Dekan 1 Fak. Pertanian"> Wakil Dekan 1 Fakultas Pertanian </option>
                                            <option value="Wakil Dekan 1 Fak. Keguruan"> Wakil Dekan 1 Fakultas Keguruan </option>
                                            <option value="Wakil Dekan 1 Fak. Teknik"> Wakil Dekan 1 Fakultas Teknik </option>
                                            <option selected value="Wakil Dekan 2 Fak. Hukum"> Wakil Dekan 2 Fakultas Hukum </option>
                                            <option value="Wakil Dekan 2 Fak. Ekonomi"> Wakil Dekan 2 Fakultas Ekonomi </option>
                                            <option value="Wakil Dekan 2 Fak. Pertanian"> Wakil Dekan 2 Fakultas Pertanian </option>
                                            <option value="Wakil Dekan 2 Fak. Keguruan"> Wakil Dekan 2 Fakultas Keguruan </option>
                                            <option value="Wakil Dekan 2 Fak. Teknik"> Wakil Dekan 2 Fakultas Teknik </option>';
                                }
                                else if($d['biro_posisi'] == 'Wakil Dekan 2 Fak. Ekonomi')
                                {
                                    echo '<option value="Dekan Fak. Hukum"> Dekan Fakultas Hukum</option>
                                            <option value="Dekan Fak. Ekonomi"> Dekan Fakultas Ekonomi</option>
                                            <option value="Dekan Fak. Pertanian"> Dekan Fakultas Pertanian</option>
                                            <option value="Dekan Fak. Keguruan"> Dekan Fakultas Keguruan</option>
                                            <option value="Dekan Fak. Teknik"> Dekan Fakultas Teknik</option>
                                            <option value="Wakil Dekan 1 Fak. Hukum"> Wakil Dekan 1 Fakultas Hukum </option>
                                            <option value="Wakil Dekan 1 Fak. Ekonomi"> Wakil Dekan 1 Fakultas Ekonomi </option>
                                            <option value="Wakil Dekan 1 Fak. Pertanian"> Wakil Dekan 1 Fakultas Pertanian </option>
                                            <option value="Wakil Dekan 1 Fak. Keguruan"> Wakil Dekan 1 Fakultas Keguruan </option>
                                            <option value="Wakil Dekan 1 Fak. Teknik"> Wakil Dekan 1 Fakultas Teknik </option>
                                            <option value="Wakil Dekan 2 Fak. Hukum"> Wakil Dekan 2 Fakultas Hukum </option>
                                            <option selected value="Wakil Dekan 2 Fak. Ekonomi"> Wakil Dekan 2 Fakultas Ekonomi </option>
                                            <option value="Wakil Dekan 2 Fak. Pertanian"> Wakil Dekan 2 Fakultas Pertanian </option>
                                            <option value="Wakil Dekan 2 Fak. Keguruan"> Wakil Dekan 2 Fakultas Keguruan </option>
                                            <option value="Wakil Dekan 2 Fak. Teknik"> Wakil Dekan 2 Fakultas Teknik </option>';
                                }
                                else if($d['biro_posisi'] == 'Wakil Dekan 2 Fak. Pertanian')
                                {
                                    echo '<option value="Dekan Fak. Hukum"> Dekan Fakultas Hukum</option>
                                            <option value="Dekan Fak. Ekonomi"> Dekan Fakultas Ekonomi</option>
                                            <option value="Dekan Fak. Pertanian"> Dekan Fakultas Pertanian</option>
                                            <option value="Dekan Fak. Keguruan"> Dekan Fakultas Keguruan</option>
                                            <option value="Dekan Fak. Teknik"> Dekan Fakultas Teknik</option>
                                            <option value="Wakil Dekan 1 Fak. Hukum"> Wakil Dekan 1 Fakultas Hukum </option>
                                            <option value="Wakil Dekan 1 Fak. Ekonomi"> Wakil Dekan 1 Fakultas Ekonomi </option>
                                            <option value="Wakil Dekan 1 Fak. Pertanian"> Wakil Dekan 1 Fakultas Pertanian </option>
                                            <option value="Wakil Dekan 1 Fak. Keguruan"> Wakil Dekan 1 Fakultas Keguruan </option>
                                            <option value="Wakil Dekan 1 Fak. Teknik"> Wakil Dekan 1 Fakultas Teknik </option>
                                            <option value="Wakil Dekan 2 Fak. Hukum"> Wakil Dekan 2 Fakultas Hukum </option>
                                            <option value="Wakil Dekan 2 Fak. Ekonomi"> Wakil Dekan 2 Fakultas Ekonomi </option>
                                            <option selected value="Wakil Dekan 2 Fak. Pertanian"> Wakil Dekan 2 Fakultas Pertanian </option>
                                            <option value="Wakil Dekan 2 Fak. Keguruan"> Wakil Dekan 2 Fakultas Keguruan </option>
                                            <option value="Wakil Dekan 2 Fak. Teknik"> Wakil Dekan 2 Fakultas Teknik </option>';
                                }
                                else if($d['biro_posisi'] == 'Wakil Dekan 2 Fak. Keguruan')
                                {
                                    echo '<option value="Dekan Fak. Hukum"> Dekan Fakultas Hukum</option>
                                            <option value="Dekan Fak. Ekonomi"> Dekan Fakultas Ekonomi</option>
                                            <option value="Dekan Fak. Pertanian"> Dekan Fakultas Pertanian</option>
                                            <option value="Dekan Fak. Keguruan"> Dekan Fakultas Keguruan</option>
                                            <option value="Dekan Fak. Teknik"> Dekan Fakultas Teknik</option>
                                            <option value="Wakil Dekan 1 Fak. Hukum"> Wakil Dekan 1 Fakultas Hukum </option>
                                            <option value="Wakil Dekan 1 Fak. Ekonomi"> Wakil Dekan 1 Fakultas Ekonomi </option>
                                            <option value="Wakil Dekan 1 Fak. Pertanian"> Wakil Dekan 1 Fakultas Pertanian </option>
                                            <option value="Wakil Dekan 1 Fak. Keguruan"> Wakil Dekan 1 Fakultas Keguruan </option>
                                            <option value="Wakil Dekan 1 Fak. Teknik"> Wakil Dekan 1 Fakultas Teknik </option>
                                            <option value="Wakil Dekan 2 Fak. Hukum"> Wakil Dekan 2 Fakultas Hukum </option>
                                            <option value="Wakil Dekan 2 Fak. Ekonomi"> Wakil Dekan 2 Fakultas Ekonomi </option>
                                            <option value="Wakil Dekan 2 Fak. Pertanian"> Wakil Dekan 2 Fakultas Pertanian </option>
                                            <option selected value="Wakil Dekan 2 Fak. Keguruan"> Wakil Dekan 2 Fakultas Keguruan </option>
                                            <option value="Wakil Dekan 2 Fak. Teknik"> Wakil Dekan 2 Fakultas Teknik </option>';
                                }
                                else if($d['biro_posisi'] == 'Wakil Dekan 2 Fak. Teknik')
                                {
                                    echo '<option value="Dekan Fak. Hukum"> Dekan Fakultas Hukum</option>
                                            <option value="Dekan Fak. Ekonomi"> Dekan Fakultas Ekonomi</option>
                                            <option value="Dekan Fak. Pertanian"> Dekan Fakultas Pertanian</option>
                                            <option value="Dekan Fak. Keguruan"> Dekan Fakultas Keguruan</option>
                                            <option value="Dekan Fak. Teknik"> Dekan Fakultas Teknik</option>
                                            <option value="Wakil Dekan 1 Fak. Hukum"> Wakil Dekan 1 Fakultas Hukum </option>
                                            <option value="Wakil Dekan 1 Fak. Ekonomi"> Wakil Dekan 1 Fakultas Ekonomi </option>
                                            <option value="Wakil Dekan 1 Fak. Pertanian"> Wakil Dekan 1 Fakultas Pertanian </option>
                                            <option value="Wakil Dekan 1 Fak. Keguruan"> Wakil Dekan 1 Fakultas Keguruan </option>
                                            <option value="Wakil Dekan 1 Fak. Teknik"> Wakil Dekan 1 Fakultas Teknik </option>
                                            <option value="Wakil Dekan 2 Fak. Hukum"> Wakil Dekan 2 Fakultas Hukum </option>
                                            <option value="Wakil Dekan 2 Fak. Ekonomi"> Wakil Dekan 2 Fakultas Ekonomi </option>
                                            <option value="Wakil Dekan 2 Fak. Pertanian"> Wakil Dekan 2 Fakultas Pertanian </option>
                                            <option value="Wakil Dekan 2 Fak. Keguruan"> Wakil Dekan 2 Fakultas Keguruan </option>
                                            <option selected value="Wakil Dekan 2 Fak. Teknik"> Wakil Dekan 2 Fakultas Teknik </option>';
                                }
                            ?>
                            
                        </select>
                    </p>
                </div>
                <div class="col-md-6">
                    <label>EMAIL :</label>
                    <p>
                        <input
                            type="text"
                            name="biro_email"
                            class="form-control"
                            value="<?php echo $d['biro_email'] ?>"
                            required>
                    </p>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-light-grey">
                Cancel
            </button>
            <button type="submit" class="btn btn-blue">
                Update
            </button>
        </div>
    </form>
</div>
<?php } ?> 
