<div class="container">
    <div class="navbar-header">
        <!-- start: RESPONSIVE MENU TOGGLER -->
        <button data-target="navbar-collapse" data-toggle="collapse" class="navbar-toggle" type="button">
        <span class="clip-list-2"></span>
    </button>
        <!-- end: RESPONSIVE MENU TOGGLER -->
        <!-- start: LOGO -->
        <br/>
         <h3 align="center" class="navbar hidden-xs hidden-md"><font color="#5f5959" face="Century Gothic"><img style="width: 35px; height: 35px;" src="<?php echo base_url(); ?>vendor/login/images/unsam.png">  <b><i>Smart</i>Sam</b> | Universitas Samudra</font></h3>
         <h3 align="center" class="navbar hidden-lg"><font color="#5f5959" face="Century Gothic"><img style="width: 30px; height: 30px;" src="<?php echo base_url(); ?>vendor/img/sipecola/icon/logo.png">  SMARTSAM</font></h3>
        <!-- end: LOGO -->
    </div>
    <div class="navbar-tools">
        <!-- start: TOP NAVIGATION MENU -->
        <ul class="nav navbar-right hidden-xs hidden-md">
            <!-- start: TO-DO DROPDOWN -->
            		<!-- ISI TO-DO DROPDOWN -->
            <!-- end: TO-DO DROPDOWN-->
            <!-- start: NOTIFICATION DROPDOWN -->
            		<!-- ISI NOTIFICATION DROPDOWN -->
            <!-- end: NOTIFICATION DROPDOWN -->
            <!-- start: MESSAGE DROPDOWN -->
            		<!-- ISI MESSAGE DROPDOWN -->
            <!-- end: MESSAGE DROPDOWN --> 
            <!-- start: USER DROPDOWN -->
            <li class="dropdown current-user">
                <a data-toggle="dropdown" data-hover="dropdown" class="dropdown-toggle" data-close-others="true" href="#">
                    <img style="width: 30px; height: 28px;" src="<?php echo base_url(); ?>vendor/assets/images/web/user.png" class="circle-img" alt="">
                    <span class="username"><?php echo $nama; ?></span>
                    <i class="clip-chevron-down"></i>
                </a>
                <ul class="dropdown-menu">
                    <li>
                        <a href="#">
                            <i class="clip-user-2"></i> &nbsp;My Profile
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <i class="clip-calendar"></i> &nbsp;My Calendar
                        </a>
                        <li>
                            <a href="#">
                                <i class="clip-bubble-4"></i> &nbsp;My Messages (3)
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <i class="clip-locked"></i> &nbsp;Lock Screen
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo base_url();?>index.php/web/logout/">
                                <i class="clip-exit"></i> &nbsp;Log Out
                            </a>
                        </li>
               		</li>
                </ul>
            </li>
                <!-- end: USER DROPDOWN -->
                <!-- start: PAGE SIDEBAR TOGGLE -->
                <li>
                    <a class="sb-toggle" href="#"><i class="fa fa-outdent"></i></a>
                </li>
                <!-- end: PAGE SIDEBAR TOGGLE -->
        </ul>
        <!-- end: TOP NAVIGATION MENU -->
    </div>
</div>