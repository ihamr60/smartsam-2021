<?php 
    foreach($data_pelaksana->result_array() as $d){
?>
<div id="modalEditPelaksanaFak<?php echo $d['staff_uuid'] ?>" class="modal fade" tabindex="-1" data-width="760" style="display: none;">
    <form role="form" action="<?php echo base_url() ?>index.php/super_admin/editPelaksanaFak" method="post" enctype="multipart/form-data" >
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                &times;
            </button>
            <h4 class="modal-title">EDIT DATA PELAKSANA FAKULTAS</h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-6">
                    <label>USERNAME:</label>
                    <p>
                        <input name="staff_uuid" type="hidden" value="<?php echo $d['staff_uuid'] ?>">
                        <input
                            type="text"
                            name="staff_username"
                            class="form-control"
                            value="<?php echo $d['staff_username'] ?>"
                            required
                            readonly>
                    </p>
                </div>
                <div class="col-md-6">
                    <label>NAMA LENGKAP:</label>
                    <p>
                        <input
                            type="text"
                            name="staff_nama"
                            class="form-control"
                            value="<?php echo $d['staff_nama'] ?>"
                            required>
                    </p>
                </div>
                <div class="col-md-6">
                    <label>JABATAN/POSISI:</label>
                    <p>
                        <input
                            type="text"
                            name="staff_posisi"
                            class="form-control"
                            value="<?php echo $d['staff_posisi'] ?>"
                            required>
                    </p>
                </div>
                <div class="col-md-6">
                    <label>EMAIL:</label>
                    <p>
                        <input
                            type="text"
                            name="staff_email"
                            class="form-control"
                            value="<?php echo $d['staff_email'] ?>"
                            required>
                    </p>
                </div>
                <div class="col-md-6">
                    <label>PILIH ATASAN::</label>
                    <p>
                        <select
                            name="staff_uuid_kasubbag"
                            class="form-control">
                            <option value="">Please Select</option>
                            <?php
                                foreach($data_kasubbag->result_array() as $e)
                                {
                                    if($e['kasubbag_uuid'] == $d['staff_uuid_kasubbag'])
                                    {
                                        echo '<option selected value="'.$e['kasubbag_uuid'].'">'.$e['kasubbag_nama'].'</option>';
                                    }
                                    echo '<option value="'.$e['kasubbag_uuid'].'">'.$e['kasubbag_nama'].'</option>';
                                }

                                foreach($data_kabag->result_array() as $e)
                                {
                                    if($e['kabag_uuid'] == $d['staff_uuid_kasubbag'])
                                    {
                                        echo '<option selected value="'.$e['kabag_uuid'].'">'.$e['kabag_nama'].'</option>';
                                    }
                                    echo '<option value="'.$e['kabag_uuid'].'">'.$e['kabag_nama'].'</option>';
                                }
                            ?>
                        </select>
                    </p>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-light-grey">
                Cancel
            </button>
            <button type="submit" class="btn btn-blue">
                Update
            </button>
        </div>
    </form>
</div>
<?php } ?>
