<?php 
    foreach($data_kepala->result_array() as $d){
?>
<div id="modalEditKepalaLpppm<?php echo $d['biro_uuid'] ?>" class="modal fade" tabindex="-1" data-width="760" style="display: none;">
    <form role="form" action="<?php echo base_url() ?>index.php/super_admin/editKepalaLpppm" method="post" enctype="multipart/form-data" >
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                &times;
            </button>
            <h4 class="modal-title">EDIT KETUA LPPPM & PM</h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-6">
                    <label>USERNAME:</label>
                    <p>
                        <input type="hidden" value="<?php echo $d['biro_uuid'] ?>" name="biro_uuid">
                        <input
                            type="text"
                            name="biro_username"
                            class="form-control"
                            value="<?php echo $d['biro_username'] ?>"
                            required
                            readonly>
                    </p>
                </div>
                <div class="col-md-6">
                    <label>NAMA KETUA LPPPM:</label>
                    <p>
                        <input
                            type="text"
                            name="biro_nama"
                            class="form-control"
                            value="<?php echo $d['biro_nama'] ?>"
                            required>
                    </p>
                </div>
                <div class="col-md-6">
                    <label>JABATAN/POSISI:</label>
                    <p>
                        <input
                            type="text"
                            name="biro_posisi"
                            class="form-control"
                            value="<?php echo $d['biro_posisi'] ?>"
                            required
                            readonly>
                    </p>
                </div>
                <div class="col-md-6">
                    <label>EMAIL :</label>
                    <p>
                        <input
                            type="text"
                            name="biro_email"
                            class="form-control"
                            value="<?php echo $d['biro_email'] ?>"
                            required>
                    </p>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-light-grey">
                Cancel
            </button>
            <button type="submit" class="btn btn-blue">
                Update
            </button>
        </div>
    </form>
</div>
<?php } ?> 
