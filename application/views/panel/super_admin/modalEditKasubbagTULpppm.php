<?php 
    foreach($data_kasubbag->result_array() as $d){
?>
<div id="modalEditKasubbagTULpppm<?php echo $d['kasubbag_uuid'] ?>" class="modal fade" tabindex="-1" data-width="760" style="display: none;">
    <form role="form" action="<?php echo base_url() ?>index.php/super_admin/editKasubbagTULpppm" method="post" enctype="multipart/form-data" >
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                &times;
            </button>
            <h4 class="modal-title">EDIT DATA KASUBBAG LPPPM </h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-6">
                    <label>USERNAME:</label>
                    <p>
                        <input name="kasubbag_uuid" type="hidden" value="<?php echo $d['kasubbag_uuid'] ?>">
                        <input name="kasubbag_posisi_before" type="hidden" value="<?php echo $d['kasubbag_posisi'] ?>">
                        <input
                            type="text"
                            name="kasubbag_username"
                            class="form-control"
                            value="<?php echo $d['kasubbag_username'] ?>"
                            required
                            readonly>
                    </p>
                </div>
                <div class="col-md-6">
                    <label>NAMA LENGKAP:</label>
                    <p>
                        <input
                            type="text"
                            name="kasubbag_nama"
                            class="form-control"
                            value="<?php echo $d['kasubbag_nama'] ?>"
                            required>
                    </p>
                </div>

                <div class="col-md-6">
                    <label>JABATAN/POSISI:</label>
                    <p>
                        <select
                            name="kasubbag_posisi"
                            class="form-control">
                            <option selected value="Kasubbag TU LPPPM & PM">Kasubbag TU LPPPM & PM</option>
                        </select>
                    </p>
                </div>
                <div class="col-md-6">
                    <label>EMAIL:</label>
                    <p>
                        <input
                            type="text"
                            name="kasubbag_email"
                            class="form-control"
                            value="<?php echo $d['kasubbag_email'] ?>"
                            required>
                    </p>
                </div>
                <div class="col-md-6">
                    <label>PILIH ATASAN::</label>
                    <p>
                        <select
                            name="kasubbag_uuid_kabag"
                            class="form-control">
                            <?php
                                foreach($data_kabag->result_array() as $e)
                                {
                                    if($d['kasubbag_uuid_kabag'] == $e['kabag_uuid'] )
                                    {
                                        echo '<option selected value="'.$e['kabag_uuid'].'">'.$e['kabag_nama'].'</option>';
                                    }
                                }
                            ?>
                        </select>
                    </p>
                </div>

            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-light-grey">
                Cancel
            </button>
            <button type="submit" class="btn btn-blue">
                Update
            </button>
        </div>
    </form>
</div>
<?php } ?>
