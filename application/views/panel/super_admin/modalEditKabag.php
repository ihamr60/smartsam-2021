<?php 
    foreach($data_kabag->result_array() as $d){
?>
<div id="modalEditKabag<?php echo $d['kabag_uuid'] ?>" class="modal fade" tabindex="-1" data-width="760" style="display: none;">
    <form role="form" action="<?php echo base_url() ?>index.php/super_admin/editKabag" method="post" enctype="multipart/form-data" >
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                &times;
            </button>
            <h4 class="modal-title">EDIT DATA AKUN KABAG</h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <label>Username:</label>
                    <p>
                        <input name="kabag_uuid" type="hidden" value="<?php echo $d['kabag_uuid'] ?>">
                        <input
                            type="text"
                            name="username"
                            class="form-control"
                            value="<?php echo $d['kabag_username'] ?>"
                            required
                            readonly>
                    </p>
                </div>
                <div class="col-md-12">
                    <label>Nama Lengkap:</label>
                    <p>
                        <input
                            type="text"
                            name="nama_lengkap"
                            class="form-control"
                            value="<?php echo $d['kabag_nama'] ?>"
                            required>
                    </p>
                </div>
                <div class="col-md-12">
                    <label>Posisi / Jabatan Kabag:</label>
                    <p>
                        <select
                            name="posisi"
                            class="form-control">
                            <?php 
                                if($d['kabag_posisi'] == "Kepala Bagian Akademik dan Kemahasiswaan")
                                {
                                    echo '
                                    <option selected value="Kepala Bagian Akademik dan Kemahasiswaan">Kepala Bagian Akademik dan Kemahasiswaan</option>
                                    <option value="Kepala Bagian Perencanaan dan Kerja sama">Kepala Bagian Perencanaan dan Kerja sama</option>
                                    <option value="Kepala Bagian Keuangan">Kepala Bagian Keuangan</option>
                                    <option value="Kepala Bagian Umum">Kepala Bagian Umum</option>';
                                }
                                else if($d['kabag_posisi'] == "Kepala Bagian Perencanaan dan Kerja sama")
                                {
                                    echo '
                                    <option value="Kepala Bagian Akademik dan Kemahasiswaan">Kepala Bagian Akademik dan Kemahasiswaan</option>
                                    <option selected value="Kepala Bagian Perencanaan dan Kerja sama">Kepala Bagian Perencanaan dan Kerja sama</option>
                                    <option value="Kepala Bagian Keuangan">Kepala Bagian Keuangan</option>
                                    <option value="Kepala Bagian Umum">Kepala Bagian Umum</option>';
                                }
                                else if($d['kabag_posisi'] == "Kepala Bagian Keuangan")
                                {
                                    echo '
                                    <option value="Kepala Bagian Akademik dan Kemahasiswaan">Kepala Bagian Akademik dan Kemahasiswaan</option>
                                    <option value="Kepala Bagian Perencanaan dan Kerja sama">Kepala Bagian Perencanaan dan Kerja sama</option>
                                    <option selected value="Kepala Bagian Keuangan">Kepala Bagian Keuangan</option>
                                    <option value="Kepala Bagian Umum">Kepala Bagian Umum</option>';
                                }
                                else if($d['kabag_posisi'] == "Kepala Bagian Umum")
                                {
                                    echo '
                                    <option value="Kepala Bagian Akademik dan Kemahasiswaan">Kepala Bagian Akademik dan Kemahasiswaan</option>
                                    <option value="Kepala Bagian Perencanaan dan Kerja sama">Kepala Bagian Perencanaan dan Kerja sama</option>
                                    <option value="Kepala Bagian Keuangan">Kepala Bagian Keuangan</option>
                                    <option selected value="Kepala Bagian Umum">Kepala Bagian Umum</option>';
                                }
                            ?>
                        </select>
                    </p>
                </div>

                <div class="col-md-12">
                    <label>Atasan Kabag:</label>
                    <p>
                        <select
                            name="atasan"
                            class="form-control">
                            <?php 
                                foreach($data_biro->result_array() as $e)
                                {
                                    if($d['kabag_uuid_biro'] == $e['biro_uuid'])
                                    {
                                        echo "<option style='text-transform: uppercase;' selected value=".$e['biro_uuid'].">".$e['biro_nama']." / (".$e['biro_posisi'].")</option>";
                                    }
                                    else
                                    {
                                        echo "<option style='text-transform: uppercase;' value=".$e['biro_uuid'].">".$e['biro_nama']." / (".$e['biro_posisi'].")</option>";
                                    }
                                }
                            ?>
                        </select>
                    </p>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-light-grey">
                Cancel
            </button>
            <button type="submit" class="btn btn-blue">
                Tambahkan
            </button>
        </div>
    </form>
</div>
<?php } ?>
