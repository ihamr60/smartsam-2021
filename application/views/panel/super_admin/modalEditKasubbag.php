<?php 
    foreach($data_kasubbag->result_array() as $d){
?>
<div id="modalEditKasubbag<?php echo $d['kasubbag_uuid'] ?>" class="modal fade" tabindex="-1" data-width="760" style="display: none;">
    <form role="form" action="<?php echo base_url() ?>index.php/super_admin/editKasubbag" method="post" enctype="multipart/form-data" >
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                &times;
            </button>
            <h4 class="modal-title">EDIT DATA AKUN KASUBBAG</h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <label>Username:</label>
                    <p>
                        <input name="kasubbag_uuid" type="hidden" value="<?php echo $d['kasubbag_uuid'] ?>">
                        <input
                            type="text"
                            name="username"
                            class="form-control"
                            value="<?php echo $d['kasubbag_username'] ?>"
                            required
                            readonly>
                    </p>
                </div>
                <div class="col-md-12">
                    <label>Nama Lengkap:</label>
                    <p>
                        <input
                            type="text"
                            name="nama_lengkap"
                            class="form-control"
                            value="<?php echo $d['kasubbag_nama'] ?>"
                            required>
                    </p>
                </div>
                <div class="col-md-12">
                    <label>Posisi / Jabatan:</label>
                    <p>
                        <select
                            name="posisi"
                            class="form-control">
                            <?php 
                                if($d['kasubbag_posisi'] == "Kasubbag Perbendaharaan")
                                {
                                    echo '
                                    <option selected value="Kasubbag Perbendaharaan">Kasubbag Perbendaharaan</option>
                                    <option value="Kasubbag Kerjasama & Humas">Kasubbag Kerjasama & Humas</option>
                                    <option value="Kasubbag Akuntansi & Pelaporan">Kasubbag Akuntansi & Pelaporan</option>
                                    <option value="Kasubbag TU & Ketatalaksanaan">Kasubbag TU & Ketatalaksanaan</option>
                                    <option value="Kasubbag RT & BM">Kasubbag RT & BM</option>
                                    <option value="Kasubbag Kepegawaian">Kasubbag Kepegawaian</option>
                                    <option value="Kasubbag Pendidikan & Evaluasi">Kasubbag Pendidikan & Evaluasi</option>
                                    <option value="Kasubbag Registrasi & Statistik">Kasubbag Registrasi & Statistik</option>
                                    <option value="Kasubbag Kemahasiswaan">Kasubbag Kemahasiswaan</option>
                                    <option value="Kasubbag Perencanaan">Kasubbag Perencanaan</option>';
                                }
                                else if($d['kasubbag_posisi'] == "Kasubbag Kerjasama & Humas")
                                {
                                    echo '
                                    <option value="Kasubbag Perbendaharaan">Kasubbag Perbendaharaan</option>
                                    <option selected value="Kasubbag Kerjasama & Humas">Kasubbag Kerjasama & Humas</option>
                                    <option value="Kasubbag Akuntansi & Pelaporan">Kasubbag Akuntansi & Pelaporan</option>
                                    <option value="Kasubbag TU & Ketatalaksanaan">Kasubbag TU & Ketatalaksanaan</option>
                                    <option value="Kasubbag RT & BM">Kasubbag RT & BM</option>
                                    <option value="Kasubbag Kepegawaian">Kasubbag Kepegawaian</option>
                                    <option value="Kasubbag Pendidikan & Evaluasi">Kasubbag Pendidikan & Evaluasi</option>
                                    <option value="Kasubbag Registrasi & Statistik">Kasubbag Registrasi & Statistik</option>
                                    <option value="Kasubbag Kemahasiswaan">Kasubbag Kemahasiswaan</option>
                                    <option value="Kasubbag Perencanaan">Kasubbag Perencanaan</option>';
                                }
                                else if($d['kasubbag_posisi'] == "Kasubbag Akuntansi & Pelaporan")
                                {
                                    echo '
                                    <option value="Kasubbag Perbendaharaan">Kasubbag Perbendaharaan</option>
                                    <option value="Kasubbag Kerjasama & Humas">Kasubbag Kerjasama & Humas</option>
                                    <option selected value="Kasubbag Akuntansi & Pelaporan">Kasubbag Akuntansi & Pelaporan</option>
                                    <option value="Kasubbag TU & Ketatalaksanaan">Kasubbag TU & Ketatalaksanaan</option>
                                    <option value="Kasubbag RT & BM">Kasubbag RT & BM</option>
                                    <option value="Kasubbag Kepegawaian">Kasubbag Kepegawaian</option>
                                    <option value="Kasubbag Pendidikan & Evaluasi">Kasubbag Pendidikan & Evaluasi</option>
                                    <option value="Kasubbag Registrasi & Statistik">Kasubbag Registrasi & Statistik</option>
                                    <option value="Kasubbag Kemahasiswaan">Kasubbag Kemahasiswaan</option>
                                    <option value="Kasubbag Perencanaan">Kasubbag Perencanaan</option>';
                                }
                                else if($d['kasubbag_posisi'] == "Kasubbag TU & Ketatalaksanaan")
                                {
                                    echo '
                                    <option value="Kasubbag Perbendaharaan">Kasubbag Perbendaharaan</option>
                                    <option value="Kasubbag Kerjasama & Humas">Kasubbag Kerjasama & Humas</option>
                                    <option value="Kasubbag Akuntansi & Pelaporan">Kasubbag Akuntansi & Pelaporan</option>
                                    <option selected value="Kasubbag TU & Ketatalaksanaan">Kasubbag TU & Ketatalaksanaan</option>
                                    <option value="Kasubbag RT & BM">Kasubbag RT & BM</option>
                                    <option value="Kasubbag Kepegawaian">Kasubbag Kepegawaian</option>
                                    <option value="Kasubbag Pendidikan & Evaluasi">Kasubbag Pendidikan & Evaluasi</option>
                                    <option value="Kasubbag Registrasi & Statistik">Kasubbag Registrasi & Statistik</option>
                                    <option value="Kasubbag Kemahasiswaan">Kasubbag Kemahasiswaan</option>
                                    <option value="Kasubbag Perencanaan">Kasubbag Perencanaan</option>';
                                }
                                else if($d['kasubbag_posisi'] == "Kasubbag RT & BM")
                                {
                                    echo '
                                    <option value="Kasubbag Perbendaharaan">Kasubbag Perbendaharaan</option>
                                    <option value="Kasubbag Kerjasama & Humas">Kasubbag Kerjasama & Humas</option>
                                    <option value="Kasubbag Akuntansi & Pelaporan">Kasubbag Akuntansi & Pelaporan</option>
                                    <option value="Kasubbag TU & Ketatalaksanaan">Kasubbag TU & Ketatalaksanaan</option>
                                    <option selected value="Kasubbag RT & BM">Kasubbag RT & BM</option>
                                    <option value="Kasubbag Kepegawaian">Kasubbag Kepegawaian</option>
                                    <option value="Kasubbag Pendidikan & Evaluasi">Kasubbag Pendidikan & Evaluasi</option>
                                    <option value="Kasubbag Registrasi & Statistik">Kasubbag Registrasi & Statistik</option>
                                    <option value="Kasubbag Kemahasiswaan">Kasubbag Kemahasiswaan</option>
                                    <option value="Kasubbag Perencanaan">Kasubbag Perencanaan</option>';
                                }
                                else if($d['kasubbag_posisi'] == "Kasubbag Kepegawaian")
                                {
                                    echo '
                                    <option value="Kasubbag Perbendaharaan">Kasubbag Perbendaharaan</option>
                                    <option value="Kasubbag Kerjasama & Humas">Kasubbag Kerjasama & Humas</option>
                                    <option value="Kasubbag Akuntansi & Pelaporan">Kasubbag Akuntansi & Pelaporan</option>
                                    <option value="Kasubbag TU & Ketatalaksanaan">Kasubbag TU & Ketatalaksanaan</option>
                                    <option value="Kasubbag RT & BM">Kasubbag RT & BM</option>
                                    <option selected value="Kasubbag Kepegawaian">Kasubbag Kepegawaian</option>
                                    <option value="Kasubbag Pendidikan & Evaluasi">Kasubbag Pendidikan & Evaluasi</option>
                                    <option value="Kasubbag Registrasi & Statistik">Kasubbag Registrasi & Statistik</option>
                                    <option value="Kasubbag Kemahasiswaan">Kasubbag Kemahasiswaan</option>
                                    <option value="Kasubbag Perencanaan">Kasubbag Perencanaan</option>';
                                }
                                else if($d['kasubbag_posisi'] == "Kasubbag Pendidikan & Evaluasi")
                                {
                                    echo '
                                    <option value="Kasubbag Perbendaharaan">Kasubbag Perbendaharaan</option>
                                    <option value="Kasubbag Kerjasama & Humas">Kasubbag Kerjasama & Humas</option>
                                    <option value="Kasubbag Akuntansi & Pelaporan">Kasubbag Akuntansi & Pelaporan</option>
                                    <option value="Kasubbag TU & Ketatalaksanaan">Kasubbag TU & Ketatalaksanaan</option>
                                    <option value="Kasubbag RT & BM">Kasubbag RT & BM</option>
                                    <option value="Kasubbag Kepegawaian">Kasubbag Kepegawaian</option>
                                    <option selected value="Kasubbag Pendidikan & Evaluasi">Kasubbag Pendidikan & Evaluasi</option>
                                    <option value="Kasubbag Registrasi & Statistik">Kasubbag Registrasi & Statistik</option>
                                    <option value="Kasubbag Kemahasiswaan">Kasubbag Kemahasiswaan</option>
                                    <option value="Kasubbag Perencanaan">Kasubbag Perencanaan</option>';
                                }
                                else if($d['kasubbag_posisi'] == "Kasubbag Registrasi & Statistik")
                                {
                                    echo '
                                    <option value="Kasubbag Perbendaharaan">Kasubbag Perbendaharaan</option>
                                    <option value="Kasubbag Kerjasama & Humas">Kasubbag Kerjasama & Humas</option>
                                    <option value="Kasubbag Akuntansi & Pelaporan">Kasubbag Akuntansi & Pelaporan</option>
                                    <option value="Kasubbag TU & Ketatalaksanaan">Kasubbag TU & Ketatalaksanaan</option>
                                    <option value="Kasubbag RT & BM">Kasubbag RT & BM</option>
                                    <option value="Kasubbag Kepegawaian">Kasubbag Kepegawaian</option>
                                    <option value="Kasubbag Pendidikan & Evaluasi">Kasubbag Pendidikan & Evaluasi</option>
                                    <option selected value="Kasubbag Registrasi & Statistik">Kasubbag Registrasi & Statistik</option>
                                    <option value="Kasubbag Kemahasiswaan">Kasubbag Kemahasiswaan</option>
                                    <option value="Kasubbag Perencanaan">Kasubbag Perencanaan</option>';
                                }
                                else if($d['kasubbag_posisi'] == "Kasubbag Kemahasiswaan")
                                {
                                    echo '
                                    <option value="Kasubbag Perbendaharaan">Kasubbag Perbendaharaan</option>
                                    <option value="Kasubbag Kerjasama & Humas">Kasubbag Kerjasama & Humas</option>
                                    <option value="Kasubbag Akuntansi & Pelaporan">Kasubbag Akuntansi & Pelaporan</option>
                                    <option value="Kasubbag TU & Ketatalaksanaan">Kasubbag TU & Ketatalaksanaan</option>
                                    <option value="Kasubbag RT & BM">Kasubbag RT & BM</option>
                                    <option value="Kasubbag Kepegawaian">Kasubbag Kepegawaian</option>
                                    <option value="Kasubbag Pendidikan & Evaluasi">Kasubbag Pendidikan & Evaluasi</option>
                                    <option value="Kasubbag Registrasi & Statistik">Kasubbag Registrasi & Statistik</option>
                                    <option selected value="Kasubbag Kemahasiswaan">Kasubbag Kemahasiswaan</option>
                                    <option value="Kasubbag Perencanaan">Kasubbag Perencanaan</option>';
                                }
                                else if($d['kasubbag_posisi'] == "Kasubbag Perencanaan")
                                {
                                    echo '
                                    <option value="Kasubbag Perbendaharaan">Kasubbag Perbendaharaan</option>
                                    <option value="Kasubbag Kerjasama & Humas">Kasubbag Kerjasama & Humas</option>
                                    <option value="Kasubbag Akuntansi & Pelaporan">Kasubbag Akuntansi & Pelaporan</option>
                                    <option value="Kasubbag TU & Ketatalaksanaan">Kasubbag TU & Ketatalaksanaan</option>
                                    <option value="Kasubbag RT & BM">Kasubbag RT & BM</option>
                                    <option value="Kasubbag Kepegawaian">Kasubbag Kepegawaian</option>
                                    <option value="Kasubbag Pendidikan & Evaluasi">Kasubbag Pendidikan & Evaluasi</option>
                                    <option value="Kasubbag Registrasi & Statistik">Kasubbag Registrasi & Statistik</option>
                                    <option value="Kasubbag Kemahasiswaan">Kasubbag Kemahasiswaan</option>
                                    <option selected value="Kasubbag Perencanaan">Kasubbag Perencanaan</option>';
                                }
                            ?>
                        </select>
                    </p>
                </div>
                <div class="col-md-12">
                    <label>Atasan Kasubbag:</label>
                    <p>
                        <select
                            name="atasan"
                            class="form-control">
                            <?php 
                                foreach($data_kabag->result_array() as $e)
                                {
                                    if($d['kasubbag_uuid_kabag'] == $e['kabag_uuid'])
                                    {
                                        echo "<option style='text-transform: uppercase;' selected value=".$e['kabag_uuid'].">".$e['kabag_nama']." / (".$e['kabag_posisi'].")</option>";
                                    }
                                    else
                                    {
                                        echo "<option style='text-transform: uppercase;' value=".$e['kabag_uuid'].">".$e['kabag_nama']." / (".$e['kabag_posisi'].")</option>";
                                    }
                                }
                            ?>
                        </select>
                    </p>
                </div>

            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-light-grey">
                Cancel
            </button>
            <button type="submit" class="btn btn-blue">
                Tambahkan
            </button>
        </div>
    </form>
</div>
<?php } ?>
