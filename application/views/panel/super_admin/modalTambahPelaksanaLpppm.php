
<div id="modalTambahPelaksanaLpppm" class="modal fade" tabindex="-1" data-width="760" style="display: none;">
    <form role="form" action="<?php echo base_url() ?>index.php/super_admin/tambahPelaksanaLpppm" method="post" enctype="multipart/form-data" >
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                &times;
            </button>
            <h4 class="modal-title">TAMBAH DATA PELAKSANA LPPPM & PM</h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-6">
                    <label>USERNAME:</label>
                    <p>
                        <input
                            type="text"
                            name="staff_username"
                            class="form-control"
                            placeholder="Ex: ismail232"
                            required>
                    </p>
                </div>
                <div class="col-md-6">
                    <label>NAMA PELAKSANA LPPPM & PM:</label>
                    <p>
                        <input
                            type="text"
                            name="staff_nama"
                            class="form-control"
                            placeholder="Ex: Ismail, M.T"
                            required>
                    </p>
                </div>
                <div class="col-md-6">
                    <label>JABATAN/POSISI:</label>
                    <p>
                        <input
                            type="text"
                            name="staff_posisi"
                            class="form-control"
                            placeholder="Ex: Pengelola Keuangan"
                            required>
                    </p>
                </div>
                <div class="col-md-6">
                    <label>PILIH ATASAN::</label>
                    <p>
                        <select
                            name="staff_uuid_kasubbag"
                            class="form-control">
                            <option value="">Please Select</option>
                            <?php
                                foreach($data_kasubbag->result_array() as $d)
                                {
                                    echo '<option value="'.$d['kasubbag_uuid'].'">'.$d['kasubbag_nama'].'</option>';
                                }
                            ?>
                        </select>
                    </p>
                </div>
                <div class="col-md-6">
                    <label>EMAIL :</label>
                    <p>
                        <input
                            type="text"
                            name="staff_email"
                            class="form-control"
                            placeholder="Ex: ilhamr6000@gmail.com"
                            required>
                    </p>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-light-grey">
                Cancel
            </button>
            <button type="submit" class="btn btn-blue">
                Tambahkan
            </button>
        </div>
    </form>
</div>
