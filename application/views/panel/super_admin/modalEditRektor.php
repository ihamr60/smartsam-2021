<?php
    foreach($data_rektor->result_array() as $d)
    {
?>
<div id="modalEditRektor<?php echo $d['rektor_uuid'] ?>" class="modal fade" tabindex="-1" data-width="760" style="display: none;">
    <form role="form" action="<?php echo base_url() ?>index.php/super_admin/editRektor" method="post" enctype="multipart/form-data" >
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                &times;
            </button>
            <h4 class="modal-title">EDIT DATA REKTOR / WAKIL REKTOR</h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <label>Username:</label>
                    <p>
                        <input type="hidden" name="rektor_uuid" value="<?php echo $d['rektor_uuid'] ?>">
                        <input
                            type="text"
                            name="username"
                            class="form-control"
                            value="<?php echo $d['rektor_username'] ?>"
                            required
                            readonly>
                    </p>
                </div>
                <div class="col-md-12">
                    <label>Nama Lengkap:</label>
                    <p>
                        <input
                            type="text"
                            name="nama_lengkap"
                            class="form-control"
                            value="<?php echo $d['rektor_nama'] ?>"
                            required>
                    </p>
                </div>
                <div class="col-md-12">
                    <label>Posisi / Jabatan:</label>
                    <p>
                        <select
                            name="posisi"
                            class="form-control">
                            <?php
                                if($d['rektor_posisi'] == "Rektor")
                                {
                                    echo '
                                        <option selected value="Rektor">Rektor</option>
                                        <option value="Wakil Rektor Bidang Akademik">Wakil Rektor Bidang Akademik</option>
                                        <option value="Wakil Rektor Bidang Umum dan Keuangan">Wakil Rektor Bidang Umum dan Keuangan</option>
                                        <option value="Wakil Rektor Bidang Kemahasiswaan">Wakil Rektor Bidang Kemahasiswaan</option>';
                                }
                                else if($d['rektor_posisi'] == "Wakil Rektor Bidang Akademik")
                                {
                                    echo '
                                        <option value="Rektor">Rektor</option>
                                        <option selected value="Wakil Rektor Bidang Akademik">Wakil Rektor Bidang Akademik</option>
                                        <option value="Wakil Rektor Bidang Umum dan Keuangan">Wakil Rektor Bidang Umum dan Keuangan</option>
                                        <option value="Wakil Rektor Bidang Kemahasiswaan">Wakil Rektor Bidang Kemahasiswaan</option>';
                                }
                                else if($d['rektor_posisi'] == "Wakil Rektor Bidang Umum dan Keuangan")
                                {
                                    echo '
                                        <option value="Rektor">Rektor</option>
                                        <option value="Wakil Rektor Bidang Akademik">Wakil Rektor Bidang Akademik</option>
                                        <option selected value="Wakil Rektor Bidang Umum dan Keuangan">Wakil Rektor Bidang Umum dan Keuangan</option>
                                        <option value="Wakil Rektor Bidang Kemahasiswaan">Wakil Rektor Bidang Kemahasiswaan</option>';
                                }
                                else if($d['rektor_posisi'] == "Wakil Rektor Bidang Kemahasiswaan")
                                {
                                    echo '
                                        <option value="Rektor">Rektor</option>
                                        <option value="Wakil Rektor Bidang Akademik">Wakil Rektor Bidang Akademik</option>
                                        <option value="Wakil Rektor Bidang Umum dan Keuangan">Wakil Rektor Bidang Umum dan Keuangan</option>
                                        <option selected value="Wakil Rektor Bidang Kemahasiswaan">Wakil Rektor Bidang Kemahasiswaan</option>';
                                }
                            ?>
                            
                        </select>
                    </p>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-light-grey">
                Cancel
            </button>
            <button type="submit" class="btn btn-blue">
                Tambahkan
            </button>
        </div>
    </form>
</div>

<?php } ?>
