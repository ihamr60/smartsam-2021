
<div id="modalTambahAdminSuratLabBhs" class="modal fade" tabindex="-1" data-width="760" style="display: none;">
    <form role="form" action="<?php echo base_url() ?>index.php/super_admin/tambahAdminSuratLabBhs" method="post" enctype="multipart/form-data" >
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                &times;
            </button>
            <h4 class="modal-title">TAMBAH DATA AKUN ADMIN PERSURATAN LAB BAHASA</h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <label>Username:</label>
                    <p>
                        <input
                            type="text"
                            name="username"
                            class="form-control"
                            placeholder="Ex: adminsurat_labbhs"
                            maxlength="16"
                            required>
                    </p>
                </div>
                <div class="col-md-12">
                    <label>Nama Lengkap:</label>
                    <p>
                        <input
                            type="text"
                            name="nama_lengkap"
                            class="form-control"
                            placeholder="Ex: Ilham Ramadhan, S.Tr.Kom"
                            required>
                    </p>
                </div>
                <div class="col-md-12">
                    <label>Posisi / Jabatan:</label>
                    <p>
                        <input
                            type="text"
                            name="posisi"
                            class="form-control"
                            value="Admin Surat Lab. Bahasa"
                            required
                            readonly>
                    </p>
                </div>
                <div class="col-md-12">
                    <label>Atasan:</label>
                    <p>
                        <select
                            name="atasan"
                            class="form-control"
                            required>
                            <option value="">Pilih Atasan</option>
                            <?php 
                                foreach($data_kasubbag->result_array() as $d)
                                {
                                    if($d['kasubbag_posisi']=="Ka Lab Bahasa")
                                    {
                                        echo "<option value='".$d['kasubbag_uuid']."'>".$d['kasubbag_nama']." (".$d['kasubbag_posisi'].")</option>";
                                    }
                                }
                            ?>
                        </select>
                    </p>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-light-grey">
                Cancel
            </button>
            <button type="submit" class="btn btn-blue">
                Tambahkan
            </button>
        </div>
    </form>
</div>
