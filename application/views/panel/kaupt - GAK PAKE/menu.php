<ul class="main-navigation-menu">
    <?php
        $color = "#0c5174";
    ?>
    <li>
        <a 
            <?php if (isset($_GET['home']) && $_GET['home']==1){echo 'style="background: '.$color.'; color: white;"';}?> 
            href="<?php echo base_url();?>index.php/kabag/bg_home?home=1">
            <i class="clip-home-3" <?php if (isset($_GET['home']) && $_GET['home']==1){echo 'style="background: '.$color.'; color: white;"';}?>></i>
            <span class="title"><b>DASHBOARD</b></span>
        </a>
    </li>
    <li>
        <a <?php if (isset($_GET['suratMasuk']) && $_GET['suratMasuk']==1){echo 'style="background: '.$color.'; color: white;"';}?> 
        href="<?php echo base_url();?>index.php/kabag/bg_suratMasuk?suratMasuk=1">
            <i class="clip-file-2" <?php if (isset($_GET['suratMasuk']) && $_GET['suratMasuk']==1){echo 'style="background: '.$color.'; color: white;"';}?>></i>
            <span class="title"><b>SURAT MASUK</b></span>
        </a>
    </li>
    <li>
        <a <?php if (isset($_GET['draft']) && $_GET['draft']==1){echo 'style="background: '.$color.'; color: white;"';}?> 
        href="<?php echo base_url();?>index.php/kabag/bg_draft_surat?draft=1">
            <i class="clip-file-2" <?php if (isset($_GET['draft']) && $_GET['draft']==1){echo 'style="background: '.$color.'; color: white;"';}?>></i>
            <span class="title"><b>DRAFT SURAT KELUAR</b></span>
        </a>
    </li>
    <li>
        <a <?php if (isset($_GET['suratKeluar']) && $_GET['suratKeluar']==1){echo 'style="background: '.$color.'; color: white;"';}?> 
        href="<?php echo base_url();?>index.php/kabag/bg_suratKeluar?suratKeluar=1">
            <i class="clip-file-2" <?php if (isset($_GET['suratKeluar']) && $_GET['suratKeluar']==1){echo 'style="background: '.$color.'; color: white;"';}?>></i>
            <span class="title"><b>SURAT KELUAR</b></span>
        </a>
    </li>
   <!-- <li>
        <a <?php if (isset($_GET['pengumuman']) && $_GET['pengumuman']==1){echo 'style="background: '.$color.'; color: white;"';}?> 
        href="javascript:void(0)">
            <i class="clip-user-2" <?php if (isset($_GET['pengumuman']) && $_GET['pengumuman']==1){echo 'style="background: '.$color.'; color: white;"';}?>></i>
            <span class="title"><b>DATA PENGGUNA</b></span><i <?php if (isset($_GET['dt_peg']) && $_GET['dt_peg']==1){echo 'style="background: #346da4; color: white;"';}?> class="icon-arrow"></i>
        </a>
        <ul class="sub-menu">
            <li>
                <a href="<?php echo base_url();?>index.php/admin/bg_pegawai?dt_peg=1">
                    <i class="clip-user-2"></i>
                    <span class="title">Rektor / WR</span>
                </a>
                <a href="<?php echo base_url();?>index.php/admin/bg_pegawai?dt_peg=1">
                    <i class="clip-user-2"></i>
                    <span class="title">Ka. Biro</span>
                </a>
                <a href="<?php echo base_url();?>index.php/admin/bg_pegawai?dt_peg=1">
                    <i class="clip-user-2"></i>
                    <span class="title">Ka. Bagian</span>
                </a>
                <a href="<?php echo base_url();?>index.php/admin/bg_pegawai?dt_peg=1">
                    <i class="clip-user-2"></i>
                    <span class="title">Ka. Subbag</span>
                </a>
                <a href="<?php echo base_url();?>index.php/admin/bg_pegawai?dt_peg=1">
                    <i class="clip-user-2"></i>
                    <span class="title">Staff Pelaksana</span>
                </a>
                <a href="<?php echo base_url();?>index.php/admin/bg_pegawai?dt_peg=1">
                    <i class="clip-user-2"></i>
                    <span class="title">Pegawai</span>
                </a>
            </li>
        </ul>
    </li> -->
</ul>