<!DOCTYPE html>
<!--[if IE 8]><html class="ie8 no-js" lang="en"><![endif]-->
<!--[if IE 9]><html class="ie9 no-js" lang="en"><![endif]-->
<!--[if !IE]><!-->
<html class="no-js">
<!--<![endif]-->

<head>
    <title>SmartSAM Home</title>
    <link rel="shortcut icon" href="<?php echo base_url() ?>vendor/assets/images/web/unsam.ico" />
    <!-- start: META -->
    <meta charset="utf-8" />
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content="IE=edge,IE=9,IE=8,chrome=1" /><![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta content="Responsive Admin Template build with Twitter Bootstrap and jQuery" name="description" />
    <meta content="ClipTheme" name="author" />
    <!-- end: META -->
    <!-- start: MAIN CSS -->
    <link type="text/css" rel="stylesheet" href="//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700|Raleway:400,100,200,300,500,600,700,800,900/" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/bower_components/bootstrap/dist/css/bootstrap.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/bower_components/font-awesome/css/font-awesome.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/assets/fonts/clip-font.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/bower_components/iCheck/skins/all.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/bower_components/perfect-scrollbar/css/perfect-scrollbar.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/bower_components/sweetalert/dist/sweetalert.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/assets/css/main.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/assets/css/main-responsive.min.css" />
    <link type="text/css" rel="stylesheet" media="print" href="<?php echo base_url(); ?>vendor/assets/css/print.min.css" />
    <link type="text/css" rel="stylesheet" id="skin_color" href="<?php echo base_url(); ?>vendor/assets/css/theme/light.min.css" />

    <!-- end: MAIN CSS -->
    <!-- start: CSS REQUIRED FOR THIS PAGE ONLY -->
    <link href="<?php echo base_url();?>vendor/bower_components/select2/dist/css/select2.min.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>vendor/bower_components/datatables/media/css/dataTables.bootstrap.min.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>vendor/bower_components/datatables/media/css/jquery.dataTables.min.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>vendor/bower_components/datatables.net-buttons-dt/css/buttons.dataTables.min.css" rel="stylesheet" />
    
    <link href="<?php echo base_url();?>vendor/bower_components/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>vendor/bower_components/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" />

    <!--  BUTTON -->
    <link href="<?php echo base_url();?>vendor/bower_components/ladda-bootstrap/dist/ladda-themeless.min.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>vendor/bower_components/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.min.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>vendor/bower_components/bootstrap-social/bootstrap-social.css" rel="stylesheet" />
    <!-- end: CSS REQUIRED FOR THIS PAGE ONLY -->
</head>

<body>

    <!-- start: HEADER -->
    <div class="navbar navbar-inverse navbar-fixed-top">
        <!-- start: TOP NAVIGATION CONTAINER -->
        	<?php echo $atas; ?>
        <!-- end: TOP NAVIGATION CONTAINER -->
    </div>
    <!-- end: HEADER -->
        <br>
    <!-- start: MAIN CONTAINER -->
    <div class="main-container">
        <div class="navbar-content">
            <!-- start: SIDEBAR -->
            <div class="main-navigation navbar-collapse collapse">
                <!-- start: MAIN MENU TOGGLER BUTTON -->
                <div class="navigation-toggler">
                    <i class="clip-chevron-left"></i>
                    <i class="clip-chevron-right"></i>
                </div>
                <!-- end: MAIN MENU TOGGLER BUTTON -->
                <!-- start: MAIN NAVIGATION MENU -->
                <?php echo $menu; ?>
                <!-- end: MAIN NAVIGATION MENU -->
            </div>
            <!-- end: SIDEBAR -->
        </div>

        <!-- start: PAGE -->
        <div class="main-content">
           
            <div class="container">
                <!-- start: PAGE HEADER -->
                <div class="row">
                    <div class="col-sm-12">
                        
                        <!-- start: PAGE TITLE & BREADCRUMB -->
                        <ol class="breadcrumb">
                            <li class="active">
                                <i class="clip-home-3"></i>
                                <a href="">
                                    Home
                                </a>
                            </li>
                            <li class="active">
                                    Surat Masuk 
                            </li>
                           
                            <li class="search-box">
                                <form class="sidebar-search">
                                    <div class="form-group">
                                        <input type="text" placeholder="Start Searching...">
                                        <button class="submit">
                                            <i class="clip-search-3"></i>
                                        </button>
                                    </div>
                                </form>
                            </li>
                        </ol>
                        <div class="page-header">
                            <?php echo $bio; ?>
                        </div>
                        <!-- end: PAGE TITLE & BREADCRUMB -->
                    </div>
                </div>
                <!-- end: PAGE HEADER -->
                <!-- start: PAGE CONTENT -->
                <!--<marquee><span class='label label-info'><font color="black">SELAMAT DATANG DI SISTEM INFORMASI E-PDAM TIRTA KEUMUENENG KOTA LANGSA</font></span></marquee> -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="tabbable">
                            <ul class="nav nav-tabs tab-padding tab-space-3 tab-blue" id="myTab4">
                                <li class="active">
                                    <a data-toggle="tab" href="#panel_overview">
                                        Detail Section
                                    </a>
                                </li>
                                <li>
                            </ul>
                            <div class="tab-content">
                                <div id="panel_overview" class="tab-pane in active">
                                    <div class="row">
                                        <div class="col-sm-5 col-md-4">
                                            <div class="user-left">
                                                
                                                <table class="table table-condensed table-hover">
                                                    <thead>
                                                        <tr>
                                                            <th colspan="3">Informasi Umum (Draft Surat Keluar)</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td>Nomor Draft</td>
                                                            <td>
                                                                <a href="#">
                                                                    <b><?php echo $data_draft['draft_no'] ?></b>
                                                                </a>
                                                            </td>
                                                            <td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Tanggal Dibuat / Diajukan</td>
                                                            <td>
                                                                <a href="#">
                                                                    <?php echo $data_draft['draft_created_at'] ?>
                                                                </a>
                                                            </td>
                                                            <td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Perihal</td>
                                                            <td>
                                                                <a href="#">
                                                                    <?php echo $data_draft['draft_perihal'] ?>
                                                                </a>
                                                            </td>
                                                            <td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Author / Penulis</td>
                                                            <td>
                                                                <a href="">
                                                                    <b><?php echo $data_draft['draft_asal'] ?></b>
                                                                </a>
                                                            </td>
                                                            <td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Status ACC</td>
                                                            <td><?php echo $data_draft['draft_status'] ?></td>
                                                            <td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
                                                        </tr>
                                                
                                                    </tbody>
                                                </table>
                                                <form method="post" name="posting_form" action="<?php echo base_url();?>index.php/kabag/add_konsep">
                                                    <table class="table table-condensed table-hover">
                                                        <thead>
                                                            <tr>
                                                                <th colspan="3">Form Penulisan Konsep Ulang</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <td>Konsep</td>
                                                                <td><br><textarea name="konsep" cols="30" rows="3"></textarea></td>
                                                                <input type="hidden" name="draft_uuid" value="<?php echo $data_draft['draft_uuid'] ?>">
                                                                <input type="hidden" name="draft_asal" value="<?php echo $data_draft['draft_asal'] ?>">
                                                                <td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
                                                            </tr>
                                                        </tbody>
                                                    </table> 
                                                    <button class="btn btn-blue btn-block" data-placement="top" data-original-title="Hapus" onclick="return confirm('Anda yakin mengirimkan konsep yang baru')"><i class="fa fa-check"></i> Masukan Konsep</button>
                                                </form>
                                            </div>
                                        </div>
                                        <div class="col-sm-7 col-md-8">
                                            
                                            <div class="panel panel-white">
                                                <div class="panel-heading">
                                                    <i class="clip-menu"></i> RIWAYAT KOREKSI DRAFT SURAT KELUAR
                                                    <div class="panel-tools">
                                                        <a class="btn btn-xs btn-link panel-collapse collapses" href="#">
                                                        </a>
                                                        <a class="btn btn-xs btn-link panel-config" href="#panel-config" data-toggle="modal">
                                                            <i class="fa fa-wrench"></i>
                                                        </a>
                                                        <a class="btn btn-xs btn-link panel-refresh" href="#">
                                                            <i class="fa fa-refresh"></i>
                                                        </a>
                                                        <a class="btn btn-xs btn-link panel-close" href="#">
                                                            <i class="fa fa-times"></i>
                                                        </a>
                                                    </div>
                                                </div>
                                                <div class="panel-body" style="height:100%">
                                                    <ul class="activities">
                                                        <?php 
                                                            foreach($riwayat_konsep->result_array() as $d)
                                                            {
                                                        ?>
                                                        <li>
                                                            <a class="activity">
                                                                <img src="<?php echo base_url() ?>vendor/assets/images/web/memo.png" width="30px">
                                                                <br><br>
                                                                <span class="desc"><?php echo $d['konsep_isi'] ?></span>
                                                                <div class="time">
                                                                    <i class="fa fa-time bigger-110"></i> Ditulis oleh <?php echo $d['konsep_author'] ?>, <?php echo $d['konsep_updated_at'] ?> WIB
                                                                </div>
                                                            </a>
                                                        </li>
                                                        <?php } ?>
                                                    </ul>
                                                </div>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                               
                                
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end: PAGE CONTENT-->
            </div>
        </div>
        <!-- end: PAGE -->
    </div>
    <!-- end: MAIN CONTAINER -->
    <!-- start: FOOTER -->
    <div class="footer clearfix">
        <div class="footer-inner">
            <script>
                document.write(new Date().getFullYear())
            </script> &copy; <?php $this->load->view('versi'); ?>
        </div>
        <div class="footer-items">
            <span class="go-top"><i class="clip-chevron-up"></i></span>
        </div>
    </div>
    <!-- end: FOOTER -->
    <!-- start: RIGHT SIDEBAR -->
    	<!-- ISI RIGHT SIDE BAR -->
    <!-- end: RIGHT SIDEBAR -->
 
    <!-- start: MAIN JAVASCRIPTS -->
    <!--[if lt IE 9]>
        <script src="<?php echo base_url(); ?>vendor/bower_components/respond/dest/respond.min.js"></script>
        <script src="<?php echo base_url(); ?>vendor/bower_components/Flot/excanvas.min.js"></script>
        <script src="<?php echo base_url(); ?>vendor/bower_components/jquery-1.x/dist/jquery.min.js"></script>
        <![endif]-->
    <!--[if gte IE 9]><!-->
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/jquery/dist/jquery.min.js"></script>
    <!--<![endif]-->

    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/jquery-ui/jquery-ui.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/blockUI/jquery.blockUI.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/iCheck/icheck.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/perfect-scrollbar/js/min/perfect-scrollbar.jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/jquery.cookie/jquery.cookie.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/sweetalert/dist/sweetalert.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/assets/js/min/main.min.js"></script>
    <!-- end: MAIN JAVASCRIPTS -->
    <!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
    
    <script src="<?php echo base_url();?>vendor/bower_components/bootstrap-modal/js/bootstrap-modal.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/bootstrap-modal/js/bootstrap-modalmanager.js"></script>
    <script src="<?php echo base_url();?>vendor/assets/js/min/ui-modals.min.js"></script>

    <script src="<?php echo base_url();?>vendor/bower_components/select2/dist/js/select2.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/jszip/dist/jszip.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/pdfmake/build/pdfmake.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/pdfmake/build/vfs_fonts.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?php echo base_url();?>vendor/assets/js/min/table-export.min.js"></script>
    <!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->

    <?php echo $this->session->flashdata('info2'); ?>
    
     <script>
        jQuery(document).ready(function() {
            Main.init();
            TableExport.init();
            UIModals.init();
        });
    </script>

</body>

</html>