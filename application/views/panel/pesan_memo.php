<tr>
    <td>
        <p>
            Pesan
        </p>
    </td>
    <td>
        <div class="radio">
            <label>
                <input type="radio" value="Tindaklanjuti" id="radio" name="radio">
                Tindaklanjuti
            </label>
        </div>
        <div class="radio">
            <label>
                <input type="radio" value="Proses Sesuai Ketentuan/Peraturan yang Berlaku" id="radio" name="radio">
                Proses Sesuai Ketentuan/Peraturan yang Berlaku
            </label>
        </div>
        <div class="radio">
            <label>
                <input type="radio" value="Koordinasi/Konfirmasi dengan Ybs/Unit Kerja Terkait" id="radio" name="radio">
                Koordinasi/Konfirmasi dengan Ybs/Unit Kerja Terkait
            </label>
        </div>
        <div class="radio">
            <label>
                <input type="radio" value="Pelajari/Tela'ah dan Berikan Saran/Masukan" id="radio" name="radio">
                Pelajari/Tela'ah dan Berikan Saran/Masukan
            </label>
        </div>
        <div class="radio">
            <label>
                <input type="radio" value="Wakili/Hadir/Terima/Laporkan Hasilnya" id="radio" name="radio">
                Wakili/Hadir/Terima/Laporkan Hasilnya
            </label>
        </div>
        <div class="radio">
            <label>
                <input type="radio" value="Agendakan/Persiapkan/Koordinasikan" id="radio" name="radio">
                Agendakan/Persiapkan/Koordinasikan
            </label>
        </div>
        <div class="radio">
            <label>
                <input type="radio" value="Dijawab" name="radio" id="radio">
                Dijawab
            </label>
        </div>
        <div class="radio">
            <label>
                <input type="radio" value="Untuk Diketahui" name="radio" id="radio">
                Untuk Diketahui
            </label>
        </div>
        <div class="radio">
            <label>
                <input type="radio" value="Untuk Dimaklumi" name="radio" id="radio">
                Untuk Dimaklumi
            </label>
        </div>

        <div class="radio">
            <label>
                <input type="radio" value="Teruskan" name="radio" id="radio">
                Teruskan
            </label>
        </div>
        <div class="radio">
            <label>
                <input type="radio" value="Arsipkan" name="radio" id="radio">
                Arsipkan
            </label>
        </div>
    </td>
</tr>