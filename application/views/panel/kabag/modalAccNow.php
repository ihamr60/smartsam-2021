
<div id="modalAccNow" class="modal fade" tabindex="-1" data-width="760" style="display: none;">
    <form role="form" action="<?php echo base_url() ?>index.php/kabag/add_konsep_no_rektor" method="post" enctype="multipart/form-data" >
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                &times;
            </button>
            <h4 class="modal-title">MASUKAN PESAN / MEMO / KOREKSI UNTUK MELAKUKAN ACC: </h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <label>Pesan / Memo:</label>
                    <p>
                        <textarea name="konsep" cols="30" rows="3" class="form-control"></textarea>
                        <input type="hidden" name="draft_uuid" value="<?php echo $data_draft['draft_uuid'] ?>">
                        <input type="hidden" name="draft_asal" value="<?php echo $data_draft['draft_asal'] ?>">
                    </p>
                </div>

                <!--<div class="col-md-6">
                    <label>Disposisi Ke:</label>
                    <p>
                        <select
                            class="form-control"
                            name="disposisi"
                            required>
                            <option>Pilih Disposisi</option>
                            <?php
                            if($posisi == "Admin Persuratan Subbag TU")
                            { 
                                foreach($data_kabag_umum->result_array() as $d){
                                    echo "<option value='".$d['kabag_username']."'>".$d['kabag_nama']." (".$d['kabag_posisi'].")</option>";
                                }
                            }
                            else if($posisi == "Admin Persuratan TIK")
                            { 
                                foreach($data_ka_upt_tik->result_array() as $d){

                                    if($d['kasubbag_posisi']=="Ka UPT TIK")
                                    {
                                        echo '<option value="'.$d['kasubbag_username'].'">'.$d['kasubbag_nama'].' ('.$d['kasubbag_posisi'].')</option>"';    
                                    }
                                    
                                }
                            }
                            ?>
                        </select>
                    </p>
                </div> -->
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-light-grey">
                Cancel
            </button>
            <button type="submit" class="btn btn-blue">
                Submit
            </button>
        </div>
    </form>
</div>
