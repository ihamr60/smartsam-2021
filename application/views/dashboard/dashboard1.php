<div class="col-sm-3">
    <div class="panel panel-default">
        <div class="panel-heading">
            <i class="clip-stats"></i><b>PERSENTASE SURAT KELUAR</b>
            <div class="panel-tools">
                <a class="btn btn-xs btn-link panel-collapse collapses" href="#">
                </a>
                <a class="btn btn-xs btn-link panel-config" href="#panel-config" data-toggle="modal">
                    <i class="fa fa-wrench"></i>
                </a>
                <a class="btn btn-xs btn-link panel-refresh" href="#">
                    <i class="fa fa-refresh"></i>
                </a>
                <a class="btn btn-xs btn-link panel-close" href="#">
                    <i class="fa fa-times"></i>
                </a>
            </div>
        </div>
        <div class="panel-body">
            <div class="flot-small-container">
               <div id="report3" class="flot-placeholder"></div>
            </div>
        </div>
    </div>
</div>



<div class="col-sm-6">
    <div class="panel panel-default">
        <div class="panel-heading">
            <i class="clip-stats"></i><b>GRAFIK---</b>
            <div class="panel-tools">
                <a class="btn btn-xs btn-link panel-collapse collapses" href="#">
                </a>
                <a class="btn btn-xs btn-link panel-config" href="#panel-config" data-toggle="modal">
                    <i class="fa fa-wrench"></i>
                </a>
                <a class="btn btn-xs btn-link panel-refresh" href="#">
                    <i class="fa fa-refresh"></i>
                </a>
                <a class="btn btn-xs btn-link panel-close" href="#">
                    <i class="fa fa-times"></i>
                </a>
            </div>
        </div>
        <div class="panel-body">
            <div class="flot-small-container">
               <div id="report1" class="flot-placeholder"></div>
            </div>
        </div>
    </div>
</div>


<div class="col-sm-3">
    <div class="panel panel-default">
        <div class="panel-heading">
            <i class="clip-stats"></i><b>PERSENTASE SURAT MASUK</b>
            <div class="panel-tools">
                <a class="btn btn-xs btn-link panel-collapse collapses" href="#">
                </a>
                <a class="btn btn-xs btn-link panel-config" href="#panel-config" data-toggle="modal">
                    <i class="fa fa-wrench"></i>
                </a>
                <a class="btn btn-xs btn-link panel-refresh" href="#">
                    <i class="fa fa-refresh"></i>
                </a>
                <a class="btn btn-xs btn-link panel-close" href="#">
                    <i class="fa fa-times"></i>
                </a>
            </div>
        </div>
        <div class="panel-body">
            <div class="flot-small-container">
               <div id="report2" class="flot-placeholder"></div>
            </div>
        </div>
    </div>
</div>


