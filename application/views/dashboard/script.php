<?php 

  $selesai   = $this->load->web_app_model->getCount1Where('tbl_distribusi_surat','dist_status','Done');
  $process   = $this->load->web_app_model->getCount1Where('tbl_distribusi_surat','dist_status','Process');

  //SURAT KELUAR
  $selesai_keluar   = $this->load->web_app_model->getCount1Where('tbl_surat_keluar','out_status','Done');
  $process_keluar   = $this->load->web_app_model->getCount1Where('tbl_surat_keluar','out_status','Menunggu Upload');
?>

<script type="text/javascript">
  // Make monochrome colors
var pieColors = (function () {
  var colors = [],
    base = Highcharts.getOptions().colors[0],
    i;

  for (i = 0; i < 10; i += 1) {
    // Start out with a darkened base color (negative brighten), and end
    // up with a much brighter color
    colors.push(Highcharts.color(base).brighten((i - 3) / 7).get());
  }
  return colors;
}());

// Build the chart
Highcharts.chart('report2', {
  chart: {
    plotBackgroundColor: null,
    plotBorderWidth: null,
    plotShadow: false,
    type: 'pie'
  },
  title: {
    text: 'PERSENTASE PROGRESS DISTRIBUSI SURAT MASUK (ALL)'
  },
  tooltip: {
    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
  },
  accessibility: {
    point: {
      valueSuffix: '%'
    }
  },
  plotOptions: {
    pie: {
      allowPointSelect: true,
      cursor: 'pointer',
      colors: pieColors,
      dataLabels: {
        enabled: true,
        format: '<b>{point.name}</b><br>{point.percentage:.1f} %',
        distance: -50,
        filter: {
          property: 'percentage',
          operator: '>',
          value: 4
        }
      }
    }
  },
  series: [{
    name: 'Total',
    data: [
      { name: 'SELESAI', y: <?php echo $selesai['TOTAL'] ?> },
      { name: 'PROSES', y: <?php echo $process['TOTAL'] ?> },
    ]
  }]
});
</script>



<script type="text/javascript">
  // Make monochrome colors
var pieColors = (function () {
  var colors = [],
    base = Highcharts.getOptions().colors[0],
    i;

  for (i = 0; i < 10; i += 1) {
    // Start out with a darkened base color (negative brighten), and end
    // up with a much brighter color
    colors.push(Highcharts.color(base).brighten((i - 3) / 7).get());
  }
  return colors;
}());

// Build the chart
Highcharts.chart('report3', {
  chart: {
    plotBackgroundColor: null,
    plotBorderWidth: null,
    plotShadow: false,
    type: 'pie'
  },
  title: {
    text: 'PERSENTASE PROGRESS SURAT KELUAR (ALL)'
  },
  tooltip: {
    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
  },
  accessibility: {
    point: {
      valueSuffix: '%'
    }
  },
  plotOptions: {
    pie: {
      allowPointSelect: true,
      cursor: 'pointer',
      colors: pieColors,
      dataLabels: {
        enabled: true,
        format: '<b>{point.name}</b><br>{point.percentage:.1f} %',
        distance: -50,
        filter: {
          property: 'percentage',
          operator: '>',
          value: 4
        }
      }
    }
  },
  series: [{
    name: 'Total',
    data: [
      { name: 'Menunggu Upload', y: <?php echo $process_keluar['TOTAL'] ?> },
      { name: 'SELESAI', y: <?php echo $selesai_keluar['TOTAL'] ?> },
    ]
  }]
});
</script>