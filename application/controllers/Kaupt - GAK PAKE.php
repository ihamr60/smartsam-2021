<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kaupt extends MY_Controller {

	/**
	 Created by Ilham Ramadhan S.Tr.kom
	 0853 6188 5100
	 ilhamr6000@gmail.com
	 */

	public function bg_home()
	{
		$cek  = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='kaupt')
		{
			$bc['kabag_uuid'] 		= $this->session->userdata('uuid');
			$bc['posisi'] 			= $this->session->userdata('posisi');
			$bc['username'] 		= $this->session->userdata('username');
			$bc['nama'] 			= $this->session->userdata('nama');
			$bc['status'] 			= $this->session->userdata('stts');
			$bc['atas'] 			= $this->load->view('panel/atas',$bc,true);
			$bc['menu'] 			= $this->load->view('panel/kaupt/menu',$bc,true);
			$bc['bio'] 				= $this->load->view('panel/bio',$bc,true);	
			$this->load->view('panel/kaupt/bg_home',$bc);
		}
		else
		{
			header('location:'.base_url().'index.php/web');
		}
	}

























	//--=========================================

	public function bg_suratKeluar()
	{
		$cek  = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='kabag')
		{

			$bc['suratKeluar']		= $this->web_app_model->getAllData('tbl_surat_keluar');

			$bc['posisi'] 			= $this->session->userdata('posisi');
			$bc['nama'] 			= $this->session->userdata('nama');
			$bc['status'] 			= $this->session->userdata('stts');
			$bc['atas'] 			= $this->load->view('panel/atas',$bc,true);
			$bc['menu'] 			= $this->load->view('panel/kabag/menu',$bc,true);
			$bc['bio'] 				= $this->load->view('panel/bio',$bc,true);	
			$this->load->view('panel/kabag/bg_suratKeluar',$bc);
		}
		else
		{
			header('location:'.base_url().'index.php/web');
		}
	}

	public function bg_draft_surat()
	{
		$cek  			= $this->session->userdata('logged_in');
		$stts 			= $this->session->userdata('stts');
		$uuid_kabag 	= $this->session->userdata('uuid');
		if(!empty($cek) && $stts=='kabag')
		{
			$bc['data_draft'] 	= $this->web_app_model->get2JoinAllWhereGroup('konsep_uuid_draft','draft_uuid','kabag_uuid','konsep_tujuan','tbl_konsep_draft','tbl_draft_surat','tbl_kabag','tbl_konsep_draft','konsep_tujuan',$uuid_kabag,'draft_uuid');

			$bc['kabag_uuid'] 	= $this->session->userdata('uuid');
			$bc['posisi'] 			= $this->session->userdata('posisi');
			$bc['username'] 		= $this->session->userdata('username');
			$bc['nama'] 			= $this->session->userdata('nama');
			$bc['status'] 			= $this->session->userdata('stts');
			$bc['atas'] 			= $this->load->view('panel/atas',$bc,true);
			$bc['menu'] 			= $this->load->view('panel/kabag/menu',$bc,true);
			$bc['bio'] 				= $this->load->view('panel/bio',$bc,true);	
			$this->load->view('panel/kabag/bg_draft_surat',$bc);
		}
		else
		{
			header('location:'.base_url().'index.php/panel');
		}
	}

	public function add_konsep()
	{
		$draft_asal 				= $this->input->post('draft_asal');
		$draft_author 				= $this->session->userdata('username');
		$draft_uuid					= $this->input->post('draft_uuid');
		$draft_penerima				= $this->session->userdata('id_atasan');
		$draft_konsep				= $this->input->post('konsep');
		$kasubbag_uuid 				= $this->session->userdata('uuid');

		$konsep_uuid				= $this->get_id();

		$data1 = array(		
			'draft_penerima' 	=> $draft_penerima,
			);

		$where1 = array(		
			'draft_uuid' 		=> $draft_uuid,
			);
		
		$data2 = array(		
			'konsep_uuid' 		=> $konsep_uuid,
			'konsep_uuid_draft' => $draft_uuid,
			'konsep_asal' 		=> $draft_asal,
			'konsep_tujuan' 	=> $draft_penerima,
			'konsep_isi' 		=> $draft_konsep,
			'konsep_author' 	=> $draft_author,
			);

		$this->web_app_model->insertData($data2,'tbl_konsep_draft');
		$this->web_app_model->updateDataWhere($where1, $data1,'tbl_draft_surat');
		header('location:'.base_url().'index.php/kabag/bg_detail_draft/'.$draft_uuid.'/'.$kasubbag_uuid.'?draft=1/');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
												<button type='button' class='close' data-dismiss='alert'>
													<i class='icon-remove'></i>
												</button>
		
												<p>
													<strong>
														<i class='icon-ok'></i>
														Success! - 
													</strong>
													Konsep baru Berhasil Direkam!
												</p>
											</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
											     swal({
											                title: 'Success!!',
											                text:  'Konsep berhasil direkam!',
											                type: 'success',
											                timer: 3000,
											                showConfirmButton: true
											            });  
											     },10);  
											    </script>
											    ");
	}

	public function bg_detail_draft()
	{
		$cek  		= $this->session->userdata('logged_in');
		$stts 		= $this->session->userdata('stts');
		$username 	= $this->session->userdata('username');
		if(!empty($cek) && $stts=='kabag')
		{

			$bc['data_draft'] 	= $this->web_app_model->getJoinOneWhere('konsep_uuid_draft','draft_uuid','tbl_konsep_draft','tbl_draft_surat','draft_uuid',$this->uri->segment(3));
			
			$bc['riwayat_konsep'] 	= $this->web_app_model->getJoinAllWhere('konsep_uuid_draft','draft_uuid','tbl_konsep_draft','tbl_draft_surat','konsep_uuid_draft',$this->uri->segment(3),'konsep_tujuan',$username);

			$bc['data_staff']	= $this->web_app_model->getAllData('tbl_staff');

			//$bc['modalTambahSurat'] = $this->load->view('panel/modalTambahSurat',$bc,true);

			$bc['kabag_uuid'] 		= $this->session->userdata('uuid');
			$bc['username'] 		= $this->session->userdata('username');
			$bc['posisi'] 			= $this->session->userdata('posisi');
			$bc['nama'] 			= $this->session->userdata('nama');
			$bc['status'] 			= $this->session->userdata('stts');
			$bc['atas'] 			= $this->load->view('panel/atas',$bc,true);
			$bc['menu'] 			= $this->load->view('panel/kabag/menu',$bc,true);
			$bc['bio'] 				= $this->load->view('panel/bio',$bc,true);	
			$this->load->view('panel/kabag/bg_detail_draft',$bc);
		}
		else
		{
			header('location:'.base_url().'index.php/panel');
		}
	}

	public function bg_suratMasuk()
	{
		$cek  		= $this->session->userdata('logged_in');
		$stts 		= $this->session->userdata('stts');
		$username 	= $this->session->userdata('username');
		if(!empty($cek) && $stts=='kabag')
		{
			//$bc['data_rektor'] 		= $this->web_app_model->getAllData('tbl_rektor');
			//$bc['data_suratmasuk'] 	= $this->web_app_model->get2JoinAllWhereGroup('riw_dist_uuid','dist_uuid','biro_username','riw_disposisi','tbl_riwayat_disposisi','tbl_distribusi_surat','tbl_biro','tbl_riwayat_disposisi','dist_disposisi',$username,'dist_uuid');

			$bc['data_suratmasuk'] 	= $this->web_app_model->get2JoinAllWhereGroup('riw_dist_uuid','dist_uuid','kabag_username','riw_disposisi','tbl_riwayat_disposisi','tbl_distribusi_surat','tbl_kabag','tbl_riwayat_disposisi','riw_disposisi',$username,'dist_uuid');

			//$bc['modalTambahSurat'] = $this->load->view('panel/modalTambahSurat',$bc,true);
			$bc['kabag_uuid'] 		= $this->session->userdata('uuid');
			$bc['posisi'] 			= $this->session->userdata('posisi');
			$bc['username'] 		= $this->session->userdata('username');
			$bc['nama'] 			= $this->session->userdata('nama');
			$bc['status'] 			= $this->session->userdata('stts');
			$bc['atas'] 			= $this->load->view('panel/atas',$bc,true);
			$bc['menu'] 			= $this->load->view('panel/kabag/menu',$bc,true);
			$bc['bio'] 				= $this->load->view('panel/bio',$bc,true);	
			$this->load->view('panel/kabag/bg_suratMasuk',$bc);
		}
		else
		{
			header('location:'.base_url().'index.php/panel');
		}
	}

	public function bg_detail_surat()
	{
		$cek  		= $this->session->userdata('logged_in');
		$stts 		= $this->session->userdata('stts');
		$username 	= $this->session->userdata('username');
		if(!empty($cek) && $stts=='kabag')
		{
			//$bc['data_rektor'] 		= $this->web_app_model->getAllData('tbl_rektor');
			//$bc['data_suratmasuk'] 	= $this->web_app_model->get3JoinOneWhere('dist_disposisi','biro_username','riw_dist_uuid','dist_uuid','rektor_username','riw_asal','tbl_distribusi_surat','tbl_biro','tbl_riwayat_disposisi','tbl_distribusi_surat','tbl_rektor','tbl_riwayat_disposisi','dist_uuid',$this->uri->segment(3));

			$bc['data_suratmasuk'] 	= $this->web_app_model->getJoinOneWhere('riw_dist_uuid','dist_uuid','tbl_riwayat_disposisi','tbl_distribusi_surat','dist_uuid',$this->uri->segment(3));
			
			$bc['riwayat_memo'] 	= $this->web_app_model->getJoinAllWhere('riw_dist_uuid','dist_uuid','tbl_riwayat_disposisi','tbl_distribusi_surat','riw_dist_uuid',$this->uri->segment(3),'riw_disposisi',$username);

			$bc['data_kasubbag']	= $this->web_app_model->getWhereAllItem($this->uri->segment(4),'kasubbag_uuid_kabag','tbl_kasubbag');
			$bc['data_rektor']		= $this->web_app_model->getAllData('tbl_rektor');

			//$bc['modalTambahSurat'] = $this->load->view('panel/modalTambahSurat',$bc,true);
			$bc['kabag_uuid'] 		= $this->session->userdata('uuid');
			$bc['posisi'] 			= $this->session->userdata('posisi');
			$bc['username'] 		= $this->session->userdata('username');
			$bc['nama'] 			= $this->session->userdata('nama');
			$bc['status'] 			= $this->session->userdata('stts');
			$bc['atas'] 			= $this->load->view('panel/atas',$bc,true);
			$bc['menu'] 			= $this->load->view('panel/kabag/menu',$bc,true);
			$bc['bio'] 				= $this->load->view('panel/bio',$bc,true);	
			$this->load->view('panel/kabag/bg_detail_surat',$bc);
		}
		else
		{
			header('location:'.base_url().'index.php/panel');
		}
	}

	public function aksi_disposisi()
	{
		$username 					= $this->session->userdata('username');

		$riw_uuid					= $this->get_id();
		$dist_uuid					= $this->input->post('dist_uuid');
		$disposisi_kepada			= $this->input->post('disposisi_kepada');
		$riw_memo					= $this->input->post('memo');

		$riw_uuid_2					= $this->get_id();
		$riw_memo_2					= $this->input->post('radio');

		$data = array(		
			'riw_uuid' 			=> $riw_uuid,
			'riw_dist_uuid' 	=> $dist_uuid,
			'riw_asal' 			=> $username,
			'riw_disposisi' 	=> $disposisi_kepada,
			'riw_memo' 			=> $riw_memo,
			);

		$data_2 = array(		
			'riw_uuid' 			=> $riw_uuid_2,
			'riw_dist_uuid' 	=> $dist_uuid,
			'riw_asal' 			=> $username,
			'riw_disposisi' 	=> $disposisi_kepada,
			'riw_memo' 			=> $riw_memo_2,
			);
		
		$data2 = array(		
			'dist_disposisi' 	=> $disposisi_kepada,
			);

		$where = array(		
			'dist_uuid' 		=> $dist_uuid,
			);


		$this->web_app_model->updateDataWhere($where,$data2,'tbl_distribusi_surat');
		$this->web_app_model->insertData($data,'tbl_riwayat_disposisi');
		$this->web_app_model->insertData($data_2,'tbl_riwayat_disposisi');
		header('location:'.base_url().'index.php/kabag/bg_suratMasuk?suratMasuk=1/');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
												<button type='button' class='close' data-dismiss='alert'>
													<i class='icon-remove'></i>
												</button>
		
												<p>
													<strong>
														<i class='icon-ok'></i>
														Success! - 
													</strong>
													Data Surat Masuk berhasil ditambahkan!
												</p>
											</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
											     swal({
											                title: 'Success!!',
											                text:  'Data Surat Masuk berhasil ditambahkan!',
											                type: 'success',
											                timer: 3000,
											                showConfirmButton: true
											            });  
											     },10);  
											    </script>
											    ");
	}
//==========================================================




















	

	

	


//======================================================






















	

	

	















	// =================================================================================

	

	public function tambahSurat()
	{
		$dist_uuid					= $this->get_id();
		$dist_pengirim				= $this->input->post('pengirim');
		$dist_nomorSurat			= $this->input->post('nomorSurat');
		$dist_lampiran				= $this->input->post('lampiran');
		$dist_perihal				= $this->input->post('perihal');
		$dist_disposisi				= $this->input->post('disposisi');
		$dist_status				= "Process";

		// get foto
		 $config['upload_path'] 	= './upload/suratMasuk';
		 $config['allowed_types'] 	= 'jpg|pdf|jpeg';
		 $config['max_size'] 		= '5000';  //5MB max
		 //$config['max_width'] 	= '4480'; // pixel
		 //$config['max_height'] 	= '4480'; // pixel
		 $config['overwrite']		= true;
	     $config['file_name'] 		= $dist_pengirim.'-'.$dist_uuid;

     	 $this->load->library('upload', $config);

			if(!empty($_FILES['dokumen']['name'])) 
			{
		        if ( $this->upload->do_upload('dokumen') ) 
		        {
		            $foto = $this->upload->data();		

					$data = array(		
						'dist_uuid' 		=> $dist_uuid,
						'dist_pengirim' 	=> $dist_pengirim,
						'dist_nomorSurat' 	=> $dist_nomorSurat,
						'dist_lampiran' 	=> $dist_lampiran,
						'dist_perihal' 		=> $dist_perihal,
						'dist_disposisi' 	=> $dist_disposisi,
						'dist_status' 		=> $dist_status,
						'dist_dokumen' 		=> $foto['file_name'],
						);
		
					$this->web_app_model->insertData($data,'tbl_distribusi_surat');
					header('location:'.base_url().'index.php/panel/bg_suratMasuk?suratMasuk=1/');
					$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
															<button type='button' class='close' data-dismiss='alert'>
																<i class='icon-remove'></i>
															</button>
					
															<p>
																<strong>
																	<i class='icon-ok'></i>
																	Success! - 
																</strong>
																Data Surat Masuk berhasil ditambahkan!
															</p>
														</div>");

					$this->session->set_flashdata("info2","<script type='text/javascript'>
														     setTimeout(function () { 
														     swal({
														                title: 'Success!!',
														                text:  'Data Surat Masuk berhasil ditambahkan!',
														                type: 'success',
														                timer: 3000,
														                showConfirmButton: true
														            });  
														     },10);  
														    </script>
														    ");
				}
				else 
				{
		          	header('location:'.base_url().'index.php/panel/bg_suratMasuk?suratMasuk=1/');
					$this->session->set_flashdata("info2","<script type='text/javascript'>
										     setTimeout(function () { 
										     swal({
										                title: 'Maaf!',
										                text:  'Data gagal tersimpan, pastikan dokumen max 5 Mb atau hubungi IT PDAM',
										                type: 'warning',
										                timer: 300000,
										                showConfirmButton: true
										            });  
										     },10);  
										    </script>");
		    	}
		    }
		    else 
			{
	          	header('location:'.base_url().'index.php/panel/bg_suratMasuk?suratMasuk=1/');
				$this->session->set_flashdata("info2","<script type='text/javascript'>
									     setTimeout(function () { 
									     swal({
									                title: 'Dokumen kosong!',
									                text:  'Mohon lampirkan dokumen surat masuk',
									                type: 'warning',
									                timer: 300000,
									                showConfirmButton: true
									            });  
									     },10);  
									    </script>");
		    }
	}
}
