<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Super_admin extends MY_Controller {

	/**
	 Created by Ilham Ramadhan S.Tr.kom
	 0853 6188 5100
	 ilhamr6000@gmail.com
	 */

	public function bg_home()
	{
		$cek  = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='super admin')
		{	
			$bc['posisi'] 			= $this->session->userdata('posisi');
			$bc['nama'] 			= $this->session->userdata('nama');
			$bc['status'] 			= $this->session->userdata('stts');
			$bc['dashboard'] 		= $this->load->view('dashboard/dashboard1',$bc,true);
			$bc['script'] 			= $this->load->view('dashboard/script',$bc,true);
			$bc['atas'] 			= $this->load->view('panel/atas',$bc,true);
			$bc['menu'] 			= $this->load->view('panel/super_admin/menu',$bc,true);
			$bc['bio'] 				= $this->load->view('panel/bio',$bc,true);	
			$this->load->view('panel/super_admin/bg_home',$bc);
		}
		else
		{
			header('location:'.base_url().'index.php/web');
		}
	}

	public function bg_rektor()
	{
		$cek  = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='super admin')
		{
			$bc['data_rektor'] 		= $this->web_app_model->getAllData('tbl_rektor');

			//$bc['data_rektor'] 		= $this->web_app_model->getAllData('tbl_rektor');

			$bc['posisi'] 			= $this->session->userdata('posisi');
			$bc['nama'] 			= $this->session->userdata('nama');
			$bc['status'] 			= $this->session->userdata('stts');
			$bc['modalEditRektor']= $this->load->view('panel/super_admin/modalEditRektor',$bc,true);			
			$bc['modalTambahRektor']= $this->load->view('panel/super_admin/modalTambahRektor',$bc,true);
			$bc['atas'] 			= $this->load->view('panel/atas',$bc,true);
			$bc['menu'] 			= $this->load->view('panel/super_admin/menu',$bc,true);
			$bc['bio'] 				= $this->load->view('panel/bio',$bc,true);	
			$this->load->view('panel/super_admin/bg_rektor',$bc);
		}
		else
		{
			header('location:'.base_url().'index.php/web');
		}
	}

	public function bg_dekan()
	{
		$cek  = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='super admin')
		{
			//$bc['data_dekan'] 		= $this->web_app_model->getAll2Join('fak_dekan_fakultas','fak_fakultas_no','fak_dekan','fak_fakultas');
			
			$bc['data_dekan'] 		= $this->web_app_model->get_dekan();
			$bc['data_kabag'] 		= $this->web_app_model->get_kabag_prodi_dekan('kabag fakultas');
			$bc['data_kasubbag'] 	= $this->web_app_model->get_kasubbag_dekan('kasubbag fakultas');
			$bc['data_pelaksana'] 	= $this->web_app_model->get_pelaksana_dekan('pelaksana fakultas');
			$bc['data_admin_surat'] = $this->web_app_model->get_admsurat_dekan('Admin Surat Fak. Hukum','Admin Surat Fak. Ekonomi','Admin Surat Fak. Pertanian','Admin Surat Fak. Keguruan','Admin Surat Fak. Teknik');

			$bc['data_prodi'] 		= $this->web_app_model->getAllData('tbl_prodi');

			$bc['posisi'] 			= $this->session->userdata('posisi');
			$bc['nama'] 			= $this->session->userdata('nama');
			$bc['status'] 			= $this->session->userdata('stts');
			$bc['modalEditDekan']	= $this->load->view('panel/super_admin/modalEditDekan',$bc,true);			
			$bc['modalTambahDekan']	= $this->load->view('panel/super_admin/modalTambahDekan',$bc,true);

			$bc['modalEditKabagProdi']		= $this->load->view('panel/super_admin/modalEditKabagProdi',$bc,true);			
			$bc['modalTambahKabagProdi']	= $this->load->view('panel/super_admin/modalTambahKabagProdi',$bc,true);

			$bc['modalEditAdminSuratFak']	= $this->load->view('panel/super_admin/modalEditAdminSuratFak',$bc,true);			
			$bc['modalTambahAdminSuratFak']	= $this->load->view('panel/super_admin/modalTambahAdminSuratFak',$bc,true);

			$bc['modalEditPelaksanaFak']	= $this->load->view('panel/super_admin/modalEditPelaksanaFak',$bc,true);			
			$bc['modalTambahPelaksanaFak']	= $this->load->view('panel/super_admin/modalTambahPelaksanaFak',$bc,true);

			$bc['modalEditKasubbagFak']		= $this->load->view('panel/super_admin/modalEditKasubbagFak',$bc,true);			
			$bc['modalTambahKasubbagFak']	= $this->load->view('panel/super_admin/modalTambahKasubbagFak',$bc,true);

			$bc['atas'] 			= $this->load->view('panel/atas',$bc,true);
			$bc['menu'] 			= $this->load->view('panel/super_admin/menu',$bc,true);
			$bc['bio'] 				= $this->load->view('panel/bio',$bc,true);	
			$this->load->view('panel/super_admin/bg_dekan',$bc);
		}
		else
		{
			header('location:'.base_url().'index.php/web');
		}
	}

	public function bg_lpppm()
	{
		$cek  = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='super admin')
		{
			$bc['data_kepala'] 		= $this->web_app_model->getWhereAllItem('lpppm','biro_kategori','tbl_biro');
			$bc['data_sekretaris'] 	= $this->web_app_model->getWhereAllItem('lpppm','kabag_kategori','tbl_kabag');
			$bc['data_kasubbag'] 	= $this->web_app_model->getWhereAllItem('lpppm','kasubbag_kategori','tbl_kasubbag');
			$bc['data_pelaksana'] 	= $this->web_app_model->getWhereAllItem('lpppm','staff_kategori','tbl_staff');
			$bc['data_admin_surat'] = $this->web_app_model->getWhereAllItem('Admin Surat LPPPM & PM','jabatan','tbl_users');
			$bc['data_kabag'] 		= $this->web_app_model->getWhereAllItem('Sekretaris LPPPM & PM','kabag_posisi','tbl_kabag');
			
			$bc['data_dekan'] 		= $this->web_app_model->get_dekan();
			//$bc['data_kabag'] 	= $this->web_app_model->get_kabag_prodi_dekan('kabag fakultas');
			//$bc['data_kasubbag'] 	= $this->web_app_model->get_kasubbag_dekan('kasubbag fakultas');
			//$bc['data_pelaksana'] = $this->web_app_model->get_pelaksana_dekan('pelaksana fakultas');
			//$bc['data_admin_surat'] = $this->web_app_model->get_admsurat_dekan('Admin Surat Fak. Hukum','Admin Surat Fak. Ekonomi','Admin Surat Fak. Pertanian','Admin Surat Fak. Keguruan','Admin Surat Fak. Teknik');

			$bc['data_prodi'] 		= $this->web_app_model->getAllData('tbl_prodi');

			$bc['posisi'] 			= $this->session->userdata('posisi');
			$bc['nama'] 			= $this->session->userdata('nama');
			$bc['status'] 			= $this->session->userdata('stts');
			$bc['modalEditKepala']	= $this->load->view('panel/super_admin/modalEditKepalaLpppm',$bc,true);			
			$bc['modalTambahKepala']	= $this->load->view('panel/super_admin/modalTambahKepalaLpppm',$bc,true);

			$bc['modalEditSekretarisLpppm']		= $this->load->view('panel/super_admin/modalEditSekretarisLpppm',$bc,true);			
			$bc['modalTambahSekretarisLpppm']	= $this->load->view('panel/super_admin/modalTambahSekretarisLpppm',$bc,true);

			$bc['modalEditAdminSuratLpppm']		= $this->load->view('panel/super_admin/modalEditAdminSuratLpppm',$bc,true);			
			$bc['modalTambahAdminSuratLpppm']	= $this->load->view('panel/super_admin/modalTambahAdminSuratLpppm',$bc,true);

			$bc['modalEditPelaksanaLpppm']		= $this->load->view('panel/super_admin/modalEditPelaksanaLpppm',$bc,true);			
			$bc['modalTambahPelaksanaLpppm']	= $this->load->view('panel/super_admin/modalTambahPelaksanaLpppm',$bc,true);

			$bc['modalEditKasubbagTULpppm']		= $this->load->view('panel/super_admin/modalEditKasubbagTULpppm',$bc,true);			
			$bc['modalTambahKasubbagTULpppm']	= $this->load->view('panel/super_admin/modalTambahKasubbagTULpppm',$bc,true);

			$bc['atas'] 			= $this->load->view('panel/atas',$bc,true);
			$bc['menu'] 			= $this->load->view('panel/super_admin/menu',$bc,true);
			$bc['bio'] 				= $this->load->view('panel/bio',$bc,true);	
			$this->load->view('panel/super_admin/bg_lpppm',$bc);
		}
		else
		{
			header('location:'.base_url().'index.php/web');
		}
	}

	public function editAdminSuratLpppm()
	{
		//date_default_timezone_set('Asia/Jakarta');

		//$staff_uuid				= $this->input->post('staff_uuid');
		$username			= $this->input->post('username');
		$nama_lengkap		= $this->input->post('nama_lengkap');
		$jabatan			= $this->input->post('posisi');
		$uuid_atasan		= $this->input->post('atasan');

		//$staff_uuid_kasubbag	= $this->input->post('staff_uuid_kasubbag');

		$data = array(		
			'nama_lengkap' 			=> $nama_lengkap,
			'jabatan' 				=> $jabatan,
			'uuid_atasan' 			=> $uuid_atasan,
			);

		$where = array(
			'username' 				=> $username,
			);
		
		//$cek_posisi 			= $this->web_app_model->getWhereAllItem($kasubbag_posisi,'kasubbag_posisi','tbl_kasubbag');
	//	$cek_posisi 			= $this->web_app_model->get2WhereAllItem($staff_posisi,'staff_posisi',$staff_uuid,'staff_uuid','tbl_staff');

		
			$this->web_app_model->updateDataWhere($where,$data,'tbl_users');
			header('location:'.base_url().'index.php/super_admin/bg_lpppm?data_users=1/');
			$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
													<button type='button' class='close' data-dismiss='alert'>
														<i class='icon-remove'></i>
													</button>
			
													<p>
														<strong>
															<i class='icon-ok'></i>
															Success! - 
														</strong>
														Data Admin Surat LPPPM & PM berhasil diupdate!
													</p>
												</div>");

			$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
														     swal({
														                title: 'Success!!',
														                text:  'Data Admin Surat LPPPM & PM berhasil diupdate!',
														                type: 'success',
														                timer: 3000,
														                showConfirmButton: true
														            });  
														     },10);  
														    </script>
														    ");
		
	}

	public function tambahAdminSuratLpppm()
	{
		date_default_timezone_set('Asia/Jakarta');

		$username 				= $this->input->post('username');
		$nama_lengkap			= $this->input->post('nama_lengkap');
		$jabatan				= $this->input->post('posisi');
		$status					= "admin surat";
		$uuid_atasan			= $this->input->post('atasan');

		$data = array(		
			'username' 			=> $username,
			'nama_lengkap' 		=> $nama_lengkap,
			'jabatan' 			=> $jabatan,
			'status' 			=> $status,
			'uuid_atasan' 		=> $uuid_atasan,
			);

		$login = array(		
			'username' 		=> $username,
			'stts' 			=> 'admin surat',
			'password' 		=> md5($username),
			);

		$this->web_app_model->insertData($data,'tbl_users');
		$this->web_app_model->insertData($login,'tbl_login');
		header('location:'.base_url().'index.php/super_admin/bg_lpppm?data_users=1/');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
												<button type='button' class='close' data-dismiss='alert'>
													<i class='icon-remove'></i>
												</button>
		
												<p>
													<strong>
														<i class='icon-ok'></i>
														Success! - 
													</strong>
													Data Admin Persuratan LPPPM & PM berhasil ditambahkan!
												</p>
											</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
														     swal({
														                title: 'Success!!',
														                text:  'Data Admin Persuratan LPPPM & PM berhasil ditambahkan!',
														                type: 'success',
														                timer: 3000,
														                showConfirmButton: true
														            });  
														     },10);  
														    </script>
														    ");			
	}

	public function tambahPelaksanaLpppm()
	{
		$staff_username 		= $this->input->post('staff_username');
		$staff_nama				= $this->input->post('staff_nama');
		$staff_posisi			= $this->input->post('staff_posisi');
		$staff_uuid_kasubbag	= $this->input->post('staff_uuid_kasubbag');
		$staff_email			= $this->input->post('staff_email');
		$staff_uuid				= $this->get_id();

		$cek_posisi 				= $this->web_app_model->getWhereAllItem($staff_posisi,'staff_posisi','tbl_staff');
		$cek_username 				= $this->web_app_model->getWhereAllItem($staff_username,'username','tbl_login');

		$data = array(		
			'staff_uuid' 		=> $staff_uuid,
			'staff_username' 	=> $staff_username,
			'staff_nama' 		=> $staff_nama,
			'staff_posisi' 		=> $staff_posisi,
			'staff_uuid_kasubbag' 	=> $staff_uuid_kasubbag,
			'staff_email' 		=> $staff_email,
			'staff_kategori' 	=> 'lpppm',
			);

		$login = array(		
			'username' 		=> $staff_username,
			'stts' 			=> 'pelaksana',
			'password' 		=> md5($staff_username),
			);

			$this->web_app_model->insertData($login,'tbl_login');
			$this->web_app_model->insertData($data,'tbl_staff');
			header('location:'.base_url().'index.php/super_admin/bg_lpppm?data_users=1/');
			$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
													<button type='button' class='close' data-dismiss='alert'>
														<i class='icon-remove'></i>
													</button>
			
													<p>
														<strong>
															<i class='icon-ok'></i>
															Success! - 
														</strong>
														Data Staff / Pelaksana LPPPM & PM berhasil ditambahkan!
													</p>
												</div>");

			$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
														     swal({
														                title: 'Success!!',
														                text:  'Data Staff / Pelaksana LPPPM & PM berhasil ditambahkan!',
														                type: 'success',
														                timer: 3000,
														                showConfirmButton: true
														            });  
														     },10);  
														    </script>
														    ");
	}

	public function tambahKasubbagTULpppm()
	{
		$kasubbag_username 			= $this->input->post('kasubbag_username');
		$kasubbag_nama				= $this->input->post('kasubbag_nama');
		$kasubbag_posisi			= $this->input->post('kasubbag_posisi');
		$kasubbag_uuid_kabag		= $this->input->post('kasubbag_uuid_kabag');
		$kasubbag_email				= $this->input->post('kasubbag_email');
		$kasubbag_uuid				= $this->get_id();

		$cek_posisi 				= $this->web_app_model->getWhereAllItem($kasubbag_posisi,'kasubbag_posisi','tbl_kasubbag');
		$cek_username 				= $this->web_app_model->getWhereAllItem($kasubbag_username,'username','tbl_login');

		$data = array(		
			'kasubbag_uuid' 		=> $kasubbag_uuid,
			'kasubbag_username' 	=> $kasubbag_username,
			'kasubbag_nama' 		=> $kasubbag_nama,
			'kasubbag_posisi' 		=> $kasubbag_posisi,
			'kasubbag_uuid_kabag' 	=> $kasubbag_uuid_kabag,
			'kasubbag_email' 		=> $kasubbag_email,
			'kasubbag_kategori' 	=> 'lpppm',
			);

		$login = array(		
			'username' 		=> $kasubbag_username,
			'stts' 			=> 'kasubbag',
			'password' 		=> md5($kasubbag_username),
			);

		if($cek_posisi->num_rows() > 0 || $cek_username->num_rows() > 0)
		{
			header('location:'.base_url().'index.php/super_admin/bg_lpppm?data_users=1/');
			$this->session->set_flashdata("info","<div class='alert alert-block alert-warning'>
													<button type='button' class='close' data-dismiss='alert'>
														<i class='icon-remove'></i>
													</button>
			
													<p>
														<strong>
															<i class='icon-ok'></i>
															Gagal! - 
														</strong>
														Data dengan posisi / username tersebut sudah digunakan!
													</p>
												</div>");

			$this->session->set_flashdata("info2","<script type='text/javascript'>
												     setTimeout(function () { 
														     swal({
														                title: 'Gagal!!',
														                text:  'Data dengan posisi / username tersebut sudah digunakan!',
														                type: 'warning',
														                timer: 3000,
														                showConfirmButton: true
														            });  
														     },10);  
														    </script>
														    ");
		}
		else
		{
			$this->web_app_model->insertData($login,'tbl_login');
			$this->web_app_model->insertData($data,'tbl_kasubbag');
			header('location:'.base_url().'index.php/super_admin/bg_lpppm?data_users=1/');
			$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
													<button type='button' class='close' data-dismiss='alert'>
														<i class='icon-remove'></i>
													</button>
			
													<p>
														<strong>
															<i class='icon-ok'></i>
															Success! - 
														</strong>
														Data Kasubbag TU LPPPM & PM berhasil ditambahkan!
													</p>
												</div>");

			$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
														     swal({
														                title: 'Success!!',
														                text:  'Data Kasubbag TU LPPPM & PM berhasil ditambahkan!',
														                type: 'success',
														                timer: 3000,
														                showConfirmButton: true
														            });  
														     },10);  
														    </script>
														    ");
		}			
	}

	public function tambahKepalaLpppm()
	{
		$biro_username 			= $this->input->post('biro_username');
		$biro_nama				= $this->input->post('biro_nama');
		$biro_posisi			= $this->input->post('biro_posisi');
		$biro_email				= $this->input->post('biro_email');
		$biro_uuid				= $this->get_id();

		$cek_posisi 			= $this->web_app_model->getWhereAllItem($biro_posisi,'biro_posisi','tbl_biro');
		$cek_username 			= $this->web_app_model->getWhereAllItem($biro_username,'username','tbl_login');

		$data = array(		
			'biro_uuid' 		=> $biro_uuid,
			'biro_username' 	=> $biro_username,
			'biro_nama' 		=> $biro_nama,
			'biro_posisi' 		=> $biro_posisi,
			'biro_email' 		=> $biro_email,
			'biro_kategori' 	=> 'lpppm',
			);

		$login = array(		
			'username' 		=> $biro_username,
			'stts' 			=> 'biro',
			'password' 		=> md5($biro_username),
			);

		if($cek_posisi->num_rows() > 0 || $cek_username->num_rows() > 0)
		{
			header('location:'.base_url().'index.php/super_admin/bg_lpppm?data_users=1/');
			$this->session->set_flashdata("info","<div class='alert alert-block alert-warning'>
													<button type='button' class='close' data-dismiss='alert'>
														<i class='icon-remove'></i>
													</button>
			
													<p>
														<strong>
															<i class='icon-ok'></i>
															Gagal! - 
														</strong>
														Data dengan posisi / username tersebut sudah digunakan!
													</p>
												</div>");

			$this->session->set_flashdata("info2","<script type='text/javascript'>
												     setTimeout(function () { 
														     swal({
														                title: 'Gagal!!',
														                text:  'Data dengan posisi / username tersebut sudah digunakan!',
														                type: 'warning',
														                timer: 3000,
														                showConfirmButton: true
														            });  
														     },10);  
														    </script>
														    ");
		}
		else
		{
			$this->web_app_model->insertData($login,'tbl_login');
			$this->web_app_model->insertData($data,'tbl_biro');
			header('location:'.base_url().'index.php/super_admin/bg_lpppm?data_users=1/');
			$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
													<button type='button' class='close' data-dismiss='alert'>
														<i class='icon-remove'></i>
													</button>
			
													<p>
														<strong>
															<i class='icon-ok'></i>
															Success! - 
														</strong>
														Data Ketua LPPPM & PM berhasil ditambahkan!
													</p>
												</div>");

			$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
														     swal({
														                title: 'Success!!',
														                text:  'Data Ketua LPPPM & PM berhasil ditambahkan!',
														                type: 'success',
														                timer: 3000,
														                showConfirmButton: true
														            });  
														     },10);  
														    </script>
														    ");
		}			
	}

	public function editKepalaLpppm()
	{
		date_default_timezone_set('Asia/Jakarta');

		$biro_posisi_before			= $this->input->post('biro_posisi_before');
		$biro_uuid					= $this->input->post('biro_uuid');
		$biro_nama					= $this->input->post('biro_nama');
		$biro_posisi				= $this->input->post('biro_posisi');
		$biro_email					= $this->input->post('biro_email');

		$data = array(		
			'biro_nama' 			=> $biro_nama,
			'biro_posisi' 			=> $biro_posisi,
			'biro_email' 			=> $biro_email,
			);

		$where = array(
			'biro_uuid' 			=> $biro_uuid,
			);

		//$cek_posisi 			= $this->web_app_model->getWhereAllItem($biro_posisi,'biro_posisi','tbl_biro');
		$cek_posisi 			= $this->web_app_model->get2WhereAllItem($biro_posisi,'biro_posisi',$biro_uuid,'biro_uuid','tbl_biro');

		if($cek_posisi->num_rows() < 1)
		{
			header('location:'.base_url().'index.php/super_admin/bg_lpppm?data_users=1/');
			$this->session->set_flashdata("info","<div class='alert alert-block alert-warning'>
													<button type='button' class='close' data-dismiss='alert'>
														<i class='icon-remove'></i>
													</button>
			
													<p>
														<strong>
															<i class='icon-ok'></i>
															Gagal! - 
														</strong>
														Data dengan posisi / username tersebut sudah digunakan!
													</p>
												</div>");

			$this->session->set_flashdata("info2","<script type='text/javascript'>
												     setTimeout(function () { 
														     swal({
														                title: 'Gagal!!',
														                text:  'Data dengan posisi / username tersebut sudah digunakan!',
														                type: 'warning',
														                timer: 3000,
														                showConfirmButton: true
														            });  
														     },10);  
														    </script>
														    ");
		}
		else
		{
			$this->web_app_model->updateDataWhere($where, $data,'tbl_biro');
			header('location:'.base_url().'index.php/super_admin/bg_lpppm?data_users=1/');
			$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
													<button type='button' class='close' data-dismiss='alert'>
														<i class='icon-remove'></i>
													</button>
			
													<p>
														<strong>
															<i class='icon-ok'></i>
															Success! - 
														</strong>
														Data Ketua LPPPM & PM berhasil diupdate!
													</p>
												</div>");

			$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
														     swal({
														                title: 'Success!!',
														                text:  'Data Ketua LPPPM & PM berhasil diupdate!',
														                type: 'success',
														                timer: 3000,
														                showConfirmButton: true
														            });  
														     },10);  
														    </script>
														    ");
		}	

	}

	public function tambahSekretarisLpppm()
	{
		$kabag_username 		= $this->input->post('kabag_username');
		$kabag_nama				= $this->input->post('kabag_nama');
		$kabag_posisi			= $this->input->post('kabag_posisi');
		$kabag_uuid_biro		= $this->input->post('kabag_uuid_biro');
		$kabag_email			= $this->input->post('kabag_email');
		$kabag_uuid				= $this->get_id();

		$cek_posisi 			= $this->web_app_model->getWhereAllItem($kabag_posisi,'kabag_posisi','tbl_kabag');
		$cek_username 			= $this->web_app_model->getWhereAllItem($kabag_username,'username','tbl_login');

		$data = array(		
			'kabag_uuid' 		=> $kabag_uuid,
			'kabag_username' 	=> $kabag_username,
			'kabag_nama' 		=> $kabag_nama,
			'kabag_posisi' 		=> $kabag_posisi,
			'kabag_uuid_biro' 	=> $kabag_uuid_biro,
			'kabag_email' 		=> $kabag_email,
			'kabag_kategori' 	=> 'lpppm',
			);

		$login = array(		
			'username' 		=> $kabag_username,
			'stts' 			=> 'kabag',
			'password' 		=> md5($kabag_username),
			);

		if($cek_posisi->num_rows() > 0 || $cek_username->num_rows() > 0)
		{
			header('location:'.base_url().'index.php/super_admin/bg_lpppm?data_users=1/');
			$this->session->set_flashdata("info","<div class='alert alert-block alert-warning'>
													<button type='button' class='close' data-dismiss='alert'>
														<i class='icon-remove'></i>
													</button>
			
													<p>
														<strong>
															<i class='icon-ok'></i>
															Gagal! - 
														</strong>
														Data dengan posisi / username tersebut sudah digunakan!
													</p>
												</div>");

			$this->session->set_flashdata("info2","<script type='text/javascript'>
												     setTimeout(function () { 
														     swal({
														                title: 'Gagal!!',
														                text:  'Data dengan posisi / username tersebut sudah digunakan!',
														                type: 'warning',
														                timer: 3000,
														                showConfirmButton: true
														            });  
														     },10);  
														    </script>
														    ");
		}
		else
		{
			$this->web_app_model->insertData($login,'tbl_login');
			$this->web_app_model->insertData($data,'tbl_kabag');
			header('location:'.base_url().'index.php/super_admin/bg_lpppm?data_users=1/');
			$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
													<button type='button' class='close' data-dismiss='alert'>
														<i class='icon-remove'></i>
													</button>
			
													<p>
														<strong>
															<i class='icon-ok'></i>
															Success! - 
														</strong>
														Data Sekretaris LPPPM & PM berhasil ditambahkan!
													</p>
												</div>");

			$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
														     swal({
														                title: 'Success!!',
														                text:  'Data Sekretaris LPPPM & PM berhasil ditambahkan!',
														                type: 'success',
														                timer: 3000,
														                showConfirmButton: true
														            });  
														     },10);  
														    </script>
														    ");
		}			
	}

	public function editKasubbagTULpppm()
	{
		date_default_timezone_set('Asia/Jakarta');

		$kasubbag_username			= $this->input->post('kasubbag_username');
		$kasubbag_nama				= $this->input->post('kasubbag_nama');
		$kasubbag_posisi			= $this->input->post('kasubbag_posisi');
		$kasubbag_uuid				= $this->input->post('kasubbag_uuid');
		$kasubbag_email				= $this->input->post('kasubbag_email');
		$kasubbag_uuid_kabag		= $this->input->post('kasubbag_uuid_kabag');

		$data = array(		
			'kasubbag_nama' 		=> $kasubbag_nama,
			'kasubbag_posisi' 		=> $kasubbag_posisi,
			'kasubbag_email' 		=> $kasubbag_email,
			'kasubbag_uuid_kabag' 	=> $kasubbag_uuid_kabag,
			);

		$where = array(
			'kasubbag_uuid' 		=> $kasubbag_uuid,
			);

		
		
		$cek_posisi 			= $this->web_app_model->getWhereAllItem($kasubbag_posisi,'kasubbag_posisi','tbl_kasubbag');
		$cek_username 			= $this->web_app_model->getWhereAllItem($kasubbag_username,'username','tbl_login');

		if($cek_posisi->num_rows() > 0 && $cek_username->num_rows() < 1)
		{
			header('location:'.base_url().'index.php/super_admin/bg_lpppm?data_users=1/');
			$this->session->set_flashdata("info","<div class='alert alert-block alert-warning'>
													<button type='button' class='close' data-dismiss='alert'>
														<i class='icon-remove'></i>
													</button>
			
													<p>
														<strong>
															<i class='icon-ok'></i>
															Gagal! - 
														</strong>
														Data dengan posisi / username tersebut sudah digunakan!
													</p>
												</div>");

			$this->session->set_flashdata("info2","<script type='text/javascript'>
												     setTimeout(function () { 
														     swal({
														                title: 'Gagal!!',
														                text:  'Data dengan posisi / username tersebut sudah digunakan!',
														                type: 'warning',
														                timer: 3000,
														                showConfirmButton: true
														            });  
														     },10);  
														    </script>
														    ");
		}
		else
		{
			$this->web_app_model->updateDataWhere($where, $data,'tbl_kasubbag');
			header('location:'.base_url().'index.php/super_admin/bg_lpppm?data_users=1/');
			$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
													<button type='button' class='close' data-dismiss='alert'>
														<i class='icon-remove'></i>
													</button>
			
													<p>
														<strong>
															<i class='icon-ok'></i>
															Success! - 
														</strong>
														Data Kasubbag TU LPPPM & PM berhasil diupdate!
													</p>
												</div>");

			$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
														     swal({
														                title: 'Success!!',
														                text:  'Data Kasubbag TU LPPPM & PM berhasil diupdate!',
														                type: 'success',
														                timer: 3000,
														                showConfirmButton: true
														            });  
														     },10);  
														    </script>
														    ");
		}
	}

	public function editSekretarisLpppm()
	{
		date_default_timezone_set('Asia/Jakarta');

		$kabag_username			= $this->input->post('kabag_username');
		$kabag_nama				= $this->input->post('kabag_nama');
		$kabag_posisi			= $this->input->post('kabag_posisi');
		$kabag_uuid				= $this->input->post('kabag_uuid');
		$kabag_email			= $this->input->post('kabag_email');
		$kabag_uuid_biro		= $this->input->post('kabag_uuid_biro');

		$data = array(		
			'kabag_nama' 		=> $kabag_nama,
			'kabag_posisi' 		=> $kabag_posisi,
			'kabag_email' 		=> $kabag_email,
			'kabag_uuid_biro' 	=> $kabag_uuid_biro,
			);

		$where = array(
			'kabag_uuid' 		=> $kabag_uuid,
			);

		
		
		$cek_posisi 			= $this->web_app_model->getWhereAllItem($kabag_posisi,'kabag_posisi','tbl_kabag');
		$cek_username 			= $this->web_app_model->get2WhereAllItem($kabag_posisi,'kabag_posisi',$kabag_uuid,'kabag_uuid','tbl_kabag');

		if($cek_posisi->num_rows() > 0 && $cek_username->num_rows() < 1)
		{
			header('location:'.base_url().'index.php/super_admin/bg_lpppm?data_users=1/');
			$this->session->set_flashdata("info","<div class='alert alert-block alert-warning'>
													<button type='button' class='close' data-dismiss='alert'>
														<i class='icon-remove'></i>
													</button>
			
													<p>
														<strong>
															<i class='icon-ok'></i>
															Gagal! - 
														</strong>
														Data dengan posisi / username tersebut sudah digunakan!
													</p>
												</div>");

			$this->session->set_flashdata("info2","<script type='text/javascript'>
												     setTimeout(function () { 
														     swal({
														                title: 'Gagal!!',
														                text:  'Data dengan posisi / username tersebut sudah digunakan!',
														                type: 'warning',
														                timer: 3000,
														                showConfirmButton: true
														            });  
														     },10);  
														    </script>
														    ");
		}
		else
		{
			$this->web_app_model->updateDataWhere($where, $data,'tbl_kabag');
			header('location:'.base_url().'index.php/super_admin/bg_lpppm?data_users=1/');
			$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
													<button type='button' class='close' data-dismiss='alert'>
														<i class='icon-remove'></i>
													</button>
			
													<p>
														<strong>
															<i class='icon-ok'></i>
															Success! - 
														</strong>
														Data Sekretaris LPPPM & PM berhasil diupdate!
													</p>
												</div>");

			$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
														     swal({
														                title: 'Success!!',
														                text:  'Data Sekretaris LPPPM & PM berhasil diupdate!',
														                type: 'success',
														                timer: 3000,
														                showConfirmButton: true
														            });  
														     },10);  
														    </script>
														    ");
		}
	}


	public function bg_fakultas()
	{
		$cek  = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='super admin')
		{
			$bc['data_fakultas'] 		= $this->web_app_model->getAllData('fak_fakultas');

			//$bc['data_rektor'] 		= $this->web_app_model->getAllData('tbl_rektor');

			$bc['posisi'] 				= $this->session->userdata('posisi');
			$bc['nama'] 				= $this->session->userdata('nama');
			$bc['status'] 				= $this->session->userdata('stts');
			$bc['modalEditFakultas']	= $this->load->view('panel/super_admin/modalEditFakultas',$bc,true);			
			$bc['modalTambahFakultas']	= $this->load->view('panel/super_admin/modalTambahFakultas',$bc,true);
			$bc['atas'] 				= $this->load->view('panel/atas',$bc,true);
			$bc['menu'] 				= $this->load->view('panel/super_admin/menu',$bc,true);
			$bc['bio'] 					= $this->load->view('panel/bio',$bc,true);	
			$this->load->view('panel/super_admin/bg_fakultas',$bc);
		}
		else
		{
			header('location:'.base_url().'index.php/web');
		}
	}

	public function bg_kode_hal()
	{
		$cek  		= $this->session->userdata('logged_in');
		$stts 		= $this->session->userdata('stts');
		$username 	= $this->session->userdata('username');
		if(!empty($cek) && $stts=='super admin')
		{
			$bc['data_hal']			= $this->web_app_model->getAllData('tbl_kode_hal');
			$bc['pelaksana_uuid'] 	= $this->session->userdata('uuid');
			$bc['posisi'] 			= $this->session->userdata('posisi');
			$bc['username'] 		= $this->session->userdata('username');
			$bc['nama'] 			= $this->session->userdata('nama');
			$bc['status'] 			= $this->session->userdata('stts');
			$bc['atas'] 			= $this->load->view('panel/atas',$bc,true);
			$bc['menu'] 			= $this->load->view('panel/super_admin/menu',$bc,true);
			$bc['bio'] 				= $this->load->view('panel/bio',$bc,true);	
			$this->load->view('panel/super_admin/bg_kode_hal',$bc);
		}
		else
		{
			header('location:'.base_url().'index.php/web');
		}
	}

	public function bg_kode_bagian()
	{
		$cek  		= $this->session->userdata('logged_in');
		$stts 		= $this->session->userdata('stts');
		$username 	= $this->session->userdata('username');
		if(!empty($cek) && $stts=='super admin')
		{
			$bc['data_instansi']	= $this->web_app_model->getAllData('tbl_kode_instansi');

			$bc['pelaksana_uuid'] 	= $this->session->userdata('uuid');
			$bc['posisi'] 			= $this->session->userdata('posisi');
			$bc['username'] 		= $this->session->userdata('username');
			$bc['nama'] 			= $this->session->userdata('nama');
			$bc['status'] 			= $this->session->userdata('stts');
			$bc['atas'] 			= $this->load->view('panel/atas',$bc,true);
			$bc['menu'] 			= $this->load->view('panel/super_admin/menu',$bc,true);
			$bc['bio'] 				= $this->load->view('panel/bio',$bc,true);	
			$this->load->view('panel/super_admin/bg_kode_bagian',$bc);
		}
		else
		{
			header('location:'.base_url().'index.php/web');
		}
	}

	public function bg_ganti_password()
	{
		$cek  = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='super admin')
		{
		
			$bc['username'] 		= $this->session->userdata('username');
			$bc['posisi'] 			= $this->session->userdata('posisi');
			$bc['nama'] 			= $this->session->userdata('nama');
			$bc['status'] 			= $this->session->userdata('stts');
			$bc['atas'] 			= $this->load->view('panel/atas',$bc,true);
			$bc['menu'] 			= $this->load->view('panel/super_admin/menu',$bc,true);
			$bc['bio'] 				= $this->load->view('panel/bio',$bc,true);	
			$this->load->view('panel/super_admin/bg_ganti_password',$bc);
		}
		else
		{
			header('location:'.base_url().'index.php/web');
		}
	}

	public function hapus_kode_bagian()
	{

		$id 			= $this->uri->segment(3);
		$hapus 			= array('instansi_no'=>$id);


		$this->web_app_model->deleteData('tbl_kode_instansi',$hapus);
		//$this->web_app_model->deleteData('tbl_dosen_wali',$hapus);
		header('location:'.base_url().'index.php/super_admin/bg_kode_bagian?kd_bagian=1');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
									<button type='button' class='close' data-dismiss='alert'>
										<i class='icon-remove'></i>
									</button>

									<p>
										<strong>
											<i class='icon-ok'></i>
											Success! - 
										</strong>
										Kode Bagian berhasil dihapus...!
									</p>
								</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
										     setTimeout(function () { 
										     swal({
										                title: 'Success!!',
										                text:  'Kode Bagian berhasil dihapus!',
										                type: 'success',
										                timer: 3000,
										                showConfirmButton: true
										            });  
										     },10);  
										    </script>
										    ");
	}

	public function add_kode_bagian()
	{
		$instansi_kode				= $this->input->post('kode');
		$instansi_nama				= $this->input->post('bagian');

		$data = array(		
			'instansi_kode' 		=> $instansi_kode,
			'instansi_nama' 		=> $instansi_nama,
			);

		$this->web_app_model->insertData($data,'tbl_kode_instansi');
		header('location:'.base_url().'index.php/super_admin/bg_kode_bagian?kd_bagian=1/');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
												<button type='button' class='close' data-dismiss='alert'>
													<i class='icon-remove'></i>
												</button>
		
												<p>
													<strong>
														<i class='icon-ok'></i>
														Success! - 
													</strong>
													Kode Bagian berhasil ditambahkan!
												</p>
											</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
											     swal({
											                title: 'Success!!',
											                text:  'Kode Bagian berhasil ditambahkan!',
											                type: 'success',
											                timer: 3000,
											                showConfirmButton: true
											            });  
											     },10);  
											    </script>
											    ");
	}

	public function add_kode_hal()
	{
		$hal_kode				= $this->input->post('kode');
		$hal_nama				= $this->input->post('hal');

		$data = array(		
			'hal_kode' 			=> $hal_kode,
			'hal_nama' 			=> $hal_nama,
			);

		$this->web_app_model->insertData($data,'tbl_kode_hal');
		header('location:'.base_url().'index.php/super_admin/bg_kode_hal?kd_hal=1/');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
												<button type='button' class='close' data-dismiss='alert'>
													<i class='icon-remove'></i>
												</button>
		
												<p>
													<strong>
														<i class='icon-ok'></i>
														Success! - 
													</strong>
													Kode Hal berhasil ditambahkan!
												</p>
											</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
											     swal({
											                title: 'Success!!',
											                text:  'Kode Hal berhasil ditambahkan!',
											                type: 'success',
											                timer: 3000,
											                showConfirmButton: true
											            });  
											     },10);  
											    </script>
											    ");
	}

	public function hapus_kode_hal()
	{

		$id 			= $this->uri->segment(3);
		$hapus 			= array('hal_no'=>$id);


		$this->web_app_model->deleteData('tbl_kode_hal',$hapus);
		//$this->web_app_model->deleteData('tbl_dosen_wali',$hapus);
		header('location:'.base_url().'index.php/super_admin/bg_kode_hal?kd_hal=1');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
									<button type='button' class='close' data-dismiss='alert'>
										<i class='icon-remove'></i>
									</button>

									<p>
										<strong>
											<i class='icon-ok'></i>
											Success! - 
										</strong>
										Kode Hal berhasil dihapus...!
									</p>
								</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
										     setTimeout(function () { 
										     swal({
										                title: 'Success!!',
										                text:  'Kode Hal berhasil dihapus!',
										                type: 'success',
										                timer: 3000,
										                showConfirmButton: true
										            });  
										     },10);  
										    </script>
										    ");
	}

	public function update_password()
	{

		$username 					= $this->input->post('username');
		$password_lama 				= $this->input->post('password_lama');
		$password_baru				= $this->input->post('password_baru');
		$password_baru_2			= $this->input->post('password_baru_2');

		$akun_lama					= $this->web_app_model->getWhereOneItem($username,'username','tbl_login');

		$password_lama_encrypt 		= md5($password_lama);

		if($username == $akun_lama['username'] && $password_lama_encrypt == $akun_lama['password'] && $password_baru == $password_baru_2)
		{
			$where = array(		
			'username' 				=> $username,
			);
		
			$data = array(		
				'password' 				=> md5($password_baru_2),
				);

			$this->web_app_model->updateDataWhere($where, $data,'tbl_login');
			header('location:'.base_url().'index.php/super_admin/bg_ganti_password?settings=1/');
			$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
													<button type='button' class='close' data-dismiss='alert'>
														<i class='icon-remove'></i>
													</button>
			
													<p>
														<strong>
															<i class='icon-ok'></i>
															Success! - 
														</strong>
														Password Berhasi di Update!
													</p>
												</div>");

			$this->session->set_flashdata("info2","<script type='text/javascript'>
												     setTimeout(function () { 
												     swal({
												                title: 'Success!!',
												                text:  'Password berhasil diupdate!',
												                type: 'success',
												                timer: 3000,
												                showConfirmButton: true
												            });  
												     },10);  
												    </script>
												    ");
		}
		else
		{
			header('location:'.base_url().'index.php/super_admin/bg_ganti_password?settings=1/');
			$this->session->set_flashdata("info","<div class='alert alert-block alert-danger'>
													<button type='button' class='close' data-dismiss='alert'>
														<i class='icon-remove'></i>
													</button>
			
													<p>
														<strong>
															<i class='icon-ok'></i>
															Gagal! - 
														</strong>
														Periksa kembali!
													</p>
												</div>");

			$this->session->set_flashdata("info2","<script type='text/javascript'>
												     setTimeout(function () { 
												     swal({
												                title: 'Gagal!!',
												                text:  'Periksa Kembali!',
												                type: 'warning',
												                timer: 3000,
												                showConfirmButton: true
												            });  
												     },10);  
												    </script>
												    ");
		}
	}

	public function bg_biro()
	{
		// BIRO UMUM
		$cek  = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='super admin')
		{
			//$bc['data_biro'] 				= $this->web_app_model->getAllData('tbl_biro');
			$bc['data_biro'] 				= $this->web_app_model->getWhereAllItem('','biro_kategori','tbl_biro');

			//================
			$bc['data_admin_surat'] 		= $this->web_app_model->getJoinAllWhere('uuid_atasan','kasubbag_uuid','tbl_users','tbl_kasubbag','jabatan','Admin Surat Subbag. TU');

			$bc['data_kabag'] 				= $this->web_app_model->getJoinAllWhere('kabag_uuid_biro','biro_uuid','tbl_kabag','tbl_biro','kabag_kategori','biro');

			$bc['data_kasubbag'] 			= $this->web_app_model->getJoinAllWhere('kasubbag_uuid_kabag','kabag_uuid','tbl_kasubbag','tbl_kabag','kasubbag_kategori','biro');

			$bc['data_pelaksana'] 			= $this->web_app_model->getJoinAllWhere('staff_uuid_kasubbag','kasubbag_uuid','tbl_staff','tbl_kasubbag','staff_kategori','biro');
			//================

			$bc['posisi'] 					= $this->session->userdata('posisi');
			$bc['nama'] 					= $this->session->userdata('nama');
			$bc['status'] 					= $this->session->userdata('stts');
			$bc['modalTambahBiro'] 			= $this->load->view('panel/super_admin/modalTambahBiro',$bc,true);
			$bc['modalTambahKabag'] 		= $this->load->view('panel/super_admin/modalTambahKabag',$bc,true);
			$bc['modalTambahKasubbag'] 		= $this->load->view('panel/super_admin/modalTambahKasubbag',$bc,true);
			$bc['modalTambahPelaksanaBiro'] = $this->load->view('panel/super_admin/modalTambahPelaksanaBiro',$bc,true);

			$bc['modalEditBiro'] 			= $this->load->view('panel/super_admin/modalEditBiro',$bc,true);
			$bc['modalEditKabag'] 			= $this->load->view('panel/super_admin/modalEditKabag',$bc,true);
			$bc['modalEditKasubbag'] 		= $this->load->view('panel/super_admin/modalEditKasubbag',$bc,true);
			$bc['modalEditPelaksana'] 		= $this->load->view('panel/super_admin/modalEditPelaksana',$bc,true);

			$bc['modalTambahAdminSuratTU'] 	= $this->load->view('panel/super_admin/modalTambahAdminSuratTU',$bc,true);
			$bc['modalEditAdminSuratTU'] 	= $this->load->view('panel/super_admin/modalEditAdminSuratTU',$bc,true);

			$bc['atas'] 					= $this->load->view('panel/atas',$bc,true);
			$bc['menu'] 					= $this->load->view('panel/super_admin/menu',$bc,true);
			$bc['bio'] 						= $this->load->view('panel/bio',$bc,true);	
			$this->load->view('panel/super_admin/bg_biro',$bc);
		}
		else
		{
			header('location:'.base_url().'index.php/web');
		}
	}

	public function bg_upt_tik()
	{
		$cek  = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='super admin')
		{
			//$bc['data_kabag'] 		= $this->web_app_model->getAll2Join('kabag_uuid_biro','biro_uuid','tbl_kabag','tbl_biro');

			$bc['data_admin_surat'] 		= $this->web_app_model->getJoinAllWhere('uuid_atasan','kasubbag_uuid','tbl_users','tbl_kasubbag','jabatan','Admin Surat UPT TIK');

			$bc['data_kaUptTik'] 			= $this->web_app_model->getWhereAllItem('upt tik','kasubbag_kategori','tbl_kasubbag');

			$bc['data_kasubbag'] 			= $this->web_app_model->getAllData('tbl_kasubbag');

			$bc['data_pelaksanaUptTik'] = $this->web_app_model->getJoinAllWhere('staff_uuid_kasubbag','kasubbag_uuid','tbl_staff','tbl_kasubbag','staff_kategori','upt tik');

			//$bc['data_pelaksanaUptTik'] = $this->web_app_model->getJoinAllWhere('staff_uuid_kasubbag','kasubbag_uuid','tbl_staff','tbl_kasubbag','staff_kategori','upt tik');

			$bc['posisi'] 			= $this->session->userdata('posisi');
			$bc['nama'] 			= $this->session->userdata('nama');
			$bc['status'] 			= $this->session->userdata('stts');
			
			$bc['modalTambahAdminSuratUptTik'] = $this->load->view('panel/super_admin/modalTambahAdminSuratUptTik',$bc,true);

			$bc['modalTambahPelaksanaUptTik'] = $this->load->view('panel/super_admin/modalTambahPelaksanaUptTik',$bc,true);
			$bc['modalTambahKaUptTik'] = $this->load->view('panel/super_admin/modalTambahKaUptTik',$bc,true);

			$bc['modalEditAdminSuratUptTik'] = $this->load->view('panel/super_admin/modalEditAdminSuratUptTik',$bc,true);
			$bc['modalEditPelaksanaUptTik'] = $this->load->view('panel/super_admin/modalEditPelaksanaUptTik',$bc,true);
			$bc['modalEditKaUptTik'] = $this->load->view('panel/super_admin/modalEditKaUptTik',$bc,true);

			$bc['atas'] 			= $this->load->view('panel/atas',$bc,true);
			$bc['menu'] 			= $this->load->view('panel/super_admin/menu',$bc,true);
			$bc['bio'] 				= $this->load->view('panel/bio',$bc,true);	
			$this->load->view('panel/super_admin/bg_upt_tik',$bc);
		}
		else
		{
			header('location:'.base_url().'index.php/web');
		}
	}

	public function bg_upt_labdas()
	{
		$cek  = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='super admin')
		{

			$bc['data_kaLabdas'] 				= $this->web_app_model->getWhereAllItem('upt lab dasar','kasubbag_kategori','tbl_kasubbag');
			$bc['data_kasubbag']				= $this->web_app_model->getAllData('tbl_kasubbag');

			//$bc['data_pelaksanaUptTik'] = $this->web_app_model->getJoinAllWhere('staff_uuid_kasubbag','kasubbag_uuid','tbl_staff','tbl_kasubbag','staff_kategori','upt tik');

			$bc['data_admin_surat'] 		= $this->web_app_model->getJoinAllWhere('uuid_atasan','kasubbag_uuid','tbl_users','tbl_kasubbag','jabatan','Admin Surat Lab. Dasar');

			$bc['data_pelaksanaLabdas'] 		= $this->web_app_model->getJoinAllWhere('staff_uuid_kasubbag','kasubbag_uuid','tbl_staff','tbl_kasubbag','staff_kategori','upt lab dasar');

			$bc['posisi'] 						= $this->session->userdata('posisi');
			$bc['nama'] 						= $this->session->userdata('nama');
			$bc['status'] 						= $this->session->userdata('stts');
			$bc['modalTambahPelaksanaLabdas'] 	= $this->load->view('panel/super_admin/modalTambahPelaksanaLabdas',$bc,true);
			$bc['modalEditAdminSuratLabdas'] 	= $this->load->view('panel/super_admin/modalEditAdminSuratLabdas',$bc,true);
			$bc['modalTambahKaLabdas'] 			= $this->load->view('panel/super_admin/modalTambahKaLabdas',$bc,true);
			$bc['modalTambahAdminSuratLabdas'] 	= $this->load->view('panel/super_admin/modalTambahAdminSuratLabdas',$bc,true);

			$bc['modalEditPelaksanaLabdas'] 	= $this->load->view('panel/super_admin/modalEditPelaksanaLabdas',$bc,true);
			$bc['modalEditKaLabdas'] 			= $this->load->view('panel/super_admin/modalEditKaLabdas',$bc,true);

			$bc['atas'] 						= $this->load->view('panel/atas',$bc,true);
			$bc['menu'] 						= $this->load->view('panel/super_admin/menu',$bc,true);
			$bc['bio'] 							= $this->load->view('panel/bio',$bc,true);	
			$this->load->view('panel/super_admin/bg_upt_labdas',$bc);
		}
		else
		{
			header('location:'.base_url().'index.php/web');
		}
	}

	public function bg_upt_lab_bhs()
	{
		$cek  = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='super admin')
		{

			$bc['data_kaLabBhs'] 			= $this->web_app_model->getWhereAllItem('upt lab bahasa','kasubbag_kategori','tbl_kasubbag');

			$bc['data_admin_surat'] 		= $this->web_app_model->getJoinAllWhere('uuid_atasan','kasubbag_uuid','tbl_users','tbl_kasubbag','jabatan','Admin Surat Lab. Bahasa');

			$bc['data_kasubbag']			= $this->web_app_model->getAllData('tbl_kasubbag');

			//$bc['data_pelaksanaUptTik'] = $this->web_app_model->getJoinAllWhere('staff_uuid_kasubbag','kasubbag_uuid','tbl_staff','tbl_kasubbag','staff_kategori','upt tik');

			$bc['data_pelaksanaLabBhs'] = $this->web_app_model->getJoinAllWhere('staff_uuid_kasubbag','kasubbag_uuid','tbl_staff','tbl_kasubbag','staff_kategori','upt lab bahasa');

			$bc['posisi'] 			= $this->session->userdata('posisi');
			$bc['nama'] 			= $this->session->userdata('nama');
			$bc['status'] 			= $this->session->userdata('stts');
			$bc['modalTambahPelaksanaLabBhs'] = $this->load->view('panel/super_admin/modalTambahPelaksanaLabBhs',$bc,true);
			$bc['modalTambahKaLabBhs'] = $this->load->view('panel/super_admin/modalTambahKaLabBhs',$bc,true);
			
			$bc['modalTambahAdminSuratLabBhs'] = $this->load->view('panel/super_admin/modalTambahAdminSuratLabBhs',$bc,true);

			$bc['modalEditAdminSuratLabBhs'] = $this->load->view('panel/super_admin/modalEditAdminSuratLabBhs',$bc,true);
			$bc['modalEditPelaksanaLabBhs'] = $this->load->view('panel/super_admin/modalEditPelaksanaLabBhs',$bc,true);
			$bc['modalEditKaLabBhs'] = $this->load->view('panel/super_admin/modalEditKaLabBhs',$bc,true);

			$bc['atas'] 			= $this->load->view('panel/atas',$bc,true);
			$bc['menu'] 			= $this->load->view('panel/super_admin/menu',$bc,true);
			$bc['bio'] 				= $this->load->view('panel/bio',$bc,true);	
			$this->load->view('panel/super_admin/bg_upt_lab_bhs',$bc);
		}
		else
		{
			header('location:'.base_url().'index.php/web');
		}
	}

	public function bg_upt_perpus()
	{
		$cek  = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='super admin')
		{

			$bc['data_kaPerpus'] 				= $this->web_app_model->getWhereAllItem('upt perpustakaan','kasubbag_kategori','tbl_kasubbag');
			$bc['data_admin_surat'] 			= $this->web_app_model->getJoinAllWhere('uuid_atasan','kasubbag_uuid','tbl_users','tbl_kasubbag','jabatan','Admin Surat Perpustakaan');
			$bc['data_kasubbag']				= $this->web_app_model->getAllData('tbl_kasubbag');
			$bc['data_pelaksanaPerpus'] 		= $this->web_app_model->getJoinAllWhere('staff_uuid_kasubbag','kasubbag_uuid','tbl_staff','tbl_kasubbag','staff_kategori','upt perpustakaan');

			$bc['posisi'] 						= $this->session->userdata('posisi');
			$bc['nama'] 						= $this->session->userdata('nama');
			$bc['status'] 						= $this->session->userdata('stts');

			$bc['modalTambahPelaksanaPerpus'] 	= $this->load->view('panel/super_admin/modalTambahPelaksanaPerpus',$bc,true);
			$bc['modalTambahKaPerpus'] 			= $this->load->view('panel/super_admin/modalTambahKaPerpus',$bc,true);
			$bc['modalTambahAdminSuratPerpus'] 	= $this->load->view('panel/super_admin/modalTambahAdminSuratPerpus',$bc,true);

			$bc['modalEditAdminSuratPerpus'] 	= $this->load->view('panel/super_admin/modalEditAdminSuratPerpus',$bc,true);
			$bc['modalEditPelaksanaPerpus'] 	= $this->load->view('panel/super_admin/modalEditPelaksanaPerpus',$bc,true);
			$bc['modalEditKaPerpus'] 			= $this->load->view('panel/super_admin/modalEditKaPerpus',$bc,true);

			$bc['atas'] 						= $this->load->view('panel/atas',$bc,true);
			$bc['menu'] 						= $this->load->view('panel/super_admin/menu',$bc,true);
			$bc['bio'] 							= $this->load->view('panel/bio',$bc,true);	
			$this->load->view('panel/super_admin/bg_upt_perpus',$bc);
		}
		else
		{
			header('location:'.base_url().'index.php/web');
		}
	}

	public function bg_kabag()
	{
		$cek  = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='super admin')
		{
			$bc['data_kabag'] 		= $this->web_app_model->getAll2Join('kabag_uuid_biro','biro_uuid','tbl_kabag','tbl_biro');

			$bc['data_spi'] 		= $this->web_app_model->getWhereAllItem('Ka SP','kabag_posisi','tbl_kabag');

			$bc['data_biro']		= $this->web_app_model->getAllData('tbl_biro');

			$bc['posisi'] 			= $this->session->userdata('posisi');
			$bc['nama'] 			= $this->session->userdata('nama');
			$bc['status'] 			= $this->session->userdata('stts');
			//$bc['modalTambahKabag'] = $this->load->view('panel/super_admin/modalTambahKabag',$bc,true);
			$bc['modalTambahKaSP'] = $this->load->view('panel/super_admin/modalTambahKaSP',$bc,true);
			$bc['atas'] 			= $this->load->view('panel/atas',$bc,true);
			$bc['menu'] 			= $this->load->view('panel/super_admin/menu',$bc,true);
			$bc['bio'] 				= $this->load->view('panel/bio',$bc,true);	
			$this->load->view('panel/super_admin/bg_kabag',$bc);
		}
		else
		{
			header('location:'.base_url().'index.php/web');
		}
	}

	public function bg_sp()
	{
		$cek  = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='super admin')
		{
			//=========
			$bc['data_admin_surat'] = $this->web_app_model->getJoinAllWhere('uuid_atasan','kasubbag_uuid','tbl_users','tbl_kasubbag','jabatan','Admin Surat SP');

			$bc['data_kasp'] 		= $this->web_app_model->getWhereAllItem('spi','kabag_kategori','tbl_kabag');

			$bc['data_seksp'] 		= $this->web_app_model->getJoinAllWhere('kasubbag_uuid_kabag','kabag_uuid','tbl_kasubbag','tbl_kabag','kasubbag_kategori','spi');

			$bc['data_spi'] 		= $this->web_app_model->getJoinAllWhere('staff_uuid_kasubbag','kasubbag_uuid','tbl_staff','tbl_kasubbag','staff_kategori','spi');

			$bc['data_kasubbag'] 	= $this->web_app_model->getJoinAllWhere('kasubbag_uuid_kabag','kabag_uuid','tbl_kasubbag','tbl_kabag','kasubbag_kategori','spi');
			//===========

			$bc['posisi'] 			= $this->session->userdata('posisi');
			$bc['nama'] 			= $this->session->userdata('nama');
			$bc['status'] 			= $this->session->userdata('stts');
			
			$bc['modalTambahAdminSuratSP'] 	= $this->load->view('panel/super_admin/modalTambahAdminSuratSP',$bc,true);
			$bc['modalEditAdminSuratSP'] 	= $this->load->view('panel/super_admin/modalEditAdminSuratSP',$bc,true);
			$bc['modalTambahKaSP'] 	= $this->load->view('panel/super_admin/modalTambahKaSP',$bc,true);
			$bc['modalTambahAnggotaSP'] = $this->load->view('panel/super_admin/modalTambahAnggotaSP',$bc,true);
			$bc['modalTambahSekSP'] 	= $this->load->view('panel/super_admin/modalTambahSekSP',$bc,true);

			$bc['modalEditKaSP'] 	= $this->load->view('panel/super_admin/modalEditKaSP',$bc,true);
			$bc['modalEditSekSP'] 	= $this->load->view('panel/super_admin/modalEditSekSP',$bc,true);
			$bc['modalEditAnggotaSP'] 	= $this->load->view('panel/super_admin/modalEditAnggotaSP',$bc,true);
			
			$bc['atas'] 			= $this->load->view('panel/atas',$bc,true);
			$bc['menu'] 			= $this->load->view('panel/super_admin/menu',$bc,true);
			$bc['bio'] 				= $this->load->view('panel/bio',$bc,true);	
			$this->load->view('panel/super_admin/bg_sp',$bc);
		}
		else
		{
			header('location:'.base_url().'index.php/web');
		}
	}

	public function bg_kasubbag()
	{
		$cek  = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='super admin')
		{
			$bc['data_kasubbag'] 	= $this->web_app_model->getAll2Join('kasubbag_uuid_kabag','kabag_uuid','tbl_kasubbag','tbl_kabag');

			$bc['posisi'] 			= $this->session->userdata('posisi');
			$bc['nama'] 			= $this->session->userdata('nama');
			$bc['status'] 			= $this->session->userdata('stts');
			//$bc['modalTambahKasubbag'] = $this->load->view('panel/super_admin/modalTambahKasubbag',$bc,true);
			$bc['modalTambahUPT'] = $this->load->view('panel/super_admin/modalTambahUPT',$bc,true);
			$bc['atas'] 			= $this->load->view('panel/atas',$bc,true);
			$bc['menu'] 			= $this->load->view('panel/super_admin/menu',$bc,true);
			$bc['bio'] 				= $this->load->view('panel/bio',$bc,true);	
			$this->load->view('panel/super_admin/bg_kasubbag',$bc);
		}
		else
		{
			header('location:'.base_url().'index.php/web');
		}
	}

	public function bg_pelaksana()
	{
		$cek  = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='super admin')
		{
			$bc['data_pelaksana'] 	= $this->web_app_model->getAll2Join('staff_uuid_kasubbag','kasubbag_uuid','tbl_staff','tbl_kasubbag');

			$bc['posisi'] 			= $this->session->userdata('posisi');
			$bc['nama'] 			= $this->session->userdata('nama');
			$bc['status'] 			= $this->session->userdata('stts');
			$bc['atas'] 			= $this->load->view('panel/atas',$bc,true);
			$bc['menu'] 			= $this->load->view('panel/super_admin/menu',$bc,true);
			$bc['bio'] 				= $this->load->view('panel/bio',$bc,true);	
			$this->load->view('panel/super_admin/bg_pelaksana',$bc);
		}
		else
		{
			header('location:'.base_url().'index.php/web');
		}
	}




// TAMBAH DATA ------------------------------------------------------------

	public function tambahRektor()
	{
		date_default_timezone_set('Asia/Jakarta');

		$rektor_username 			= $this->input->post('username');
		$rektor_nama				= $this->input->post('nama_lengkap');
		$rektor_posisi				= $this->input->post('posisi');
		$rektor_uuid				= $this->get_id();

		$cek_posisi 				= $this->web_app_model->getWhereAllItem($rektor_posisi,'rektor_posisi','tbl_rektor');

		$data = array(		
			'rektor_uuid' 		=> $rektor_uuid,
			'rektor_username' 	=> $rektor_username,
			'rektor_nama' 		=> $rektor_nama,
			'rektor_posisi' 	=> $rektor_posisi,
			);

		$login = array(		
			'username' 		=> $rektor_username,
			'stts' 			=> 'rektor',
			'password' 		=> md5($rektor_username),
			);

		if($cek_posisi->num_rows() > 0)
		{
			header('location:'.base_url().'index.php/super_admin/bg_rektor?data_users=1/');
			$this->session->set_flashdata("info","<div class='alert alert-block alert-warning'>
													<button type='button' class='close' data-dismiss='alert'>
														<i class='icon-remove'></i>
													</button>
			
													<p>
														<strong>
															<i class='icon-ok'></i>
															Gagal! - 
														</strong>
														Data dengan posisi tersebut sudah digunakan!
													</p>
												</div>");

			$this->session->set_flashdata("info2","<script type='text/javascript'>
												     setTimeout(function () { 
														     swal({
														                title: 'Gagal!!',
														                text:  'Data dengan posisi tersebut sudah digunakan!',
														                type: 'warning',
														                timer: 3000,
														                showConfirmButton: true
														            });  
														     },10);  
														    </script>
														    ");	
		}
		else
		{
			$this->web_app_model->insertData($data,'tbl_rektor');
			$this->web_app_model->insertData($login,'tbl_login');
			header('location:'.base_url().'index.php/super_admin/bg_rektor?data_users=1/');
			$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
													<button type='button' class='close' data-dismiss='alert'>
														<i class='icon-remove'></i>
													</button>
			
													<p>
														<strong>
															<i class='icon-ok'></i>
															Success! - 
														</strong>
														Data Rektor berhasil ditambahkan!
													</p>
												</div>");

			$this->session->set_flashdata("info2","<script type='text/javascript'>
												     setTimeout(function () { 
														     swal({
														                title: 'Success!!',
														                text:  'Data Rektor / Warek berhasil ditambahkan!',
														                type: 'success',
														                timer: 3000,
														                showConfirmButton: true
														            });  
														     },10);  
														    </script>
														    ");	
		}

		

				
	}

	public function tambahBiro()
	{
		date_default_timezone_set('Asia/Jakarta');

		$biro_username 			= $this->input->post('username');
		$biro_nama				= $this->input->post('nama_lengkap');
		$biro_posisi			= $this->input->post('posisi');
		$biro_uuid				= $this->get_id();

		$cek_posisi 			= $this->web_app_model->getWhereAllItem($biro_posisi,'biro_posisi','tbl_biro');

		$data = array(		
			'biro_uuid' 		=> $biro_uuid,
			'biro_username' 	=> $biro_username,
			'biro_nama' 		=> $biro_nama,
			'biro_posisi' 		=> $biro_posisi,
			);

		$login = array(		
			'username' 		=> $biro_username,
			'stts' 			=> 'biro',
			'password' 		=> md5($biro_username),
			);

		if($cek_posisi->num_rows() > 0)
		{
			header('location:'.base_url().'index.php/super_admin/bg_biro?data_users=1/');
			$this->session->set_flashdata("info","<div class='alert alert-block alert-warning'>
													<button type='button' class='close' data-dismiss='alert'>
														<i class='icon-remove'></i>
													</button>
			
													<p>
														<strong>
															<i class='icon-ok'></i>
															Gagal! - 
														</strong>
														Data dengan posisi tersebut sudah digunakan!
													</p>
												</div>");

			$this->session->set_flashdata("info2","<script type='text/javascript'>
												     setTimeout(function () { 
														     swal({
														                title: 'Gagal!!',
														                text:  'Data dengan posisi tersebut sudah digunakan!',
														                type: 'warning',
														                timer: 3000,
														                showConfirmButton: true
														            });  
														     },10);  
														    </script>
														    ");
		}
		else
		{
			$this->web_app_model->insertData($data,'tbl_biro');
			$this->web_app_model->insertData($login,'tbl_login');
			header('location:'.base_url().'index.php/super_admin/bg_biro?data_users=1/');
			$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
													<button type='button' class='close' data-dismiss='alert'>
														<i class='icon-remove'></i>
													</button>
			
													<p>
														<strong>
															<i class='icon-ok'></i>
															Success! - 
														</strong>
														Data Biro berhasil ditambahkan!
													</p>
												</div>");

			$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
														     swal({
														                title: 'Success!!',
														                text:  'Data Biro berhasil ditambahkan!',
														                type: 'success',
														                timer: 3000,
														                showConfirmButton: true
														            });  
														     },10);  
														    </script>
														    ");
		}
	}

	public function tambahDekan()
	{
		$biro_username 			= $this->input->post('biro_username');
		$biro_nama				= $this->input->post('biro_nama');
		$biro_posisi			= $this->input->post('biro_posisi');
		$biro_email				= $this->input->post('biro_email');
		$biro_uuid				= $this->get_id();

		$cek_posisi 			= $this->web_app_model->getWhereAllItem($biro_posisi,'biro_posisi','tbl_biro');
		$cek_username 			= $this->web_app_model->getWhereAllItem($biro_username,'username','tbl_login');

		$data = array(		
			'biro_uuid' 		=> $biro_uuid,
			'biro_username' 	=> $biro_username,
			'biro_nama' 		=> $biro_nama,
			'biro_posisi' 		=> $biro_posisi,
			'biro_email' 		=> $biro_email,
			'biro_kategori' 	=> 'dekan fakultas',
			);

		$login = array(		
			'username' 		=> $biro_username,
			'stts' 			=> 'biro',
			'password' 		=> md5($biro_username),
			);

		if($cek_posisi->num_rows() > 0 || $cek_username->num_rows() > 0)
		{
			header('location:'.base_url().'index.php/super_admin/bg_dekan?data_users=1/');
			$this->session->set_flashdata("info","<div class='alert alert-block alert-warning'>
													<button type='button' class='close' data-dismiss='alert'>
														<i class='icon-remove'></i>
													</button>
			
													<p>
														<strong>
															<i class='icon-ok'></i>
															Gagal! - 
														</strong>
														Data dengan posisi / username tersebut sudah digunakan!
													</p>
												</div>");

			$this->session->set_flashdata("info2","<script type='text/javascript'>
												     setTimeout(function () { 
														     swal({
														                title: 'Gagal!!',
														                text:  'Data dengan posisi / username tersebut sudah digunakan!',
														                type: 'warning',
														                timer: 3000,
														                showConfirmButton: true
														            });  
														     },10);  
														    </script>
														    ");
		}
		else
		{
			$this->web_app_model->insertData($login,'tbl_login');
			$this->web_app_model->insertData($data,'tbl_biro');
			header('location:'.base_url().'index.php/super_admin/bg_dekan?data_users=1/');
			$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
													<button type='button' class='close' data-dismiss='alert'>
														<i class='icon-remove'></i>
													</button>
			
													<p>
														<strong>
															<i class='icon-ok'></i>
															Success! - 
														</strong>
														Data Dekan berhasil ditambahkan!
													</p>
												</div>");

			$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
														     swal({
														                title: 'Success!!',
														                text:  'Data Dekan berhasil ditambahkan!',
														                type: 'success',
														                timer: 3000,
														                showConfirmButton: true
														            });  
														     },10);  
														    </script>
														    ");
		}			
	}

	public function tambahKabagProdi()
	{
		$kabag_username 		= $this->input->post('kabag_username');
		$kabag_nama				= $this->input->post('kabag_nama');
		$kabag_posisi			= $this->input->post('kabag_posisi');
		//$kabag_uuid_biro		= $this->input->post('kabag_uuid_biro');
		$kabag_email			= $this->input->post('kabag_email');
		$kabag_uuid				= $this->get_id();

		$cek_posisi 			= $this->web_app_model->getWhereAllItem($kabag_posisi,'kabag_posisi','tbl_kabag');
		$cek_username 			= $this->web_app_model->getWhereAllItem($kabag_username,'username','tbl_login');

		$data = array(		
			'kabag_uuid' 		=> $kabag_uuid,
			'kabag_username' 	=> $kabag_username,
			'kabag_nama' 		=> $kabag_nama,
			'kabag_posisi' 		=> $kabag_posisi,
			'kabag_email' 		=> $kabag_email,
			'kabag_kategori' 	=> 'kabag fakultas',
			);

		$login = array(		
			'username' 		=> $kabag_username,
			'stts' 			=> 'kabag',
			'password' 		=> md5($kabag_username),
			);

		if($cek_posisi->num_rows() > 0 || $cek_username->num_rows() > 0)
		{
			header('location:'.base_url().'index.php/super_admin/bg_dekan?data_users=1/');
			$this->session->set_flashdata("info","<div class='alert alert-block alert-warning'>
													<button type='button' class='close' data-dismiss='alert'>
														<i class='icon-remove'></i>
													</button>
			
													<p>
														<strong>
															<i class='icon-ok'></i>
															Gagal! - 
														</strong>
														Data dengan posisi / username tersebut sudah digunakan!
													</p>
												</div>");

			$this->session->set_flashdata("info2","<script type='text/javascript'>
												     setTimeout(function () { 
														     swal({
														                title: 'Gagal!!',
														                text:  'Data dengan posisi / username tersebut sudah digunakan!',
														                type: 'warning',
														                timer: 3000,
														                showConfirmButton: true
														            });  
														     },10);  
														    </script>
														    ");
		}
		else
		{
			$this->web_app_model->insertData($login,'tbl_login');
			$this->web_app_model->insertData($data,'tbl_kabag');
			header('location:'.base_url().'index.php/super_admin/bg_dekan?data_users=1/');
			$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
													<button type='button' class='close' data-dismiss='alert'>
														<i class='icon-remove'></i>
													</button>
			
													<p>
														<strong>
															<i class='icon-ok'></i>
															Success! - 
														</strong>
														Data Dekan berhasil ditambahkan!
													</p>
												</div>");

			$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
														     swal({
														                title: 'Success!!',
														                text:  'Data Dekan berhasil ditambahkan!',
														                type: 'success',
														                timer: 3000,
														                showConfirmButton: true
														            });  
														     },10);  
														    </script>
														    ");
		}			
	}

	public function tambahKasubbagFak()
	{
		$kasubbag_username 			= $this->input->post('kasubbag_username');
		$kasubbag_nama				= $this->input->post('kasubbag_nama');
		$kasubbag_posisi			= $this->input->post('kasubbag_posisi');
		$kasubbag_uuid_kabag		= $this->input->post('kasubbag_uuid_kabag');
		$kasubbag_email				= $this->input->post('kasubbag_email');
		$kasubbag_uuid				= $this->get_id();

		$cek_posisi 				= $this->web_app_model->getWhereAllItem($kasubbag_posisi,'kasubbag_posisi','tbl_kasubbag');
		$cek_username 				= $this->web_app_model->getWhereAllItem($kasubbag_username,'username','tbl_login');

		$data = array(		
			'kasubbag_uuid' 		=> $kasubbag_uuid,
			'kasubbag_username' 	=> $kasubbag_username,
			'kasubbag_nama' 		=> $kasubbag_nama,
			'kasubbag_posisi' 		=> $kasubbag_posisi,
			'kasubbag_uuid_kabag' 	=> $kasubbag_uuid_kabag,
			'kasubbag_email' 		=> $kasubbag_email,
			'kasubbag_kategori' 	=> 'kasubbag fakultas',
			);

		$login = array(		
			'username' 		=> $kasubbag_username,
			'stts' 			=> 'kasubbag',
			'password' 		=> md5($kasubbag_username),
			);

		if($cek_posisi->num_rows() > 0 || $cek_username->num_rows() > 0)
		{
			header('location:'.base_url().'index.php/super_admin/bg_dekan?data_users=1/');
			$this->session->set_flashdata("info","<div class='alert alert-block alert-warning'>
													<button type='button' class='close' data-dismiss='alert'>
														<i class='icon-remove'></i>
													</button>
			
													<p>
														<strong>
															<i class='icon-ok'></i>
															Gagal! - 
														</strong>
														Data dengan posisi / username tersebut sudah digunakan!
													</p>
												</div>");

			$this->session->set_flashdata("info2","<script type='text/javascript'>
												     setTimeout(function () { 
														     swal({
														                title: 'Gagal!!',
														                text:  'Data dengan posisi / username tersebut sudah digunakan!',
														                type: 'warning',
														                timer: 3000,
														                showConfirmButton: true
														            });  
														     },10);  
														    </script>
														    ");
		}
		else
		{
			$this->web_app_model->insertData($login,'tbl_login');
			$this->web_app_model->insertData($data,'tbl_kasubbag');
			header('location:'.base_url().'index.php/super_admin/bg_dekan?data_users=1/');
			$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
													<button type='button' class='close' data-dismiss='alert'>
														<i class='icon-remove'></i>
													</button>
			
													<p>
														<strong>
															<i class='icon-ok'></i>
															Success! - 
														</strong>
														Data Kasubbag berhasil ditambahkan!
													</p>
												</div>");

			$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
														     swal({
														                title: 'Success!!',
														                text:  'Data Kasubbag berhasil ditambahkan!',
														                type: 'success',
														                timer: 3000,
														                showConfirmButton: true
														            });  
														     },10);  
														    </script>
														    ");
		}			
	}

	public function tambahPelaksanaFak()
	{
		$staff_username 		= $this->input->post('staff_username');
		$staff_nama				= $this->input->post('staff_nama');
		$staff_posisi			= $this->input->post('staff_posisi');
		$staff_uuid_kasubbag	= $this->input->post('staff_uuid_kasubbag');
		$staff_email			= $this->input->post('staff_email');
		$staff_uuid				= $this->get_id();

		$cek_posisi 				= $this->web_app_model->getWhereAllItem($staff_posisi,'staff_posisi','tbl_staff');
		$cek_username 				= $this->web_app_model->getWhereAllItem($staff_username,'username','tbl_login');

		$data = array(		
			'staff_uuid' 		=> $staff_uuid,
			'staff_username' 	=> $staff_username,
			'staff_nama' 		=> $staff_nama,
			'staff_posisi' 		=> $staff_posisi,
			'staff_uuid_kasubbag' 	=> $staff_uuid_kasubbag,
			'staff_email' 		=> $staff_email,
			'staff_kategori' 	=> 'pelaksana fakultas',
			);

		$login = array(		
			'username' 		=> $staff_username,
			'stts' 			=> 'pelaksana',
			'password' 		=> md5($staff_username),
			);

			$this->web_app_model->insertData($login,'tbl_login');
			$this->web_app_model->insertData($data,'tbl_staff');
			header('location:'.base_url().'index.php/super_admin/bg_dekan?data_users=1/');
			$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
													<button type='button' class='close' data-dismiss='alert'>
														<i class='icon-remove'></i>
													</button>
			
													<p>
														<strong>
															<i class='icon-ok'></i>
															Success! - 
														</strong>
														Data Staff / Pelaksana berhasil ditambahkan!
													</p>
												</div>");

			$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
														     swal({
														                title: 'Success!!',
														                text:  'Data Staff / Pelaksana berhasil ditambahkan!',
														                type: 'success',
														                timer: 3000,
														                showConfirmButton: true
														            });  
														     },10);  
														    </script>
														    ");
	}

	public function tambahFakultas()
	{
		date_default_timezone_set('Asia/Jakarta');

		$fak_fakultas_nama 		= $this->input->post('fak_fakultas_nama');

		$data = array(		
			'fak_fakultas_nama'	=> $fak_fakultas_nama,
			);

		$this->web_app_model->insertData($data,'fak_fakultas');
		header('location:'.base_url().'index.php/super_admin/bg_fakultas?kd_fakultas=1/');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
												<button type='button' class='close' data-dismiss='alert'>
													<i class='icon-remove'></i>
												</button>
		
												<p>
													<strong>
														<i class='icon-ok'></i>
														Success! - 
													</strong>
													Data Fakultas berhasil ditambahkan!
												</p>
											</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
														     swal({
														                title: 'Success!!',
														                text:  'Data Fakultas berhasil ditambahkan!',
														                type: 'success',
														                timer: 3000,
														                showConfirmButton: true
														            });  
														     },10);  
														    </script>
														    ");			
	}

	public function tambahAdminSurat()
	{
		date_default_timezone_set('Asia/Jakarta');

		$username 				= $this->input->post('username');
		$nama_lengkap			= $this->input->post('nama_lengkap');
		$jabatan				= $this->input->post('posisi');
		$status					= "admin surat";
		$uuid_atasan			= $this->input->post('atasan');

		$data = array(		
			'username' 			=> $username,
			'nama_lengkap' 		=> $nama_lengkap,
			'jabatan' 			=> $jabatan,
			'status' 			=> $status,
			'uuid_atasan' 		=> $uuid_atasan,
			);

		$login = array(		
			'username' 		=> $username,
			'stts' 			=> 'admin surat',
			'password' 		=> md5($username),
			);

		$this->web_app_model->insertData($data,'tbl_users');
		$this->web_app_model->insertData($login,'tbl_login');
		header('location:'.base_url().'index.php/super_admin/bg_biro?data_users=1/');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
												<button type='button' class='close' data-dismiss='alert'>
													<i class='icon-remove'></i>
												</button>
		
												<p>
													<strong>
														<i class='icon-ok'></i>
														Success! - 
													</strong>
													Data Admin Persuratan Biro berhasil ditambahkan!
												</p>
											</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
														     swal({
														                title: 'Success!!',
														                text:  'Data Admin Persuratan Biro berhasil ditambahkan!',
														                type: 'success',
														                timer: 3000,
														                showConfirmButton: true
														            });  
														     },10);  
														    </script>
														    ");			
	}

	public function tambahAdminSuratLabBhs()
	{
		date_default_timezone_set('Asia/Jakarta');

		$username 				= $this->input->post('username');
		$nama_lengkap			= $this->input->post('nama_lengkap');
		$jabatan				= $this->input->post('posisi');
		$status					= "admin surat";
		$uuid_atasan			= $this->input->post('atasan');

		$data = array(		
			'username' 			=> $username,
			'nama_lengkap' 		=> $nama_lengkap,
			'jabatan' 			=> $jabatan,
			'status' 			=> $status,
			'uuid_atasan' 		=> $uuid_atasan,
			);

		$login = array(		
			'username' 		=> $username,
			'stts' 			=> 'admin surat',
			'password' 		=> md5($username),
			);

		$this->web_app_model->insertData($data,'tbl_users');
		$this->web_app_model->insertData($login,'tbl_login');
		header('location:'.base_url().'index.php/super_admin/bg_upt_lab_bhs?data_users=1/');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
												<button type='button' class='close' data-dismiss='alert'>
													<i class='icon-remove'></i>
												</button>
		
												<p>
													<strong>
														<i class='icon-ok'></i>
														Success! - 
													</strong>
													Data Admin Persuratan Lab Bahasa berhasil ditambahkan!
												</p>
											</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
														     swal({
														                title: 'Success!!',
														                text:  'Data Admin Persuratan Lab Bahsa berhasil ditambahkan!',
														                type: 'success',
														                timer: 3000,
														                showConfirmButton: true
														            });  
														     },10);  
														    </script>
														    ");			
	}

	public function tambahAdminSuratFak()
	{
		date_default_timezone_set('Asia/Jakarta');

		$username 				= $this->input->post('username');
		$nama_lengkap			= $this->input->post('nama_lengkap');
		$jabatan				= $this->input->post('posisi');
		$status					= "admin surat";
		$uuid_atasan			= $this->input->post('atasan');

		$data = array(		
			'username' 			=> $username,
			'nama_lengkap' 		=> $nama_lengkap,
			'jabatan' 			=> $jabatan,
			'status' 			=> $status,
			'uuid_atasan' 		=> $uuid_atasan,
			);

		$login = array(		
			'username' 		=> $username,
			'stts' 			=> 'admin surat',
			'password' 		=> md5($username),
			);

		$this->web_app_model->insertData($data,'tbl_users');
		$this->web_app_model->insertData($login,'tbl_login');
		header('location:'.base_url().'index.php/super_admin/bg_dekan?data_users=1/');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
												<button type='button' class='close' data-dismiss='alert'>
													<i class='icon-remove'></i>
												</button>
		
												<p>
													<strong>
														<i class='icon-ok'></i>
														Success! - 
													</strong>
													Data Admin Persuratan Fakultas berhasil ditambahkan!
												</p>
											</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
														     swal({
														                title: 'Success!!',
														                text:  'Data Admin Persuratan Fakultas berhasil ditambahkan!',
														                type: 'success',
														                timer: 3000,
														                showConfirmButton: true
														            });  
														     },10);  
														    </script>
														    ");			
	}

	public function tambahAdminSuratPerpus()
	{
		date_default_timezone_set('Asia/Jakarta');

		$username 				= $this->input->post('username');
		$nama_lengkap			= $this->input->post('nama_lengkap');
		$jabatan				= $this->input->post('posisi');
		$status					= "admin surat";
		$uuid_atasan			= $this->input->post('atasan');

		$data = array(		
			'username' 			=> $username,
			'nama_lengkap' 		=> $nama_lengkap,
			'jabatan' 			=> $jabatan,
			'status' 			=> $status,
			'uuid_atasan' 		=> $uuid_atasan,
			);

		$login = array(		
			'username' 		=> $username,
			'stts' 			=> 'admin surat',
			'password' 		=> md5($username),
			);

		$this->web_app_model->insertData($data,'tbl_users');
		$this->web_app_model->insertData($login,'tbl_login');
		header('location:'.base_url().'index.php/super_admin/bg_upt_perpus?data_users=1/');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
												<button type='button' class='close' data-dismiss='alert'>
													<i class='icon-remove'></i>
												</button>
		
												<p>
													<strong>
														<i class='icon-ok'></i>
														Success! - 
													</strong>
													Data Admin Persuratan UPT Perpustakaan berhasil ditambahkan!
												</p>
											</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
														     swal({
														                title: 'Success!!',
														                text:  'Data Admin Persuratan UPT Perpustakaan berhasil ditambahkan!',
														                type: 'success',
														                timer: 3000,
														                showConfirmButton: true
														            });  
														     },10);  
														    </script>
														    ");			
	}

	public function tambahAdminSuratLabDas()
	{
		date_default_timezone_set('Asia/Jakarta');

		$username 				= $this->input->post('username');
		$nama_lengkap			= $this->input->post('nama_lengkap');
		$jabatan				= $this->input->post('posisi');
		$status					= "admin surat";
		$uuid_atasan			= $this->input->post('atasan');

		$data = array(		
			'username' 			=> $username,
			'nama_lengkap' 		=> $nama_lengkap,
			'jabatan' 			=> $jabatan,
			'status' 			=> $status,
			'uuid_atasan' 		=> $uuid_atasan,
			);

		$login = array(		
			'username' 		=> $username,
			'stts' 			=> 'admin surat',
			'password' 		=> md5($username),
			);

		$this->web_app_model->insertData($data,'tbl_users');
		$this->web_app_model->insertData($login,'tbl_login');
		header('location:'.base_url().'index.php/super_admin/bg_upt_labdas?data_users=1/');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
												<button type='button' class='close' data-dismiss='alert'>
													<i class='icon-remove'></i>
												</button>
		
												<p>
													<strong>
														<i class='icon-ok'></i>
														Success! - 
													</strong>
													Data Admin Persuratan Lab Dasar berhasil ditambahkan!
												</p>
											</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
														     swal({
														                title: 'Success!!',
														                text:  'Data Admin Persuratan Lab Dasar berhasil ditambahkan!',
														                type: 'success',
														                timer: 3000,
														                showConfirmButton: true
														            });  
														     },10);  
														    </script>
														    ");			
	}

	public function tambahAdminSuratUptTik()
	{
		date_default_timezone_set('Asia/Jakarta');

		$username 				= $this->input->post('username');
		$nama_lengkap			= $this->input->post('nama_lengkap');
		$jabatan				= $this->input->post('posisi');
		$status					= "admin surat";
		$uuid_atasan			= $this->input->post('atasan');

		$data = array(		
			'username' 			=> $username,
			'nama_lengkap' 		=> $nama_lengkap,
			'jabatan' 			=> $jabatan,
			'status' 			=> $status,
			'uuid_atasan' 		=> $uuid_atasan,
			);

		$login = array(		
			'username' 		=> $username,
			'stts' 			=> 'admin surat',
			'password' 		=> md5($username),
			);

		$this->web_app_model->insertData($data,'tbl_users');
		$this->web_app_model->insertData($login,'tbl_login');
		header('location:'.base_url().'index.php/super_admin/bg_upt_tik?data_users=1/');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
												<button type='button' class='close' data-dismiss='alert'>
													<i class='icon-remove'></i>
												</button>
		
												<p>
													<strong>
														<i class='icon-ok'></i>
														Success! - 
													</strong>
													Data Admin Persuratan UPT TIK berhasil ditambahkan!
												</p>
											</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
														     swal({
														                title: 'Success!!',
														                text:  'Data Admin Persuratan UPT TIK berhasil ditambahkan!',
														                type: 'success',
														                timer: 3000,
														                showConfirmButton: true
														            });  
														     },10);  
														    </script>
														    ");			
	}

	public function tambahAdminSuratSP()
	{
		date_default_timezone_set('Asia/Jakarta');

		$username 				= $this->input->post('username');
		$nama_lengkap			= $this->input->post('nama_lengkap');
		$jabatan				= $this->input->post('posisi');
		$status					= "admin surat";
		$uuid_atasan			= $this->input->post('atasan');

		$data = array(		
			'username' 			=> $username,
			'nama_lengkap' 		=> $nama_lengkap,
			'jabatan' 			=> $jabatan,
			'status' 			=> $status,
			'uuid_atasan' 		=> $uuid_atasan,
			);

		$login = array(		
			'username' 		=> $username,
			'stts' 			=> 'admin surat',
			'password' 		=> md5($username),
			);

		$this->web_app_model->insertData($data,'tbl_users');
		$this->web_app_model->insertData($login,'tbl_login');
		header('location:'.base_url().'index.php/super_admin/bg_sp?data_users=1/');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
												<button type='button' class='close' data-dismiss='alert'>
													<i class='icon-remove'></i>
												</button>
		
												<p>
													<strong>
														<i class='icon-ok'></i>
														Success! - 
													</strong>
													Data Admin Persuratan SP berhasil ditambahkan!
												</p>
											</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
														     swal({
														                title: 'Success!!',
														                text:  'Data Admin Persuratan SP berhasil ditambahkan!',
														                type: 'success',
														                timer: 3000,
														                showConfirmButton: true
														            });  
														     },10);  
														    </script>
														    ");			
	}

	public function tambahKaSP()
	{
		date_default_timezone_set('Asia/Jakarta');

		$kabag_username 		= $this->input->post('username');
		$kabag_nama				= $this->input->post('nama_lengkap');
		$kabag_posisi			= $this->input->post('posisi');
		$kabag_uuid				= $this->get_id();

		$cek_posisi 			= $this->web_app_model->getWhereAllItem($kabag_posisi,'kabag_posisi','tbl_kabag');

		$data = array(		
			'kabag_uuid' 		=> $kabag_uuid,
			'kabag_username' 	=> $kabag_username,
			'kabag_nama' 		=> $kabag_nama,
			'kabag_posisi' 		=> $kabag_posisi,
			'kabag_uuid_biro' 	=> "0",
			'kabag_kategori' 	=> "spi",
			);

		$login = array(		
			'username' 		=> $kabag_username,
			'stts' 			=> 'kabag',
			'password' 		=> md5($kabag_username),
			);

		if($cek_posisi->num_rows() > 0)
		{
			header('location:'.base_url().'index.php/super_admin/bg_sp?data_users=1/');
			$this->session->set_flashdata("info","<div class='alert alert-block alert-warning'>
													<button type='button' class='close' data-dismiss='alert'>
														<i class='icon-remove'></i>
													</button>
			
													<p>
														<strong>
															<i class='icon-ok'></i>
															Gagal! - 
														</strong>
														Data dengan posisi tersebut sudah digunakan!
													</p>
												</div>");

			$this->session->set_flashdata("info2","<script type='text/javascript'>
												     setTimeout(function () { 
														     swal({
														                title: 'Gagal!!',
														                text:  'Data dengan posisi tersebut sudah digunakan!',
														                type: 'warning',
														                timer: 3000,
														                showConfirmButton: true
														            });  
														     },10);  
														    </script>
														    ");
		}
		else
		{

			$this->web_app_model->insertData($data,'tbl_kabag');
			$this->web_app_model->insertData($login,'tbl_login');
			header('location:'.base_url().'index.php/super_admin/bg_sp?data_users=1/');
			$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
													<button type='button' class='close' data-dismiss='alert'>
														<i class='icon-remove'></i>
													</button>
			
													<p>
														<strong>
															<i class='icon-ok'></i>
															Success! - 
														</strong>
														Data Ketua SP berhasil ditambahkan!
													</p>
												</div>");

			$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
														     swal({
														                title: 'Success!!',
														                text:  'Data Ketua SP berhasil ditambahkan!',
														                type: 'success',
														                timer: 3000,
														                showConfirmButton: true
														            });  
														     },10);  
														    </script>
														    ");		
		}	
	}

	public function tambahAnggotaSP()
	{
		date_default_timezone_set('Asia/Jakarta');

		$staff_username 		= $this->input->post('username');
		$staff_nama				= $this->input->post('nama_lengkap');
		$staff_posisi			= $this->input->post('posisi');
		$staff_uuid_kasubbag	= $this->input->post('atasan');
		$staff_uuid				= $this->get_id();

		$data = array(		
			'staff_uuid' 			=> $staff_uuid,
			'staff_username' 		=> $staff_username,
			'staff_nama' 			=> $staff_nama,
			'staff_posisi' 			=> $staff_posisi,
			'staff_uuid_kasubbag' 	=> $staff_uuid_kasubbag,
			'staff_kategori' 		=> "spi",
			);

		$login = array(		
			'username' 		=> $staff_username,
			'stts' 			=> 'pelaksana',
			'password' 		=> md5($staff_username),
			);

		$this->web_app_model->insertData($data,'tbl_staff');
		$this->web_app_model->insertData($login,'tbl_login');
		header('location:'.base_url().'index.php/super_admin/bg_sp?data_users=1/');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
												<button type='button' class='close' data-dismiss='alert'>
													<i class='icon-remove'></i>
												</button>
		
												<p>
													<strong>
														<i class='icon-ok'></i>
														Success! - 
													</strong>
													Data Anggota SP berhasil ditambahkan!
												</p>
											</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
														     swal({
														                title: 'Success!!',
														                text:  'Data Anggota SP berhasil ditambahkan!',
														                type: 'success',
														                timer: 3000,
														                showConfirmButton: true
														            });  
														     },10);  
														    </script>
														    ");			
	}

	public function tambahSekSP()
	{
		date_default_timezone_set('Asia/Jakarta');

		$kasubbag_username 			= $this->input->post('username');
		$kasubbag_nama				= $this->input->post('nama_lengkap');
		$kasubbag_posisi			= $this->input->post('posisi');
		$kasubbag_uuid_kabag		= $this->input->post('atasan');
		$kasubbag_uuid				= $this->get_id();

		$cek_posisi 				= $this->web_app_model->getWhereAllItem($kasubbag_posisi,'kasubbag_posisi','tbl_kasubbag');

		$data = array(		
			'kasubbag_uuid' 			=> $kasubbag_uuid,
			'kasubbag_username' 		=> $kasubbag_username,
			'kasubbag_nama' 			=> $kasubbag_nama,
			'kasubbag_posisi' 			=> $kasubbag_posisi,
			'kasubbag_uuid_kabag' 		=> $kasubbag_uuid_kabag,
			'kasubbag_kategori' 		=> "spi",
			);

		$login = array(		
			'username' 		=> $kasubbag_username,
			'stts' 			=> 'kasubbag',
			'password' 		=> md5($kasubbag_username),
			);

		if($cek_posisi->num_rows() > 0)
		{
			header('location:'.base_url().'index.php/super_admin/bg_sp?data_users=1/');
			$this->session->set_flashdata("info","<div class='alert alert-block alert-warning'>
													<button type='button' class='close' data-dismiss='alert'>
														<i class='icon-remove'></i>
													</button>
			
													<p>
														<strong>
															<i class='icon-ok'></i>
															Gagal! - 
														</strong>
														Data dengan posisi tersebut sudah digunakan!
													</p>
												</div>");

			$this->session->set_flashdata("info2","<script type='text/javascript'>
												     setTimeout(function () { 
														     swal({
														                title: 'Gagal!!',
														                text:  'Data dengan posisi tersebut sudah digunakan!',
														                type: 'warning',
														                timer: 3000,
														                showConfirmButton: true
														            });  
														     },10);  
														    </script>
														    ");
		}
		else
		{

			$this->web_app_model->insertData($data,'tbl_kasubbag');
			$this->web_app_model->insertData($login,'tbl_login');
			header('location:'.base_url().'index.php/super_admin/bg_sp?data_users=1/');
			$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
													<button type='button' class='close' data-dismiss='alert'>
														<i class='icon-remove'></i>
													</button>
			
													<p>
														<strong>
															<i class='icon-ok'></i>
															Success! - 
														</strong>
														Data Sekretaris Satuan Pengawasan berhasil ditambahkan!
													</p>
												</div>");

			$this->session->set_flashdata("info2","<script type='text/javascript'>
												     setTimeout(function () { 
														     swal({
														                title: 'Success!!',
														                text:  'Data Sekretaris Satuan Pengawasan berhasil ditambahkan!',
														                type: 'success',
														                timer: 3000,
														                showConfirmButton: true
														            });  
														     },10);  
														    </script>
														    ");			
		}
	}

	public function tambahKabag()
	{
		date_default_timezone_set('Asia/Jakarta');

		$kabag_username 		= $this->input->post('username');
		$kabag_nama				= $this->input->post('nama_lengkap');
		$kabag_posisi			= $this->input->post('posisi');
		$kabag_uuid_biro		= $this->input->post('atasan');
		$kabag_uuid				= $this->get_id();

		$cek_posisi 			= $this->web_app_model->getWhereAllItem($kabag_posisi,'kabag_posisi','tbl_kabag');

		$data = array(		
			'kabag_uuid' 		=> $kabag_uuid,
			'kabag_username' 	=> $kabag_username,
			'kabag_nama' 		=> $kabag_nama,
			'kabag_posisi' 		=> $kabag_posisi,
			'kabag_uuid_biro' 	=> $kabag_uuid_biro,
			'kabag_kategori' 	=> "biro",
			);

		$login = array(		
			'username' 		=> $kabag_username,
			'stts' 			=> 'kabag',
			'password' 		=> md5($kabag_username),
			);

		if($cek_posisi->num_rows() > 0)
		{
			header('location:'.base_url().'index.php/super_admin/bg_biro?data_users=1/');
			$this->session->set_flashdata("info","<div class='alert alert-block alert-warning'>
													<button type='button' class='close' data-dismiss='alert'>
														<i class='icon-remove'></i>
													</button>
			
													<p>
														<strong>
															<i class='icon-ok'></i>
															Gagal! - 
														</strong>
														Data dengan posisi tersebut sudah digunakan!
													</p>
												</div>");

			$this->session->set_flashdata("info2","<script type='text/javascript'>
												     setTimeout(function () { 
														     swal({
														                title: 'Gagal!!',
														                text:  'Data dengan posisi tersebut sudah digunakan!',
														                type: 'warning',
														                timer: 3000,
														                showConfirmButton: true
														            });  
														     },10);  
														    </script>
														    ");
		}
		else
		{
			$this->web_app_model->insertData($data,'tbl_kabag');
			$this->web_app_model->insertData($login,'tbl_login');
			header('location:'.base_url().'index.php/super_admin/bg_biro?data_users=1/');
			$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
													<button type='button' class='close' data-dismiss='alert'>
														<i class='icon-remove'></i>
													</button>
			
													<p>
														<strong>
															<i class='icon-ok'></i>
															Success! - 
														</strong>
														Data Kabag berhasil ditambahkan!
													</p>
												</div>");

			$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
														     swal({
														                title: 'Success!!',
														                text:  'Data Kabag berhasil ditambahkan!',
														                type: 'success',
														                timer: 3000,
														                showConfirmButton: true
														            });  
														     },10);  
														    </script>
														    ");	
		}		
	}

	public function tambahKasubbag()
	{
		//=== BIROOO
		date_default_timezone_set('Asia/Jakarta');

		$kasubbag_username 		= $this->input->post('username');
		$kasubbag_nama			= $this->input->post('nama_lengkap');
		$kasubbag_posisi		= $this->input->post('posisi');
		$kasubbag_uuid_kabag	= $this->input->post('atasan');
		$kasubbag_uuid			= $this->get_id();

		$cek_posisi 			= $this->web_app_model->getWhereAllItem($kasubbag_posisi,'kasubbag_posisi','tbl_kasubbag');

		$data = array(		
			'kasubbag_uuid' 		=> $kasubbag_uuid,
			'kasubbag_username' 	=> $kasubbag_username,
			'kasubbag_nama' 		=> $kasubbag_nama,
			'kasubbag_posisi' 		=> $kasubbag_posisi,
			'kasubbag_uuid_kabag' 	=> $kasubbag_uuid_kabag,
			'kasubbag_kategori' 	=> "biro",
			);

		$login = array(		
			'username' 		=> $kasubbag_username,
			'stts' 			=> 'kasubbag',
			'password' 		=> md5($kasubbag_username),
			);

		if($cek_posisi->num_rows() > 0)
		{
			header('location:'.base_url().'index.php/super_admin/bg_biro?data_users=1/');
			$this->session->set_flashdata("info","<div class='alert alert-block alert-warning'>
													<button type='button' class='close' data-dismiss='alert'>
														<i class='icon-remove'></i>
													</button>
			
													<p>
														<strong>
															<i class='icon-ok'></i>
															Gagal! - 
														</strong>
														Data dengan posisi tersebut sudah digunakan!
													</p>
												</div>");

			$this->session->set_flashdata("info2","<script type='text/javascript'>
												     setTimeout(function () { 
														     swal({
														                title: 'Gagal!!',
														                text:  'Data dengan posisi tersebut sudah digunakan!',
														                type: 'warning',
														                timer: 3000,
														                showConfirmButton: true
														            });  
														     },10);  
														    </script>
														    ");
		}
		else
		{
			$this->web_app_model->insertData($data,'tbl_kasubbag');
			$this->web_app_model->insertData($login,'tbl_login');
			header('location:'.base_url().'index.php/super_admin/bg_biro?data_users=1/');
			$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
													<button type='button' class='close' data-dismiss='alert'>
														<i class='icon-remove'></i>
													</button>
			
													<p>
														<strong>
															<i class='icon-ok'></i>
															Success! - 
														</strong>
														Data Kasubbag berhasil ditambahkan!
													</p>
												</div>");

			$this->session->set_flashdata("info2","<script type='text/javascript'>
												     setTimeout(function () { 
														     swal({
														                title: 'Success!!',
														                text:  'Data Kasubbag berhasil ditambahkan!',
														                type: 'success',
														                timer: 3000,
														                showConfirmButton: true
														            });  
														     },10);  
														    </script>
														    ");
		}			
	}

	public function tambahPelaksanaBiro()
	{

		// Level Pelaksana tidak perlu di batasi double user nya, krn dia level akhir, tidak
		// memiliki bawahan lagi

		date_default_timezone_set('Asia/Jakarta');

		$staff_username 		= $this->input->post('username');
		$staff_nama				= $this->input->post('nama_lengkap');
		$staff_posisi			= $this->input->post('posisi');
		$staff_uuid_kasubbag	= $this->input->post('atasan');
		$staff_uuid				= $this->get_id();

		$data = array(		
			'staff_uuid' 			=> $staff_uuid,
			'staff_username' 		=> $staff_username,
			'staff_nama' 			=> $staff_nama,
			'staff_posisi' 			=> $staff_posisi,
			'staff_uuid_kasubbag' 	=> $staff_uuid_kasubbag,
			'staff_kategori' 		=> "biro",
			);

		$login = array(		
			'username' 		=> $staff_username,
			'stts' 			=> 'pelaksana',
			'password' 		=> md5($staff_username),
			);

		$this->web_app_model->insertData($data,'tbl_staff');
		$this->web_app_model->insertData($login,'tbl_login');
		header('location:'.base_url().'index.php/super_admin/bg_biro?data_users=1/');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
												<button type='button' class='close' data-dismiss='alert'>
													<i class='icon-remove'></i>
												</button>
		
												<p>
													<strong>
														<i class='icon-ok'></i>
														Success! - 
													</strong>
													Data Pelaksana berhasil ditambahkan!
												</p>
											</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
														     swal({
														                title: 'Success!!',
														                text:  'Data Pelaksana berhasil ditambahkan!',
														                type: 'success',
														                timer: 3000,
														                showConfirmButton: true
														            });  
														     },10);  
														    </script>
														    ");			
	}

	public function tambahPelaksanaUptTik()
	{
		date_default_timezone_set('Asia/Jakarta');

		$staff_username 		= $this->input->post('username');
		$staff_nama				= $this->input->post('nama_lengkap');
		$staff_posisi			= $this->input->post('posisi');
		$staff_uuid_kasubbag	= $this->input->post('atasan');
		$staff_uuid				= $this->get_id();

		$data = array(		
			'staff_uuid' 			=> $staff_uuid,
			'staff_username' 		=> $staff_username,
			'staff_nama' 			=> $staff_nama,
			'staff_posisi' 			=> $staff_posisi,
			'staff_uuid_kasubbag' 	=> $staff_uuid_kasubbag,
			'staff_kategori' 		=> "upt tik",
			);

		$login = array(		
			'username' 		=> $staff_username,
			'stts' 			=> 'pelaksana',
			'password' 		=> md5($staff_username),
			);

		$this->web_app_model->insertData($data,'tbl_staff');
		$this->web_app_model->insertData($login,'tbl_login');
		header('location:'.base_url().'index.php/super_admin/bg_upt_tik?data_users=1/');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
												<button type='button' class='close' data-dismiss='alert'>
													<i class='icon-remove'></i>
												</button>
		
												<p>
													<strong>
														<i class='icon-ok'></i>
														Success! - 
													</strong>
													Data Pelaksana UPT TIK berhasil ditambahkan!
												</p>
											</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
														     swal({
														                title: 'Success!!',
														                text:  'Data Pelaksana UPT TIK berhasil ditambahkan!',
														                type: 'success',
														                timer: 3000,
														                showConfirmButton: true
														            });  
														     },10);  
														    </script>
														    ");			
	}

	public function tambahKaUptTik()
	{
		date_default_timezone_set('Asia/Jakarta');

		$kasubbag_username 			= $this->input->post('username');
		$kasubbag_nama				= $this->input->post('nama_lengkap');
		$kasubbag_posisi			= $this->input->post('posisi');
		$kasubbag_uuid_kabag		= "0";
		$kasubbag_uuid				= $this->get_id();

		$cek_posisi 			= $this->web_app_model->getWhereAllItem($kasubbag_posisi,'kasubbag_posisi','tbl_kasubbag');

		$data = array(		
			'kasubbag_uuid' 			=> $kasubbag_uuid,
			'kasubbag_username' 		=> $kasubbag_username,
			'kasubbag_nama' 			=> $kasubbag_nama,
			'kasubbag_posisi' 			=> $kasubbag_posisi,
			'kasubbag_uuid_kabag' 		=> $kasubbag_uuid_kabag,
			'kasubbag_kategori' 		=> "upt tik",
			);

		$login = array(		
			'username' 		=> $kasubbag_username,
			'stts' 			=> 'kasubbag',
			'password' 		=> md5($kasubbag_username),
			);

		if($cek_posisi->num_rows() > 0)
		{
			header('location:'.base_url().'index.php/super_admin/bg_upt_tik?data_users=1/');
			$this->session->set_flashdata("info","<div class='alert alert-block alert-warning'>
													<button type='button' class='close' data-dismiss='alert'>
														<i class='icon-remove'></i>
													</button>
			
													<p>
														<strong>
															<i class='icon-ok'></i>
															Gagal! - 
														</strong>
														Data dengan posisi tersebut sudah digunakan!
													</p>
												</div>");

			$this->session->set_flashdata("info2","<script type='text/javascript'>
												     setTimeout(function () { 
														     swal({
														                title: 'Gagal!!',
														                text:  'Data dengan posisi tersebut sudah digunakan!',
														                type: 'warning',
														                timer: 3000,
														                showConfirmButton: true
														            });  
														     },10);  
														    </script>
														    ");
		}
		else
		{
			$this->web_app_model->insertData($data,'tbl_kasubbag');
			$this->web_app_model->insertData($login,'tbl_login');
			header('location:'.base_url().'index.php/super_admin/bg_upt_tik?data_users=1/');
			$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
													<button type='button' class='close' data-dismiss='alert'>
														<i class='icon-remove'></i>
													</button>
			
													<p>
														<strong>
															<i class='icon-ok'></i>
															Success! - 
														</strong>
														Data Kepala UPT TIK berhasil ditambahkan!
													</p>
												</div>");

			$this->session->set_flashdata("info2","<script type='text/javascript'>
												     setTimeout(function () { 
															     swal({
															                title: 'Success!!',
															                text:  'Data Kepala UPT TIK berhasil ditambahkan!',
															                type: 'success',
														                timer: 3000,
														                showConfirmButton: true
														            });  
														     },10);  
														    </script>
														    ");			
		}
	}

	public function tambahKaLabdas()
	{
		date_default_timezone_set('Asia/Jakarta');

		$kasubbag_username 			= $this->input->post('username');
		$kasubbag_nama				= $this->input->post('nama_lengkap');
		$kasubbag_posisi			= $this->input->post('posisi');
		$kasubbag_uuid_kabag		= "0";
		$kasubbag_uuid				= $this->get_id();

		$cek_posisi 			= $this->web_app_model->getWhereAllItem($kasubbag_posisi,'kasubbag_posisi','tbl_kasubbag');

		$data = array(		
			'kasubbag_uuid' 			=> $kasubbag_uuid,
			'kasubbag_username' 		=> $kasubbag_username,
			'kasubbag_nama' 			=> $kasubbag_nama,
			'kasubbag_posisi' 			=> $kasubbag_posisi,
			'kasubbag_uuid_kabag' 		=> $kasubbag_uuid_kabag,
			'kasubbag_kategori' 		=> "upt lab dasar",
			);

		$login = array(		
			'username' 		=> $kasubbag_username,
			'stts' 			=> 'kasubbag',
			'password' 		=> md5($kasubbag_username),
			);

		if($cek_posisi->num_rows() > 0)
		{
			header('location:'.base_url().'index.php/super_admin/bg_upt_labdas?data_users=1/');
			$this->session->set_flashdata("info","<div class='alert alert-block alert-warning'>
													<button type='button' class='close' data-dismiss='alert'>
														<i class='icon-remove'></i>
													</button>
			
													<p>
														<strong>
															<i class='icon-ok'></i>
															Gagal! - 
														</strong>
														Data dengan posisi tersebut sudah digunakan!
													</p>
												</div>");

			$this->session->set_flashdata("info2","<script type='text/javascript'>
												     setTimeout(function () { 
														     swal({
														                title: 'Gagal!!',
														                text:  'Data dengan posisi tersebut sudah digunakan!',
														                type: 'warning',
														                timer: 3000,
														                showConfirmButton: true
														            });  
														     },10);  
														    </script>
														    ");
		}
		else
		{
			$this->web_app_model->insertData($data,'tbl_kasubbag');
			$this->web_app_model->insertData($login,'tbl_login');
			header('location:'.base_url().'index.php/super_admin/bg_upt_labdas?data_users=1/');
			$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
													<button type='button' class='close' data-dismiss='alert'>
														<i class='icon-remove'></i>
													</button>
			
													<p>
														<strong>
															<i class='icon-ok'></i>
															Success! - 
														</strong>
														Data Kepala UPT Laboratorium Dasar berhasil ditambahkan!
													</p>
												</div>");

			$this->session->set_flashdata("info2","<script type='text/javascript'>
												     setTimeout(function () { 
															     swal({
															                title: 'Success!!',
															                text:  'Data Kepala UPT Laboratorium Dasar berhasil ditambahkan!',
															                type: 'success',
															                timer: 3000,
															                showConfirmButton: true
															            });  
															     },10);  
															    </script>
															    ");			
		}
	}

	public function tambahKaLabBhs()
	{
		date_default_timezone_set('Asia/Jakarta');

		$kasubbag_username 			= $this->input->post('username');
		$kasubbag_nama				= $this->input->post('nama_lengkap');
		$kasubbag_posisi			= $this->input->post('posisi');
		$kasubbag_uuid_kabag		= "0";
		$kasubbag_uuid				= $this->get_id();

		$cek_posisi 			= $this->web_app_model->getWhereAllItem($kasubbag_posisi,'kasubbag_posisi','tbl_kasubbag');

		$data = array(		
			'kasubbag_uuid' 			=> $kasubbag_uuid,
			'kasubbag_username' 		=> $kasubbag_username,
			'kasubbag_nama' 			=> $kasubbag_nama,
			'kasubbag_posisi' 			=> $kasubbag_posisi,
			'kasubbag_uuid_kabag' 		=> $kasubbag_uuid_kabag,
			'kasubbag_kategori' 		=> "upt lab bahasa",
			);

		$login = array(		
			'username' 		=> $kasubbag_username,
			'stts' 			=> 'kasubbag',
			'password' 		=> md5($kasubbag_username),
			);

		if($cek_posisi->num_rows() > 0)
		{
			header('location:'.base_url().'index.php/super_admin/bg_upt_lab_bhs?data_users=1/');
			$this->session->set_flashdata("info","<div class='alert alert-block alert-warning'>
													<button type='button' class='close' data-dismiss='alert'>
														<i class='icon-remove'></i>
													</button>
			
													<p>
														<strong>
															<i class='icon-ok'></i>
															Gagal! - 
														</strong>
														Data dengan posisi tersebut sudah digunakan!
													</p>
												</div>");

			$this->session->set_flashdata("info2","<script type='text/javascript'>
												     setTimeout(function () { 
														     swal({
														                title: 'Gagal!!',
														                text:  'Data dengan posisi tersebut sudah digunakan!',
														                type: 'warning',
														                timer: 3000,
														                showConfirmButton: true
														            });  
														     },10);  
														    </script>
														    ");
		}
		else
		{

			$this->web_app_model->insertData($data,'tbl_kasubbag');
			$this->web_app_model->insertData($login,'tbl_login');
			header('location:'.base_url().'index.php/super_admin/bg_upt_lab_bhs?data_users=1/');
			$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
													<button type='button' class='close' data-dismiss='alert'>
														<i class='icon-remove'></i>
													</button>
			
													<p>
														<strong>
															<i class='icon-ok'></i>
															Success! - 
														</strong>
														Data Kepala UPT Laboratorium Bahasa berhasil ditambahkan!
													</p>
												</div>");

			$this->session->set_flashdata("info2","<script type='text/javascript'>
												     setTimeout(function () { 
															     swal({
															                title: 'Success!!',
															                text:  'Data Kepala UPT Laboratorium Bahasa berhasil ditambahkan!',
														                type: 'success',
														                timer: 3000,
														                showConfirmButton: true
														            });  
														     },10);  
														    </script>
														    ");	
		}		
	}

	public function tambahKaPerpus()
	{
		date_default_timezone_set('Asia/Jakarta');

		$kasubbag_username 			= $this->input->post('username');
		$kasubbag_nama				= $this->input->post('nama_lengkap');
		$kasubbag_posisi			= $this->input->post('posisi');
		$kasubbag_uuid_kabag		= "0";
		$kasubbag_uuid				= $this->get_id();

		$cek_posisi 				= $this->web_app_model->getWhereAllItem($kasubbag_posisi,'kasubbag_posisi','tbl_kasubbag');

		$data = array(		
			'kasubbag_uuid' 			=> $kasubbag_uuid,
			'kasubbag_username' 		=> $kasubbag_username,
			'kasubbag_nama' 			=> $kasubbag_nama,
			'kasubbag_posisi' 			=> $kasubbag_posisi,
			'kasubbag_uuid_kabag' 		=> $kasubbag_uuid_kabag,
			'kasubbag_kategori' 		=> "upt perpustakaan",
			);

		$login = array(		
			'username' 		=> $kasubbag_username,
			'stts' 			=> 'kasubbag',
			'password' 		=> md5($kasubbag_username),
			);

		if($cek_posisi->num_rows() > 0)
		{
			header('location:'.base_url().'index.php/super_admin/bg_upt_perpus?data_users=1/');
			$this->session->set_flashdata("info","<div class='alert alert-block alert-warning'>
													<button type='button' class='close' data-dismiss='alert'>
														<i class='icon-remove'></i>
													</button>
			
													<p>
														<strong>
															<i class='icon-ok'></i>
															Gagal! - 
														</strong>
														Data dengan posisi tersebut sudah digunakan!
													</p>
												</div>");

			$this->session->set_flashdata("info2","<script type='text/javascript'>
												     setTimeout(function () { 
														     swal({
														                title: 'Gagal!!',
														                text:  'Data dengan posisi tersebut sudah digunakan!',
														                type: 'warning',
														                timer: 3000,
														                showConfirmButton: true
														            });  
														     },10);  
														    </script>
														    ");
		}
		else
		{

			$this->web_app_model->insertData($data,'tbl_kasubbag');
			$this->web_app_model->insertData($login,'tbl_login');
			header('location:'.base_url().'index.php/super_admin/bg_upt_perpus?data_users=1/');
			$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
													<button type='button' class='close' data-dismiss='alert'>
														<i class='icon-remove'></i>
													</button>
			
													<p>
														<strong>
															<i class='icon-ok'></i>
															Success! - 
														</strong>
														Data Kepala UPT Perpustakaan berhasil ditambahkan!
													</p>
												</div>");

			$this->session->set_flashdata("info2","<script type='text/javascript'>
												     setTimeout(function () { 
															     swal({
															                title: 'Success!!',
															                text:  'Data Kepala UPT Perpustakaan berhasil ditambahkan!',
														                type: 'success',
														                timer: 3000,
														                showConfirmButton: true
														            });  
														     },10);  
														    </script>
														    ");	
		}		
	}

	public function tambahPelaksanaLabdas()
	{
		date_default_timezone_set('Asia/Jakarta');

		$staff_username 		= $this->input->post('username');
		$staff_nama				= $this->input->post('nama_lengkap');
		$staff_posisi			= $this->input->post('posisi');
		$staff_uuid_kasubbag	= $this->input->post('atasan');
		$staff_uuid				= $this->get_id();

		$data = array(		
			'staff_uuid' 			=> $staff_uuid,
			'staff_username' 		=> $staff_username,
			'staff_nama' 			=> $staff_nama,
			'staff_posisi' 			=> $staff_posisi,
			'staff_uuid_kasubbag' 	=> $staff_uuid_kasubbag,
			'staff_kategori' 		=> "upt lab dasar",
			);

		$login = array(		
			'username' 		=> $staff_username,
			'stts' 			=> 'pelaksana',
			'password' 		=> md5($staff_username),
			);

		$this->web_app_model->insertData($data,'tbl_staff');
		$this->web_app_model->insertData($login,'tbl_login');
		header('location:'.base_url().'index.php/super_admin/bg_upt_labdas?data_users=1/');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
												<button type='button' class='close' data-dismiss='alert'>
													<i class='icon-remove'></i>
												</button>
		
												<p>
													<strong>
														<i class='icon-ok'></i>
														Success! - 
													</strong>
													Data Pelaksana UPT Laboratorium Dasar berhasil ditambahkan!
												</p>
											</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
														     swal({
														                title: 'Success!!',
														                text:  'Data Pelaksana UPT Laboratorium Dasar berhasil ditambahkan!',
														                type: 'success',
														                timer: 3000,
														                showConfirmButton: true
														            });  
														     },10);  
														    </script>
														    ");			
	}

	public function tambahPelaksanaLabBhs()
	{
		date_default_timezone_set('Asia/Jakarta');

		$staff_username 		= $this->input->post('username');
		$staff_nama				= $this->input->post('nama_lengkap');
		$staff_posisi			= $this->input->post('posisi');
		$staff_uuid_kasubbag	= $this->input->post('atasan');
		$staff_uuid				= $this->get_id();

		$data = array(		
			'staff_uuid' 			=> $staff_uuid,
			'staff_username' 		=> $staff_username,
			'staff_nama' 			=> $staff_nama,
			'staff_posisi' 			=> $staff_posisi,
			'staff_uuid_kasubbag' 	=> $staff_uuid_kasubbag,
			'staff_kategori' 		=> "upt lab bahasa",
			);

		$login = array(		
			'username' 		=> $staff_username,
			'stts' 			=> 'pelaksana',
			'password' 		=> md5($staff_username),
			);

		$this->web_app_model->insertData($data,'tbl_staff');
		$this->web_app_model->insertData($login,'tbl_login');
		header('location:'.base_url().'index.php/super_admin/bg_upt_lab_bhs?data_users=1/');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
												<button type='button' class='close' data-dismiss='alert'>
													<i class='icon-remove'></i>
												</button>
		
												<p>
													<strong>
														<i class='icon-ok'></i>
														Success! - 
													</strong>
													Data Pelaksana UPT Laboratorium Bahasa berhasil ditambahkan!
												</p>
											</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
														     swal({
														                title: 'Success!!',
														                text:  'Data Pelaksana UPT Laboratorium Bahasa berhasil ditambahkan!',
														                type: 'success',
														                timer: 3000,
														                showConfirmButton: true
														            });  
														     },10);  
														    </script>
														    ");			
	}

	public function tambahPelaksanaPerpus()
	{
		date_default_timezone_set('Asia/Jakarta');

		$staff_username 		= $this->input->post('username');
		$staff_nama				= $this->input->post('nama_lengkap');
		$staff_posisi			= $this->input->post('posisi');
		$staff_uuid_kasubbag	= $this->input->post('atasan');
		$staff_uuid				= $this->get_id();

		$data = array(		
			'staff_uuid' 			=> $staff_uuid,
			'staff_username' 		=> $staff_username,
			'staff_nama' 			=> $staff_nama,
			'staff_posisi' 			=> $staff_posisi,
			'staff_uuid_kasubbag' 	=> $staff_uuid_kasubbag,
			'staff_kategori' 		=> "upt perpustakaan",
			);

		$login = array(		
			'username' 		=> $staff_username,
			'stts' 			=> 'pelaksana',
			'password' 		=> md5($staff_username),
			);

		$this->web_app_model->insertData($data,'tbl_staff');
		$this->web_app_model->insertData($login,'tbl_login');
		header('location:'.base_url().'index.php/super_admin/bg_upt_perpus?data_users=1/');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
												<button type='button' class='close' data-dismiss='alert'>
													<i class='icon-remove'></i>
												</button>
		
												<p>
													<strong>
														<i class='icon-ok'></i>
														Success! - 
													</strong>
													Data Pelaksana UPT Perpustakaan berhasil ditambahkan!
												</p>
											</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
														     swal({
														                title: 'Success!!',
														                text:  'Data Pelaksana UPT Perpustakaan berhasil ditambahkan!',
														                type: 'success',
														                timer: 3000,
														                showConfirmButton: true
														            });  
														     },10);  
														    </script>
														    ");			
	}





//=======EDIT DATA -----------

	public function editRektor()
	{
		date_default_timezone_set('Asia/Jakarta');

		$rektor_nama				= $this->input->post('nama_lengkap');
		$rektor_posisi				= $this->input->post('posisi');
		$rektor_uuid				= $this->input->post('rektor_uuid');

		$data = array(		
			'rektor_nama' 			=> $rektor_nama,
			'rektor_posisi' 		=> $rektor_posisi,
			);

		$where = array(
			'rektor_uuid' 			=> $rektor_uuid,
			);

		
		$this->web_app_model->updateDataWhere($where,$data,'tbl_rektor');
		header('location:'.base_url().'index.php/super_admin/bg_rektor?data_users=1/');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
												<button type='button' class='close' data-dismiss='alert'>
													<i class='icon-remove'></i>
												</button>
		
												<p>
													<strong>
														<i class='icon-ok'></i>
														Success! - 
													</strong>
													Data Rektor berhasil diupdate!
												</p>
											</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
														     swal({
														                title: 'Success!!',
														                text:  'Data Rektor berhasil diupdate!',
														                type: 'success',
														                timer: 3000,
														                showConfirmButton: true
														            });  
														     },10);  
														    </script>
														    ");			
	}

	public function editDekan()
	{
		date_default_timezone_set('Asia/Jakarta');

		$biro_posisi_before			= $this->input->post('biro_posisi_before');
		$biro_uuid					= $this->input->post('biro_uuid');
		$biro_nama					= $this->input->post('biro_nama');
		$biro_posisi				= $this->input->post('biro_posisi');
		$biro_email					= $this->input->post('biro_email');

		$data = array(		
			'biro_nama' 			=> $biro_nama,
			'biro_posisi' 			=> $biro_posisi,
			'biro_email' 			=> $biro_email,
			);

		$where = array(
			'biro_uuid' 			=> $biro_uuid,
			);

		//$cek_posisi 			= $this->web_app_model->getWhereAllItem($biro_posisi,'biro_posisi','tbl_biro');
		$cek_posisi 			= $this->web_app_model->get2WhereAllItem($biro_posisi,'biro_posisi',$biro_uuid,'biro_uuid','tbl_biro');

		if($cek_posisi->num_rows() < 1)
		{
			header('location:'.base_url().'index.php/super_admin/bg_dekan?data_users=1/');
			$this->session->set_flashdata("info","<div class='alert alert-block alert-warning'>
													<button type='button' class='close' data-dismiss='alert'>
														<i class='icon-remove'></i>
													</button>
			
													<p>
														<strong>
															<i class='icon-ok'></i>
															Gagal! - 
														</strong>
														Data dengan posisi / username tersebut sudah digunakan!
													</p>
												</div>");

			$this->session->set_flashdata("info2","<script type='text/javascript'>
												     setTimeout(function () { 
														     swal({
														                title: 'Gagal!!',
														                text:  'Data dengan posisi / username tersebut sudah digunakan!',
														                type: 'warning',
														                timer: 3000,
														                showConfirmButton: true
														            });  
														     },10);  
														    </script>
														    ");
		}
		else
		{
			$this->web_app_model->updateDataWhere($where, $data,'tbl_biro');
			header('location:'.base_url().'index.php/super_admin/bg_dekan?data_users=1/');
			$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
													<button type='button' class='close' data-dismiss='alert'>
														<i class='icon-remove'></i>
													</button>
			
													<p>
														<strong>
															<i class='icon-ok'></i>
															Success! - 
														</strong>
														Data Dekan berhasil diupdate!
													</p>
												</div>");

			$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
														     swal({
														                title: 'Success!!',
														                text:  'Data Dekan berhasil diupdate!',
														                type: 'success',
														                timer: 3000,
														                showConfirmButton: true
														            });  
														     },10);  
														    </script>
														    ");
		}	

	}

	public function editFakultas()
	{
		date_default_timezone_set('Asia/Jakarta');

		$fak_fakultas_nama			= $this->input->post('fak_fakultas_nama');
		$fak_fakultas_no			= $this->input->post('fak_fakultas_no');

		$data = array(		
			'fak_fakultas_nama' 	=> $fak_fakultas_nama,
			);

		$where = array(
			'fak_fakultas_no' 		=> $fak_fakultas_no,
			);

		
		$this->web_app_model->updateDataWhere($where,$data,'fak_fakultas');
		header('location:'.base_url().'index.php/super_admin/bg_fakultas?kd_fakultas=1/');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
												<button type='button' class='close' data-dismiss='alert'>
													<i class='icon-remove'></i>
												</button>
		
												<p>
													<strong>
														<i class='icon-ok'></i>
														Success! - 
													</strong>
													Data Fakultas berhasil diupdate!
												</p>
											</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
														     swal({
														                title: 'Success!!',
														                text:  'Data Fakultas berhasil diupdate!',
														                type: 'success',
														                timer: 3000,
														                showConfirmButton: true
														            });  
														     },10);  
														    </script>
														    ");			
	}

	public function editBiro()
	{
		date_default_timezone_set('Asia/Jakarta');

		$biro_nama				= $this->input->post('nama_lengkap');
		$biro_posisi			= $this->input->post('posisi');
		$biro_uuid				= $this->input->post('biro_uuid');

		$data = array(		
			'biro_nama' 		=> $biro_nama,
			'biro_posisi' 		=> $biro_posisi,
			);

		$where = array(
			'biro_uuid' 		=> $biro_uuid,
			);

		
		$this->web_app_model->updateDataWhere($where,$data,'tbl_biro');
		header('location:'.base_url().'index.php/super_admin/bg_biro?data_users=1/');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
												<button type='button' class='close' data-dismiss='alert'>
													<i class='icon-remove'></i>
												</button>
		
												<p>
													<strong>
														<i class='icon-ok'></i>
														Success! - 
													</strong>
													Data Biro berhasil diupdate!
												</p>
											</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
														     swal({
														                title: 'Success!!',
														                text:  'Data Biro berhasil diupdate!',
														                type: 'success',
														                timer: 3000,
														                showConfirmButton: true
														            });  
														     },10);  
														    </script>
														    ");			
	}

	public function editKabag()
	{
		date_default_timezone_set('Asia/Jakarta');

		$kabag_nama				= $this->input->post('nama_lengkap');
		$kabag_posisi			= $this->input->post('posisi');
		$kabag_uuid				= $this->input->post('kabag_uuid');
		$kabag_uuid_biro		= $this->input->post('atasan');

		$data = array(		
			'kabag_nama' 		=> $kabag_nama,
			'kabag_posisi' 		=> $kabag_posisi,
			'kabag_uuid_biro' 	=> $kabag_uuid_biro,
			);

		$where = array(
			'kabag_uuid' 		=> $kabag_uuid,
			);

		$cek_posisi 			= $this->web_app_model->get2WhereAllItem($kabag_posisi,'kabag_posisi',$kabag_uuid,'kabag_uuid','tbl_kabag');

		if($cek_posisi->num_rows() < 1)
		{
			header('location:'.base_url().'index.php/super_admin/bg_dekan?data_users=1/');
			$this->session->set_flashdata("info","<div class='alert alert-block alert-warning'>
													<button type='button' class='close' data-dismiss='alert'>
														<i class='icon-remove'></i>
													</button>
			
													<p>
														<strong>
															<i class='icon-ok'></i>
															Gagal! - 
														</strong>
														Data dengan posisi / username tersebut sudah digunakan!
													</p>
												</div>");

			$this->session->set_flashdata("info2","<script type='text/javascript'>
												     setTimeout(function () { 
														     swal({
														                title: 'Gagal!!',
														                text:  'Data dengan posisi / username tersebut sudah digunakan!',
														                type: 'warning',
														                timer: 3000,
														                showConfirmButton: true
														            });  
														     },10);  
														    </script>
														    ");
		}
		else
		{
		$this->web_app_model->updateDataWhere($where,$data,'tbl_kabag');
		header('location:'.base_url().'index.php/super_admin/bg_biro?data_users=1/');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
												<button type='button' class='close' data-dismiss='alert'>
													<i class='icon-remove'></i>
												</button>
		
												<p>
													<strong>
														<i class='icon-ok'></i>
														Success! - 
													</strong>
													Data Kabag berhasil diupdate!
												</p>
											</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
														     swal({
														                title: 'Success!!',
														                text:  'Data Kabag berhasil diupdate!',
														                type: 'success',
														                timer: 3000,
														                showConfirmButton: true
														            });  
														     },10);  
														    </script>
														    ");	
		}		
	}

	public function editKabagProdi()
	{
		date_default_timezone_set('Asia/Jakarta');

		$kabag_username			= $this->input->post('kabag_username');
		$kabag_nama				= $this->input->post('kabag_nama');
		$kabag_posisi			= $this->input->post('kabag_posisi');
		$kabag_uuid				= $this->input->post('kabag_uuid');
		$kabag_email			= $this->input->post('kabag_email');
		$kabag_uuid_biro		= $this->input->post('atasan');

		$data = array(		
			'kabag_nama' 		=> $kabag_nama,
			'kabag_posisi' 		=> $kabag_posisi,
			'kabag_email' 		=> $kabag_email,
			'kabag_uuid_biro' 	=> $kabag_uuid_biro,
			);

		$where = array(
			'kabag_uuid' 		=> $kabag_uuid,
			);

		
		
		$cek_posisi 			= $this->web_app_model->getWhereAllItem($kabag_posisi,'kabag_posisi','tbl_kabag');
		$cek_username 			= $this->web_app_model->get2WhereAllItem($kabag_posisi,'kabag_posisi',$kabag_uuid,'kabag_uuid','tbl_kabag');

		if($cek_posisi->num_rows() > 0 && $cek_username->num_rows() < 1)
		{
			header('location:'.base_url().'index.php/super_admin/bg_dekan?data_users=1/');
			$this->session->set_flashdata("info","<div class='alert alert-block alert-warning'>
													<button type='button' class='close' data-dismiss='alert'>
														<i class='icon-remove'></i>
													</button>
			
													<p>
														<strong>
															<i class='icon-ok'></i>
															Gagal! - 
														</strong>
														Data dengan posisi / username tersebut sudah digunakan!
													</p>
												</div>");

			$this->session->set_flashdata("info2","<script type='text/javascript'>
												     setTimeout(function () { 
														     swal({
														                title: 'Gagal!!',
														                text:  'Data dengan posisi / username tersebut sudah digunakan!',
														                type: 'warning',
														                timer: 3000,
														                showConfirmButton: true
														            });  
														     },10);  
														    </script>
														    ");
		}
		else
		{
			$this->web_app_model->updateDataWhere($where, $data,'tbl_kabag');
			header('location:'.base_url().'index.php/super_admin/bg_dekan?data_users=1/');
			$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
													<button type='button' class='close' data-dismiss='alert'>
														<i class='icon-remove'></i>
													</button>
			
													<p>
														<strong>
															<i class='icon-ok'></i>
															Success! - 
														</strong>
														Data Kabag / Kaprodi (fakultas) berhasil diupdate!
													</p>
												</div>");

			$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
														     swal({
														                title: 'Success!!',
														                text:  'Data Kabag / Kaprodi (fakultas) berhasil diupdate!',
														                type: 'success',
														                timer: 3000,
														                showConfirmButton: true
														            });  
														     },10);  
														    </script>
														    ");
		}
	}

	public function editKasubbagFak()
	{
		date_default_timezone_set('Asia/Jakarta');

		$kasubbag_username			= $this->input->post('kasubbag_username');
		$kasubbag_nama				= $this->input->post('kasubbag_nama');
		$kasubbag_posisi			= $this->input->post('kasubbag_posisi');
		$kasubbag_uuid				= $this->input->post('kasubbag_uuid');
		$kasubbag_email				= $this->input->post('kasubbag_email');
		$kasubbag_uuid_kabag		= $this->input->post('kasubbag_uuid_kabag');

		$data = array(		
			'kasubbag_nama' 		=> $kasubbag_nama,
			'kasubbag_posisi' 		=> $kasubbag_posisi,
			'kasubbag_email' 		=> $kasubbag_email,
			'kasubbag_uuid_kabag' 	=> $kasubbag_uuid_kabag,
			);

		$where = array(
			'kasubbag_uuid' 		=> $kasubbag_uuid,
			);

		
		
		//$cek_posisi 			= $this->web_app_model->getWhereAllItem($kasubbag_posisi,'kasubbag_posisi','tbl_kasubbag');
		$cek_posisi 			= $this->web_app_model->get2WhereAllItem($kasubbag_posisi,'kasubbag_posisi',$kasubbag_uuid,'kasubbag_uuid','tbl_kasubbag');

		if($cek_posisi->num_rows() < 1)
		{
			header('location:'.base_url().'index.php/super_admin/bg_dekan?data_users=1/');
			$this->session->set_flashdata("info","<div class='alert alert-block alert-warning'>
													<button type='button' class='close' data-dismiss='alert'>
														<i class='icon-remove'></i>
													</button>
			
													<p>
														<strong>
															<i class='icon-ok'></i>
															Gagal! - 
														</strong>
														Data dengan posisi / username tersebut sudah digunakan!
													</p>
												</div>");

			$this->session->set_flashdata("info2","<script type='text/javascript'>
												     setTimeout(function () { 
														     swal({
														                title: 'Gagal!!',
														                text:  'Data dengan posisi / username tersebut sudah digunakan!',
														                type: 'warning',
														                timer: 3000,
														                showConfirmButton: true
														            });  
														     },10);  
														    </script>
														    ");
		}
		else
		{
			$this->web_app_model->updateDataWhere($where, $data,'tbl_kasubbag');
			header('location:'.base_url().'index.php/super_admin/bg_dekan?data_users=1/');
			$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
													<button type='button' class='close' data-dismiss='alert'>
														<i class='icon-remove'></i>
													</button>
			
													<p>
														<strong>
															<i class='icon-ok'></i>
															Success! - 
														</strong>
														Data Kasubbag (fakultas) berhasil diupdate!
													</p>
												</div>");

			$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
														     swal({
														                title: 'Success!!',
														                text:  'Data Kasubbag (fakultas) berhasil diupdate!',
														                type: 'success',
														                timer: 3000,
														                showConfirmButton: true
														            });  
														     },10);  
														    </script>
														    ");
		}
	}

	public function editPelaksanaFak()
	{
		//date_default_timezone_set('Asia/Jakarta');

		$staff_uuid				= $this->input->post('staff_uuid');
		$staff_username			= $this->input->post('staff_username');
		$staff_nama				= $this->input->post('staff_nama');
		$staff_posisi			= $this->input->post('staff_posisi');
		$staff_email			= $this->input->post('staff_email');

		$staff_uuid_kasubbag	= $this->input->post('staff_uuid_kasubbag');

		$data = array(		
			'staff_nama' 			=> $staff_nama,
			'staff_posisi' 			=> $staff_posisi,
			'staff_email' 			=> $staff_email,
			'staff_uuid_kasubbag' 	=> $staff_uuid_kasubbag,
			);

		$where = array(
			'staff_uuid' 		=> $staff_uuid,
			);

		
		
		//$cek_posisi 			= $this->web_app_model->getWhereAllItem($kasubbag_posisi,'kasubbag_posisi','tbl_kasubbag');
		$cek_posisi 			= $this->web_app_model->get2WhereAllItem($staff_posisi,'staff_posisi',$staff_uuid,'staff_uuid','tbl_staff');

		if($cek_posisi->num_rows() < 1)
		{
			header('location:'.base_url().'index.php/super_admin/bg_dekan?data_users=1/');
			$this->session->set_flashdata("info","<div class='alert alert-block alert-warning'>
													<button type='button' class='close' data-dismiss='alert'>
														<i class='icon-remove'></i>
													</button>
			
													<p>
														<strong>
															<i class='icon-ok'></i>
															Gagal! - 
														</strong>
														Data dengan posisi / username tersebut sudah digunakan!
													</p>
												</div>");

			$this->session->set_flashdata("info2","<script type='text/javascript'>
												     setTimeout(function () { 
														     swal({
														                title: 'Gagal!!',
														                text:  'Data dengan posisi / username tersebut sudah digunakan!',
														                type: 'warning',
														                timer: 3000,
														                showConfirmButton: true
														            });  
														     },10);  
														    </script>
														    ");
		}
		else
		{
			$this->web_app_model->updateDataWhere($where,$data,'tbl_staff');
			header('location:'.base_url().'index.php/super_admin/bg_dekan?data_users=1/');
			$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
													<button type='button' class='close' data-dismiss='alert'>
														<i class='icon-remove'></i>
													</button>
			
													<p>
														<strong>
															<i class='icon-ok'></i>
															Success! - 
														</strong>
														Data Pelaksana (fakultas) berhasil diupdate!
													</p>
												</div>");

			$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
														     swal({
														                title: 'Success!!',
														                text:  'Data Pelaksana (fakultas) berhasil diupdate!',
														                type: 'success',
														                timer: 3000,
														                showConfirmButton: true
														            });  
														     },10);  
														    </script>
														    ");
		}
	}

	public function editPelaksanaLpppm()
	{
		//date_default_timezone_set('Asia/Jakarta');

		$staff_uuid				= $this->input->post('staff_uuid');
		$staff_username			= $this->input->post('staff_username');
		$staff_nama				= $this->input->post('staff_nama');
		$staff_posisi			= $this->input->post('staff_posisi');
		$staff_email			= $this->input->post('staff_email');

		$staff_uuid_kasubbag	= $this->input->post('staff_uuid_kasubbag');

		$data = array(		
			'staff_nama' 			=> $staff_nama,
			'staff_posisi' 			=> $staff_posisi,
			'staff_email' 			=> $staff_email,
			'staff_uuid_kasubbag' 	=> $staff_uuid_kasubbag,
			);

		$where = array(
			'staff_uuid' 		=> $staff_uuid,
			);

		
		
			$this->web_app_model->updateDataWhere($where,$data,'tbl_staff');
			header('location:'.base_url().'index.php/super_admin/bg_lpppm?data_users=1/');
			$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
													<button type='button' class='close' data-dismiss='alert'>
														<i class='icon-remove'></i>
													</button>
			
													<p>
														<strong>
															<i class='icon-ok'></i>
															Success! - 
														</strong>
														Data Pelaksana LPPPM & PM berhasil diupdate!
													</p>
												</div>");

			$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
														     swal({
														                title: 'Success!!',
														                text:  'Data Pelaksana LPPPM & PM berhasil diupdate!',
														                type: 'success',
														                timer: 3000,
														                showConfirmButton: true
														            });  
														     },10);  
														    </script>
														    ");

	}

	public function editAdminSuratFak()
	{
		//date_default_timezone_set('Asia/Jakarta');

		//$staff_uuid				= $this->input->post('staff_uuid');
		$username			= $this->input->post('username');
		$nama_lengkap		= $this->input->post('nama_lengkap');
		$jabatan			= $this->input->post('posisi');
		$uuid_atasan		= $this->input->post('atasan');

		//$staff_uuid_kasubbag	= $this->input->post('staff_uuid_kasubbag');

		$data = array(		
			'nama_lengkap' 			=> $nama_lengkap,
			'jabatan' 				=> $jabatan,
			'uuid_atasan' 			=> $uuid_atasan,
			);

		$where = array(
			'username' 				=> $username,
			);
		
		//$cek_posisi 			= $this->web_app_model->getWhereAllItem($kasubbag_posisi,'kasubbag_posisi','tbl_kasubbag');
	//	$cek_posisi 			= $this->web_app_model->get2WhereAllItem($staff_posisi,'staff_posisi',$staff_uuid,'staff_uuid','tbl_staff');

		
			$this->web_app_model->updateDataWhere($where,$data,'tbl_users');
			header('location:'.base_url().'index.php/super_admin/bg_dekan?data_users=1/');
			$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
													<button type='button' class='close' data-dismiss='alert'>
														<i class='icon-remove'></i>
													</button>
			
													<p>
														<strong>
															<i class='icon-ok'></i>
															Success! - 
														</strong>
														Data Admin Surat (fakultas) berhasil diupdate!
													</p>
												</div>");

			$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
														     swal({
														                title: 'Success!!',
														                text:  'Data Admin Surat (fakultas) berhasil diupdate!',
														                type: 'success',
														                timer: 3000,
														                showConfirmButton: true
														            });  
														     },10);  
														    </script>
														    ");
		
	}

	public function editKasubbag()
	{
		date_default_timezone_set('Asia/Jakarta');

		$kasubbag_nama				= $this->input->post('nama_lengkap');
		$kasubbag_posisi			= $this->input->post('posisi');
		$kasubbag_uuid				= $this->input->post('kasubbag_uuid');
		$kasubbag_uuid_kabag		= $this->input->post('atasan');

		$data = array(		
			'kasubbag_nama' 		=> $kasubbag_nama,
			'kasubbag_posisi' 		=> $kasubbag_posisi,
			'kasubbag_uuid_kabag' 	=> $kasubbag_uuid_kabag,
			);

		$where = array(
			'kasubbag_uuid' 		=> $kasubbag_uuid,
			);

		
		$this->web_app_model->updateDataWhere($where,$data,'tbl_kasubbag');
		header('location:'.base_url().'index.php/super_admin/bg_biro?data_users=1/');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
												<button type='button' class='close' data-dismiss='alert'>
													<i class='icon-remove'></i>
												</button>
		
												<p>
													<strong>
														<i class='icon-ok'></i>
														Success! - 
													</strong>
													Data Kasubbag berhasil diupdate!
												</p>
											</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
														     swal({
														                title: 'Success!!',
														                text:  'Data Kasubbag berhasil diupdate!',
														                type: 'success',
														                timer: 3000,
														                showConfirmButton: true
														            });  
														     },10);  
														    </script>
														    ");			
	}

	public function editPelaksana()
	{
		date_default_timezone_set('Asia/Jakarta');

		$staff_nama					= $this->input->post('nama_lengkap');
		$staff_posisi				= $this->input->post('posisi');
		$staff_uuid					= $this->input->post('staff_uuid');
		$staff_uuid_kasubbag		= $this->input->post('atasan');

		$data = array(		
			'staff_nama' 			=> $staff_nama,
			'staff_posisi' 			=> $staff_posisi,
			'staff_uuid_kasubbag' 	=> $staff_uuid_kasubbag,
			);

		$where = array(
			'staff_uuid' 			=> $staff_uuid,
			);

		
		$this->web_app_model->updateDataWhere($where,$data,'tbl_staff');
		header('location:'.base_url().'index.php/super_admin/bg_biro?data_users=1/');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
												<button type='button' class='close' data-dismiss='alert'>
													<i class='icon-remove'></i>
												</button>
		
												<p>
													<strong>
														<i class='icon-ok'></i>
														Success! - 
													</strong>
													Data Pelaksana berhasil diupdate!
												</p>
											</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
														     swal({
														                title: 'Success!!',
														                text:  'Data Pelaksana berhasil diupdate!',
														                type: 'success',
														                timer: 3000,
														                showConfirmButton: true
														            });  
														     },10);  
														    </script>
														    ");			
	}

	public function editAnggotaSP()
	{
		date_default_timezone_set('Asia/Jakarta');

		$staff_nama					= $this->input->post('nama_lengkap');
		$staff_posisi				= $this->input->post('posisi');
		$staff_uuid					= $this->input->post('staff_uuid');
		$staff_uuid_kasubbag		= $this->input->post('atasan');

		$data = array(		
			'staff_nama' 			=> $staff_nama,
			'staff_posisi' 			=> $staff_posisi,
			'staff_uuid_kasubbag' 	=> $staff_uuid_kasubbag,
			);

		$where = array(
			'staff_uuid' 			=> $staff_uuid,
			);

		
		$this->web_app_model->updateDataWhere($where,$data,'tbl_staff');
		header('location:'.base_url().'index.php/super_admin/bg_sp?data_users=1/');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
												<button type='button' class='close' data-dismiss='alert'>
													<i class='icon-remove'></i>
												</button>
		
												<p>
													<strong>
														<i class='icon-ok'></i>
														Success! - 
													</strong>
													Data Anggota SP berhasil diupdate!
												</p>
											</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
														     swal({
														                title: 'Success!!',
														                text:  'Data Anggota SP berhasil diupdate!',
														                type: 'success',
														                timer: 3000,
														                showConfirmButton: true
														            });  
														     },10);  
														    </script>
														    ");			
	}

	public function editSekSP()
	{
		date_default_timezone_set('Asia/Jakarta');

		$kasubbag_nama					= $this->input->post('nama_lengkap');
		$kasubbag_posisi				= $this->input->post('posisi');
		$kasubbag_uuid					= $this->input->post('kasubbag_uuid');
		$kasubbag_uuid_kabag			= $this->input->post('atasan');

		$data = array(		
			'kasubbag_nama' 			=> $kasubbag_nama,
			'kasubbag_posisi' 			=> $kasubbag_posisi,
			'kasubbag_uuid_kabag' 		=> $kasubbag_uuid_kabag,
			);

		$where = array(
			'kasubbag_uuid' 			=> $kasubbag_uuid,
			);

		
		$this->web_app_model->updateDataWhere($where,$data,'tbl_kasubbag');
		header('location:'.base_url().'index.php/super_admin/bg_sp?data_users=1/');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
												<button type='button' class='close' data-dismiss='alert'>
													<i class='icon-remove'></i>
												</button>
		
												<p>
													<strong>
														<i class='icon-ok'></i>
														Success! - 
													</strong>
													Data Sekretaris SP berhasil diupdate!
												</p>
											</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
														     swal({
														                title: 'Success!!',
														                text:  'Data Sekretaris SP berhasil diupdate!',
														                type: 'success',
														                timer: 3000,
														                showConfirmButton: true
														            });  
														     },10);  
														    </script>
														    ");			
	}

	public function editKaSP()
	{
		date_default_timezone_set('Asia/Jakarta');

		$kabag_nama					= $this->input->post('nama_lengkap');
		$kabag_posisi				= $this->input->post('posisi');
		$kabag_uuid					= $this->input->post('kabag_uuid');
		$kabag_uuid_biro			= "0";

		$data = array(		
			'kabag_nama' 			=> $kabag_nama,
			'kabag_posisi' 			=> $kabag_posisi,
			'kabag_uuid_biro' 		=> $kabag_uuid_biro,
			);

		$where = array(
			'kabag_uuid' 			=> $kabag_uuid,
			);

		
		$this->web_app_model->updateDataWhere($where,$data,'tbl_kabag');
		header('location:'.base_url().'index.php/super_admin/bg_sp?data_users=1/');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
												<button type='button' class='close' data-dismiss='alert'>
													<i class='icon-remove'></i>
												</button>
		
												<p>
													<strong>
														<i class='icon-ok'></i>
														Success! - 
													</strong>
													Data Ketua SP berhasil diupdate!
												</p>
											</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
														     swal({
														                title: 'Success!!',
														                text:  'Data Ketua SP berhasil diupdate!',
														                type: 'success',
														                timer: 3000,
														                showConfirmButton: true
														            });  
														     },10);  
														    </script>
														    ");			
	}

	public function editKaUptTik()
	{
		date_default_timezone_set('Asia/Jakarta');

		$kasubbag_nama					= $this->input->post('nama_lengkap');
		$kasubbag_posisi				= $this->input->post('posisi');
		$kasubbag_uuid					= $this->input->post('kasubbag_uuid');
		$kasubbag_uuid_kabag			= "0";

		$data = array(		
			'kasubbag_nama' 			=> $kasubbag_nama,
			'kasubbag_posisi' 			=> $kasubbag_posisi,
			'kasubbag_uuid_kabag' 		=> $kasubbag_uuid_kabag,
			);

		$where = array(
			'kasubbag_uuid' 			=> $kasubbag_uuid,
			);

		
		$this->web_app_model->updateDataWhere($where,$data,'tbl_kasubbag');
		header('location:'.base_url().'index.php/super_admin/bg_upt_tik?data_users=1/');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
												<button type='button' class='close' data-dismiss='alert'>
													<i class='icon-remove'></i>
												</button>
		
												<p>
													<strong>
														<i class='icon-ok'></i>
														Success! - 
													</strong>
													Data Kepala UPT TIK berhasil diupdate!
												</p>
											</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
														     swal({
														                title: 'Success!!',
														                text:  'Data Kepala UPT TIK berhasil diupdate!',
														                type: 'success',
														                timer: 3000,
														                showConfirmButton: true
														            });  
														     },10);  
														    </script>
														    ");			
	}

	public function editAdminSurat()
	{
		date_default_timezone_set('Asia/Jakarta');

		$username 				= $this->input->post('username');
		$nama_lengkap			= $this->input->post('nama_lengkap');
		$jabatan				= $this->input->post('posisi');
		$status					= "admin surat";
		$uuid_atasan			= $this->input->post('atasan');

		$data = array(		
			'nama_lengkap' 		=> $nama_lengkap,
			'jabatan' 			=> $jabatan,
			'status' 			=> $status,
			'uuid_atasan' 		=> $uuid_atasan,
			);

		$where = array(		
			'username' 		=> $username,
			);

		$this->web_app_model->updateDataWhere($where,$data,'tbl_users');
		header('location:'.base_url().'index.php/super_admin/bg_biro?data_users=1/');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
												<button type='button' class='close' data-dismiss='alert'>
													<i class='icon-remove'></i>
												</button>
		
												<p>
													<strong>
														<i class='icon-ok'></i>
														Success! - 
													</strong>
													Data Admin Persuratan Biro berhasil diupdate!
												</p>
											</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
														     swal({
														                title: 'Success!!',
														                text:  'Data Admin Persuratan Biro berhasil diupdate!',
														                type: 'success',
														                timer: 3000,
														                showConfirmButton: true
														            });  
														     },10);  
														    </script>
														    ");			
	}

	public function editAdminSuratLabdas()
	{
		date_default_timezone_set('Asia/Jakarta');

		$username 				= $this->input->post('username');
		$nama_lengkap			= $this->input->post('nama_lengkap');
		$jabatan				= $this->input->post('posisi');
		$status					= "admin surat";
		$uuid_atasan			= $this->input->post('atasan');

		$data = array(		
			'nama_lengkap' 		=> $nama_lengkap,
			'jabatan' 			=> $jabatan,
			'status' 			=> $status,
			'uuid_atasan' 		=> $uuid_atasan,
			);

		$where = array(		
			'username' 		=> $username,
			);

		$this->web_app_model->updateDataWhere($where,$data,'tbl_users');
		header('location:'.base_url().'index.php/super_admin/bg_upt_labdas?data_users=1/');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
												<button type='button' class='close' data-dismiss='alert'>
													<i class='icon-remove'></i>
												</button>
		
												<p>
													<strong>
														<i class='icon-ok'></i>
														Success! - 
													</strong>
													Data Admin Persuratan Lab Dasar berhasil diupdate!
												</p>
											</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
														     swal({
														                title: 'Success!!',
														                text:  'Data Admin Persuratan Lab Dasar berhasil diupdate!',
														                type: 'success',
														                timer: 3000,
														                showConfirmButton: true
														            });  
														     },10);  
														    </script>
														    ");			
	}

	public function editAdminSuratLabBhs()
	{
		date_default_timezone_set('Asia/Jakarta');

		$username 				= $this->input->post('username');
		$nama_lengkap			= $this->input->post('nama_lengkap');
		$jabatan				= $this->input->post('posisi');
		$status					= "admin surat";
		$uuid_atasan			= $this->input->post('atasan');

		$data = array(		
			'nama_lengkap' 		=> $nama_lengkap,
			'jabatan' 			=> $jabatan,
			'status' 			=> $status,
			'uuid_atasan' 		=> $uuid_atasan,
			);

		$where = array(		
			'username' 		=> $username,
			);

		$this->web_app_model->updateDataWhere($where,$data,'tbl_users');
		header('location:'.base_url().'index.php/super_admin/bg_upt_lab_bhs?data_users=1/');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
												<button type='button' class='close' data-dismiss='alert'>
													<i class='icon-remove'></i>
												</button>
		
												<p>
													<strong>
														<i class='icon-ok'></i>
														Success! - 
													</strong>
													Data Admin Persuratan Lab Bahasa berhasil diupdate!
												</p>
											</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
														     swal({
														                title: 'Success!!',
														                text:  'Data Admin Persuratan Lab Bahasa berhasil diupdate!',
														                type: 'success',
														                timer: 3000,
														                showConfirmButton: true
														            });  
														     },10);  
														    </script>
														    ");			
	}

	public function editAdminSuratPerpus()
	{
		date_default_timezone_set('Asia/Jakarta');

		$username 				= $this->input->post('username');
		$nama_lengkap			= $this->input->post('nama_lengkap');
		$jabatan				= $this->input->post('posisi');
		$status					= "admin surat";
		$uuid_atasan			= $this->input->post('atasan');

		$data = array(		
			'nama_lengkap' 		=> $nama_lengkap,
			'jabatan' 			=> $jabatan,
			'status' 			=> $status,
			'uuid_atasan' 		=> $uuid_atasan,
			);

		$where = array(		
			'username' 		=> $username,
			);

		$this->web_app_model->updateDataWhere($where,$data,'tbl_users');
		header('location:'.base_url().'index.php/super_admin/bg_upt_perpus?data_users=1/');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
												<button type='button' class='close' data-dismiss='alert'>
													<i class='icon-remove'></i>
												</button>
		
												<p>
													<strong>
														<i class='icon-ok'></i>
														Success! - 
													</strong>
													Data Admin Persuratan UPT Perpustakaan berhasil diupdate!
												</p>
											</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
														     swal({
														                title: 'Success!!',
														                text:  'Data Admin Persuratan UPT Perpustakaan berhasil diupdate!',
														                type: 'success',
														                timer: 3000,
														                showConfirmButton: true
														            });  
														     },10);  
														    </script>
														    ");			
	}

	public function editAdminSuratSP()
	{
		date_default_timezone_set('Asia/Jakarta');

		$username 				= $this->input->post('username');
		$nama_lengkap			= $this->input->post('nama_lengkap');
		$jabatan				= $this->input->post('posisi');
		$status					= "admin surat";
		$uuid_atasan			= $this->input->post('atasan');

		$data = array(		
			'nama_lengkap' 		=> $nama_lengkap,
			'jabatan' 			=> $jabatan,
			'status' 			=> $status,
			'uuid_atasan' 		=> $uuid_atasan,
			);

		$where = array(		
			'username' 		=> $username,
			);

		$this->web_app_model->updateDataWhere($where,$data,'tbl_users');
		header('location:'.base_url().'index.php/super_admin/bg_sp?data_users=1/');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
												<button type='button' class='close' data-dismiss='alert'>
													<i class='icon-remove'></i>
												</button>
		
												<p>
													<strong>
														<i class='icon-ok'></i>
														Success! - 
													</strong>
													Data Admin Persuratan Satuan Pengawasan berhasil diupdate!
												</p>
											</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
														     swal({
														                title: 'Success!!',
														                text:  'Data Admin Persuratan Satuan Pengawasan berhasil diupdate!',
														                type: 'success',
														                timer: 3000,
														                showConfirmButton: true
														            });  
														     },10);  
														    </script>
														    ");			
	}

	public function editAdminSuratUptTik()
	{
		date_default_timezone_set('Asia/Jakarta');

		$username 				= $this->input->post('username');
		$nama_lengkap			= $this->input->post('nama_lengkap');
		$jabatan				= $this->input->post('posisi');
		$status					= "admin surat";
		$uuid_atasan			= $this->input->post('atasan');

		$data = array(		
			'nama_lengkap' 		=> $nama_lengkap,
			'jabatan' 			=> $jabatan,
			'status' 			=> $status,
			'uuid_atasan' 		=> $uuid_atasan,
			);

		$where = array(		
			'username' 		=> $username,
			);

		$this->web_app_model->updateDataWhere($where,$data,'tbl_users');
		header('location:'.base_url().'index.php/super_admin/bg_upt_tik?data_users=1/');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
												<button type='button' class='close' data-dismiss='alert'>
													<i class='icon-remove'></i>
												</button>
		
												<p>
													<strong>
														<i class='icon-ok'></i>
														Success! - 
													</strong>
													Data Admin Persuratan UPT TIK berhasil diupdate!
												</p>
											</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
														     swal({
														                title: 'Success!!',
														                text:  'Data Admin Persuratan UPT TIK berhasil diupdate!',
														                type: 'success',
														                timer: 3000,
														                showConfirmButton: true
														            });  
														     },10);  
														    </script>
														    ");			
	}

	public function editPelaksanaUptTik()
	{
		date_default_timezone_set('Asia/Jakarta');

		$staff_nama					= $this->input->post('nama_lengkap');
		$staff_posisi				= $this->input->post('posisi');
		$staff_uuid					= $this->input->post('staff_uuid');
		$staff_uuid_kasubbag		= $this->input->post('atasan');

		$data = array(		
			'staff_nama' 			=> $staff_nama,
			'staff_posisi' 			=> $staff_posisi,
			'staff_uuid_kasubbag' 	=> $staff_uuid_kasubbag,
			);

		$where = array(
			'staff_uuid' 			=> $staff_uuid,
			);

		
		$this->web_app_model->updateDataWhere($where,$data,'tbl_staff');
		header('location:'.base_url().'index.php/super_admin/bg_upt_tik?data_users=1/');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
												<button type='button' class='close' data-dismiss='alert'>
													<i class='icon-remove'></i>
												</button>
		
												<p>
													<strong>
														<i class='icon-ok'></i>
														Success! - 
													</strong>
													Data Pelaksana UPT TIK berhasil diupdate!
												</p>
											</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
														     swal({
														                title: 'Success!!',
														                text:  'Data Pelaksana UPT TIK berhasil diupdate!',
														                type: 'success',
														                timer: 3000,
														                showConfirmButton: true
														            });  
														     },10);  
														    </script>
														    ");			
	}

	public function editKaLabdas()
	{
		date_default_timezone_set('Asia/Jakarta');

		$kasubbag_nama					= $this->input->post('nama_lengkap');
		$kasubbag_posisi				= $this->input->post('posisi');
		$kasubbag_uuid					= $this->input->post('kasubbag_uuid');
		$kasubbag_uuid_kabag			= "0";

		$data = array(		
			'kasubbag_nama' 			=> $kasubbag_nama,
			'kasubbag_posisi' 			=> $kasubbag_posisi,
			'kasubbag_uuid_kabag' 		=> $kasubbag_uuid_kabag,
			);

		$where = array(
			'kasubbag_uuid' 			=> $kasubbag_uuid,
			);

		
		$this->web_app_model->updateDataWhere($where,$data,'tbl_kasubbag');
		header('location:'.base_url().'index.php/super_admin/bg_upt_labdas?data_users=1/');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
												<button type='button' class='close' data-dismiss='alert'>
													<i class='icon-remove'></i>
												</button>
		
												<p>
													<strong>
														<i class='icon-ok'></i>
														Success! - 
													</strong>
													Data Kepala UPT Lab Dasar berhasil diupdate!
												</p>
											</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
														     swal({
														                title: 'Success!!',
														                text:  'Data Kepala UPT Lab Dasar berhasil diupdate!',
														                type: 'success',
														                timer: 3000,
														                showConfirmButton: true
														            });  
														     },10);  
														    </script>
														    ");			
	}

	public function editPelaksanaLabdas()
	{
		date_default_timezone_set('Asia/Jakarta');

		$staff_nama					= $this->input->post('nama_lengkap');
		$staff_posisi				= $this->input->post('posisi');
		$staff_uuid					= $this->input->post('staff_uuid');
		$staff_uuid_kasubbag		= $this->input->post('atasan');

		$data = array(		
			'staff_nama' 			=> $staff_nama,
			'staff_posisi' 			=> $staff_posisi,
			'staff_uuid_kasubbag' 	=> $staff_uuid_kasubbag,
			);

		$where = array(
			'staff_uuid' 			=> $staff_uuid,
			);

		
		$this->web_app_model->updateDataWhere($where,$data,'tbl_staff');
		header('location:'.base_url().'index.php/super_admin/bg_upt_labdas?data_users=1/');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
												<button type='button' class='close' data-dismiss='alert'>
													<i class='icon-remove'></i>
												</button>
		
												<p>
													<strong>
														<i class='icon-ok'></i>
														Success! - 
													</strong>
													Data Pelaksana UPT Lab Dasar berhasil diupdate!
												</p>
											</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
														     swal({
														                title: 'Success!!',
														                text:  'Data Pelaksana UPT Lab Dasar berhasil diupdate!',
														                type: 'success',
														                timer: 3000,
														                showConfirmButton: true
														            });  
														     },10);  
														    </script>
														    ");			
	}

	public function editKaLabBhs()
	{
		date_default_timezone_set('Asia/Jakarta');

		$kasubbag_nama					= $this->input->post('nama_lengkap');
		$kasubbag_posisi				= $this->input->post('posisi');
		$kasubbag_uuid					= $this->input->post('kasubbag_uuid');
		$kasubbag_uuid_kabag			= "0";

		$data = array(		
			'kasubbag_nama' 			=> $kasubbag_nama,
			'kasubbag_posisi' 			=> $kasubbag_posisi,
			'kasubbag_uuid_kabag' 		=> $kasubbag_uuid_kabag,
			);

		$where = array(
			'kasubbag_uuid' 			=> $kasubbag_uuid,
			);

		
		$this->web_app_model->updateDataWhere($where,$data,'tbl_kasubbag');
		header('location:'.base_url().'index.php/super_admin/bg_upt_lab_bhs?data_users=1/');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
												<button type='button' class='close' data-dismiss='alert'>
													<i class='icon-remove'></i>
												</button>
		
												<p>
													<strong>
														<i class='icon-ok'></i>
														Success! - 
													</strong>
													Data Kepala UPT Lab Bahasa berhasil diupdate!
												</p>
											</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
														     swal({
														                title: 'Success!!',
														                text:  'Data Kepala UPT Lab Bahasa berhasil diupdate!',
														                type: 'success',
														                timer: 3000,
														                showConfirmButton: true
														            });  
														     },10);  
														    </script>
														    ");			
	}

	public function editKaPerpus()
	{
		date_default_timezone_set('Asia/Jakarta');

		$kasubbag_nama					= $this->input->post('nama_lengkap');
		$kasubbag_posisi				= $this->input->post('posisi');
		$kasubbag_uuid					= $this->input->post('kasubbag_uuid');
		$kasubbag_uuid_kabag			= "0";

		$data = array(		
			'kasubbag_nama' 			=> $kasubbag_nama,
			'kasubbag_posisi' 			=> $kasubbag_posisi,
			'kasubbag_uuid_kabag' 		=> $kasubbag_uuid_kabag,
			);

		$where = array(
			'kasubbag_uuid' 			=> $kasubbag_uuid,
			);

		
		$this->web_app_model->updateDataWhere($where,$data,'tbl_kasubbag');
		header('location:'.base_url().'index.php/super_admin/bg_upt_perpus?data_users=1/');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
												<button type='button' class='close' data-dismiss='alert'>
													<i class='icon-remove'></i>
												</button>
		
												<p>
													<strong>
														<i class='icon-ok'></i>
														Success! - 
													</strong>
													Data Kepala UPT Perpustakaan berhasil diupdate!
												</p>
											</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
														     swal({
														                title: 'Success!!',
														                text:  'Data Kepala UPT Perpustakaan berhasil diupdate!',
														                type: 'success',
														                timer: 3000,
														                showConfirmButton: true
														            });  
														     },10);  
														    </script>
														    ");			
	}

	public function editPelaksanaLabBhs()
	{
		date_default_timezone_set('Asia/Jakarta');

		$staff_nama					= $this->input->post('nama_lengkap');
		$staff_posisi				= $this->input->post('posisi');
		$staff_uuid					= $this->input->post('staff_uuid');
		$staff_uuid_kasubbag		= $this->input->post('atasan');

		$data = array(		
			'staff_nama' 			=> $staff_nama,
			'staff_posisi' 			=> $staff_posisi,
			'staff_uuid_kasubbag' 	=> $staff_uuid_kasubbag,
			);

		$where = array(
			'staff_uuid' 			=> $staff_uuid,
			);

		
		$this->web_app_model->updateDataWhere($where,$data,'tbl_staff');
		header('location:'.base_url().'index.php/super_admin/bg_upt_lab_bhs?data_users=1/');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
												<button type='button' class='close' data-dismiss='alert'>
													<i class='icon-remove'></i>
												</button>
		
												<p>
													<strong>
														<i class='icon-ok'></i>
														Success! - 
													</strong>
													Data Pelaksana UPT Lab Bahasa berhasil diupdate!
												</p>
											</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
														     swal({
														                title: 'Success!!',
														                text:  'Data Pelaksana UPT Lab Bahasa berhasil diupdate!',
														                type: 'success',
														                timer: 3000,
														                showConfirmButton: true
														            });  
														     },10);  
														    </script>
														    ");			
	}

	public function editPelaksanaPerpus()
	{
		date_default_timezone_set('Asia/Jakarta');

		$staff_nama					= $this->input->post('nama_lengkap');
		$staff_posisi				= $this->input->post('posisi');
		$staff_uuid					= $this->input->post('staff_uuid');
		$staff_uuid_kasubbag		= $this->input->post('atasan');

		$data = array(		
			'staff_nama' 			=> $staff_nama,
			'staff_posisi' 			=> $staff_posisi,
			'staff_uuid_kasubbag' 	=> $staff_uuid_kasubbag,
			);

		$where = array(
			'staff_uuid' 			=> $staff_uuid,
			);

		
		$this->web_app_model->updateDataWhere($where,$data,'tbl_staff');
		header('location:'.base_url().'index.php/super_admin/bg_upt_perpus?data_users=1/');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
												<button type='button' class='close' data-dismiss='alert'>
													<i class='icon-remove'></i>
												</button>
		
												<p>
													<strong>
														<i class='icon-ok'></i>
														Success! - 
													</strong>
													Data Pelaksana UPT Perpustakaan berhasil diupdate!
												</p>
											</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
														     swal({
														                title: 'Success!!',
														                text:  'Data Pelaksana UPT Perpustakaan berhasil diupdate!',
														                type: 'success',
														                timer: 3000,
														                showConfirmButton: true
														            });  
														     },10);  
														    </script>
														    ");			
	}


	// FUNGSI HAPUS DATAA ----------------------------

	public function hapus_dekan()
	{

		$id 			= $this->uri->segment(3);
		$hapus 			= array('biro_username'=>$id);
		$hapus_login 	= array('username'=>$id);


		$this->web_app_model->deleteData('tbl_biro',$hapus);
		$this->web_app_model->deleteData('tbl_login',$hapus_login);
		//$this->web_app_model->deleteData('tbl_dosen_wali',$hapus);
		header('location:'.base_url().'index.php/super_admin/bg_dekan?dt_users=1');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
									<button type='button' class='close' data-dismiss='alert'>
										<i class='icon-remove'></i>
									</button>

									<p>
										<strong>
											<i class='icon-ok'></i>
											Success! - 
										</strong>
										Data Dekan berhasil dihapus...!
									</p>
								</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
										     setTimeout(function () { 
										     swal({
										                title: 'Success!!',
										                text:  'Data Dekan berhasil dihapus!',
										                type: 'success',
										                timer: 3000,
										                showConfirmButton: true
										            });  
										     },10);  
										    </script>
										    ");
	}

	public function hapus_kabagProdi()
	{

		$id 			= $this->uri->segment(3);
		$hapus 			= array('kabag_username'=>$id);
		$hapus_login 	= array('username'=>$id);


		$this->web_app_model->deleteData('tbl_kabag',$hapus);
		$this->web_app_model->deleteData('tbl_login',$hapus_login);
		//$this->web_app_model->deleteData('tbl_dosen_wali',$hapus);
		header('location:'.base_url().'index.php/super_admin/bg_dekan?dt_users=1');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
									<button type='button' class='close' data-dismiss='alert'>
										<i class='icon-remove'></i>
									</button>

									<p>
										<strong>
											<i class='icon-ok'></i>
											Success! - 
										</strong>
										Data Kabag / Kaprodi berhasil dihapus...!
									</p>
								</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
										     setTimeout(function () { 
										     swal({
										                title: 'Success!!',
										                text:  'Data Kabag / Kaprodi berhasil dihapus!',
										                type: 'success',
										                timer: 3000,
										                showConfirmButton: true
										            });  
										     },10);  
										    </script>
										    ");
	}

	public function hapus_kasubbagFak()
	{

		$id 			= $this->uri->segment(3);
		$hapus 			= array('kasubbag_username'=>$id);
		$hapus_login 	= array('username'=>$id);


		$this->web_app_model->deleteData('tbl_kasubbag',$hapus);
		$this->web_app_model->deleteData('tbl_login',$hapus_login);
		//$this->web_app_model->deleteData('tbl_dosen_wali',$hapus);
		header('location:'.base_url().'index.php/super_admin/bg_dekan?dt_users=1');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
									<button type='button' class='close' data-dismiss='alert'>
										<i class='icon-remove'></i>
									</button>

									<p>
										<strong>
											<i class='icon-ok'></i>
											Success! - 
										</strong>
										Data Kasubbag berhasil dihapus...!
									</p>
								</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
										     setTimeout(function () { 
										     swal({
										                title: 'Success!!',
										                text:  'Data Kasubabg berhasil dihapus!',
										                type: 'success',
										                timer: 3000,
										                showConfirmButton: true
										            });  
										     },10);  
										    </script>
										    ");
	}

	public function hapus_rektor()
	{

		$id 			= $this->uri->segment(3);
		$hapus 			= array('rektor_username'=>$id);
		$hapus_login 	= array('username'=>$id);


		$this->web_app_model->deleteData('tbl_rektor',$hapus);
		$this->web_app_model->deleteData('tbl_login',$hapus_login);
		//$this->web_app_model->deleteData('tbl_dosen_wali',$hapus);
		header('location:'.base_url().'index.php/super_admin/bg_rektor?dt_users=1');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
									<button type='button' class='close' data-dismiss='alert'>
										<i class='icon-remove'></i>
									</button>

									<p>
										<strong>
											<i class='icon-ok'></i>
											Success! - 
										</strong>
										Data Rektor berhasil dihapus...!
									</p>
								</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
										     setTimeout(function () { 
										     swal({
										                title: 'Success!!',
										                text:  'Data Rektor berhasil dihapus!',
										                type: 'success',
										                timer: 3000,
										                showConfirmButton: true
										            });  
										     },10);  
										    </script>
										    ");
	}

	public function hapus_fakultas()
	{

		$id 			= $this->uri->segment(3);
		$hapus 			= array('fak_fakultas_no'=>$id);

		$this->web_app_model->deleteData('fak_fakultas',$hapus);
		//$this->web_app_model->deleteData('tbl_dosen_wali',$hapus);
		header('location:'.base_url().'index.php/super_admin/bg_fakultas?kd_fakultas=1');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
									<button type='button' class='close' data-dismiss='alert'>
										<i class='icon-remove'></i>
									</button>

									<p>
										<strong>
											<i class='icon-ok'></i>
											Success! - 
										</strong>
										Data Fakultas berhasil dihapus...!
									</p>
								</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
										     setTimeout(function () { 
										     swal({
										                title: 'Success!!',
										                text:  'Data Fakultas berhasil dihapus!',
										                type: 'success',
										                timer: 3000,
										                showConfirmButton: true
										            });  
										     },10);  
										    </script>
										    ");
	}

	public function hapus_biro()
	{

		$id 			= $this->uri->segment(3);
		$hapus 			= array('biro_username'=>$id);
		$hapus_login 	= array('username'=>$id);


		$this->web_app_model->deleteData('tbl_biro',$hapus);
		$this->web_app_model->deleteData('tbl_login',$hapus_login);
		//$this->web_app_model->deleteData('tbl_dosen_wali',$hapus);
		header('location:'.base_url().'index.php/super_admin/bg_biro?dt_users=1');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
									<button type='button' class='close' data-dismiss='alert'>
										<i class='icon-remove'></i>
									</button>

									<p>
										<strong>
											<i class='icon-ok'></i>
											Success! - 
										</strong>
										Data Biro berhasil dihapus...!
									</p>
								</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
										     setTimeout(function () { 
										     swal({
										                title: 'Success!!',
										                text:  'Data Biro berhasil dihapus!',
										                type: 'success',
										                timer: 3000,
										                showConfirmButton: true
										            });  
										     },10);  
										    </script>
										    ");
	}

	public function hapus_kabag()
	{

		$id 			= $this->uri->segment(3);
		$hapus 			= array('kabag_username'=>$id);
		$hapus_login 	= array('username'=>$id);


		$this->web_app_model->deleteData('tbl_kabag',$hapus);
		$this->web_app_model->deleteData('tbl_login',$hapus_login);
		//$this->web_app_model->deleteData('tbl_dosen_wali',$hapus);
		header('location:'.base_url().'index.php/super_admin/bg_biro?dt_users=1');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
									<button type='button' class='close' data-dismiss='alert'>
										<i class='icon-remove'></i>
									</button>

									<p>
										<strong>
											<i class='icon-ok'></i>
											Success! - 
										</strong>
										Data Kabag berhasil dihapus...!
									</p>
								</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
										     setTimeout(function () { 
										     swal({
										                title: 'Success!!',
										                text:  'Data Kabag berhasil dihapus!',
										                type: 'success',
										                timer: 3000,
										                showConfirmButton: true
										            });  
										     },10);  
										    </script>
										    ");
	}

	public function hapus_kasubbag()
	{

		$id 			= $this->uri->segment(3);
		$hapus 			= array('kasubbag_username'=>$id);
		$hapus_login 	= array('username'=>$id);


		$this->web_app_model->deleteData('tbl_kasubbag',$hapus);
		$this->web_app_model->deleteData('tbl_login',$hapus_login);
		//$this->web_app_model->deleteData('tbl_dosen_wali',$hapus);
		header('location:'.base_url().'index.php/super_admin/bg_biro?dt_users=1');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
									<button type='button' class='close' data-dismiss='alert'>
										<i class='icon-remove'></i>
									</button>

									<p>
										<strong>
											<i class='icon-ok'></i>
											Success! - 
										</strong>
										Data Kasubbag berhasil dihapus...!
									</p>
								</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
										     setTimeout(function () { 
										     swal({
										                title: 'Success!!',
										                text:  'Data Kasubbag berhasil dihapus!',
										                type: 'success',
										                timer: 3000,
										                showConfirmButton: true
										            });  
										     },10);  
										    </script>
										    ");
	}

	public function hapus_pelaksana()
	{

		$id 			= $this->uri->segment(3);
		$hapus 			= array('staff_username'=>$id);
		$hapus_login 	= array('username'=>$id);


		$this->web_app_model->deleteData('tbl_staff',$hapus);
		$this->web_app_model->deleteData('tbl_login',$hapus_login);
		//$this->web_app_model->deleteData('tbl_dosen_wali',$hapus);
		header('location:'.base_url().'index.php/super_admin/bg_biro?dt_users=1');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
									<button type='button' class='close' data-dismiss='alert'>
										<i class='icon-remove'></i>
									</button>

									<p>
										<strong>
											<i class='icon-ok'></i>
											Success! - 
										</strong>
										Data Staff berhasil dihapus...!
									</p>
								</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
										     setTimeout(function () { 
										     swal({
										                title: 'Success!!',
										                text:  'Data Staff berhasil dihapus!',
										                type: 'success',
										                timer: 3000,
										                showConfirmButton: true
										            });  
										     },10);  
										    </script>
										    ");
	}

	public function hapus_kasp()
	{

		$id 			= $this->uri->segment(3);
		$hapus 			= array('kabag_username'=>$id);
		$hapus_login 	= array('username'=>$id);


		$this->web_app_model->deleteData('tbl_kabag',$hapus);
		$this->web_app_model->deleteData('tbl_login',$hapus_login);
		//$this->web_app_model->deleteData('tbl_dosen_wali',$hapus);
		header('location:'.base_url().'index.php/super_admin/bg_sp?dt_users=1');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
									<button type='button' class='close' data-dismiss='alert'>
										<i class='icon-remove'></i>
									</button>

									<p>
										<strong>
											<i class='icon-ok'></i>
											Success! - 
										</strong>
										Data Ketua Satuan Pengawasan berhasil dihapus...!
									</p>
								</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
										     setTimeout(function () { 
										     swal({
										                title: 'Success!!',
										                text:  'Data Ketua Satuan Pengawasan berhasil dihapus!',
										                type: 'success',
										                timer: 3000,
										                showConfirmButton: true
										            });  
										     },10);  
										    </script>
										    ");
	}

	public function hapus_admin_surat()
	{

		$id 			= $this->uri->segment(3);
		$hapus 			= array('username'=>$id);
		$hapus_login 	= array('username'=>$id);


		$this->web_app_model->deleteData('tbl_users',$hapus);
		$this->web_app_model->deleteData('tbl_login',$hapus_login);
		//$this->web_app_model->deleteData('tbl_dosen_wali',$hapus);
		header('location:'.base_url().'index.php/super_admin/bg_biro?dt_users=1');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
									<button type='button' class='close' data-dismiss='alert'>
										<i class='icon-remove'></i>
									</button>

									<p>
										<strong>
											<i class='icon-ok'></i>
											Success! - 
										</strong>
										Data Admin Persuratan Biro berhasil dihapus...!
									</p>
								</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
										     setTimeout(function () { 
										     swal({
										                title: 'Success!!',
										                text:  'Data Admin Persuratan Biro berhasil dihapus!',
										                type: 'success',
										                timer: 3000,
										                showConfirmButton: true
										            });  
										     },10);  
										    </script>
										    ");
	}

	public function hapus_admin_surat_labBhs()
	{

		$id 			= $this->uri->segment(3);
		$hapus 			= array('username'=>$id);
		$hapus_login 	= array('username'=>$id);


		$this->web_app_model->deleteData('tbl_users',$hapus);
		$this->web_app_model->deleteData('tbl_login',$hapus_login);
		//$this->web_app_model->deleteData('tbl_dosen_wali',$hapus);
		header('location:'.base_url().'index.php/super_admin/bg_upt_lab_bhs?dt_users=1');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
									<button type='button' class='close' data-dismiss='alert'>
										<i class='icon-remove'></i>
									</button>

									<p>
										<strong>
											<i class='icon-ok'></i>
											Success! - 
										</strong>
										Data Admin Persuratan Lab Bahasa berhasil dihapus...!
									</p>
								</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
										     setTimeout(function () { 
										     swal({
										                title: 'Success!!',
										                text:  'Data Admin Persuratan Lab Bahasa berhasil dihapus!',
										                type: 'success',
										                timer: 3000,
										                showConfirmButton: true
										            });  
										     },10);  
										    </script>
										    ");
	}

	public function hapus_admin_surat_perpus()
	{

		$id 			= $this->uri->segment(3);
		$hapus 			= array('username'=>$id);
		$hapus_login 	= array('username'=>$id);


		$this->web_app_model->deleteData('tbl_users',$hapus);
		$this->web_app_model->deleteData('tbl_login',$hapus_login);
		//$this->web_app_model->deleteData('tbl_dosen_wali',$hapus);
		header('location:'.base_url().'index.php/super_admin/bg_upt_perpus?dt_users=1');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
									<button type='button' class='close' data-dismiss='alert'>
										<i class='icon-remove'></i>
									</button>

									<p>
										<strong>
											<i class='icon-ok'></i>
											Success! - 
										</strong>
										Data Admin Persuratan UPT Perpustakaan berhasil dihapus...!
									</p>
								</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
										     setTimeout(function () { 
										     swal({
										                title: 'Success!!',
										                text:  'Data Admin Persuratan UPT Perpustakaan berhasil dihapus!',
										                type: 'success',
										                timer: 3000,
										                showConfirmButton: true
										            });  
										     },10);  
										    </script>
										    ");
	}

	public function hapus_admin_surat_labdas()
	{

		$id 			= $this->uri->segment(3);
		$hapus 			= array('username'=>$id);
		$hapus_login 	= array('username'=>$id);


		$this->web_app_model->deleteData('tbl_users',$hapus);
		$this->web_app_model->deleteData('tbl_login',$hapus_login);
		//$this->web_app_model->deleteData('tbl_dosen_wali',$hapus);
		header('location:'.base_url().'index.php/super_admin/bg_upt_labdas?dt_users=1');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
									<button type='button' class='close' data-dismiss='alert'>
										<i class='icon-remove'></i>
									</button>

									<p>
										<strong>
											<i class='icon-ok'></i>
											Success! - 
										</strong>
										Data Admin Persuratan Lab Dasar berhasil dihapus...!
									</p>
								</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
										     setTimeout(function () { 
										     swal({
										                title: 'Success!!',
										                text:  'Data Admin Persuratan Lab Dasar berhasil dihapus!',
										                type: 'success',
										                timer: 3000,
										                showConfirmButton: true
										            });  
										     },10);  
										    </script>
										    ");
	}

	public function hapus_adminSuratUptTik()
	{

		$id 			= $this->uri->segment(3);
		$hapus 			= array('username'=>$id);
		$hapus_login 	= array('username'=>$id);


		$this->web_app_model->deleteData('tbl_users',$hapus);
		$this->web_app_model->deleteData('tbl_login',$hapus_login);
		//$this->web_app_model->deleteData('tbl_dosen_wali',$hapus);
		header('location:'.base_url().'index.php/super_admin/bg_upt_tik?dt_users=1');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
									<button type='button' class='close' data-dismiss='alert'>
										<i class='icon-remove'></i>
									</button>

									<p>
										<strong>
											<i class='icon-ok'></i>
											Success! - 
										</strong>
										Data Admin Persuratan UPT TIK berhasil dihapus...!
									</p>
								</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
										     setTimeout(function () { 
										     swal({
										                title: 'Success!!',
										                text:  'Data Admin Persuratan UPT TIK berhasil dihapus!',
										                type: 'success',
										                timer: 3000,
										                showConfirmButton: true
										            });  
										     },10);  
										    </script>
										    ");
	}

	public function hapus_admin_surat_SP()
	{

		$id 			= $this->uri->segment(3);
		$hapus 			= array('username'=>$id);
		$hapus_login 	= array('username'=>$id);


		$this->web_app_model->deleteData('tbl_users',$hapus);
		$this->web_app_model->deleteData('tbl_login',$hapus_login);
		//$this->web_app_model->deleteData('tbl_dosen_wali',$hapus);
		header('location:'.base_url().'index.php/super_admin/bg_sp?dt_users=1');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
									<button type='button' class='close' data-dismiss='alert'>
										<i class='icon-remove'></i>
									</button>

									<p>
										<strong>
											<i class='icon-ok'></i>
											Success! - 
										</strong>
										Data Admin Persuratan SP berhasil dihapus...!
									</p>
								</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
										     setTimeout(function () { 
										     swal({
										                title: 'Success!!',
										                text:  'Data Admin Persuratan SP berhasil dihapus!',
										                type: 'success',
										                timer: 3000,
										                showConfirmButton: true
										            });  
										     },10);  
										    </script>
										    ");
	}

	public function hapus_seksp()
	{

		$id 			= $this->uri->segment(3);
		$hapus 			= array('kasubbag_username'=>$id);
		$hapus_login 	= array('username'=>$id);


		$this->web_app_model->deleteData('tbl_kasubbag',$hapus);
		$this->web_app_model->deleteData('tbl_login',$hapus_login);
		//$this->web_app_model->deleteData('tbl_dosen_wali',$hapus);
		header('location:'.base_url().'index.php/super_admin/bg_sp?dt_users=1');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
									<button type='button' class='close' data-dismiss='alert'>
										<i class='icon-remove'></i>
									</button>

									<p>
										<strong>
											<i class='icon-ok'></i>
											Success! - 
										</strong>
										Data Sekretaris SP berhasil dihapus...!
									</p>
								</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
										     setTimeout(function () { 
										     swal({
										                title: 'Success!!',
										                text:  'Data Sekretaris SP berhasil dihapus!',
										                type: 'success',
										                timer: 3000,
										                showConfirmButton: true
										            });  
										     },10);  
										    </script>
										    ");
	}

	public function hapus_spi()
	{

		$id 			= $this->uri->segment(3);
		$hapus 			= array('staff_username'=>$id);
		$hapus_login 	= array('username'=>$id);


		$this->web_app_model->deleteData('tbl_staff',$hapus);
		$this->web_app_model->deleteData('tbl_login',$hapus_login);
		//$this->web_app_model->deleteData('tbl_dosen_wali',$hapus);
		header('location:'.base_url().'index.php/super_admin/bg_sp?dt_users=1');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
									<button type='button' class='close' data-dismiss='alert'>
										<i class='icon-remove'></i>
									</button>

									<p>
										<strong>
											<i class='icon-ok'></i>
											Success! - 
										</strong>
										Data Anggota SP berhasil dihapus...!
									</p>
								</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
										     setTimeout(function () { 
										     swal({
										                title: 'Success!!',
										                text:  'Data Anggota SP berhasil dihapus!',
										                type: 'success',
										                timer: 3000,
										                showConfirmButton: true
										            });  
										     },10);  
										    </script>
										    ");
	}

	public function hapus_kaUptTik()
	{

		$id 			= $this->uri->segment(3);
		$hapus 			= array('kasubbag_username'=>$id);
		$hapus_login 	= array('username'=>$id);


		$this->web_app_model->deleteData('tbl_kasubbag',$hapus);
		$this->web_app_model->deleteData('tbl_login',$hapus_login);
		//$this->web_app_model->deleteData('tbl_dosen_wali',$hapus);
		header('location:'.base_url().'index.php/super_admin/bg_upt_tik?dt_users=1');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
									<button type='button' class='close' data-dismiss='alert'>
										<i class='icon-remove'></i>
									</button>

									<p>
										<strong>
											<i class='icon-ok'></i>
											Success! - 
										</strong>
										Data Kepala UPT TIK berhasil dihapus...!
									</p>
								</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
										     setTimeout(function () { 
										     swal({
										                title: 'Success!!',
										                text:  'Data Kepala UPT TIK berhasil dihapus!',
										                type: 'success',
										                timer: 3000,
										                showConfirmButton: true
										            });  
										     },10);  
										    </script>
										    ");
	}

	public function hapus_pelaksanaUptTik()
	{

		$id 			= $this->uri->segment(3);
		$hapus 			= array('staff_username'=>$id);
		$hapus_login 	= array('username'=>$id);


		$this->web_app_model->deleteData('tbl_staff',$hapus);
		$this->web_app_model->deleteData('tbl_login',$hapus_login);
		//$this->web_app_model->deleteData('tbl_dosen_wali',$hapus);
		header('location:'.base_url().'index.php/super_admin/bg_upt_tik?dt_users=1');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
									<button type='button' class='close' data-dismiss='alert'>
										<i class='icon-remove'></i>
									</button>

									<p>
										<strong>
											<i class='icon-ok'></i>
											Success! - 
										</strong>
										Data Pelaksana UPT TIK berhasil dihapus...!
									</p>
								</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
										     setTimeout(function () { 
										     swal({
										                title: 'Success!!',
										                text:  'Data Pelaksana UPT TIK berhasil dihapus!',
										                type: 'success',
										                timer: 3000,
										                showConfirmButton: true
										            });  
										     },10);  
										    </script>
										    ");
	}

	public function hapus_kaLabdas()
	{

		$id 			= $this->uri->segment(3);
		$hapus 			= array('kasubbag_username'=>$id);
		$hapus_login 	= array('username'=>$id);


		$this->web_app_model->deleteData('tbl_kasubbag',$hapus);
		$this->web_app_model->deleteData('tbl_login',$hapus_login);
		//$this->web_app_model->deleteData('tbl_dosen_wali',$hapus);
		header('location:'.base_url().'index.php/super_admin/bg_upt_labdas?dt_users=1');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
									<button type='button' class='close' data-dismiss='alert'>
										<i class='icon-remove'></i>
									</button>

									<p>
										<strong>
											<i class='icon-ok'></i>
											Success! - 
										</strong>
										Data Kepala UPT Lab Dasar berhasil dihapus...!
									</p>
								</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
										     setTimeout(function () { 
										     swal({
										                title: 'Success!!',
										                text:  'Data Kepala UPT Lab Dasar berhasil dihapus!',
										                type: 'success',
										                timer: 3000,
										                showConfirmButton: true
										            });  
										     },10);  
										    </script>
										    ");
	}

	public function hapus_pelaksanaLabdas()
	{

		$id 			= $this->uri->segment(3);
		$hapus 			= array('staff_username'=>$id);
		$hapus_login 	= array('username'=>$id);


		$this->web_app_model->deleteData('tbl_staff',$hapus);
		$this->web_app_model->deleteData('tbl_login',$hapus_login);
		//$this->web_app_model->deleteData('tbl_dosen_wali',$hapus);
		header('location:'.base_url().'index.php/super_admin/bg_upt_labdas?dt_users=1');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
									<button type='button' class='close' data-dismiss='alert'>
										<i class='icon-remove'></i>
									</button>

									<p>
										<strong>
											<i class='icon-ok'></i>
											Success! - 
										</strong>
										Data Pelaksana UPT Lab Dasar berhasil dihapus...!
									</p>
								</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
										     setTimeout(function () { 
										     swal({
										                title: 'Success!!',
										                text:  'Data Pelaksana UPT Lab Dasar berhasil dihapus!',
										                type: 'success',
										                timer: 3000,
										                showConfirmButton: true
										            });  
										     },10);  
										    </script>
										    ");
	}

	public function hapus_kaLabBhs()
	{

		$id 			= $this->uri->segment(3);
		$hapus 			= array('kasubbag_username'=>$id);
		$hapus_login 	= array('username'=>$id);


		$this->web_app_model->deleteData('tbl_kasubbag',$hapus);
		$this->web_app_model->deleteData('tbl_login',$hapus_login);
		//$this->web_app_model->deleteData('tbl_dosen_wali',$hapus);
		header('location:'.base_url().'index.php/super_admin/bg_upt_lab_bhs?dt_users=1');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
									<button type='button' class='close' data-dismiss='alert'>
										<i class='icon-remove'></i>
									</button>

									<p>
										<strong>
											<i class='icon-ok'></i>
											Success! - 
										</strong>
										Data Kepala UPT Lab Bahasa berhasil dihapus...!
									</p>
								</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
										     setTimeout(function () { 
										     swal({
										                title: 'Success!!',
										                text:  'Data Kepala UPT Lab Bahasa berhasil dihapus!',
										                type: 'success',
										                timer: 3000,
										                showConfirmButton: true
										            });  
										     },10);  
										    </script>
										    ");
	}

	public function hapus_kaPerpus()
	{

		$id 			= $this->uri->segment(3);
		$hapus 			= array('kasubbag_username'=>$id);
		$hapus_login 	= array('username'=>$id);


		$this->web_app_model->deleteData('tbl_kasubbag',$hapus);
		$this->web_app_model->deleteData('tbl_login',$hapus_login);
		//$this->web_app_model->deleteData('tbl_dosen_wali',$hapus);
		header('location:'.base_url().'index.php/super_admin/bg_upt_perpus?dt_users=1');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
									<button type='button' class='close' data-dismiss='alert'>
										<i class='icon-remove'></i>
									</button>

									<p>
										<strong>
											<i class='icon-ok'></i>
											Success! - 
										</strong>
										Data Kepala UPT Perpustakaan berhasil dihapus...!
									</p>
								</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
										     setTimeout(function () { 
										     swal({
										                title: 'Success!!',
										                text:  'Data Kepala UPT Perpustakaan berhasil dihapus!',
										                type: 'success',
										                timer: 3000,
										                showConfirmButton: true
										            });  
										     },10);  
										    </script>
										    ");
	}

	public function hapus_pelaksanaLabBhs()
	{

		$id 			= $this->uri->segment(3);
		$hapus 			= array('staff_username'=>$id);
		$hapus_login 	= array('username'=>$id);


		$this->web_app_model->deleteData('tbl_staff',$hapus);
		$this->web_app_model->deleteData('tbl_login',$hapus_login);
		//$this->web_app_model->deleteData('tbl_dosen_wali',$hapus);
		header('location:'.base_url().'index.php/super_admin/bg_upt_labdas?dt_users=1');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
									<button type='button' class='close' data-dismiss='alert'>
										<i class='icon-remove'></i>
									</button>

									<p>
										<strong>
											<i class='icon-ok'></i>
											Success! - 
										</strong>
										Data Pelaksana UPT Lab Bahasa berhasil dihapus...!
									</p>
								</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
										     setTimeout(function () { 
										     swal({
										                title: 'Success!!',
										                text:  'Data Pelaksana UPT Lab Bahasa berhasil dihapus!',
										                type: 'success',
										                timer: 3000,
										                showConfirmButton: true
										            });  
										     },10);  
										    </script>
										    ");
	}

	public function hapus_pelaksanaPerpus()
	{

		$id 			= $this->uri->segment(3);
		$hapus 			= array('staff_username'=>$id);
		$hapus_login 	= array('username'=>$id);


		$this->web_app_model->deleteData('tbl_staff',$hapus);
		$this->web_app_model->deleteData('tbl_login',$hapus_login);
		//$this->web_app_model->deleteData('tbl_dosen_wali',$hapus);
		header('location:'.base_url().'index.php/super_admin/bg_upt_perpus?dt_users=1');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
									<button type='button' class='close' data-dismiss='alert'>
										<i class='icon-remove'></i>
									</button>

									<p>
										<strong>
											<i class='icon-ok'></i>
											Success! - 
										</strong>
										Data Pelaksana UPT Perpustakaan berhasil dihapus...!
									</p>
								</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
										     setTimeout(function () { 
										     swal({
										                title: 'Success!!',
										                text:  'Data Pelaksana UPT Perpustakaan berhasil dihapus!',
										                type: 'success',
										                timer: 3000,
										                showConfirmButton: true
										            });  
										     },10);  
										    </script>
										    ");
	}
}