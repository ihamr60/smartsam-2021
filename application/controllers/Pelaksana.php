<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pelaksana extends MY_Controller {

	/**
	 Created by Ilham Ramadhan S.Tr.kom
	 0853 6188 5100
	 ilhamr6000@gmail.com
	 */

	public function bg_home()
	{
		$cek  = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='pelaksana')
		{
			$bc['pelaksana_uuid'] 	= $this->session->userdata('uuid');
			$bc['posisi'] 			= $this->session->userdata('posisi');
			$bc['username'] 		= $this->session->userdata('username');
			$bc['nama'] 			= $this->session->userdata('nama');
			$bc['status'] 			= $this->session->userdata('stts');
			$bc['dashboard'] 		= $this->load->view('dashboard/dashboard1',$bc,true);
			$bc['script'] 			= $this->load->view('dashboard/script',$bc,true);
			$bc['atas'] 			= $this->load->view('panel/atas',$bc,true);
			$bc['menu'] 			= $this->load->view('panel/pelaksana/menu',$bc,true);
			$bc['bio'] 				= $this->load->view('panel/bio',$bc,true);	
			$this->load->view('panel/pelaksana/bg_home',$bc);
		}
		else
		{
			header('location:'.base_url().'index.php/web');
		}
	}

	public function bg_suratKeluar()
	{
		$cek  		= $this->session->userdata('logged_in');
		$stts 		= $this->session->userdata('stts');
		$username 	= $this->session->userdata('username');
		if(!empty($cek) && $stts=='pelaksana')
		{

			//$bc['data_suratmasuk'] 	= $this->web_app_model->get2JoinAllWhereGroup('riw_dist_uuid','dist_uuid','staff_username','riw_disposisi','tbl_riwayat_disposisi','tbl_distribusi_surat','tbl_staff','tbl_riwayat_disposisi','riw_disposisi',$username,'dist_uuid');
			$bc['suratKeluar']		= $this->web_app_model->getAllData('tbl_surat_keluar');

			$bc['data_hal']			= $this->web_app_model->getAllData('tbl_kode_hal');
			$bc['data_instansi']	= $this->web_app_model->getAllData('tbl_kode_instansi');
			$bc['modalUploadSurat'] = $this->load->view('panel/pelaksana/modalUploadSurat',$bc,true);

			$bc['pelaksana_uuid'] 	= $this->session->userdata('uuid');
			$bc['posisi'] 			= $this->session->userdata('posisi');
			$bc['username'] 		= $this->session->userdata('username');
			$bc['nama'] 			= $this->session->userdata('nama');
			$bc['status'] 			= $this->session->userdata('stts');
			$bc['atas'] 			= $this->load->view('panel/atas',$bc,true);
			$bc['menu'] 			= $this->load->view('panel/pelaksana/menu',$bc,true);
			$bc['bio'] 				= $this->load->view('panel/bio',$bc,true);	
			$this->load->view('panel/pelaksana/bg_suratKeluar',$bc);
		}
		else
		{
			header('location:'.base_url().'index.php/panel');
		}
	}

	public function bg_kode_bagian()
	{
		$cek  		= $this->session->userdata('logged_in');
		$stts 		= $this->session->userdata('stts');
		$username 	= $this->session->userdata('username');
		if(!empty($cek) && $stts=='pelaksana')
		{

			//$bc['data_suratmasuk'] 	= $this->web_app_model->get2JoinAllWhereGroup('riw_dist_uuid','dist_uuid','staff_username','riw_disposisi','tbl_riwayat_disposisi','tbl_distribusi_surat','tbl_staff','tbl_riwayat_disposisi','riw_disposisi',$username,'dist_uuid');
			//$bc['suratKeluar']		= $this->web_app_model->getAllData('tbl_surat_keluar');

		//	$bc['data_hal']			= $this->web_app_model->getAllData('tbl_kode_hal');
			$bc['data_instansi']	= $this->web_app_model->getAllData('tbl_kode_instansi');
			//$bc['modalUploadSurat'] = $this->load->view('panel/pelaksana/modalUploadSurat',$bc,true);

			$bc['pelaksana_uuid'] 	= $this->session->userdata('uuid');
			$bc['posisi'] 			= $this->session->userdata('posisi');
			$bc['username'] 		= $this->session->userdata('username');
			$bc['nama'] 			= $this->session->userdata('nama');
			$bc['status'] 			= $this->session->userdata('stts');
			$bc['atas'] 			= $this->load->view('panel/atas',$bc,true);
			$bc['menu'] 			= $this->load->view('panel/pelaksana/menu',$bc,true);
			$bc['bio'] 				= $this->load->view('panel/bio',$bc,true);	
			$this->load->view('panel/pelaksana/bg_kode_bagian',$bc);
		}
		else
		{
			header('location:'.base_url().'index.php/panel');
		}
	}

	public function bg_kode_hal()
	{
		$cek  		= $this->session->userdata('logged_in');
		$stts 		= $this->session->userdata('stts');
		$username 	= $this->session->userdata('username');
		if(!empty($cek) && $stts=='pelaksana')
		{

		//	$bc['data_hal']			= $this->web_app_model->getAllData('tbl_kode_hal');
			$bc['data_hal']			= $this->web_app_model->getAllData('tbl_kode_hal');
			//$bc['modalUploadSurat'] = $this->load->view('panel/pelaksana/modalUploadSurat',$bc,true);

			$bc['pelaksana_uuid'] 	= $this->session->userdata('uuid');
			$bc['posisi'] 			= $this->session->userdata('posisi');
			$bc['username'] 		= $this->session->userdata('username');
			$bc['nama'] 			= $this->session->userdata('nama');
			$bc['status'] 			= $this->session->userdata('stts');
			$bc['atas'] 			= $this->load->view('panel/atas',$bc,true);
			$bc['menu'] 			= $this->load->view('panel/pelaksana/menu',$bc,true);
			$bc['bio'] 				= $this->load->view('panel/bio',$bc,true);	
			$this->load->view('panel/pelaksana/bg_kode_hal',$bc);
		}
		else
		{
			header('location:'.base_url().'index.php/panel');
		}
	}
	
	public function bg_suratMasuk()
	{
		$cek  		= $this->session->userdata('logged_in');
		$stts 		= $this->session->userdata('stts');
		$username 	= $this->session->userdata('username');
		if(!empty($cek) && $stts=='pelaksana')
		{
			//$bc['data_rektor'] 		= $this->web_app_model->getAllData('tbl_rektor');
			//$bc['data_suratmasuk'] 	= $this->web_app_model->get2JoinAllWhereGroup('riw_dist_uuid','dist_uuid','biro_username','riw_disposisi','tbl_riwayat_disposisi','tbl_distribusi_surat','tbl_biro','tbl_riwayat_disposisi','dist_disposisi',$username,'dist_uuid');

			$bc['data_suratmasuk'] 	= $this->web_app_model->get2JoinAllWhereGroup('riw_dist_uuid','dist_uuid','staff_username','riw_disposisi','tbl_riwayat_disposisi','tbl_distribusi_surat','tbl_staff','tbl_riwayat_disposisi','riw_disposisi',$username,'dist_uuid');

			//$bc['modalTambahSurat'] = $this->load->view('panel/modalTambahSurat',$bc,true);

			$bc['pelaksana_uuid'] 	= $this->session->userdata('uuid');
			$bc['posisi'] 			= $this->session->userdata('posisi');
			$bc['username'] 		= $this->session->userdata('username');
			$bc['nama'] 			= $this->session->userdata('nama');
			$bc['status'] 			= $this->session->userdata('stts');
			$bc['atas'] 			= $this->load->view('panel/atas',$bc,true);
			$bc['menu'] 			= $this->load->view('panel/pelaksana/menu',$bc,true);
			$bc['bio'] 				= $this->load->view('panel/bio',$bc,true);	
			$this->load->view('panel/pelaksana/bg_suratMasuk',$bc);
		}
		else
		{
			header('location:'.base_url().'index.php/panel');
		}
	}

	public function bg_ganti_password()
	{
		$cek  = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='pelaksana')
		{
		
			$bc['username'] 		= $this->session->userdata('username');
			$bc['posisi'] 			= $this->session->userdata('posisi');
			$bc['nama'] 			= $this->session->userdata('nama');
			$bc['status'] 			= $this->session->userdata('stts');
			$bc['atas'] 			= $this->load->view('panel/atas',$bc,true);
			$bc['menu'] 			= $this->load->view('panel/pelaksana/menu',$bc,true);
			$bc['bio'] 				= $this->load->view('panel/bio',$bc,true);	
			$this->load->view('panel/pelaksana/bg_ganti_password',$bc);
		}
		else
		{
			header('location:'.base_url().'index.php/web');
		}
	}

	public function bg_mail_config()
	{
		$cek  = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='pelaksana')
		{
			
			$bc['email']			= $this->web_app_model->getWhereOneItem($this->session->userdata('username'),'staff_username','tbl_staff');


			$bc['username'] 		= $this->session->userdata('username');
			$bc['posisi'] 			= $this->session->userdata('posisi');
			$bc['nama'] 			= $this->session->userdata('nama');
			$bc['status'] 			= $this->session->userdata('stts');
			$bc['atas'] 			= $this->load->view('panel/atas',$bc,true);
			$bc['menu'] 			= $this->load->view('panel/pelaksana/menu',$bc,true);
			$bc['bio'] 				= $this->load->view('panel/bio',$bc,true);	
			$this->load->view('panel/pelaksana/bg_mail_config',$bc);
		}
		else
		{
			header('location:'.base_url().'index.php/web');
		}
	}

	public function update_email()
	{

		$username 					= $this->input->post('username');
		$email 						= $this->input->post('email');
		$staff_nama 				= $this->input->post('staff_nama');
			
		$where = array(		
		'staff_username' 			=> $username,
		);
	
		$data = array(		
			'staff_email' 			=> $email,
			'staff_nama' 			=> $staff_nama,
			);

		$this->web_app_model->updateDataWhere($where, $data,'tbl_staff');
		header('location:'.base_url().'index.php/pelaksana/bg_mail_config?settings=1/');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
												<button type='button' class='close' data-dismiss='alert'>
													<i class='icon-remove'></i>
												</button>
		
												<p>
													<strong>
														<i class='icon-ok'></i>
														Success! - 
													</strong>
													Email Berhasi di Update!
												</p>
											</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
											     swal({
											                title: 'Success!!',
											                text:  'Email berhasil diupdate!',
											                type: 'success',
											                timer: 3000,
											                showConfirmButton: true
											            });  
											     },10);  
											    </script>
											    ");
	}

	public function update_password()
	{

		$username 					= $this->input->post('username');
		$password_lama 				= $this->input->post('password_lama');
		$password_baru				= $this->input->post('password_baru');
		$password_baru_2			= $this->input->post('password_baru_2');

		$akun_lama					= $this->web_app_model->getWhereOneItem($username,'username','tbl_login');

		$password_lama_encrypt 		= md5($password_lama);

		if($username == $akun_lama['username'] && $password_lama_encrypt == $akun_lama['password'] && $password_baru == $password_baru_2)
		{
			$where = array(		
			'username' 				=> $username,
			);
		
			$data = array(		
				'password' 				=> md5($password_baru_2),
				);

			$this->web_app_model->updateDataWhere($where, $data,'tbl_login');
			header('location:'.base_url().'index.php/pelaksana/bg_ganti_password?settings=1/');
			$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
													<button type='button' class='close' data-dismiss='alert'>
														<i class='icon-remove'></i>
													</button>
			
													<p>
														<strong>
															<i class='icon-ok'></i>
															Success! - 
														</strong>
														Password Berhasi di Update!
													</p>
												</div>");

			$this->session->set_flashdata("info2","<script type='text/javascript'>
												     setTimeout(function () { 
												     swal({
												                title: 'Success!!',
												                text:  'Password berhasil diupdate!',
												                type: 'success',
												                timer: 3000,
												                showConfirmButton: true
												            });  
												     },10);  
												    </script>
												    ");
		}
		else
		{
			header('location:'.base_url().'index.php/pelaksana/bg_ganti_password?settings=1/');
			$this->session->set_flashdata("info","<div class='alert alert-block alert-danger'>
													<button type='button' class='close' data-dismiss='alert'>
														<i class='icon-remove'></i>
													</button>
			
													<p>
														<strong>
															<i class='icon-ok'></i>
															Gagal! - 
														</strong>
														Periksa kembali!
													</p>
												</div>");

			$this->session->set_flashdata("info2","<script type='text/javascript'>
												     setTimeout(function () { 
												     swal({
												                title: 'Gagal!!',
												                text:  'Periksa Kembali!',
												                type: 'warning',
												                timer: 3000,
												                showConfirmButton: true
												            });  
												     },10);  
												    </script>
												    ");
		}
	}

	public function bg_detail_surat()
	{
		$cek  		= $this->session->userdata('logged_in');
		$stts 		= $this->session->userdata('stts');
		$username 	= $this->session->userdata('username');
		if(!empty($cek) && $stts=='pelaksana')
		{
			//$bc['data_rektor'] 		= $this->web_app_model->getAllData('tbl_rektor');
			//$bc['data_suratmasuk'] 	= $this->web_app_model->get3JoinOneWhere('dist_disposisi','biro_username','riw_dist_uuid','dist_uuid','rektor_username','riw_asal','tbl_distribusi_surat','tbl_biro','tbl_riwayat_disposisi','tbl_distribusi_surat','tbl_rektor','tbl_riwayat_disposisi','dist_uuid',$this->uri->segment(3));

			$bc['data_suratmasuk'] 	= $this->web_app_model->getJoinOneWhere('riw_dist_uuid','dist_uuid','tbl_riwayat_disposisi','tbl_distribusi_surat','dist_uuid',$this->uri->segment(3));
			
			$bc['riwayat_memo'] 	= $this->web_app_model->getJoinAllWhere('riw_dist_uuid','dist_uuid','tbl_riwayat_disposisi','tbl_distribusi_surat','riw_dist_uuid',$this->uri->segment(3),'riw_disposisi',$username);

			$bc['data_staff']	= $this->web_app_model->getAllData('tbl_staff');

			//$bc['modalTambahSurat'] = $this->load->view('panel/modalTambahSurat',$bc,true);

			$bc['pelaksana_uuid'] 	= $this->session->userdata('uuid');
			$bc['username'] 		= $this->session->userdata('username');
			$bc['posisi'] 			= $this->session->userdata('posisi');
			$bc['nama'] 			= $this->session->userdata('nama');
			$bc['status'] 			= $this->session->userdata('stts');
			$bc['atas'] 			= $this->load->view('panel/atas',$bc,true);
			$bc['menu'] 			= $this->load->view('panel/pelaksana/menu',$bc,true);
			$bc['bio'] 				= $this->load->view('panel/bio',$bc,true);	
			$this->load->view('panel/pelaksana/bg_detail_surat',$bc);
		}
		else
		{
			header('location:'.base_url().'index.php/panel');
		}
	}

	public function bg_detail_draft()
	{
		$cek  		= $this->session->userdata('logged_in');
		$stts 		= $this->session->userdata('stts');
		$username 	= $this->session->userdata('username');
		if(!empty($cek) && $stts=='pelaksana')
		{
			//$bc['data_rektor'] 		= $this->web_app_model->getAllData('tbl_rektor');
			//$bc['data_suratmasuk'] 	= $this->web_app_model->get3JoinOneWhere('dist_disposisi','biro_username','riw_dist_uuid','dist_uuid','rektor_username','riw_asal','tbl_distribusi_surat','tbl_biro','tbl_riwayat_disposisi','tbl_distribusi_surat','tbl_rektor','tbl_riwayat_disposisi','dist_uuid',$this->uri->segment(3));

			$bc['data_draft'] 	= $this->web_app_model->getJoinOneWhere('konsep_uuid_draft','draft_uuid','tbl_konsep_draft','tbl_draft_surat','draft_uuid',$this->uri->segment(3));
			
			$bc['riwayat_konsep'] 	= $this->web_app_model->getJoinAllWhere('konsep_uuid_draft','draft_uuid','tbl_konsep_draft','tbl_draft_surat','konsep_uuid_draft',$this->uri->segment(3),'konsep_tujuan',$username);

			$bc['data_staff']	= $this->web_app_model->getAllData('tbl_staff');

			//$bc['modalTambahSurat'] = $this->load->view('panel/modalTambahSurat',$bc,true);

			$bc['pelaksana_uuid'] 	= $this->session->userdata('uuid');
			$bc['username'] 		= $this->session->userdata('username');
			$bc['posisi'] 			= $this->session->userdata('posisi');
			$bc['nama'] 			= $this->session->userdata('nama');
			$bc['status'] 			= $this->session->userdata('stts');
			$bc['atas'] 			= $this->load->view('panel/atas',$bc,true);
			$bc['menu'] 			= $this->load->view('panel/pelaksana/menu',$bc,true);
			$bc['bio'] 				= $this->load->view('panel/bio',$bc,true);	
			$this->load->view('panel/pelaksana/bg_detail_draft',$bc);
		}
		else
		{
			header('location:'.base_url().'index.php/panel');
		}
	}

	public function add_surat_keluar()
	{
		

		$out_uuid					= $this->get_id();
		$out_kode_hal				= $this->input->post('kode_hal');
		$out_kode_instansi			= $this->input->post('kode_instansi');
		$out_tujuan					= $this->input->post('tujuan');
		$out_lampiran				= $this->input->post('lampiran');
		$out_perihal				= $this->input->post('perihal');
		$out_penerbit 				= $this->session->userdata('username');
		$out_status 				= "Menunggu Upload";

		$urutMax = $this->db->query("SELECT MAX(out_urut) AS ID FROM tbl_surat_keluar");
		$row     = $urutMax->row_array();
		//$nomor	 = "/".$bulan."/".$tahun;
		
		if($row['ID']=="")
		{
			$ID = "001";
		}
		else
		{
			$MaksID = $row['ID'];
			$MaksID++;
			if($MaksID < 10) $ID = "00".$MaksID;
			else if($MaksID < 100) $ID = "0".$MaksID;
			else $ID = $MaksID;
		}

		$data = array(		
			'out_uuid' 			=> $out_uuid,
			'out_urut' 			=> $ID,
			'out_kode_hal' 		=> $out_kode_hal,
			'out_tujuan' 		=> $out_tujuan,
			'out_kode_instansi' => $out_kode_instansi,
			'out_lampiran' 		=> $out_lampiran,
			'out_perihal' 		=> $out_perihal,
			'out_penerbit' 		=> $out_penerbit,
			'out_status' 		=> $out_status,

			);

		$this->web_app_model->insertData($data,'tbl_surat_keluar');
		header('location:'.base_url().'index.php/pelaksana/bg_suratKeluar?suratKeluar=1/');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
												<button type='button' class='close' data-dismiss='alert'>
													<i class='icon-remove'></i>
												</button>
		
												<p>
													<strong>
														<i class='icon-ok'></i>
														Success! - 
													</strong>
													Data Surat Keluar berhasil ditambahkan!
												</p>
											</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
											     swal({
											                title: 'Success!!',
											                text:  'Data Surat Keluar berhasil ditambahkan!',
											                type: 'success',
											                timer: 3000,
											                showConfirmButton: true
											            });  
											     },10);  
											    </script>
											    ");
	}

	public function uploadSurat()
	{
		date_default_timezone_set('Asia/Jakarta');

		$out_uuid				= $this->input->post('out_uuid');


		// get foto
		 $config['upload_path'] 	= './upload/suratKeluar';
		 $config['allowed_types'] 	= 'jpg|pdf|jpeg';
		 $config['max_size'] 		= '5000';  //5MB max
		 //$config['max_width'] 	= '4480'; // pixel
		 //$config['max_height'] 	= '4480'; // pixel
		 $config['overwrite']		= true;
	     $config['file_name'] 		= $out_uuid;

     	 $this->load->library('upload', $config);

			if(!empty($_FILES['surat']['name'])) 
			{
		        if ( $this->upload->do_upload('surat') ) 
		        {
		            $foto = $this->upload->data();		

					$data = array(		
						
						'out_status' 		=> "Done",
						'out_dokumen' 		=> $foto['file_name'],
						);

					$where = array(		
						'out_uuid' 			=> $out_uuid,
						);
		
					$this->web_app_model->updateDataWhere($where,$data,'tbl_surat_keluar');
					header('location:'.base_url().'index.php/pelaksana/bg_suratKeluar?suratKeluar=1/');
					$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
															<button type='button' class='close' data-dismiss='alert'>
																<i class='icon-remove'></i>
															</button>
					
															<p>
																<strong>
																	<i class='icon-ok'></i>
																	Success! - 
																</strong>
																Dokumen surat keluar berhasil diupload!
															</p>
														</div>");

					$this->session->set_flashdata("info2","<script type='text/javascript'>
														     setTimeout(function () { 
														     swal({
														                title: 'Success!!',
														                text:  'Dokumen Surat Keluar berhasil diupload!',
														                type: 'success',
														                timer: 3000,
														                showConfirmButton: true
														            });  
														     },10);  
														    </script>
														    ");
				}
				else 
				{
		          	header('location:'.base_url().'index.php/pelaksana/bg_suratKeluar?suratKeluar=1/');
					$this->session->set_flashdata("info2","<script type='text/javascript'>
										     setTimeout(function () { 
										     swal({
										                title: 'Maaf!',
										                text:  'Data gagal tersimpan, pastikan dokumen max 5 Mb atau UPT TIK',
										                type: 'warning',
										                timer: 300000,
										                showConfirmButton: true
										            });  
										     },10);  
										    </script>");
		    	}
		    }
		    else 
			{
	          	header('location:'.base_url().'index.php/pelaksana/bg_suratKeluar?suratKeluar=1/');
				$this->session->set_flashdata("info2","<script type='text/javascript'>
									     setTimeout(function () { 
									     swal({
									                title: 'Dokumen kosong!',
									                text:  'Mohon lampirkan dokumen',
									                type: 'warning',
									                timer: 300000,
									                showConfirmButton: true
									            });  
									     },10);  
									    </script>");
		    }
	}

	public function aksi_disposisi()
	{
		$username 					= $this->session->userdata('username');

		$riw_uuid					= $this->get_id();
		$dist_uuid					= $this->input->post('dist_uuid');
		$disposisi_kepada			= $this->input->post('disposisi_kepada');
		$riw_memo					= $this->input->post('memo');

		$data = array(		
			'riw_uuid' 			=> $riw_uuid,
			'riw_dist_uuid' 	=> $dist_uuid,
			'riw_asal' 			=> $username,
			'riw_disposisi' 	=> $disposisi_kepada,
			'riw_memo' 			=> $riw_memo,
			);

		$this->web_app_model->insertData($data,'tbl_riwayat_disposisi');
		header('location:'.base_url().'index.php/pelaksana/bg_suratMasuk?suratMasuk=1/');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
												<button type='button' class='close' data-dismiss='alert'>
													<i class='icon-remove'></i>
												</button>
		
												<p>
													<strong>
														<i class='icon-ok'></i>
														Success! - 
													</strong>
													Data Surat Masuk berhasil ditambahkan!
												</p>
											</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
											     swal({
											                title: 'Success!!',
											                text:  'Data Surat Masuk berhasil ditambahkan!',
											                type: 'success',
											                timer: 3000,
											                showConfirmButton: true
											            });  
											     },10);  
											    </script>
											    ");
	}

	public function add_kode_bagian()
	{
		$instansi_kode				= $this->input->post('kode');
		$instansi_nama				= $this->input->post('bagian');

		$data = array(		
			'instansi_kode' 		=> $instansi_kode,
			'instansi_nama' 		=> $instansi_nama,
			);

		$this->web_app_model->insertData($data,'tbl_kode_instansi');
		header('location:'.base_url().'index.php/pelaksana/bg_kode_bagian?kd_bagian=1/');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
												<button type='button' class='close' data-dismiss='alert'>
													<i class='icon-remove'></i>
												</button>
		
												<p>
													<strong>
														<i class='icon-ok'></i>
														Success! - 
													</strong>
													Kode Bagian berhasil ditambahkan!
												</p>
											</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
											     swal({
											                title: 'Success!!',
											                text:  'Kode Bagian berhasil ditambahkan!',
											                type: 'success',
											                timer: 3000,
											                showConfirmButton: true
											            });  
											     },10);  
											    </script>
											    ");
	}

	public function add_kode_hal()
	{
		$hal_kode				= $this->input->post('kode');
		$hal_nama				= $this->input->post('hal');

		$data = array(		
			'hal_kode' 			=> $hal_kode,
			'hal_nama' 			=> $hal_nama,
			);

		$this->web_app_model->insertData($data,'tbl_kode_hal');
		header('location:'.base_url().'index.php/pelaksana/bg_kode_hal?kd_hal=1/');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
												<button type='button' class='close' data-dismiss='alert'>
													<i class='icon-remove'></i>
												</button>
		
												<p>
													<strong>
														<i class='icon-ok'></i>
														Success! - 
													</strong>
													Kode Hal berhasil ditambahkan!
												</p>
											</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
											     swal({
											                title: 'Success!!',
											                text:  'Kode Hal berhasil ditambahkan!',
											                type: 'success',
											                timer: 3000,
											                showConfirmButton: true
											            });  
											     },10);  
											    </script>
											    ");
	}

	public function hapus_kode_bagian()
	{

		$id 			= $this->uri->segment(3);
		$hapus 			= array('instansi_no'=>$id);


		$this->web_app_model->deleteData('tbl_kode_instansi',$hapus);
		//$this->web_app_model->deleteData('tbl_dosen_wali',$hapus);
		header('location:'.base_url().'index.php/pelaksana/bg_kode_bagian?kd_bagian=1');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
									<button type='button' class='close' data-dismiss='alert'>
										<i class='icon-remove'></i>
									</button>

									<p>
										<strong>
											<i class='icon-ok'></i>
											Success! - 
										</strong>
										Kode Bagian berhasil dihapus...!
									</p>
								</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
										     setTimeout(function () { 
										     swal({
										                title: 'Success!!',
										                text:  'Kode Bagian berhasil dihapus!',
										                type: 'success',
										                timer: 3000,
										                showConfirmButton: true
										            });  
										     },10);  
										    </script>
										    ");
	}

	public function hapus_kode_hal()
	{

		$id 			= $this->uri->segment(3);
		$hapus 			= array('hal_no'=>$id);


		$this->web_app_model->deleteData('tbl_kode_hal',$hapus);
		//$this->web_app_model->deleteData('tbl_dosen_wali',$hapus);
		header('location:'.base_url().'index.php/pelaksana/bg_kode_hal?kd_hal=1');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
									<button type='button' class='close' data-dismiss='alert'>
										<i class='icon-remove'></i>
									</button>

									<p>
										<strong>
											<i class='icon-ok'></i>
											Success! - 
										</strong>
										Kode Hal berhasil dihapus...!
									</p>
								</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
										     setTimeout(function () { 
										     swal({
										                title: 'Success!!',
										                text:  'Kode Hal berhasil dihapus!',
										                type: 'success',
										                timer: 3000,
										                showConfirmButton: true
										            });  
										     },10);  
										    </script>
										    ");
	}

	public function add_draft()
	{
		$draft_asal 				= $this->session->userdata('username');

		$draft_uuid					= $this->get_id();
		$draft_penerima				= $this->session->userdata('id_atasan');
		$draft_perihal				= $this->input->post('perihal');
		$draft_konsep				= $this->input->post('konsep');
		$draft_tujuan_surat			= $this->input->post('tujuan_surat');
		$draft_google_doc			= $this->input->post('draft_google_doc');
		$draft_status				= "Process";


		$konsep_uuid				= $this->get_id();

		$data = array(		
			'draft_uuid' 		=> $draft_uuid,
			'draft_asal' 		=> $draft_asal,
			'draft_penerima' 	=> $draft_penerima,
			'draft_perihal' 	=> $draft_perihal,
			'draft_status' 		=> $draft_status,
			'draft_tujuan_surat'=> $draft_tujuan_surat,
			'draft_google_doc'	=> $draft_google_doc,
			);

		$data2 = array(		
			'konsep_uuid' 		=> $konsep_uuid,
			'konsep_uuid_draft' => $draft_uuid,
			'konsep_asal' 		=> $draft_asal,
			'konsep_tujuan' 	=> $draft_penerima,
			'konsep_isi' 		=> $draft_konsep,
			'konsep_author' 	=> $draft_asal,
			);

		$this->web_app_model->insertData($data,'tbl_draft_surat');
		$this->web_app_model->insertData($data2,'tbl_konsep_draft');
		header('location:'.base_url().'index.php/pelaksana/bg_draft_pending?dt_peg=1/');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
												<button type='button' class='close' data-dismiss='alert'>
													<i class='icon-remove'></i>
												</button>
		
												<p>
													<strong>
														<i class='icon-ok'></i>
														Success! - 
													</strong>
													Draft Surat Berhasil Diajukan!
												</p>
											</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
											     swal({
											                title: 'Success!!',
											                text:  'Draft berhasil diajukan!',
											                type: 'success',
											                timer: 3000,
											                showConfirmButton: true
											            });  
											     },10);  
											    </script>
											    ");


		// BATAS NOTIF

		$penerima 				= $this->web_app_model->getWhereOneItem($draft_penerima,'kasubbag_uuid','tbl_kasubbag');
		$identitas_surat		= $this->web_app_model->getWhereOneItem($draft_uuid,'draft_uuid','tbl_draft_surat');
		$nama_penerus_surat 	= $this->session->userdata('nama');
		$posisi_penerus_surat 	= $this->session->userdata('posisi');

		$time 	= date('l, d F Y | H:i');

		$TOKEN  = $this->get_token_bot_telegram();  // ganti token ini dengan token bot mu
		$chatid = $this->get_id_group_telegram(); // ini id saya di telegram @hasanudinhs silakan diganti dan disesuaikan
		$pesan 	= "<b>DRAFT SURAT KELUAR (To: ".$penerima['kasubbag_nama'].")</b>\n\nDitujukan kepada <b>".$identitas_surat['draft_tujuan_surat']."</b>.\n\nPerihal terkait <b>".$identitas_surat['draft_perihal']."</b> telah ditulis oleh <b>".$nama_penerus_surat." (".$posisi_penerus_surat.")</b> kepada <b>".$penerima['kasubbag_nama']." (".$penerima['kasubbag_posisi'].")</b>\n\n<b>Status:</b>\nMenunggu tinjut Sdr/i. ".$penerima['kasubbag_nama']." (".$penerima['kasubbag_posisi'].")\n\nRegards,\nsmartsam_bot assistant-";

		// ----------- code -------------

		$method	= "sendMessage";
		$url    = "https://api.telegram.org/bot" . $TOKEN . "/". $method;
		$post = [
		 'chat_id' => $chatid,
		  'parse_mode' => 'HTML', // aktifkan ini jika ingin menggunakan format type HTML, bisa juga diganti menjadi Markdown
		 'text' => $pesan
		];

		$header = [
		 "X-Requested-With: XMLHttpRequest",
		 "User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.84 Safari/537.36" 
		];

		// hapus 1 baris ini:
		//die('Hapus baris ini sebelum bisa berjalan, terimakasih.');


		$ch = curl_init();
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_URL, $url);
		//curl_setopt($ch, CURLOPT_REFERER, $refer);
		//curl_setopt($ch, CURLOPT_VERBOSE, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $post );   
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		$datas = curl_exec($ch);
		$error = curl_error($ch);
		$status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		curl_close($ch);

		$debug['text'] = $pesan;
		$debug['code'] = $status;
		$debug['status'] = $error;
		$debug['respon'] = json_decode($datas, true);


		// NOTIFIKASI BY EMAIL

		$perihal 			= $identitas_surat['draft_perihal'];
		
		$nama_penerima 		= $penerima['kasubbag_nama'];

		$posisi_penerima	= $penerima['kasubbag_posisi'];

		$email_penerima 	= $penerima['kasubbag_email'];

		require("vendor/PHPMailer-master/src/PHPMailer.php");
		 require("vendor/PHPMailer-master/src/SMTP.php");
		 require("vendor/PHPMailer-master/src/Exception.php");
		 require("vendor/PHPMailer-master/src/OAuth.php");
		    
		$message = '
		    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
		<html xmlns="http://www.w3.org/1999/xhtml">
		<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Untitled Document</title>
		</head>

		<body style="font-family:Verdana, Geneva, sans-serif;font-size:12px;">
		<table width="100%" cellspacing="0" cellpadding="0" align="center" style="padding:20px;border:dashed 1px #333;"><tr><td>
		PENGAJUAN DRAFT SURAT KELUAR :    <br><br>
		        <div style="float:left; width:150px; margin-bottom:5px;">Perihal  :</div>
		        <div style="float:left;"><strong>'.$perihal.'</strong></div>
		        <div style="clear:both"></div>
		        <div style="float:left; width:150px; margin-bottom:5px;">Diteruskan oleh  :</div>
		        <div style="float:left;"><strong>'.$nama_penerus_surat.' ('.$posisi_penerus_surat.')</strong></div>
		        <div style="clear:both"></div>
		        <div style="float:left; width:150px; margin-bottom:5px;">Kepada  :</div>
		        <div style="float:left;"><strong>'.$nama_penerima.' ('.$posisi_penerima.')</strong></div>
		        <div style="clear:both"></div>

		        <div style="float:left; width:150px; margin-bottom:5px;">===============</div>
		        <div style="float:left;"><strong>===============</strong></div>
		        <div style="clear:both"></div>
		        <div style="float:left; width:150px; margin-bottom:5px;">-</div>
		        <div style="float:left;"><strong>-</strong></div>
		        <div style="clear:both"></div>
		        <div style="float:left; width:150px; margin-bottom:5px;">Status Saat ini  :</div>
		        <div style="float:left;"><strong>Menunggu Tinjut Sdr/i. '.$nama_penerima.' ('.$posisi_penerima.')</strong></div>
		        <div style="clear:both"></div>
		 <td><tr></table>
		 <br><br><br><a href="'.base_url().'index.php"><b>==> Go to Smartsam <==</b></a>
		</body>
		</html>';

		  $mail = new PHPMailer\PHPMailer\PHPMailer(); 
		//$mail = new PHPMailer;
		$mail->IsSMTP();
		$mail->SMTPSecure = 'ssl';
		$mail->Host = "smtp.gmail.com"; //host masing2 provider email
		$mail->SMTPDebug = 1;
		$mail->Port = 465;
		$mail->SMTPAuth = true;
		$mail->IsHTML(true);
		$mail->Username = "smartsam@unsam.ac.id"; //user email yang sebelumnya anda buat
		$mail->Password = "smartsam123"; //password email yang sebelumnya anda buat
		$mail->SetFrom("smartsam@unsam.ac.id","SmartSam Assistant"); //set email pengirim
		$mail->Subject = "Perihal ".$perihal; //subyek email
		$mail->addAddress($email_penerima,"User SmartSam");  //tujuan email
		$mail->MsgHTML($message);
		$mail->Send();
	}

	public function add_konsep()
	{
		$draft_asal 				= $this->input->post('draft_asal');
		$draft_author 				= $this->session->userdata('username');
		$draft_uuid					= $this->input->post('draft_uuid');
		$draft_penerima				= $this->session->userdata('id_atasan');
		$draft_konsep				= $this->input->post('konsep');
		$pelaksana_uuid 			= $this->session->userdata('uuid');

		$konsep_uuid				= $this->get_id();

		$data2 = array(		
			'konsep_uuid' 		=> $konsep_uuid,
			'konsep_uuid_draft' => $draft_uuid,
			'konsep_asal' 		=> $draft_asal,
			'konsep_tujuan' 	=> $draft_penerima,
			'konsep_isi' 		=> $draft_konsep,
			'konsep_author' 	=> $draft_author,
			);

		$this->web_app_model->insertData($data2,'tbl_konsep_draft');
		header('location:'.base_url().'index.php/pelaksana/bg_detail_draft/'.$draft_uuid.'/'.$pelaksana_uuid.'?draft=1/');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
												<button type='button' class='close' data-dismiss='alert'>
													<i class='icon-remove'></i>
												</button>
		
												<p>
													<strong>
														<i class='icon-ok'></i>
														Success! - 
													</strong>
													Konsep baru Berhasil Direkam!
												</p>
											</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
											     swal({
											                title: 'Success!!',
											                text:  'Konsep berhasil direkam!',
											                type: 'success',
											                timer: 3000,
											                showConfirmButton: true
											            });  
											     },10);  
											    </script>
											    ");
	}

	public function tandai_sudah_baca()
	{
		$draft_read 				= "1";
		$draft_uuid 				= $this->uri->segment(3);
		

		$data = array(		
			'draft_read' 		=> $draft_read,
			);

		$where = array(		
			'draft_uuid' 		=> $draft_uuid,
			);

		$this->web_app_model->updateDataWhere($where,$data,'tbl_draft_surat');
		header('location:'.base_url().'index.php/pelaksana/bg_draft_acc?draft=1/');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
												<button type='button' class='close' data-dismiss='alert'>
													<i class='icon-remove'></i>
												</button>
		
												<p>
													<strong>
														<i class='icon-ok'></i>
														Success! - 
													</strong>
													Sudah ditandai!
												</p>
											</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
											     swal({
											                title: 'Success!!',
											                text:  'Berhasil ditandai!',
											                type: 'success',
											                timer: 3000,
											                showConfirmButton: true
											            });  
											     },10);  
											    </script>
											    ");
	}

	public function proses_surat()
	{
		$dist_uuid				= $this->uri->segment(3);

		$data = array(		
			'dist_status' 		=> "Done",
			);

		$where = array(		
			'dist_uuid' 		=> $dist_uuid,
			);

		$this->web_app_model->updateDataWhere($where,$data,'tbl_distribusi_surat');
		header('location:'.base_url().'index.php/pelaksana/bg_suratMasuk?suratMasuk=1/');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
												<button type='button' class='close' data-dismiss='alert'>
													<i class='icon-remove'></i>
												</button>
		
												<p>
													<strong>
														<i class='icon-ok'></i>
														Success! - 
													</strong>
													Surat berhasil ditindak lanjuti!
												</p>
											</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
											     swal({
											                title: 'Success!!',
											                text:  'Surat Berhasil Ditindak Lanjuti!',
											                type: 'success',
											                timer: 3000,
											                showConfirmButton: true
											            });  
											     },10);  
											    </script>
											    ");

		//$penerima_staff			= $this->web_app_model->getWhereOneItem($disposisi_kepada,'staff_username','tbl_staff');
		$identitas_surat		= $this->web_app_model->getWhereOneItem($dist_uuid,'dist_uuid','tbl_distribusi_surat');
		$nama_penerima_surat 	= $this->session->userdata('nama');
		$posisi_penerima_surat 	= $this->session->userdata('posisi');

		$time 	= date('l, d F Y | H:i');

		$TOKEN  = $this->get_token_bot_telegram();  // ganti token ini dengan token bot mu
		$chatid = $this->get_id_group_telegram(); // ini id saya di telegram @hasanudinhs silakan diganti dan disesuaikan
		$pesan 	= "<b>Surat Diterima (Delivered)</b>\n\nSurat dari <b>".$identitas_surat['dist_pengirim']."</b> dengan nomor <b>".$identitas_surat['dist_nomorsurat']."</b>.\n\nPerihal terkait <b>".$identitas_surat['dist_perihal']."</b> telah diterima oleh<b> ".$nama_penerima_surat." (".$posisi_penerima_surat.")</b>\n\n<b>Status:</b>\nDelivered - Sudah ditinjut oleh Sdr/i. ".$nama_penerima_surat." (".$posisi_penerima_surat.")\n\nRegards,\nsmartsam_bot assistant-";

		// ----------- code -------------

		$method	= "sendMessage";
		$url    = "https://api.telegram.org/bot" . $TOKEN . "/". $method;
		$post = [
		 'chat_id' => $chatid,
		  'parse_mode' => 'HTML', // aktifkan ini jika ingin menggunakan format type HTML, bisa juga diganti menjadi Markdown
		 'text' => $pesan
		];

		$header = [
		 "X-Requested-With: XMLHttpRequest",
		 "User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.84 Safari/537.36" 
		];

		// hapus 1 baris ini:
		//die('Hapus baris ini sebelum bisa berjalan, terimakasih.');


		$ch = curl_init();
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_URL, $url);
		//curl_setopt($ch, CURLOPT_REFERER, $refer);
		//curl_setopt($ch, CURLOPT_VERBOSE, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $post );   
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		$datas = curl_exec($ch);
		$error = curl_error($ch);
		$status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		curl_close($ch);

		$debug['text'] = $pesan;
		$debug['code'] = $status;
		$debug['status'] = $error;
		$debug['respon'] = json_decode($datas, true);
	}

	public function bg_tulis_draft_surat()
	{
		$cek  = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='pelaksana')
		{
			$bc['posisi'] 			= $this->session->userdata('posisi');

			$bc['username'] 		= $this->session->userdata('username');
			$bc['nama'] 			= $this->session->userdata('nama');
			$bc['status'] 			= $this->session->userdata('stts');
			$bc['atas'] 			= $this->load->view('panel/atas',$bc,true);
			$bc['menu'] 			= $this->load->view('panel/pelaksana/menu',$bc,true);
			$bc['bio'] 				= $this->load->view('panel/bio',$bc,true);	
			$this->load->view('panel/pelaksana/bg_tulis_draft_surat',$bc);
		}
		else
		{
			header('location:'.base_url().'index.php/panel');
		}
	}

	public function bg_draft_pending()
	{
		$cek  = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		$username 	= $this->session->userdata('username');
		if(!empty($cek) && $stts=='pelaksana')
		{
			$bc['data_draft'] 	= $this->web_app_model->get2JoinAll2WhereGroup('konsep_uuid_draft','draft_uuid','staff_username','konsep_asal','tbl_konsep_draft','tbl_draft_surat','tbl_staff','tbl_konsep_draft','konsep_asal',$username,'draft_status','Process','draft_uuid');

			$bc['pelaksana_uuid'] 	= $this->session->userdata('uuid');
			$bc['posisi'] 			= $this->session->userdata('posisi');
			$bc['username'] 		= $this->session->userdata('username');
			$bc['nama'] 			= $this->session->userdata('nama');
			$bc['status'] 			= $this->session->userdata('stts');
			$bc['atas'] 			= $this->load->view('panel/atas',$bc,true);
			$bc['menu'] 			= $this->load->view('panel/pelaksana/menu',$bc,true);
			$bc['bio'] 				= $this->load->view('panel/bio',$bc,true);	
			$this->load->view('panel/pelaksana/bg_draft_pending',$bc);
		}
		else
		{
			header('location:'.base_url().'index.php/panel');
		}
	}

	public function bg_draft_acc()
	{
		$cek  = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		$username 	= $this->session->userdata('username');
		if(!empty($cek) && $stts=='pelaksana')
		{
			$bc['data_draft'] 	= $this->web_app_model->get2JoinAll2WhereGroup('konsep_uuid_draft','draft_uuid','staff_username','konsep_asal','tbl_konsep_draft','tbl_draft_surat','tbl_staff','tbl_konsep_draft','konsep_asal',$username,'draft_status','Done','draft_uuid');

			$bc['pelaksana_uuid'] 	= $this->session->userdata('uuid');
			$bc['posisi'] 			= $this->session->userdata('posisi');
			$bc['username'] 		= $this->session->userdata('username');
			$bc['nama'] 			= $this->session->userdata('nama');
			$bc['status'] 			= $this->session->userdata('stts');
			$bc['atas'] 			= $this->load->view('panel/atas',$bc,true);
			$bc['menu'] 			= $this->load->view('panel/pelaksana/menu',$bc,true);
			$bc['bio'] 				= $this->load->view('panel/bio',$bc,true);	
			$this->load->view('panel/pelaksana/bg_draft_acc',$bc);
		}
		else
		{
			header('location:'.base_url().'index.php/panel');
		}
	}
//==========================================================




















	

	

	


//======================================================






















	

	

	




































	// =================================================================================

	

	

	public function tambahSurat()
	{
		$dist_uuid					= $this->get_id();
		$dist_pengirim				= $this->input->post('pengirim');
		$dist_nomorSurat			= $this->input->post('nomorSurat');
		$dist_lampiran				= $this->input->post('lampiran');
		$dist_perihal				= $this->input->post('perihal');
		$dist_disposisi				= $this->input->post('disposisi');
		$dist_status				= "Process";

		// get foto
		 $config['upload_path'] 	= './upload/suratMasuk';
		 $config['allowed_types'] 	= 'jpg|pdf|jpeg';
		 $config['max_size'] 		= '5000';  //5MB max
		 //$config['max_width'] 	= '4480'; // pixel
		 //$config['max_height'] 	= '4480'; // pixel
		 $config['overwrite']		= true;
	     $config['file_name'] 		= $dist_pengirim.'-'.$dist_uuid;

     	 $this->load->library('upload', $config);

			if(!empty($_FILES['dokumen']['name'])) 
			{
		        if ( $this->upload->do_upload('dokumen') ) 
		        {
		            $foto = $this->upload->data();		

					$data = array(		
						'dist_uuid' 		=> $dist_uuid,
						'dist_pengirim' 	=> $dist_pengirim,
						'dist_nomorSurat' 	=> $dist_nomorSurat,
						'dist_lampiran' 	=> $dist_lampiran,
						'dist_perihal' 		=> $dist_perihal,
						'dist_disposisi' 	=> $dist_disposisi,
						'dist_status' 		=> $dist_status,
						'dist_dokumen' 		=> $foto['file_name'],
						);
		
					$this->web_app_model->insertData($data,'tbl_distribusi_surat');
					header('location:'.base_url().'index.php/panel/bg_suratMasuk?suratMasuk=1/');
					$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
															<button type='button' class='close' data-dismiss='alert'>
																<i class='icon-remove'></i>
															</button>
					
															<p>
																<strong>
																	<i class='icon-ok'></i>
																	Success! - 
																</strong>
																Data Surat Masuk berhasil ditambahkan!
															</p>
														</div>");

					$this->session->set_flashdata("info2","<script type='text/javascript'>
														     setTimeout(function () { 
														     swal({
														                title: 'Success!!',
														                text:  'Data Surat Masuk berhasil ditambahkan!',
														                type: 'success',
														                timer: 3000,
														                showConfirmButton: true
														            });  
														     },10);  
														    </script>
														    ");
				}
				else 
				{
		          	header('location:'.base_url().'index.php/panel/bg_suratMasuk?suratMasuk=1/');
					$this->session->set_flashdata("info2","<script type='text/javascript'>
										     setTimeout(function () { 
										     swal({
										                title: 'Maaf!',
										                text:  'Data gagal tersimpan, pastikan dokumen max 5 Mb atau hubungi IT PDAM',
										                type: 'warning',
										                timer: 300000,
										                showConfirmButton: true
										            });  
										     },10);  
										    </script>");
		    	}
		    }
		    else 
			{
	          	header('location:'.base_url().'index.php/panel/bg_suratMasuk?suratMasuk=1/');
				$this->session->set_flashdata("info2","<script type='text/javascript'>
									     setTimeout(function () { 
									     swal({
									                title: 'Dokumen kosong!',
									                text:  'Mohon lampirkan dokumen surat masuk',
									                type: 'warning',
									                timer: 300000,
									                showConfirmButton: true
									            });  
									     },10);  
									    </script>");
		    }
	}
}
