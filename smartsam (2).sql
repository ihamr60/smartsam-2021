-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 03 Nov 2020 pada 21.56
-- Versi server: 10.4.10-MariaDB
-- Versi PHP: 7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `smartsam`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_biro`
--

CREATE TABLE `tbl_biro` (
  `biro_no` int(11) NOT NULL,
  `biro_uuid` varchar(32) NOT NULL,
  `biro_username` varchar(20) NOT NULL,
  `biro_nama` varchar(30) NOT NULL,
  `biro_posisi` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tbl_biro`
--

INSERT INTO `tbl_biro` (`biro_no`, `biro_uuid`, `biro_username`, `biro_nama`, `biro_posisi`) VALUES
(2, '2', 'biro_akpk', 'Bapak Biro AKPK', 'Biro AKPK'),
(1, '1', 'biro_umum', 'Bapak Biro Umum', 'Biro Umum & Keuangan');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_distribusi_surat`
--

CREATE TABLE `tbl_distribusi_surat` (
  `dist_no` int(11) NOT NULL,
  `dist_uuid` varchar(32) NOT NULL,
  `dist_pengirim` varchar(30) NOT NULL,
  `dist_nomorsurat` varchar(30) NOT NULL,
  `dist_lampiran` varchar(20) NOT NULL,
  `dist_perihal` varchar(40) NOT NULL,
  `dist_penerima` varchar(40) NOT NULL,
  `dist_disposisi` varchar(40) NOT NULL COMMENT 'Current Disposisi',
  `dist_dokumen` varchar(255) NOT NULL,
  `dist_status` varchar(30) NOT NULL,
  `dist_updated_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `dist_tgl_terima` date NOT NULL,
  `dist_tgl_surat` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tbl_distribusi_surat`
--

INSERT INTO `tbl_distribusi_surat` (`dist_no`, `dist_uuid`, `dist_pengirim`, `dist_nomorsurat`, `dist_lampiran`, `dist_perihal`, `dist_penerima`, `dist_disposisi`, `dist_dokumen`, `dist_status`, `dist_updated_at`, `dist_tgl_terima`, `dist_tgl_surat`) VALUES
(26, '06874106141811EB9ACC2CFDA1251793', 'Universitas Malikussaleh', '002/UN45/VI/2020', '2', 'Permohonan Studi Banding', '', 'pelaksana9', 'Universitas_Malikussaleh-06874106141811EB9ACC2CFDA1251793.pdf', 'Process', '2020-10-22 03:38:15', '2020-10-22', '0000-00-00'),
(21, '124E6302113C11EB846E2CFDA1251793', 'DISKOMINFO Langsa', '0212/GN98/XI/IV/2020', '0', 'Pengintegrasian Sistem Informasi Terpadu', '', '', 'DISKOMINFO_Langsa-124E6302113C11EB846E2CFDA1251793.pdf', 'Process', '2020-10-18 12:18:48', '2020-10-18', '0000-00-00'),
(27, '30D0FB32141A11EB9ACC2CFDA1251793', 'Dinas Sosial Kota Langsa', '0932/UN34/IV/X/2020', '0', 'Permohonan Sosisialisasi Kartu Indonesia', '', 'pelaksana10', 'Dinas_Sosial_Kota_Langsa-30D0FB32141A11EB9ACC2CFDA1251793.pdf', 'Done', '2020-10-22 03:53:45', '2020-10-22', '0000-00-00'),
(22, '545335F011CD11EBA64D2CFDA1251793', 'Dinas Sosial ', '402/GN98/XI/V/2020', '1', 'Kartu Indonesia Pintar', '', '', 'Dinas_Sosial_-545335F011CD11EBA64D2CFDA1251793.pdf', 'Process', '2020-10-19 05:38:34', '2020-10-19', '0000-00-00'),
(28, 'A2C2F804144011EBB8712CFDA1251793', 'Dinas Sosial Kota Langsa', '002/UN45/VI/2020', '1', 'Sosialisasi Kartu Indonesia Pintar', '', 'biro_umum', 'Dinas_Sosial_Kota_Langsa-A2C2F804144011EBB8712CFDA1251793.pdf', 'Process', '2020-10-22 08:29:00', '2020-10-22', '0000-00-00'),
(25, 'D6CC6850141311EB9ACC2CFDA1251793', 'Dinas Sosial Kota Langsa', '003/UN65/IV/X/2020', '0', 'Sosialisasi Kartu Indonesia Pintar', '', 'biro_umum', 'Dinas_Sosial_Kota_Langsa-D6CC6850141311EB9ACC2CFDA1251793.pdf', 'Process', '2020-10-22 03:08:17', '2020-10-22', '0000-00-00'),
(23, 'D9A94C15140B11EB9ACC2CFDA1251793', 'Universitas Malikussaleh', '002/UN45/VI/2020', '1', 'Permohonan Studi Banding', '', 'Pilih Disposisi', 'Universitas_Malikussaleh-D9A94C15140B11EB9ACC2CFDA1251793.pdf', 'Process', '2020-10-22 02:11:06', '2020-10-22', '0000-00-00'),
(20, 'E9117FDF113B11EB846E2CFDA1251793', 'Badan Kepegawaian Daerah', '322/GN98/XI/V/2020', '1', 'Tes Swab ASN Serentak', '', 'pelaksana16', 'Badan_Kepegawaian_Daerah-E9117FDF113B11EB846E2CFDA1251793.pdf', 'Process', '2020-10-18 12:17:38', '2020-10-18', '0000-00-00'),
(24, 'E9827AD0140B11EB9ACC2CFDA1251793', 'Universitas Malikussaleh', '002/UN45/VI/2020', '1', 'Permohonan Studi Banding', '', 'pelaksana10', 'Universitas_Malikussaleh-E9827AD0140B11EB9ACC2CFDA1251793.pdf', 'Done', '2020-10-22 02:11:33', '2020-10-22', '0000-00-00'),
(19, 'F3430781112D11EB846E2CFDA1251793', 'Badan Narkotika Nasional', '002/GN98/XI/V/2020', '1', 'Himbauan melakukan tes urin', '', 'pelaksana10', 'Badan_Narkotika_Nasional-F3430781112D11EB846E2CFDA1251793.pdf', 'Process', '2020-10-18 10:37:42', '2020-10-18', '0000-00-00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_draft_surat`
--

CREATE TABLE `tbl_draft_surat` (
  `draft_no` int(11) NOT NULL,
  `draft_uuid` varchar(32) NOT NULL,
  `draft_asal` varchar(30) NOT NULL,
  `draft_penerima` varchar(30) NOT NULL,
  `draft_perihal` varchar(40) NOT NULL,
  `draft_created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `draft_status` varchar(30) NOT NULL,
  `draft_ket` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tbl_draft_surat`
--

INSERT INTO `tbl_draft_surat` (`draft_no`, `draft_uuid`, `draft_asal`, `draft_penerima`, `draft_perihal`, `draft_created_at`, `draft_status`, `draft_ket`) VALUES
(5, '1194A66A1E0911EBB5512CFDA1251793', 'pelaksana9', '3', 'Perihal Pelaksana 9', '2020-11-03 19:16:26', 'Process', ''),
(1, '1DFD19AD1D8D11EBB39D2CFDA1251793', 'pelaksana10', '3', 'TES', '2020-11-03 04:29:09', 'Process', ''),
(4, '48AB009E1DDB11EBB5512CFDA1251793', 'pelaksana10', '2234234', 'Kunjungan kerja', '2020-11-03 13:48:41', 'Done', ''),
(2, '607B44EA1D8E11EBB39D2CFDA1251793', 'pelaksana10', '2', 'TES COBA', '2020-11-03 04:38:10', 'Process', ''),
(3, '9B6206141DA411EBB39D2CFDA1251793', 'pelaksana10', '2', 'TES', '2020-11-03 07:17:18', 'Process', '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_kabag`
--

CREATE TABLE `tbl_kabag` (
  `kabag_no` int(11) NOT NULL,
  `kabag_uuid` varchar(32) NOT NULL,
  `kabag_username` varchar(20) NOT NULL,
  `kabag_nama` varchar(30) NOT NULL,
  `kabag_posisi` varchar(40) NOT NULL,
  `kabag_uuid_biro` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tbl_kabag`
--

INSERT INTO `tbl_kabag` (`kabag_no`, `kabag_uuid`, `kabag_username`, `kabag_nama`, `kabag_posisi`, `kabag_uuid_biro`) VALUES
(1, '1', 'kabag1', 'Beni', 'Kabag Keuangan', 1),
(2, '2', 'kabag2', 'Faisal', 'Kabag Umum', 1),
(3, '3', 'kabag3', 'Ronal', 'Kabag Akademik', 2),
(4, '4', 'kabag4', 'Raditya', 'Kabag Perencana', 2);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_kasubbag`
--

CREATE TABLE `tbl_kasubbag` (
  `kasubbag_no` int(11) NOT NULL,
  `kasubbag_uuid` varchar(32) NOT NULL,
  `kasubbag_username` varchar(10) NOT NULL,
  `kasubbag_nama` varchar(30) NOT NULL,
  `kasubbag_posisi` varchar(40) NOT NULL,
  `kasubbag_uuid_kabag` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tbl_kasubbag`
--

INSERT INTO `tbl_kasubbag` (`kasubbag_no`, `kasubbag_uuid`, `kasubbag_username`, `kasubbag_nama`, `kasubbag_posisi`, `kasubbag_uuid_kabag`) VALUES
(1, '1', 'kasubbag1', 'Irfan\r\n', 'Kasubbag Perbendaharaan', '1'),
(10, '10', 'kasubbag10', 'Heru Fisiko', 'Kasubbag Kerjasama & Humas', '4'),
(2, '2', 'kasubbag2', 'Kaival', 'Kasubbag Akuntansi & Pelaporan', '1'),
(3, '3', 'kasubbag3', 'Herman', 'Kasubbag TU & Ketatalaksanaan', '2'),
(4, '4', 'kasubbag4', 'Rahmat Hidayat', 'Kasubbag RT & BM', '2'),
(5, '5', 'kasubbag5', 'Rahmat D Mirzal', 'Kasubbag Kepegawaian', '2'),
(6, '6', 'kasubbag6', 'Ratna', 'Kasubbag Pendidikan & Evaluasi', '3'),
(7, '7', 'kasubbag7', 'Zavira', 'Kasubbag Registrasi & Statistik', '3'),
(8, '8', 'kasubbag8', 'Muslia', 'Kasubbag Kemahasiswaan', '3'),
(9, '9', 'kasubbag9', 'Musliadi', 'Kasubbag Perencanaan', '4');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_konsep_draft`
--

CREATE TABLE `tbl_konsep_draft` (
  `konsep_no` int(11) NOT NULL,
  `konsep_uuid` varchar(32) NOT NULL,
  `konsep_uuid_draft` varchar(32) NOT NULL,
  `konsep_asal` varchar(30) NOT NULL,
  `konsep_tujuan` varchar(30) NOT NULL,
  `konsep_isi` text DEFAULT 'Tidak ada koreksi',
  `konsep_updated_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `konsep_author` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tbl_konsep_draft`
--

INSERT INTO `tbl_konsep_draft` (`konsep_no`, `konsep_uuid`, `konsep_uuid_draft`, `konsep_asal`, `konsep_tujuan`, `konsep_isi`, `konsep_updated_at`, `konsep_author`) VALUES
(21, '04805B161E1011EBB5512CFDA1251793', '48AB009E1DDB11EBB5512CFDA1251793', 'pelaksana10', '1', '', '2020-11-03 20:06:10', 'biro_umum'),
(19, '119518F71E0911EBB5512CFDA1251793', '1194A66A1E0911EBB5512CFDA1251793', 'pelaksana9', '3', 'TES ISI SURAT', '2020-11-03 19:16:26', 'pelaksana9'),
(9, '337D643D1DE211EBB5512CFDA1251793', '48AB009E1DDB11EBB5512CFDA1251793', 'pelaksana10', '3', 'Perihal di ganti saja menjadi\r\n\r\nPerihal : Kunjungan kerja bersama Dinkes\r\n\r\nBody:\r\nSehubungan dengan surat permohonan yang bapak ibu sampaikan. maka kami akan dengan senang hati untuk menyambut kedatangan bapak ibu dalam rangka kunjunga kerja.\r\n\r\nDemikian, Assalam\r\n', '2020-11-03 14:38:12', 'pelaksana10'),
(22, '429DAE371E1011EBB5512CFDA1251793', '48AB009E1DDB11EBB5512CFDA1251793', 'pelaksana10', '2', '', '2020-11-03 20:07:55', 'biro_umum'),
(3, '48AB32051DDB11EBB5512CFDA1251793', '48AB009E1DDB11EBB5512CFDA1251793', 'pelaksana10', '3', 'Kunjungan kerja', '2020-11-03 13:48:41', 'pelaksana10'),
(1, '607B68741D8E11EBB39D2CFDA1251793', '607B44EA1D8E11EBB39D2CFDA1251793', 'pelaksana10', '3', 'TES COBA', '2020-11-03 04:38:10', 'pelaksana10'),
(16, '6E830D951DF111EBB5512CFDA1251793', '9B6206141DA411EBB39D2CFDA1251793', 'pelaksana10', '2', 'COBA DRAFT 3', '2020-11-03 16:27:14', 'kasubbag3'),
(23, '6FF02B321E1011EBB5512CFDA1251793', '48AB009E1DDB11EBB5512CFDA1251793', 'pelaksana10', '1', '', '2020-11-03 20:09:11', 'biro_umum'),
(17, '6FF986601DF211EBB5512CFDA1251793', '607B44EA1D8E11EBB39D2CFDA1251793', 'pelaksana10', '2', 'Merah', '2020-11-03 16:34:26', 'kasubbag3'),
(4, '7438A4461DE011EBB5512CFDA1251793', '48AB009E1DDB11EBB5512CFDA1251793', 'pelaksana10', '3', '', '2020-11-03 14:25:42', 'pelaksana10'),
(25, '9814A51C1E1611EBB5512CFDA1251793', '48AB009E1DDB11EBB5512CFDA1251793', 'pelaksana10', '-', 'BY REKTOR', '2020-11-03 20:53:15', 'rektor3'),
(2, '9B623CB71DA411EBB39D2CFDA1251793', '9B6206141DA411EBB39D2CFDA1251793', 'pelaksana10', '3', 'TES', '2020-11-03 07:17:18', 'pelaksana10'),
(24, 'A335B8681E1011EBB5512CFDA1251793', '48AB009E1DDB11EBB5512CFDA1251793', 'pelaksana10', '2234234', '', '2020-11-03 20:10:37', 'biro_umum'),
(5, 'B5F33BE81DE011EBB5512CFDA1251793', '48AB009E1DDB11EBB5512CFDA1251793', 'pelaksana10', '3', 'TESSSS', '2020-11-03 14:27:32', 'pelaksana10'),
(10, 'B7004DC01DEF11EBB5512CFDA1251793', '48AB009E1DDB11EBB5512CFDA1251793', 'pelaksana10', '2', 'Kasubbag Tes', '2020-11-03 16:14:56', 'pelaksana10'),
(14, 'BAD743EB1DF011EBB5512CFDA1251793', '48AB009E1DDB11EBB5512CFDA1251793', 'pelaksana10', '2', 'TESS', '2020-11-03 16:22:12', 'kasubbag3'),
(11, 'BAF3F59C1DEF11EBB5512CFDA1251793', '48AB009E1DDB11EBB5512CFDA1251793', 'pelaksana10', '2', 'tes', '2020-11-03 16:15:03', 'pelaksana10'),
(12, 'C3FF55BA1DEF11EBB5512CFDA1251793', '48AB009E1DDB11EBB5512CFDA1251793', 'pelaksana10', '2', 'Kasubbag', '2020-11-03 16:15:18', 'pelaksana10'),
(15, 'C5562D3C1DF011EBB5512CFDA1251793', '48AB009E1DDB11EBB5512CFDA1251793', 'pelaksana10', '2', 'TESS', '2020-11-03 16:22:30', 'kasubbag3'),
(20, 'CE71E29C1E0F11EBB5512CFDA1251793', '48AB009E1DDB11EBB5512CFDA1251793', 'pelaksana10', '1', '', '2020-11-03 20:04:40', 'biro_umum'),
(18, 'D7B10F651E0811EBB5512CFDA1251793', '48AB009E1DDB11EBB5512CFDA1251793', 'pelaksana10', '1', 'OKE by Kabag', '2020-11-03 19:14:49', 'kabag2'),
(13, 'E1E915671DEF11EBB5512CFDA1251793', '48AB009E1DDB11EBB5512CFDA1251793', 'pelaksana10', '2', 'tesssss afaaa', '2020-11-03 16:16:08', 'kasubbag3'),
(6, 'E7438DEF1DE011EBB5512CFDA1251793', '48AB009E1DDB11EBB5512CFDA1251793', 'pelaksana10', '3', 'teess aaa', '2020-11-03 14:28:55', 'pelaksana10'),
(7, 'F407FF051DE011EBB5512CFDA1251793', '48AB009E1DDB11EBB5512CFDA1251793', 'pelaksana10', '3', 'teess aaa bbbb', '2020-11-03 14:29:16', 'pelaksana10'),
(8, 'FAB7C2771DE011EBB5512CFDA1251793', '48AB009E1DDB11EBB5512CFDA1251793', 'pelaksana10', '3', ' awd awdawdawda  awd awdawdawda  awd awdawdawda  awd awdawdawda  awd awdawdawda  awd awdawdawda  awd awdawdawda  awd awdawdawda  awd awdawdawda  awd awdawdawda  awd awdawdawda  awd awdawdawda  awd awdawdawda  awd awdawdawda  awd awdawdawda  awd awdawdawda  awd awdawdawda  awd awdawdawda  awd awdawdawda  awd awdawdawda  awd awdawdawda  awd awdawdawda  awd awdawdawda  awd awdawdawda  awd awdawdawda  awd awdawdawda  awd awdawdawda  awd awdawdawda  awd awdawdawda  awd awdawdawda  awd awdawdawda  awd awdawdawda  awd awdawdawda  awd awdawdawda  awd awdawdawda  awd awdawdawda  awd awdawdawda  awd awdawdawda ', '2020-11-03 14:29:28', 'pelaksana10');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_login`
--

CREATE TABLE `tbl_login` (
  `username` varchar(16) NOT NULL,
  `stts` varchar(20) NOT NULL,
  `password` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tbl_login`
--

INSERT INTO `tbl_login` (`username`, `stts`, `password`) VALUES
('admin', 'admin subbag tu', '21232f297a57a5a743894a0e4a801fc3'),
('biro_akpk', 'biro', '0959631689d0a2a92a8c3ee6722e02f6'),
('biro_umum', 'biro', '28abd1f04d287eeb1321dd6332b0ee11'),
('kabag1', 'kabag', '485ec531822b216a859ebe9151b715f6'),
('kabag2', 'kabag', '81d868eb7a14df728173e1ee147ea5fd'),
('kabag3', 'kabag', '465f5db859367a7683be83433219b609'),
('kabag4', 'kabag', 'f251a2c273f38d8c0181ce97cff39bb7'),
('kasubbag1', 'kasubbag', '1a27f515cd7cd58d5b5248ab66a295e8'),
('kasubbag10', 'kasubbag', 'f583ea4ad4a11eaebcb2d9531bd8b92d'),
('kasubbag2', 'kasubbag', '4480b2610a01e277f947c6da1ea6588d'),
('kasubbag3', 'kasubbag', '5b18490a44d521fb9b12de44cd2decff'),
('kasubbag4', 'kasubbag', '326da7a9f89af02866f6206a7c03d61b'),
('kasubbag5', 'kasubbag', '809e9fa9442bf6eb359a51121d3b1b13'),
('kasubbag6', 'kasubbag', '813b5fbcac1a39f730bf9f1549712760'),
('kasubbag7', 'kasubbag', 'ee2d2c1e6badd2c269260b582f5458c1'),
('kasubbag8', 'kasubbag', '5bd668424efba8a862d36b31aa6a9d70'),
('kasubbag9', 'kasubbag', '9ed6283fe6df769b0e356bd1138fd680'),
('pelaksana', 'pelaksana', '6875ccc2c267a8c215afb1f25f81d7a0'),
('pelaksana1', 'pelaksana', 'e32daea4f91d2d6f69efa2da385e3725'),
('pelaksana10', 'pelaksana', '5223d74665420a8d4f0288ba67fc098e'),
('pelaksana11', 'pelaksana', '58e8c2b298a4af3ddcd9471c647e149e'),
('pelaksana12', 'pelaksana', '82c21ef79bf0d351e9bfc1cc32e84494'),
('pelaksana13', 'pelaksana', '6c67361dc3924d4a558adbc47080422b'),
('pelaksana14', 'pelaksana', '3f6a48dc7ad496d7fcde260daca8bd50'),
('pelaksana15', 'pelaksana', 'bc8998908292bd2508f43e808ce3a772'),
('pelaksana16', 'pelaksana', '9ae9646ecb7588ca8820296d40aa2463'),
('pelaksana17', 'pelaksana', 'b5395bcfbced23528b410b02192e034b'),
('pelaksana18', 'pelaksana', '80df783e19e4d1ffc69840f69edaa099'),
('pelaksana19', 'pelaksana', 'bcdb21d4ecd652f48b40926e0ee55377'),
('pelaksana2', 'pelaksana', 'd4f803080f4699bddecf5f19c73ca333'),
('pelaksana20', 'pelaksana', 'b5f32940f6b88ec1986219b5ddb63e7e'),
('pelaksana21', 'pelaksana', '287f47c5b6cafead3b0cd47c41edb934'),
('pelaksana22', 'pelaksana', 'c7c0348ab82418ec1f349bfdff8868da'),
('pelaksana23', 'pelaksana', 'dde7b0feae3e725cdb8208eb420b1cae'),
('pelaksana24', 'pelaksana', '725d7020d50dffe0de99b983b518bde5'),
('pelaksana3', 'pelaksana', '1c97c30aaaab877a76c231d3d5326bf9'),
('pelaksana4', 'pelaksana', 'a7c1e10f9d32aa50fbd755cf1606f8be'),
('pelaksana5', 'pelaksana', '5072a44b03756a10b424e9287801ebfd'),
('pelaksana6', 'pelaksana', 'e3a3b65cde0283ab4a57082bfe10ea9d'),
('pelaksana7', 'pelaksana', '372428373e31bd9bb44789209bbed802'),
('pelaksana8', 'pelaksana', 'e547e873a416571bde449a192d2ebe3d'),
('pelaksana9', 'pelaksana', '6f146bed8fe5eb3dd4e7e4df7e3420dc'),
('rektor', 'rektor', '7802f18483b28d50a62e65c27e9e12a9'),
('rektor3', 'rektor', '27dad8272df43053643a89779ab2d17c');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_rektor`
--

CREATE TABLE `tbl_rektor` (
  `rektor_no` int(11) NOT NULL,
  `rektor_uuid` varchar(32) NOT NULL,
  `rektor_username` varchar(10) NOT NULL,
  `rektor_nama` varchar(30) NOT NULL,
  `rektor_posisi` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tbl_rektor`
--

INSERT INTO `tbl_rektor` (`rektor_no`, `rektor_uuid`, `rektor_username`, `rektor_nama`, `rektor_posisi`) VALUES
(1, '1324234', 'rektor', 'Bapak Rektor 1', 'Rektor'),
(2, '2234234', 'rektor3', 'Bapak Rektor 2', 'warektor');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_riwayat_disposisi`
--

CREATE TABLE `tbl_riwayat_disposisi` (
  `riw_no` int(11) NOT NULL,
  `riw_uuid` varchar(32) NOT NULL,
  `riw_dist_uuid` varchar(32) NOT NULL,
  `riw_asal` varchar(30) NOT NULL,
  `riw_disposisi` varchar(30) NOT NULL,
  `riw_update_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `riw_memo` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tbl_riwayat_disposisi`
--

INSERT INTO `tbl_riwayat_disposisi` (`riw_no`, `riw_uuid`, `riw_dist_uuid`, `riw_asal`, `riw_disposisi`, `riw_update_at`, `riw_memo`) VALUES
(50, '00DF030011D211EBA64D2CFDA1251793', '545335F011CD11EBA64D2CFDA1251793', 'rektor', 'biro_akpk', '2020-10-19 06:11:57', 'Mohon segera tindaklanjuti'),
(115, '02E8A253144611EBB8712CFDA1251793', 'A2C2F804144011EBB8712CFDA1251793', 'kabag2', 'rektor', '2020-10-22 09:07:29', 'Program sosialisasi Kartu Indonesia Pintar'),
(116, '02E8CE88144611EBB8712CFDA1251793', 'A2C2F804144011EBB8712CFDA1251793', 'kabag2', 'rektor', '2020-10-22 09:07:29', 'Teruskan'),
(8, '041EDD170DC311EB8C442CFDA1251793', '041EB1000DC311EB8C442CFDA1251793', 'staff', 'rektor2', '2020-10-14 02:13:20', 'tesss'),
(11, '04D47A310E8C11EB8C442CFDA1251793', '24D42E110B9611EBBB0B2CFDA1251793', 'rektor', 'biro2', '2020-10-15 02:11:44', 'Tes Memo ke Biro\r\n'),
(90, '068798D0141811EB9ACC2CFDA1251793', '06874106141811EB9ACC2CFDA1251793', 'admin', 'kabag2', '2020-10-22 03:38:15', 'forward to kabag umum'),
(12, '0832200E0E9411EB8C442CFDA1251793', '24D42E110B9611EBBB0B2CFDA1251793', 'biro2', 'kabag', '2020-10-15 03:09:05', 'TEs Memo untuk Kabag'),
(23, '0CEEB9AD0E9E11EB8C442CFDA1251793', '0CEE81840E9E11EB8C442CFDA1251793', 'admin', 'rektor2', '2020-10-15 04:20:48', '-'),
(42, '124E8946113C11EB846E2CFDA1251793', '124E6302113C11EB846E2CFDA1251793', 'admin', 'kabag2', '2020-10-18 12:18:48', 'Program sistem terpadu'),
(77, '13F929BE140E11EB9ACC2CFDA1251793', 'E9827AD0140B11EB9ACC2CFDA1251793', 'kasubbag3', 'pelaksana10', '2020-10-22 02:27:03', 'Persiapkan dengan matang, jangan sampai ada kesalahan'),
(81, '167922F5141411EB9ACC2CFDA1251793', 'D6CC6850141311EB9ACC2CFDA1251793', 'kabag2', 'rektor', '2020-10-22 03:10:04', 'Terkait permohonan sosialisasi di kampus Unsam tentang Kartu Indonesia Pintar'),
(82, '16794F2A141411EB9ACC2CFDA1251793', 'D6CC6850141311EB9ACC2CFDA1251793', 'kabag2', 'rektor', '2020-10-22 03:10:04', 'Teruskan'),
(110, '19AA5137141B11EB9ACC2CFDA1251793', '30D0FB32141A11EB9ACC2CFDA1251793', 'kabag2', 'kasubbag3', '2020-10-22 04:00:16', 'Umumkan kepada seluruh civitas akademika dan mahasiswa untuk ikut hadir dalam sosialisasi ini'),
(111, '19AA9104141B11EB9ACC2CFDA1251793', '30D0FB32141A11EB9ACC2CFDA1251793', 'kabag2', 'kasubbag3', '2020-10-22 04:00:16', 'Tindaklanjuti'),
(27, '1B74D9CA0E9F11EB8C442CFDA1251793', '0CEE81840E9E11EB8C442CFDA1251793', 'kabag', 'kasubbag', '2020-10-15 04:28:22', 'Tolong pastikan mahasiswa dapat hadir semua'),
(91, '222E5F65141811EB9ACC2CFDA1251793', '06874106141811EB9ACC2CFDA1251793', 'kabag2', 'kasubbag3', '2020-10-22 03:39:02', 'tindak lanjuti pak herman'),
(92, '222EC2EC141811EB9ACC2CFDA1251793', '06874106141811EB9ACC2CFDA1251793', 'kabag2', 'kasubbag3', '2020-10-22 03:39:02', 'Tindaklanjuti'),
(55, '2365AF6D135311EBA60A2CFDA1251793', '124E6302113C11EB846E2CFDA1251793', 'kabag2', 'kasubbag3', '2020-10-21 04:08:44', 'tes Koordinasi'),
(6, '24D45E4D0B9611EBBB0B2CFDA1251793', '24D42E110B9611EBBB0B2CFDA1251793', 'staff', 'rektor', '2020-10-11 07:46:28', 'Tes Memo Lagi'),
(56, '2884BA99135311EBA60A2CFDA1251793', '124E6302113C11EB846E2CFDA1251793', 'kabag2', 'kasubbag3', '2020-10-21 04:08:52', 'tes Koordinasi'),
(69, '29C2E795140911EB9ACC2CFDA1251793', '545335F011CD11EBA64D2CFDA1251793', 'kabag2', 'kasubbag3', '2020-10-22 01:51:55', 'tes'),
(70, '29C346B5140911EB9ACC2CFDA1251793', '545335F011CD11EBA64D2CFDA1251793', 'kabag2', 'kasubbag3', '2020-10-22 01:51:55', 'Arsipkan'),
(46, '2B36D7B0116C11EB846E2CFDA1251793', 'E9117FDF113B11EB846E2CFDA1251793', 'kabag3', 'kasubbag6', '2020-10-18 18:03:05', 'Lanjutkan Kasubbag Pendidikan'),
(24, '2D36E83A0E9E11EB8C442CFDA1251793', '0CEE81840E9E11EB8C442CFDA1251793', 'rektor2', 'biro2', '2020-10-15 04:21:42', 'Teruskan, pastikan semua berjalan lancar'),
(18, '2EB8F23C0E9A11EB8C442CFDA1251793', 'BBFDE6FE0E9411EB8C442CFDA1251793', 'kabag', 'kasubbag', '2020-10-15 03:53:07', 'Laksanakan sesuai perintah, Ksbg'),
(3, '3', '24D42E110B9611EBBB0B2CFDA1251793', 'wdw', 'wdw', '2020-10-14 01:13:53', 'tes\r\n'),
(103, '30D17E2D141A11EB9ACC2CFDA1251793', '30D0FB32141A11EB9ACC2CFDA1251793', 'admin', 'kabag2', '2020-10-22 03:53:45', 'surat permohonan dari dinas sosial terkait kartu indonesia pintar'),
(43, '315C2F93113C11EB846E2CFDA1251793', 'E9117FDF113B11EB846E2CFDA1251793', 'kabag2', 'rektor', '2020-10-18 12:19:40', '-'),
(35, '37D26CF3110511EB846E2CFDA1251793', '24D42E110B9611EBBB0B2CFDA1251793', 'kabag', 'kasubbag2', '2020-10-18 05:46:08', 'tes'),
(51, '3B31F19111D211EBA64D2CFDA1251793', '545335F011CD11EBA64D2CFDA1251793', 'biro_akpk', 'kabag3', '2020-10-19 06:13:35', 'Inventarisir nama calon penerima'),
(25, '3CAE017B0E9E11EB8C442CFDA1251793', '0CEE81840E9E11EB8C442CFDA1251793', 'rektor2', 'biro2', '2020-10-15 04:22:08', 'Teruskan, pastikan semua berjalan lancar'),
(93, '42200996141811EB9ACC2CFDA1251793', '06874106141811EB9ACC2CFDA1251793', 'kabag2', 'rektor', '2020-10-22 03:39:55', 'diteruskan ke rektor'),
(94, '42204611141811EB9ACC2CFDA1251793', '06874106141811EB9ACC2CFDA1251793', 'kabag2', 'rektor', '2020-10-22 03:39:55', 'Teruskan'),
(57, '43E0FFB0135311EBA60A2CFDA1251793', '124E6302113C11EB846E2CFDA1251793', 'kabag2', '', '2020-10-21 04:09:38', ''),
(36, '470B1FBB110511EB846E2CFDA1251793', '24D42E110B9611EBBB0B2CFDA1251793', 'kasubbag2', 'pelaksana', '2020-10-18 05:46:34', 'tes'),
(28, '47776E250E9F11EB8C442CFDA1251793', '0CEE81840E9E11EB8C442CFDA1251793', 'kasubbag', 'pelaksana', '2020-10-15 04:29:36', 'Laksanakan sesuai perintah dan memo terlampir ya'),
(58, '49952B40135311EBA60A2CFDA1251793', '124E6302113C11EB846E2CFDA1251793', 'kabag2', 'kasubbag3', '2020-10-21 04:09:48', 'tes'),
(67, '4B2D3DFD140811EB9ACC2CFDA1251793', '545335F011CD11EBA64D2CFDA1251793', 'kabag2', 'kasubbag3', '2020-10-22 01:45:41', 'tes'),
(113, '4D255D62141B11EB9ACC2CFDA1251793', '30D0FB32141A11EB9ACC2CFDA1251793', 'kasubbag3', 'pelaksana10', '2020-10-22 04:01:42', 'Siapkan data calon peserta sosialisasi, umumkan dan kemudian arsipkan '),
(112, '4D259963141B11EB9ACC2CFDA1251793', '30D0FB32141A11EB9ACC2CFDA1251793', 'kasubbag3', 'pelaksana10', '2020-10-22 04:01:42', 'Arsipkan'),
(71, '4E984572140911EB9ACC2CFDA1251793', '545335F011CD11EBA64D2CFDA1251793', 'kabag2', '', '2020-10-22 01:52:57', 'Pelajari'),
(72, '4E98EF61140911EB9ACC2CFDA1251793', '545335F011CD11EBA64D2CFDA1251793', 'kabag2', '', '2020-10-22 01:52:57', 'Pelajari/Tela\'ah dan Berikan Saran/Masukan'),
(15, '4F4C850C0E9511EB8C442CFDA1251793', 'BBFDE6FE0E9411EB8C442CFDA1251793', 'rektor', 'biro2', '2020-10-15 03:18:14', 'Lakukan sesuai arahan'),
(44, '50A2A89D116B11EB846E2CFDA1251793', 'E9117FDF113B11EB846E2CFDA1251793', 'rektor', 'biro_akpk', '2020-10-18 17:56:58', 'lanjutkan pak biro'),
(95, '53007F4A141811EB9ACC2CFDA1251793', '06874106141811EB9ACC2CFDA1251793', 'rektor', 'biro_umum', '2020-10-22 03:40:24', 'wakilkan pak herman'),
(96, '5300E984141811EB9ACC2CFDA1251793', '06874106141811EB9ACC2CFDA1251793', 'rektor', 'biro_umum', '2020-10-22 03:40:24', 'Wakili/Hadir/Terima/Laporkan Hasilnya'),
(66, '53E6BB9E135711EBA60A2CFDA1251793', '545335F011CD11EBA64D2CFDA1251793', 'kabag2', 'kasubbag3', '2020-10-21 04:38:43', 'tes'),
(48, '5453650811CD11EBA64D2CFDA1251793', '545335F011CD11EBA64D2CFDA1251793', 'admin', 'kabag2', '2020-10-19 05:38:34', 'Terkait Usulan Kartu Indonesia Pintar'),
(83, '54943C1F141411EB9ACC2CFDA1251793', 'D6CC6850141311EB9ACC2CFDA1251793', 'rektor', 'biro_umum', '2020-10-22 03:11:48', 'Persiapkan, untuk fasilitas gedung hubungi bapak herman bagian gudang'),
(47, '5696FABA116C11EB846E2CFDA1251793', 'E9117FDF113B11EB846E2CFDA1251793', 'kasubbag6', 'pelaksana16', '2020-10-18 18:04:18', 'Lanjutkan ke Pengelola Data Sarana Pendidikan'),
(59, '5888C5F0135411EBA60A2CFDA1251793', '124E6302113C11EB846E2CFDA1251793', 'kabag2', 'kasubbag3', '2020-10-21 04:17:22', 'tes'),
(75, '596E98C3140C11EB9ACC2CFDA1251793', 'E9827AD0140B11EB9ACC2CFDA1251793', 'kabag2', 'kasubbag3', '2020-10-22 02:14:40', 'Koordinasikan dengan pihak terkait (Pak Fadil)'),
(76, '596ECDA6140C11EB9ACC2CFDA1251793', 'E9827AD0140B11EB9ACC2CFDA1251793', 'kabag2', 'kasubbag3', '2020-10-22 02:14:40', 'Koordinasi/Konfirmasi dengan Ybs/Unit Kerja Terkait'),
(30, '67DF8D76110111EB846E2CFDA1251793', 'C395F91C110011EB846E2CFDA1251793', 'kabag', 'rektor', '2020-10-18 05:18:50', 'Dari dinas kehutanan'),
(98, '6905D596141811EB9ACC2CFDA1251793', '06874106141811EB9ACC2CFDA1251793', 'biro_umum', 'kabag2', '2020-10-22 03:41:00', 'proses pak faisal'),
(97, '69064BD3141811EB9ACC2CFDA1251793', '06874106141811EB9ACC2CFDA1251793', 'biro_umum', 'kabag2', '2020-10-22 03:41:00', 'Proses Sesuai Ketentuan/Peraturan yang Berlaku'),
(52, '6ED1A9E411D211EBA64D2CFDA1251793', '545335F011CD11EBA64D2CFDA1251793', 'kabag3', 'kasubbag8', '2020-10-19 06:15:02', 'Mintakan data-data calon dari fakultas'),
(39, '72422AD1113A11EB846E2CFDA1251793', 'F3430781112D11EB846E2CFDA1251793', 'kasubbag3', 'pelaksana10', '2020-10-18 12:07:10', 'Lanjutkan ke bagian Pengolah Data'),
(117, '727E212C179511EB9FB22CFDA1251793', 'A2C2F804144011EBB8712CFDA1251793', 'rektor', 'biro_umum', '2020-10-26 14:13:40', 'Koordinasikan juga ke bapak Reza terkait penggunaan Gedung'),
(118, '727ED2AA179511EB9FB22CFDA1251793', 'A2C2F804144011EBB8712CFDA1251793', 'rektor', 'biro_umum', '2020-10-26 14:13:40', 'Koordinasi/Konfirmasi dengan Ybs/Unit Kerja Terkait'),
(63, '72D593D5135511EBA60A2CFDA1251793', '545335F011CD11EBA64D2CFDA1251793', 'kabag2', 'kasubbag3', '2020-10-21 04:25:16', 'tes'),
(17, '73E7A5150E9611EB8C442CFDA1251793', 'BBFDE6FE0E9411EB8C442CFDA1251793', 'biro2', 'kabag', '2020-10-15 03:26:25', 'Tes Memo ke Kabag\r\n'),
(99, '78BB2CB8141811EB9ACC2CFDA1251793', '06874106141811EB9ACC2CFDA1251793', 'kabag2', 'kasubbag3', '2020-10-22 03:41:27', 'agendakan pak herman'),
(100, '78BBA620141811EB9ACC2CFDA1251793', '06874106141811EB9ACC2CFDA1251793', 'kabag2', 'kasubbag3', '2020-10-22 03:41:27', 'Agendakan/Persiapkan/Koordinasikan'),
(54, '79FB9EF2135211EBA60A2CFDA1251793', '124E6302113C11EB846E2CFDA1251793', 'kabag2', 'kasubbag3', '2020-10-21 04:03:59', 'tes Koordinasi'),
(85, '7B02E3AD141511EB9ACC2CFDA1251793', 'D6CC6850141311EB9ACC2CFDA1251793', 'biro_umum', 'kabag2', '2020-10-22 03:20:02', 'Koordinasi'),
(84, '7B032AD0141511EB9ACC2CFDA1251793', 'D6CC6850141311EB9ACC2CFDA1251793', 'biro_umum', 'kabag2', '2020-10-22 03:20:02', 'Koordinasi/Konfirmasi dengan Ybs/Unit Kerja Terkait'),
(104, '7C84E709141A11EB9ACC2CFDA1251793', '30D0FB32141A11EB9ACC2CFDA1251793', 'kabag2', 'rektor', '2020-10-22 03:55:52', 'Untuk di ketahui dan di setujui permohonan pihak dinas sosial'),
(105, '7C856560141A11EB9ACC2CFDA1251793', '30D0FB32141A11EB9ACC2CFDA1251793', 'kabag2', 'rektor', '2020-10-22 03:55:52', 'Untuk Diketahui'),
(31, '7D338FB8110111EB846E2CFDA1251793', 'C395F91C110011EB846E2CFDA1251793', 'rektor', 'biro2', '2020-10-18 05:19:25', 'Lanjutkan pak biro'),
(79, '81438697140E11EB9ACC2CFDA1251793', 'E9827AD0140B11EB9ACC2CFDA1251793', 'kasubbag3', 'pelaksana10', '2020-10-22 02:30:06', 'jangan ada kesalahan'),
(78, '8143B8F3140E11EB9ACC2CFDA1251793', 'E9827AD0140B11EB9ACC2CFDA1251793', 'kasubbag3', 'pelaksana10', '2020-10-22 02:30:06', 'Agendakan/Persiapkan/Koordinasikan'),
(32, '8CD05077110111EB846E2CFDA1251793', 'C395F91C110011EB846E2CFDA1251793', 'biro2', 'kabag', '2020-10-18 05:19:51', 'Lanjutkan pak Kabag Umum'),
(102, '8E20A6AC141811EB9ACC2CFDA1251793', '06874106141811EB9ACC2CFDA1251793', 'kasubbag3', 'pelaksana9', '2020-10-22 03:42:03', 'jawab azizah'),
(101, '8E210B83141811EB9ACC2CFDA1251793', '06874106141811EB9ACC2CFDA1251793', 'kasubbag3', 'pelaksana9', '2020-10-22 03:42:03', 'Dijawab'),
(86, '8E465BA8141711EB9ACC2CFDA1251793', 'F3430781112D11EB846E2CFDA1251793', 'rektor', '', '2020-10-22 03:34:53', 'Telaah'),
(87, '8E46D709141711EB9ACC2CFDA1251793', 'F3430781112D11EB846E2CFDA1251793', 'rektor', '', '2020-10-22 03:34:53', 'Pelajari/Tela\'ah dan Berikan Saran/Masukan'),
(19, '91ADCE350E9B11EB8C442CFDA1251793', 'BBFDE6FE0E9411EB8C442CFDA1251793', 'kasubbag', 'staff', '2020-10-15 04:03:03', 'Laksanakan sesuai perintah, jangan ada yg miss komunikasi'),
(20, '93A495160E9B11EB8C442CFDA1251793', 'BBFDE6FE0E9411EB8C442CFDA1251793', 'kasubbag', 'staff', '2020-10-15 04:03:06', 'Laksanakan sesuai perintah, jangan ada yg miss komunikasi'),
(60, '978BA6F7135411EBA60A2CFDA1251793', '124E6302113C11EB846E2CFDA1251793', 'kabag2', 'kasubbag3', '2020-10-21 04:19:08', 'tes'),
(38, '9AAC4C16113111EB846E2CFDA1251793', 'F3430781112D11EB846E2CFDA1251793', 'kabag2', 'kasubbag3', '2020-10-18 11:03:52', 'Lanjutkan'),
(33, '9CE56C53110111EB846E2CFDA1251793', 'C395F91C110011EB846E2CFDA1251793', 'kabag', 'kasubbag', '2020-10-18 05:20:18', 'Lanjutkan Kasubbag TU'),
(53, 'A118A63F11D211EBA64D2CFDA1251793', '545335F011CD11EBA64D2CFDA1251793', 'kasubbag8', 'pelaksana20', '2020-10-19 06:16:26', 'Persiapkan surat permintaan calon ke fakultas'),
(114, 'A2C32BA8144011EBB8712CFDA1251793', 'A2C2F804144011EBB8712CFDA1251793', 'admin', 'kabag2', '2020-10-22 08:29:00', 'Sosialisasi Kartu Indonesia Pintar'),
(120, 'A7670FD21DEB11EBB5512CFDA1251793', '124E6302113C11EB846E2CFDA1251793', 'kasubbag3', '', '2020-11-03 15:45:52', 'ya'),
(119, 'A7673B141DEB11EBB5512CFDA1251793', '124E6302113C11EB846E2CFDA1251793', 'kasubbag3', '', '2020-11-03 15:45:52', 'Tindaklanjuti'),
(49, 'ABE3B56B11CD11EBA64D2CFDA1251793', '545335F011CD11EBA64D2CFDA1251793', 'kabag2', 'rektor', '2020-10-19 05:41:01', 'Mohon disposisi'),
(61, 'AC420334135411EBA60A2CFDA1251793', '545335F011CD11EBA64D2CFDA1251793', 'kabag2', '', '2020-10-21 04:19:43', 'tes'),
(122, 'AEB6C68D1DEB11EBB5512CFDA1251793', 'F3430781112D11EB846E2CFDA1251793', 'kasubbag3', 'pelaksana10', '2020-11-03 15:46:05', 'tes'),
(121, 'AEB6ED381DEB11EBB5512CFDA1251793', 'F3430781112D11EB846E2CFDA1251793', 'kasubbag3', 'pelaksana10', '2020-11-03 15:46:05', 'Tindaklanjuti'),
(10, 'B016D8EF0E8B11EB8C442CFDA1251793', '24D42E110B9611EBBB0B2CFDA1251793', 'rektor', 'biro2', '2020-10-15 02:09:22', 'TES Memo ke Biro\r\n'),
(34, 'B04DAD60110111EB846E2CFDA1251793', 'C395F91C110011EB846E2CFDA1251793', 'kasubbag', 'pelaksana', '2020-10-18 05:20:51', 'Laksanakan bapak pelaksana'),
(106, 'B15F80C3141A11EB9ACC2CFDA1251793', '30D0FB32141A11EB9ACC2CFDA1251793', 'rektor', 'biro_umum', '2020-10-22 03:57:21', 'Lanjutkan, dan pastikan tidak ada masalah sampai hari H'),
(107, 'B15FC8CD141A11EB9ACC2CFDA1251793', '30D0FB32141A11EB9ACC2CFDA1251793', 'rektor', 'biro_umum', '2020-10-22 03:57:21', 'Koordinasi/Konfirmasi dengan Ybs/Unit Kerja Terkait'),
(88, 'B6E6E552141711EB9ACC2CFDA1251793', 'D6CC6850141311EB9ACC2CFDA1251793', 'rektor', 'biro_umum', '2020-10-22 03:36:02', 'telaah'),
(89, 'B6E7823A141711EB9ACC2CFDA1251793', 'D6CC6850141311EB9ACC2CFDA1251793', 'rektor', 'biro_umum', '2020-10-22 03:36:02', 'Pelajari/Tela\'ah dan Berikan Saran/Masukan'),
(62, 'B8231A25135411EBA60A2CFDA1251793', '545335F011CD11EBA64D2CFDA1251793', 'kabag2', 'kasubbag3', '2020-10-21 04:20:03', 'tes'),
(21, 'BBA0A3FD0E9B11EB8C442CFDA1251793', 'BBFDE6FE0E9411EB8C442CFDA1251793', 'kasubbag', 'staff', '2020-10-15 04:04:13', 'Laksanakan'),
(13, 'BBFE0C0C0E9411EB8C442CFDA1251793', 'BBFDE6FE0E9411EB8C442CFDA1251793', 'staff', 'rektor', '2020-10-15 03:14:07', 'Ditujuan untuk seluruh ASN melakukan tes SWAB'),
(124, 'C08C59F51DEB11EBB5512CFDA1251793', 'F3430781112D11EB846E2CFDA1251793', 'kasubbag3', 'pelaksana10', '2020-11-03 15:46:34', 'yaaa'),
(123, 'C08C7EC81DEB11EBB5512CFDA1251793', 'F3430781112D11EB846E2CFDA1251793', 'kasubbag3', 'pelaksana10', '2020-11-03 15:46:34', 'Untuk Diketahui'),
(29, 'C3962731110011EB846E2CFDA1251793', 'C395F91C110011EB846E2CFDA1251793', 'admin', 'kabag', '2020-10-18 05:14:14', '-'),
(65, 'D3D635F6135611EBA60A2CFDA1251793', '545335F011CD11EBA64D2CFDA1251793', 'kabag2', '', '2020-10-21 04:35:08', 'TES'),
(80, 'D6CC9655141311EB9ACC2CFDA1251793', 'D6CC6850141311EB9ACC2CFDA1251793', 'admin', 'kabag2', '2020-10-22 03:08:17', 'From Dinas Sosial Kota Langsa'),
(16, 'D7B78AC40E9511EB8C442CFDA1251793', 'BBFDE6FE0E9411EB8C442CFDA1251793', 'biro2', 'kabag', '2020-10-15 03:22:03', 'Lanjutkan, lakukan dengan penerapan Protokol Kesehatan '),
(73, 'D9A97CD7140B11EB9ACC2CFDA1251793', 'D9A94C15140B11EB9ACC2CFDA1251793', 'admin', 'Pilih Disposisi', '2020-10-22 02:11:06', '-'),
(109, 'DB763F91141A11EB9ACC2CFDA1251793', '30D0FB32141A11EB9ACC2CFDA1251793', 'biro_umum', 'kabag2', '2020-10-22 03:58:31', 'Koordinasikan dengan pak herman untuk izin penggunaan gedung'),
(108, 'DB76898F141A11EB9ACC2CFDA1251793', '30D0FB32141A11EB9ACC2CFDA1251793', 'biro_umum', 'kabag2', '2020-10-22 03:58:31', 'Agendakan/Persiapkan/Koordinasikan'),
(41, 'E911A135113B11EB846E2CFDA1251793', 'E9117FDF113B11EB846E2CFDA1251793', 'admin', 'kabag2', '2020-10-18 12:17:38', 'Arahan untuk melakukan tes SWAB'),
(68, 'E95CFFE2140811EB9ACC2CFDA1251793', '545335F011CD11EBA64D2CFDA1251793', 'kabag2', 'kasubbag3', '2020-10-22 01:50:07', 'agendakan'),
(74, 'E982AD6B140B11EB9ACC2CFDA1251793', 'E9827AD0140B11EB9ACC2CFDA1251793', 'admin', 'kabag2', '2020-10-22 02:11:33', '-'),
(45, 'E9C6395B116B11EB846E2CFDA1251793', 'E9117FDF113B11EB846E2CFDA1251793', 'biro_akpk', 'kabag3', '2020-10-18 18:01:15', 'Lanjutkan Kabag Akademik'),
(40, 'ECFA93A1113A11EB846E2CFDA1251793', 'F3430781112D11EB846E2CFDA1251793', 'kabag2', 'rektor', '2020-10-18 12:10:35', 'teruskan'),
(14, 'F2A0F2F60E9411EB8C442CFDA1251793', 'BBFDE6FE0E9411EB8C442CFDA1251793', 'rektor', 'biro', '2020-10-15 03:15:39', 'Lakukan sesuai arahan dalam surat terlampir, dukung sepenuhnya fasilitas untuk mensukseskan agenda tersebut'),
(37, 'F3432A4B112D11EB846E2CFDA1251793', 'F3430781112D11EB846E2CFDA1251793', 'admin', 'kabag2', '2020-10-18 10:37:42', '-'),
(64, 'F752110F135511EBA60A2CFDA1251793', '545335F011CD11EBA64D2CFDA1251793', 'kabag2', 'kasubbag3', '2020-10-21 04:28:58', 'tes'),
(26, 'F84E9DD30E9E11EB8C442CFDA1251793', '0CEE81840E9E11EB8C442CFDA1251793', 'biro2', 'kabag', '2020-10-15 04:27:23', 'Lanjutkan pak, tolong pastikan fasilitas di sediakan'),
(22, 'FB3B9F0F0E9C11EB8C442CFDA1251793', 'BBFDE6FE0E9411EB8C442CFDA1251793', 'kasubbag', 'pelaksana', '2020-10-15 04:13:09', 'Tes memo Pelaksana'),
(7, 'wdw', '24D42E110B9611EBBB0B2CFDA1251793', 'staff3e', 'rektor', '2020-10-13 14:34:20', 'TES');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_staff`
--

CREATE TABLE `tbl_staff` (
  `staff_no` int(11) NOT NULL,
  `staff_uuid` varchar(32) NOT NULL,
  `staff_username` varchar(20) NOT NULL,
  `staff_nama` varchar(30) NOT NULL,
  `staff_posisi` varchar(55) NOT NULL,
  `staff_uuid_kasubbag` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tbl_staff`
--

INSERT INTO `tbl_staff` (`staff_no`, `staff_uuid`, `staff_username`, `staff_nama`, `staff_posisi`, `staff_uuid_kasubbag`) VALUES
(1, '1', 'pelaksana1', 'Bpk Bendahara Penerimaan', 'Bendahara Penerimaan', '1'),
(10, '10', 'pelaksana10', 'Aminah', 'Pengolah Data', '3'),
(11, '11', 'pelaksana11', 'Sabrina', 'Pengelola Barang Milik Negara', '4'),
(12, '12', 'pelaksana12', 'Maulana', 'Pengelola Data BMN', '4'),
(13, '13', 'pelaksana13', 'Gian Gumelar', 'Pengelola Kepegawaian', '5'),
(14, '14', 'pelaksana14', 'Rama Rinaldi', 'Pengadministrasi Kepegawaian', '5'),
(15, '15', 'pelaksana15', 'Ricky Maulana', 'Pengolah Data Akademik', '6'),
(16, '16', 'pelaksana16', 'Riko Alfianda', 'Pengelola Data Sarana Pendidikan', '6'),
(17, '17', 'pelaksana17', 'Cut Muna', 'Pengelola Informasi Akademik', '7'),
(18, '18', 'pelaksana18', 'Azlina', 'Pengelola Data Registrasi', '7'),
(19, '19', 'pelaksana19', 'Raudhatul Fajri', 'Pengelola Program Minat Bakat & Penalaran Mahasiswa', '8'),
(2, '2', 'pelaksana2', 'Roby Maulana', 'Bendahara Pengeluaran', '1'),
(20, '20', 'pelaksana20', 'Maulida Intan', 'Pengadministrasian Minat Bakat & Penalaran Mahasiswa', '8'),
(21, '21', 'pelaksana21', 'Rahmad Phareza', 'Pengelola Data Pelaksanaan Program dan Anggaran', '9'),
(22, '22', 'pelaksana22', 'Ary', 'Pengadministrasi Umum', '9'),
(23, '23', 'pelaksana23', 'Fadil', 'Pengelola Data Kerjasama', '10'),
(24, '24', 'pelaksana24', 'Amar', 'Pengadministrasi Umum', '10'),
(3, '3', 'pelaksana3', 'Antoni', 'Pengelola Gaji', '1'),
(4, '4', 'pelaksana4', 'Mirsal', 'Pengolah Data Keuangan', '1'),
(5, '5', 'pelaksana5', 'Suharman', 'Pengelola Surat Perintah Membayar', '1'),
(6, '6', 'pelaksana6', 'Rita Meutia', 'Penata Dokumen Keuangan', '1'),
(7, '7', 'pelaksana7', 'Rio Vamontra', 'Penyusun Laporan Keuangan', '2'),
(8, '8', 'pelaksana8', 'Yosfika', 'Pengelola Keuangan', '2'),
(9, '9', 'pelaksana9', 'Azizah', 'Pengadministrasi Data & Peraturan UU', '3');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_surat_keluar`
--

CREATE TABLE `tbl_surat_keluar` (
  `out_no` int(11) NOT NULL,
  `out_uuid` varchar(32) NOT NULL,
  `out_tujuan` varchar(30) NOT NULL,
  `out_nomorsurat` varchar(30) NOT NULL,
  `out_lampiran` varchar(20) NOT NULL,
  `out_perihal` varchar(40) NOT NULL,
  `out_penerbit` varchar(40) NOT NULL,
  `out_dokumeb` varchar(255) NOT NULL,
  `out_status` varchar(30) NOT NULL,
  `out_update_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `out_created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_unit`
--

CREATE TABLE `tbl_unit` (
  `ka_upt_no` int(11) NOT NULL,
  `ka_upt_uuid` varchar(32) NOT NULL,
  `ka_upt_username` varchar(10) NOT NULL,
  `ka_upt_nama` varchar(30) NOT NULL,
  `ka_upt_posisi` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tbl_unit`
--

INSERT INTO `tbl_unit` (`ka_upt_no`, `ka_upt_uuid`, `ka_upt_username`, `ka_upt_nama`, `ka_upt_posisi`) VALUES
(1, '1', 'ka_upt_tik', 'Bapak Ka UPT TIK', 'Ka. UPT TIK');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_users`
--

CREATE TABLE `tbl_users` (
  `username` varchar(16) NOT NULL,
  `nama_lengkap` varchar(30) NOT NULL,
  `jabatan` varchar(30) NOT NULL,
  `status` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tbl_users`
--

INSERT INTO `tbl_users` (`username`, `nama_lengkap`, `jabatan`, `status`) VALUES
('admin', 'Ilham Ramadhan', 'Admin TU', 'admin subbag tu');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `tbl_biro`
--
ALTER TABLE `tbl_biro`
  ADD PRIMARY KEY (`biro_username`),
  ADD UNIQUE KEY `biro_no` (`biro_no`),
  ADD UNIQUE KEY `biro_uuid` (`biro_uuid`);

--
-- Indeks untuk tabel `tbl_distribusi_surat`
--
ALTER TABLE `tbl_distribusi_surat`
  ADD PRIMARY KEY (`dist_uuid`),
  ADD UNIQUE KEY `dist_no` (`dist_no`);

--
-- Indeks untuk tabel `tbl_draft_surat`
--
ALTER TABLE `tbl_draft_surat`
  ADD PRIMARY KEY (`draft_uuid`),
  ADD UNIQUE KEY `draft_no` (`draft_no`);

--
-- Indeks untuk tabel `tbl_kabag`
--
ALTER TABLE `tbl_kabag`
  ADD PRIMARY KEY (`kabag_username`),
  ADD UNIQUE KEY `kabag_no` (`kabag_no`),
  ADD UNIQUE KEY `kabag_uuid` (`kabag_uuid`);

--
-- Indeks untuk tabel `tbl_kasubbag`
--
ALTER TABLE `tbl_kasubbag`
  ADD PRIMARY KEY (`kasubbag_username`),
  ADD UNIQUE KEY `kasubbag_no` (`kasubbag_no`),
  ADD UNIQUE KEY `kasubbag_uuid` (`kasubbag_uuid`);

--
-- Indeks untuk tabel `tbl_konsep_draft`
--
ALTER TABLE `tbl_konsep_draft`
  ADD PRIMARY KEY (`konsep_uuid`),
  ADD UNIQUE KEY `konsep_no` (`konsep_no`);

--
-- Indeks untuk tabel `tbl_login`
--
ALTER TABLE `tbl_login`
  ADD PRIMARY KEY (`username`);

--
-- Indeks untuk tabel `tbl_rektor`
--
ALTER TABLE `tbl_rektor`
  ADD PRIMARY KEY (`rektor_no`);

--
-- Indeks untuk tabel `tbl_riwayat_disposisi`
--
ALTER TABLE `tbl_riwayat_disposisi`
  ADD PRIMARY KEY (`riw_uuid`),
  ADD UNIQUE KEY `riw_no` (`riw_no`),
  ADD KEY `riw_dist_uuid` (`riw_dist_uuid`),
  ADD KEY `riw_asal` (`riw_asal`);

--
-- Indeks untuk tabel `tbl_staff`
--
ALTER TABLE `tbl_staff`
  ADD PRIMARY KEY (`staff_username`),
  ADD UNIQUE KEY `staff_no` (`staff_no`),
  ADD UNIQUE KEY `staff_uuid` (`staff_uuid`);

--
-- Indeks untuk tabel `tbl_surat_keluar`
--
ALTER TABLE `tbl_surat_keluar`
  ADD PRIMARY KEY (`out_uuid`),
  ADD UNIQUE KEY `out_no` (`out_no`);

--
-- Indeks untuk tabel `tbl_unit`
--
ALTER TABLE `tbl_unit`
  ADD PRIMARY KEY (`ka_upt_username`),
  ADD UNIQUE KEY `ka_upt_no` (`ka_upt_no`),
  ADD UNIQUE KEY `ka_upt_uuid` (`ka_upt_uuid`);

--
-- Indeks untuk tabel `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`username`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `tbl_biro`
--
ALTER TABLE `tbl_biro`
  MODIFY `biro_no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `tbl_distribusi_surat`
--
ALTER TABLE `tbl_distribusi_surat`
  MODIFY `dist_no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT untuk tabel `tbl_draft_surat`
--
ALTER TABLE `tbl_draft_surat`
  MODIFY `draft_no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `tbl_kabag`
--
ALTER TABLE `tbl_kabag`
  MODIFY `kabag_no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `tbl_kasubbag`
--
ALTER TABLE `tbl_kasubbag`
  MODIFY `kasubbag_no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT untuk tabel `tbl_konsep_draft`
--
ALTER TABLE `tbl_konsep_draft`
  MODIFY `konsep_no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT untuk tabel `tbl_rektor`
--
ALTER TABLE `tbl_rektor`
  MODIFY `rektor_no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `tbl_riwayat_disposisi`
--
ALTER TABLE `tbl_riwayat_disposisi`
  MODIFY `riw_no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=125;

--
-- AUTO_INCREMENT untuk tabel `tbl_staff`
--
ALTER TABLE `tbl_staff`
  MODIFY `staff_no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT untuk tabel `tbl_surat_keluar`
--
ALTER TABLE `tbl_surat_keluar`
  MODIFY `out_no` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `tbl_unit`
--
ALTER TABLE `tbl_unit`
  MODIFY `ka_upt_no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
