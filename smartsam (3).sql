-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 30 Des 2020 pada 20.55
-- Versi server: 10.4.10-MariaDB
-- Versi PHP: 7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `smartsam`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_biro`
--

CREATE TABLE `tbl_biro` (
  `biro_no` int(11) NOT NULL,
  `biro_uuid` varchar(32) NOT NULL,
  `biro_username` varchar(20) NOT NULL,
  `biro_nama` varchar(30) NOT NULL,
  `biro_posisi` varchar(40) NOT NULL,
  `biro_time` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tbl_biro`
--

INSERT INTO `tbl_biro` (`biro_no`, `biro_uuid`, `biro_username`, `biro_nama`, `biro_posisi`, `biro_time`) VALUES
(6, '87B6A8053A9611EBA5442CFDA1251793', 'biro111', 'ILHAM TES', 'Biro Umum & Keuangan', '2020-12-10 04:44:36'),
(2, '2', 'biro_akpk', 'Fahmi Rizal, S.H., M.M.', 'Biro AKPK', '2020-12-10 04:44:36'),
(1, '1', 'biro_umum', 'Ir. Adnan, M.M.', 'Biro Umum & Keuangan', '2020-12-10 04:44:36');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_distribusi_surat`
--

CREATE TABLE `tbl_distribusi_surat` (
  `dist_no` int(11) NOT NULL,
  `dist_uuid` varchar(32) NOT NULL,
  `dist_pengirim` varchar(30) NOT NULL,
  `dist_nomorsurat` varchar(30) NOT NULL,
  `dist_lampiran` varchar(20) NOT NULL,
  `dist_perihal` varchar(40) NOT NULL,
  `dist_penerima` varchar(40) NOT NULL,
  `dist_disposisi` varchar(40) NOT NULL COMMENT 'Current Disposisi',
  `dist_dokumen` varchar(255) NOT NULL,
  `dist_status` varchar(30) NOT NULL,
  `dist_updated_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `dist_tgl_terima` date NOT NULL,
  `dist_tgl_surat` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tbl_distribusi_surat`
--

INSERT INTO `tbl_distribusi_surat` (`dist_no`, `dist_uuid`, `dist_pengirim`, `dist_nomorsurat`, `dist_lampiran`, `dist_perihal`, `dist_penerima`, `dist_disposisi`, `dist_dokumen`, `dist_status`, `dist_updated_at`, `dist_tgl_terima`, `dist_tgl_surat`) VALUES
(26, '06874106141811EB9ACC2CFDA1251793', 'Universitas Malikussaleh', '002/UN45/VI/2020', '2', 'Permohonan Studi Banding', '', 'admin', 'Universitas_Malikussaleh-06874106141811EB9ACC2CFDA1251793.pdf', 'Done', '2020-10-22 03:38:15', '2020-10-22', '0000-00-00'),
(21, '124E6302113C11EB846E2CFDA1251793', 'DISKOMINFO Langsa', '0212/GN98/XI/IV/2020', '0', 'Pengintegrasian Sistem Informasi Terpadu', '', 'pelaksana10', 'DISKOMINFO_Langsa-124E6302113C11EB846E2CFDA1251793.pdf', 'Done', '2020-10-18 12:18:48', '2020-10-18', '0000-00-00'),
(33, '20D61C952DFD11EBB1F82CFDA1251793', 'DISKOMINFO LANGSA (TES SPI)', '0034/UN76/XI/2020', '1', 'Kunjungan kerja', '', 'sek_sp', 'DISKOMINFO_LANGSA_(TES_SPI)-20D61C952DFD11EBB1F82CFDA1251793.pdf', 'Done', '2020-11-24 02:31:16', '2020-11-24', '0000-00-00'),
(27, '30D0FB32141A11EB9ACC2CFDA1251793', 'Dinas Sosial Kota Langsa', '0932/UN34/IV/X/2020', '0', 'Permohonan Sosisialisasi Kartu Indonesia', '', 'kabag1', 'Dinas_Sosial_Kota_Langsa-30D0FB32141A11EB9ACC2CFDA1251793.pdf', 'Done', '2020-10-22 03:53:45', '2020-10-22', '0000-00-00'),
(31, '3ED974142BC411EBBEB92CFDA1251793', 'DISKOMINFO LANGSA (LAB DASAR)', '0034/UN76/XI/2020', '1', 'TES COBA', '', 'teknisi_labor', 'DISKOMINFO_LANGSA_(LAB_DASAR)-3ED974142BC411EBBEB92CFDA1251793.pdf', 'Done', '2020-11-21 06:38:59', '2020-11-21', '0000-00-00'),
(29, '4FD066302B5311EBBEB92CFDA1251793', 'DISKOMINFO LANGSA (UPT)', '0034/UN76/XI/2020', '2', 'Uji coba akun UPT TIK', '', 'uptik', 'DISKOMINFO_LANGSA_(UPT)-4FD066302B5311EBBEB92CFDA1251793.pdf', 'Process', '2020-11-20 17:10:38', '2020-11-21', '0000-00-00'),
(22, '545335F011CD11EBA64D2CFDA1251793', 'Dinas Sosial ', '402/GN98/XI/V/2020', '1', 'Kartu Indonesia Pintar', '', '', 'Dinas_Sosial_-545335F011CD11EBA64D2CFDA1251793.pdf', 'Done', '2020-10-19 05:38:34', '2020-10-19', '0000-00-00'),
(36, '585226D8477711EB96FD2CFDA1251793', 'Dinas Kehutanan Kota Langsa', '0034/UN76/XI/2020', '1', 'Kunjungan kerja', '', 'kalabbhs211', 'Dinas_Kehutanan_Kota_Langsa-585226D8477711EB96FD2CFDA1251793.pdf', 'Process', '2020-12-26 12:38:58', '2020-12-26', '0000-00-00'),
(32, '5E1E8C952CDE11EBBEB92CFDA1251793', 'DISKOMINFO LANGSA (LAB BAHASA)', '0034/UN76/XI/2020', '1', 'Kunjungan kerja', '', 'teknisilabbahasa', 'DISKOMINFO_LANGSA_(LAB_BAHASA)-5E1E8C952CDE11EBBEB92CFDA1251793.pdf', 'Done', '2020-11-22 16:18:25', '2020-11-22', '0000-00-00'),
(34, '6E6998203E8211EB83F62CFDA1251793', 'TES', '223/39/XII/2020', '2', 'TES SURAT MASUK', '', 'biro_umum', 'TES-6E6998203E8211EB83F62CFDA1251793.pdf', 'Process', '2020-12-15 03:05:37', '2020-12-15', '0000-00-00'),
(30, '72C5E4362B5611EBBEB92CFDA1251793', 'DISKOMINFO LANGSA (UPT)', '0034/UN76/XI/2020', '1', 'TES COBA', '', 'tik', 'DISKOMINFO_LANGSA_(UPT)-72C5E4362B5611EBBEB92CFDA1251793.pdf', 'Done', '2020-11-20 17:33:06', '2020-11-21', '0000-00-00'),
(37, '98084E50477711EB96FD2CFDA1251793', 'Dinas Kehutanan Kota Langsa', '0034/UN76/XI/2020', '1', 'Kunjungan kerja', '', 'pelaklabbhs222', 'Dinas_Kehutanan_Kota_Langsa-98084E50477711EB96FD2CFDA1251793.pdf', 'Done', '2020-12-26 12:40:45', '2020-12-26', '0000-00-00'),
(38, '9B729C1D477B11EB96FD2CFDA1251793', 'TES', '223/39/XII/2020', '4', 'TES SURAT KELUAR UPT TIK', '', 'biro_umum', 'TES-9B729C1D477B11EB96FD2CFDA1251793.pdf', 'Done', '2020-12-26 13:09:28', '2020-12-26', '0000-00-00'),
(28, 'A2C2F804144011EBB8712CFDA1251793', 'Dinas Sosial Kota Langsa', '002/UN45/VI/2020', '1', 'Sosialisasi Kartu Indonesia Pintar', '', 'pelaksana5', 'Dinas_Sosial_Kota_Langsa-A2C2F804144011EBB8712CFDA1251793.pdf', 'Done', '2020-10-22 08:29:00', '2020-10-22', '0000-00-00'),
(35, 'C4F3318D477611EB96FD2CFDA1251793', 'Dinas Kehutanan Kota Langsa', '0034/UN76/XI/2020', '1', 'Sosialisasi Program Penghijauan', '', '000011111', 'Dinas_Kehutanan_Kota_Langsa-C4F3318D477611EB96FD2CFDA1251793.pdf', 'Done', '2020-12-26 12:34:51', '2020-12-26', '0000-00-00'),
(25, 'D6CC6850141311EB9ACC2CFDA1251793', 'Dinas Sosial Kota Langsa', '003/UN65/IV/X/2020', '0', 'Sosialisasi Kartu Indonesia Pintar', '', 'pelaksana6', 'Dinas_Sosial_Kota_Langsa-D6CC6850141311EB9ACC2CFDA1251793.pdf', 'Process', '2020-10-22 03:08:17', '2020-10-22', '0000-00-00'),
(23, 'D9A94C15140B11EB9ACC2CFDA1251793', 'Universitas Malikussaleh', '002/UN45/VI/2020', '1', 'Permohonan Studi Banding', '', 'Pilih Disposisi', 'Universitas_Malikussaleh-D9A94C15140B11EB9ACC2CFDA1251793.pdf', 'Process', '2020-10-22 02:11:06', '2020-10-22', '0000-00-00'),
(20, 'E9117FDF113B11EB846E2CFDA1251793', 'Badan Kepegawaian Daerah', '322/GN98/XI/V/2020', '1', 'Tes Swab ASN Serentak', '', 'pelaksana16', 'Badan_Kepegawaian_Daerah-E9117FDF113B11EB846E2CFDA1251793.pdf', 'Process', '2020-10-18 12:17:38', '2020-10-18', '0000-00-00'),
(24, 'E9827AD0140B11EB9ACC2CFDA1251793', 'Universitas Malikussaleh', '002/UN45/VI/2020', '1', 'Permohonan Studi Banding', '', 'pelaksana10', 'Universitas_Malikussaleh-E9827AD0140B11EB9ACC2CFDA1251793.pdf', 'Done', '2020-10-22 02:11:33', '2020-10-22', '0000-00-00'),
(19, 'F3430781112D11EB846E2CFDA1251793', 'Badan Narkotika Nasional', '002/GN98/XI/V/2020', '1', 'Himbauan melakukan tes urin', '', 'pelaksana10', 'Badan_Narkotika_Nasional-F3430781112D11EB846E2CFDA1251793.pdf', 'Done', '2020-10-18 10:37:42', '2020-10-18', '0000-00-00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_draft_surat`
--

CREATE TABLE `tbl_draft_surat` (
  `draft_no` int(11) NOT NULL,
  `draft_uuid` varchar(32) NOT NULL,
  `draft_asal` varchar(30) NOT NULL,
  `draft_penerima` varchar(30) NOT NULL,
  `draft_perihal` varchar(40) NOT NULL,
  `draft_created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `draft_status` varchar(30) NOT NULL,
  `draft_ket` varchar(255) NOT NULL,
  `draft_read` varchar(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tbl_draft_surat`
--

INSERT INTO `tbl_draft_surat` (`draft_no`, `draft_uuid`, `draft_asal`, `draft_penerima`, `draft_perihal`, `draft_created_at`, `draft_status`, `draft_ket`, `draft_read`) VALUES
(7, '02531C7D2BA511EBBEB92CFDA1251793', 'pelaksana_tik1', '1324234', 'TES SURAT KELUAR UPT TIK', '2020-11-21 02:55:27', 'Done', '', '0'),
(5, '1194A66A1E0911EBB5512CFDA1251793', 'pelaksana9', '3', 'Perihal Pelaksana 9', '2020-11-03 19:16:26', 'Process', '', ''),
(6, '1C1918CE21E611EB8E842CFDA1251793', 'pelaksana10', '2234234', 'Sosialisasi Program Aplikasi SmartSam', '2020-11-08 17:16:02', 'Process', '', '0'),
(1, '1DFD19AD1D8D11EBB39D2CFDA1251793', 'pelaksana10', '3', 'TES', '2020-11-03 04:29:09', 'Process', '', ''),
(4, '48AB009E1DDB11EBB5512CFDA1251793', 'pelaksana10', '2234234', 'Kunjungan kerja', '2020-11-03 13:48:41', 'Done', '', ''),
(9, '5312EF742BDC11EBBEB92CFDA1251793', 'pelaksana10', '1', 'Sosialisasi Program Aplikasi SmartSam', '2020-11-21 09:31:16', 'Process', '', '0'),
(12, '557D5B763AC711EBA5442CFDA1251793', 'pelaksana1021', '11', 'tes', '2020-12-10 09:08:52', 'Process', '', '0'),
(14, '592FC1A1404211EB97F42CFDA1251793', 'pelaksana10', 'BBAD8CA0404111EB97F42CFDA12517', 'TES SURAT KELUAR', '2020-12-17 08:32:07', 'Process', '', '0'),
(2, '607B44EA1D8E11EBB39D2CFDA1251793', 'pelaksana10', '2', 'TES COBA', '2020-11-03 04:38:10', 'Process', '', ''),
(10, '75EB85152CDF11EBBEB92CFDA1251793', 'teknisilabbahasa', '1324234', 'Kunjungan kerja (LAB BAHASA)', '2020-11-22 16:26:14', 'Done', '', '0'),
(11, '7D306A492E2F11EBB1F82CFDA1251793', 'anggota_sp', '5', 'TES SURAT KELUAR SP', '2020-11-24 08:31:37', 'Process', '', '0'),
(13, '99BB8625403F11EB97F42CFDA1251793', 'pelaksana10', '2', 'Permohonan Studi Banding dari UNIMAL', '2020-12-17 08:12:27', 'Process', '', '0'),
(3, '9B6206141DA411EBB39D2CFDA1251793', 'pelaksana10', '2', 'TES', '2020-11-03 07:17:18', 'Process', '', ''),
(8, 'FEF9F97F2BC411EBBEB92CFDA1251793', 'teknisi_labor', '1324234', 'Tes Ajukan Surat Keluar By Lab Dasar', '2020-11-21 06:44:21', 'Done', '', '0');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_kabag`
--

CREATE TABLE `tbl_kabag` (
  `kabag_no` int(11) NOT NULL,
  `kabag_uuid` varchar(32) NOT NULL,
  `kabag_username` varchar(20) NOT NULL,
  `kabag_nama` varchar(30) NOT NULL,
  `kabag_posisi` varchar(40) NOT NULL,
  `kabag_uuid_biro` varchar(32) NOT NULL,
  `kabag_time` timestamp NOT NULL DEFAULT current_timestamp(),
  `kabag_kategori` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tbl_kabag`
--

INSERT INTO `tbl_kabag` (`kabag_no`, `kabag_uuid`, `kabag_username`, `kabag_nama`, `kabag_posisi`, `kabag_uuid_biro`, `kabag_time`, `kabag_kategori`) VALUES
(8, 'BD723C31386211EBA0FF2CFDA1251793', 'Ilham5089', 'Ilham Ramadhan M.Kom Phd', '1', '1', '2020-12-10 04:43:58', 'biro'),
(1, '1', 'kabag1', 'Anwar A., S.P., M.M.', 'Kabag Keuangan', '1', '2020-12-10 04:43:58', 'biro'),
(28, 'BBAD8CA0404111EB97F42CFDA1251793', 'kabag1000', 'Ilham Ramadhan', 'Kepala Bagian Umum', '1', '2020-12-17 08:27:43', 'biro'),
(24, '2', 'kabag121', 'ADITYA', 'Kepala Bagian Umum', '1', '2020-12-15 03:04:43', 'biro'),
(13, 'C64086DC3A9611EBA5442CFDA1251793', 'kabag222', 'TES ILHAM 1 BARU TES LAGI', 'Kepala Bagian Akademik dan Kemahasiswaan', '21D467553B0711EBA5442CFDA1251793', '2020-12-10 04:43:58', 'biro'),
(17, 'CCE76C0A3AA111EBA5442CFDA1251793', 'kabag2222', 'ILHAM TES KABAG', 'Kepala Bagian Umum', '87B6A8053A9611EBA5442CFDA1251793', '2020-12-10 04:43:58', 'biro'),
(3, '3', 'kabag3', 'Abdul Hamid, M.T.', 'Kabag Akademik', '2', '2020-12-10 04:43:58', 'biro'),
(5, '5', 'kabag_sp', 'Drs. Muhammad Yakob, M.Pd.', 'Ka SP', '0', '2020-12-10 04:43:58', 'spi'),
(19, '6026A8573AFE11EBA5442CFDA1251793', 'kasp2323', 'Ilham Ramadha, S.Tr.Kom', 'Ka SP', '0', '2020-12-10 15:42:52', 'spi'),
(20, '2F5DD3903B0711EBA5442CFDA1251793', 'tes22', 'TES ILHAM', 'Kepala Bagian Perencanaan dan Kerja sama', '8CF8FD0337E011EB87212CFDA1251793', '2020-12-10 16:45:56', ''),
(15, '918EBF8B3A9B11EBA5442CFDA1251793', 'teskabag', 'TES ILHAM KABAG', 'Kepala Bagian Keuangan', '87B6A8053A9611EBA5442CFDA1251793', '2020-12-10 04:43:58', 'biro'),
(16, 'CF801A343A9B11EBA5442CFDA1251793', 'teskabag1', 'Ilham Ramadhan TES', 'Kepala Bagian Akademik dan Kemahasiswaan', '82D192EC37E011EB87212CFDA1251793', '2020-12-10 04:43:58', 'biro'),
(10, 'F883ACC1386211EBA0FF2CFDA1251793', 'uyduiwydwd', 'Ilham Ramadha, S.Tr.Kom', 'Kepala Bagian Perencanaan dan Kerja sama', '82D192EC37E011EB87212CFDA1251793', '2020-12-10 04:43:58', 'biro');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_kasubbag`
--

CREATE TABLE `tbl_kasubbag` (
  `kasubbag_no` int(11) NOT NULL,
  `kasubbag_uuid` varchar(32) NOT NULL,
  `kasubbag_username` varchar(16) NOT NULL,
  `kasubbag_nama` varchar(40) NOT NULL,
  `kasubbag_posisi` varchar(40) NOT NULL,
  `kasubbag_uuid_kabag` varchar(32) NOT NULL,
  `kasubbag_time` timestamp NOT NULL DEFAULT current_timestamp(),
  `kasubbag_kategori` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tbl_kasubbag`
--

INSERT INTO `tbl_kasubbag` (`kasubbag_no`, `kasubbag_uuid`, `kasubbag_username`, `kasubbag_nama`, `kasubbag_posisi`, `kasubbag_uuid_kabag`, `kasubbag_time`, `kasubbag_kategori`) VALUES
(16, 'CC1AC29F392D11EBB57E2CFDA1251793', '2233', 'TES', 'Sekretaris SP', '9E0E790338F711EBA0FF2CFDA1251793', '2020-12-10 04:41:15', 'spi'),
(28, '0EBCAC743B9011EBA5442CFDA1251793', 'kalabbhs211', 'Ilham Ramadha, S.Tr.Kom', 'Ka Lab Bahasa', '0', '2020-12-11 09:05:39', 'upt lab bahasa'),
(12, '12', 'kalabdas', 'Teuku Hasan Basri, S.Pd., M.Pd.', 'Ka Lab Dasar', '0', '2020-12-10 04:41:15', 'upt lab dasar'),
(27, 'A6F096D03B5C11EBA5442CFDA1251793', 'kalabdas21', 'ILHAM1', 'Ka Lab Dasar', '0', '2020-12-11 02:57:39', 'upt lab dasar'),
(1, '1', 'kasubbag1', 'Junaidi, S.Pd.', 'Kasubbag Perbendaharaan', '1', '2020-12-10 04:41:15', 'biro'),
(10, '10', 'kasubbag10', 'Muammar Khalis, S.Sos.', 'Kasubbag Kerjasama & Humas', '4', '2020-12-10 04:41:15', 'biro'),
(21, 'DDF08B093AA011EBA5442CFDA1251793', 'kasubbag211', 'TES ILHAM 22', 'Kasubbag Kerjasama & Humas', 'F883ACC1386211EBA0FF2CFDA1251793', '2020-12-10 04:41:15', 'biro'),
(22, '9A8635303AA211EBA5442CFDA1251793', 'kasubbag223', 'Ilham Ramadhan', 'Kasubbag TU & Ketatalaksanaan', '918EBF8B3A9B11EBA5442CFDA1251793', '2020-12-10 04:46:01', 'biro'),
(3, '3', 'kasubbag3', 'Hendra Rahayu, S.Hut.', 'Kasubbag TU & Ketatalaksanaan', '3', '2020-12-10 04:41:15', 'biro'),
(4, '4', 'kasubbag4', 'Muhammad Nur, S.P.', 'Kasubbag RT & BM', '2', '2020-12-10 04:41:15', 'biro'),
(5, '5', 'kasubbag5', 'Tarmizi, S.E., M.S.M.', 'Kasubbag Kepegawaian', '2', '2020-12-10 04:41:15', 'biro'),
(6, '6', 'kasubbag6', 'Budiansyah Daulay, S.E.', 'Kasubbag Pendidikan & Evaluasi', '3', '2020-12-10 04:41:15', 'biro'),
(7, '7', 'kasubbag7', 'Chairul Fuad, S.P.', 'Kasubbag Registrasi & Statistik', '3', '2020-12-10 04:41:15', 'biro'),
(8, '8', 'kasubbag8', 'Azizah, S.Pd., M.Pd.', 'Kasubbag Kemahasiswaan', '3', '2020-12-10 04:41:15', 'biro'),
(9, '9', 'kasubbag9', 'Dianawati, S.P., M.P.', 'Kasubbag Perencanaan', '0', '2020-12-10 04:41:15', 'biro'),
(18, 'BAE9AB2D3A9F11EBA5442CFDA1251793', 'kasubbagtes22', 'ILHAM TES KASUBBAG', 'Kasubbag TU & Ketatalaksanaan', '2', '2020-12-10 04:41:15', 'biro'),
(19, '50C448043AA011EBA5442CFDA1251793', 'kasubbagtes222', 'TES ILHAM', 'Kasubbag TU & Ketatalaksanaan', '2', '2020-12-10 04:41:15', 'biro'),
(20, '6697F0463AA011EBA5442CFDA1251793', 'kasubbagtes333', 'TES ILHAM', 'Kasubbag TU & Ketatalaksanaan', '2', '2020-12-10 04:41:15', 'biro'),
(11, '11', 'kaupttik', 'Munawir, S.ST., M.T.', 'Ka UPT TIK', '0', '2020-12-10 04:41:15', 'upt tik'),
(23, '6C6E0A603AC811EBA5442CFDA1251793', 'kaupttik222', 'Ilham Ramadha, S.Tr.Kom', 'Ka UPT TIK', '0', '2020-12-10 09:16:40', 'upt tik'),
(17, '1F7D0C2A3A9811EBA5442CFDA1251793', 'seksp111', 'ILHAM TES SEK', 'Sekretaris SP', 'E18D36C23A9711EBA5442CFDA1251793', '2020-12-10 04:41:15', 'spi'),
(15, '98CD8FA5392B11EBB57E2CFDA1251793', 'sekspi32', 'CUT ELIDAR, S.H., M.H.', 'Sekretaris SP', '5', '2020-12-10 04:41:15', 'spi'),
(14, '14', 'sek_sp', 'Cut Elidar, S.H., M.H.', 'Sekretaris SP', '5', '2020-12-10 04:41:15', 'spi'),
(25, '915473983B0711EBA5442CFDA1251793', 'tes11222', 'Ilham Ramadha, S.Tr.Kom', 'Kasubbag Akuntansi & Pelaporan', '8847D29B3AA211EBA5442CFDA1251793', '2020-12-10 16:48:40', ''),
(24, '58DAE50C3AFE11EBA5442CFDA1251793', 'tesseksp', 'TES ILHAM A', 'Sekretaris SP', '9E0E790338F711EBA0FF2CFDA1251793', '2020-12-10 15:42:40', 'spi');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_kode_hal`
--

CREATE TABLE `tbl_kode_hal` (
  `hal_no` int(11) NOT NULL,
  `hal_nama` varchar(30) NOT NULL,
  `hal_kode` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tbl_kode_hal`
--

INSERT INTO `tbl_kode_hal` (`hal_no`, `hal_nama`, `hal_kode`) VALUES
(1, 'Akreditasi', 'AK'),
(2, 'Banttuan Pendidikan', 'BP');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_kode_instansi`
--

CREATE TABLE `tbl_kode_instansi` (
  `instansi_no` int(11) NOT NULL,
  `instansi_nama` varchar(30) NOT NULL,
  `instansi_kode` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tbl_kode_instansi`
--

INSERT INTO `tbl_kode_instansi` (`instansi_no`, `instansi_nama`, `instansi_kode`) VALUES
(1, 'Biro', 'UN45'),
(2, 'UPTK TIK', 'UN45.8');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_konsep_draft`
--

CREATE TABLE `tbl_konsep_draft` (
  `konsep_no` int(11) NOT NULL,
  `konsep_uuid` varchar(32) NOT NULL,
  `konsep_uuid_draft` varchar(32) NOT NULL,
  `konsep_asal` varchar(30) NOT NULL,
  `konsep_tujuan` varchar(30) NOT NULL,
  `konsep_isi` text DEFAULT 'Tidak ada koreksi',
  `konsep_updated_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `konsep_author` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tbl_konsep_draft`
--

INSERT INTO `tbl_konsep_draft` (`konsep_no`, `konsep_uuid`, `konsep_uuid_draft`, `konsep_asal`, `konsep_tujuan`, `konsep_isi`, `konsep_updated_at`, `konsep_author`) VALUES
(30, '0253408C2BA511EBBEB92CFDA1251793', '02531C7D2BA511EBBEB92CFDA1251793', 'pelaksana_tik1', '11', 'TES\r\n', '2020-11-21 02:55:27', 'pelaksana_tik1'),
(21, '04805B161E1011EBB5512CFDA1251793', '48AB009E1DDB11EBB5512CFDA1251793', 'pelaksana10', '1', '', '2020-11-03 20:06:10', 'biro_umum'),
(48, '0EC8AB052CD711EBBEB92CFDA1251793', 'FEF9F97F2BC411EBBEB92CFDA1251793', 'teknisi_labor', '0', 'KA LAB DAsar', '2020-11-22 15:26:05', 'kalabdas'),
(19, '119518F71E0911EBB5512CFDA1251793', '1194A66A1E0911EBB5512CFDA1251793', 'pelaksana9', '3', 'TES ISI SURAT', '2020-11-03 19:16:26', 'pelaksana9'),
(26, '1C194FCB21E611EB8E842CFDA1251793', '1C1918CE21E611EB8E842CFDA1251793', 'pelaksana10', '3', 'TES ISI', '2020-11-08 17:16:02', 'pelaksana10'),
(33, '20457B822BA711EBBEB92CFDA1251793', '02531C7D2BA511EBBEB92CFDA1251793', 'pelaksana_tik1', '2', 'tes', '2020-11-21 03:10:36', 'kaupttik'),
(37, '21150E122BB011EBBEB92CFDA1251793', '02531C7D2BA511EBBEB92CFDA1251793', 'pelaksana_tik1', '2', NULL, '2020-11-21 04:15:03', 'kaupttik'),
(9, '337D643D1DE211EBB5512CFDA1251793', '48AB009E1DDB11EBB5512CFDA1251793', 'pelaksana10', '3', 'Perihal di ganti saja menjadi\r\n\r\nPerihal : Kunjungan kerja bersama Dinkes\r\n\r\nBody:\r\nSehubungan dengan surat permohonan yang bapak ibu sampaikan. maka kami akan dengan senang hati untuk menyambut kedatangan bapak ibu dalam rangka kunjunga kerja.\r\n\r\nDemikian, Assalam\r\n', '2020-11-03 14:38:12', 'pelaksana10'),
(58, '373C2E64404011EB97F42CFDA1251793', '99BB8625403F11EB97F42CFDA1251793', 'pelaksana10', '2', 'Tambahan:\r\nTerkait permohonan studi banding saudara, maka saudara diberikan izin dengan nomor xxxxx\r\n', '2020-12-17 08:16:51', 'kasubbag3'),
(38, '385891122BB011EBBEB92CFDA1251793', '02531C7D2BA511EBBEB92CFDA1251793', 'pelaksana_tik1', '-', 'TES TERSUKAN KE REKTOR 1', '2020-11-21 04:15:42', 'kaupttik'),
(27, '391EE4B521E611EB8E842CFDA1251793', '1C1918CE21E611EB8E842CFDA1251793', 'pelaksana10', '2', 'Isinya harus gunakan EYD', '2020-11-08 17:16:51', 'kasubbag3'),
(22, '429DAE371E1011EBB5512CFDA1251793', '48AB009E1DDB11EBB5512CFDA1251793', 'pelaksana10', '2', '', '2020-11-03 20:07:55', 'biro_umum'),
(3, '48AB32051DDB11EBB5512CFDA1251793', '48AB009E1DDB11EBB5512CFDA1251793', 'pelaksana10', '3', 'Kunjungan kerja', '2020-11-03 13:48:41', 'pelaksana10'),
(44, '531352322BDC11EBBEB92CFDA1251793', '5312EF742BDC11EBBEB92CFDA1251793', 'pelaksana10', '3', 'Sehubungan dengan blablablablablablablabla\r\nblablablablabla', '2020-11-21 09:31:16', 'pelaksana10'),
(56, '557DB3113AC711EBA5442CFDA1251793', '557D5B763AC711EBA5442CFDA1251793', 'pelaksana1021', '11', 'tes\r\n', '2020-12-10 09:08:52', 'pelaksana1021'),
(28, '5879275821E611EB8E842CFDA1251793', '1C1918CE21E611EB8E842CFDA1251793', 'pelaksana10', '1', 'udah oke', '2020-11-08 17:17:44', 'kabag2'),
(59, '592FE6E2404211EB97F42CFDA1251793', '592FC1A1404211EB97F42CFDA1251793', 'pelaksana10', '3', 'TES', '2020-12-17 08:32:07', 'pelaksana10'),
(1, '607B68741D8E11EBB39D2CFDA1251793', '607B44EA1D8E11EBB39D2CFDA1251793', 'pelaksana10', '3', 'TES COBA', '2020-11-03 04:38:10', 'pelaksana10'),
(31, '668CF84A2BA611EBBEB92CFDA1251793', '02531C7D2BA511EBBEB92CFDA1251793', 'pelaksana_tik1', '0', 'Lanjut ke Biro Umum', '2020-11-21 03:05:24', 'kaupttik'),
(16, '6E830D951DF111EBB5512CFDA1251793', '9B6206141DA411EBB39D2CFDA1251793', 'pelaksana10', '2', 'COBA DRAFT 3', '2020-11-03 16:27:14', 'kasubbag3'),
(23, '6FF02B321E1011EBB5512CFDA1251793', '48AB009E1DDB11EBB5512CFDA1251793', 'pelaksana10', '1', '', '2020-11-03 20:09:11', 'biro_umum'),
(17, '6FF986601DF211EBB5512CFDA1251793', '607B44EA1D8E11EBB39D2CFDA1251793', 'pelaksana10', '2', 'Merah', '2020-11-03 16:34:26', 'kasubbag3'),
(4, '7438A4461DE011EBB5512CFDA1251793', '48AB009E1DDB11EBB5512CFDA1251793', 'pelaksana10', '3', '', '2020-11-03 14:25:42', 'pelaksana10'),
(42, '75A444062BC111EBBEB92CFDA1251793', '02531C7D2BA511EBBEB92CFDA1251793', 'pelaksana_tik1', '-', 't', '2020-11-21 06:19:02', 'kaupttik'),
(51, '75EC02492CDF11EBBEB92CFDA1251793', '75EB85152CDF11EBBEB92CFDA1251793', 'teknisilabbahasa', '13', 'TES\r\n', '2020-11-22 16:26:14', 'teknisilabbahasa'),
(29, '7A3D101721E611EB8E842CFDA1251793', '1C1918CE21E611EB8E842CFDA1251793', 'pelaksana10', '2234234', 'Lanjutkan', '2020-11-08 17:18:40', 'biro_umum'),
(60, '7B8BEF12404211EB97F42CFDA1251793', '592FC1A1404211EB97F42CFDA1251793', 'pelaksana10', 'BBAD8CA0404111EB97F42CFDA12517', 'TES LAGI', '2020-12-17 08:33:04', 'kasubbag3'),
(54, '7D3095492E2F11EBB1F82CFDA1251793', '7D306A492E2F11EBB1F82CFDA1251793', 'anggota_sp', '14', 'TES', '2020-11-24 08:31:37', 'anggota_sp'),
(39, '861B5F362BB011EBBEB92CFDA1251793', '02531C7D2BA511EBBEB92CFDA1251793', 'pelaksana_tik1', '-', 'TERUSKAN KE REKTOR 11111', '2020-11-21 04:17:52', 'kaupttik'),
(49, '9367269F2CD711EBBEB92CFDA1251793', 'FEF9F97F2BC411EBBEB92CFDA1251793', 'teknisi_labor', '1324234', 'TES KE REKTOR dr Ka lAB DASAR', '2020-11-22 15:29:47', 'kalabdas'),
(25, '9814A51C1E1611EBB5512CFDA1251793', '48AB009E1DDB11EBB5512CFDA1251793', 'pelaksana10', '-', 'BY REKTOR', '2020-11-03 20:53:15', 'rektor3'),
(57, '99BBABD0403F11EB97F42CFDA1251793', '99BB8625403F11EB97F42CFDA1251793', 'pelaksana10', '3', 'Terkait permohonan studi banding saudara, maka ......... dst.', '2020-12-17 08:12:27', 'pelaksana10'),
(2, '9B623CB71DA411EBB39D2CFDA1251793', '9B6206141DA411EBB39D2CFDA1251793', 'pelaksana10', '3', 'TES', '2020-11-03 07:17:18', 'pelaksana10'),
(45, 'A12007502BDC11EBBEB92CFDA1251793', '5312EF742BDC11EBBEB92CFDA1251793', 'pelaksana10', '2', NULL, '2020-11-21 09:33:27', 'kasubbag3'),
(50, 'A32F35902CD711EBBEB92CFDA1251793', 'FEF9F97F2BC411EBBEB92CFDA1251793', 'teknisi_labor', '-', 'OKE ', '2020-11-22 15:30:14', 'kalabdas'),
(24, 'A335B8681E1011EBB5512CFDA1251793', '48AB009E1DDB11EBB5512CFDA1251793', 'pelaksana10', '2234234', '', '2020-11-03 20:10:37', 'biro_umum'),
(52, 'A7D2206C2CDF11EBBEB92CFDA1251793', '75EB85152CDF11EBBEB92CFDA1251793', 'teknisilabbahasa', '1324234', 'teruskan ke rektor', '2020-11-22 16:27:38', 'kalabahasa'),
(40, 'AD8AEF3B2BB011EBBEB92CFDA1251793', '02531C7D2BA511EBBEB92CFDA1251793', 'pelaksana_tik1', '1324234', 'LAGI', '2020-11-21 04:18:59', 'kaupttik'),
(34, 'AF68144A2BAE11EBBEB92CFDA1251793', '02531C7D2BA511EBBEB92CFDA1251793', 'pelaksana_tik1', '-', 'TES DONE', '2020-11-21 04:04:43', 'kaupttik'),
(32, 'B3908E372BA611EBBEB92CFDA1251793', '02531C7D2BA511EBBEB92CFDA1251793', 'pelaksana_tik1', '0', 'lanjut ke biro\r\n', '2020-11-21 03:07:34', 'kaupttik'),
(5, 'B5F33BE81DE011EBB5512CFDA1251793', '48AB009E1DDB11EBB5512CFDA1251793', 'pelaksana10', '3', 'TESSSS', '2020-11-03 14:27:32', 'pelaksana10'),
(10, 'B7004DC01DEF11EBB5512CFDA1251793', '48AB009E1DDB11EBB5512CFDA1251793', 'pelaksana10', '2', 'Kasubbag Tes', '2020-11-03 16:14:56', 'pelaksana10'),
(46, 'B7725E912BDC11EBBEB92CFDA1251793', '5312EF742BDC11EBBEB92CFDA1251793', 'pelaksana10', '2', NULL, '2020-11-21 09:34:04', 'kasubbag3'),
(14, 'BAD743EB1DF011EBB5512CFDA1251793', '48AB009E1DDB11EBB5512CFDA1251793', 'pelaksana10', '2', 'TESS', '2020-11-03 16:22:12', 'kasubbag3'),
(53, 'BADF88F72CDF11EBBEB92CFDA1251793', '75EB85152CDF11EBBEB92CFDA1251793', 'teknisilabbahasa', '-', 'Done\r\n', '2020-11-22 16:28:10', 'kalabahasa'),
(11, 'BAF3F59C1DEF11EBB5512CFDA1251793', '48AB009E1DDB11EBB5512CFDA1251793', 'pelaksana10', '2', 'tes', '2020-11-03 16:15:03', 'pelaksana10'),
(12, 'C3FF55BA1DEF11EBB5512CFDA1251793', '48AB009E1DDB11EBB5512CFDA1251793', 'pelaksana10', '2', 'Kasubbag', '2020-11-03 16:15:18', 'pelaksana10'),
(15, 'C5562D3C1DF011EBB5512CFDA1251793', '48AB009E1DDB11EBB5512CFDA1251793', 'pelaksana10', '2', 'TESS', '2020-11-03 16:22:30', 'kasubbag3'),
(20, 'CE71E29C1E0F11EBB5512CFDA1251793', '48AB009E1DDB11EBB5512CFDA1251793', 'pelaksana10', '1', '', '2020-11-03 20:04:40', 'biro_umum'),
(41, 'D3E3B08B2BB011EBBEB92CFDA1251793', '02531C7D2BA511EBBEB92CFDA1251793', 'pelaksana_tik1', '-', 'SELESAI AKSI', '2020-11-21 04:20:03', 'rektor'),
(18, 'D7B10F651E0811EBB5512CFDA1251793', '48AB009E1DDB11EBB5512CFDA1251793', 'pelaksana10', '1', 'OKE by Kabag', '2020-11-03 19:14:49', 'kabag2'),
(36, 'DA34C0C32BAF11EBBEB92CFDA1251793', '02531C7D2BA511EBBEB92CFDA1251793', 'pelaksana_tik1', '2', NULL, '2020-11-21 04:13:04', 'kaupttik'),
(13, 'E1E915671DEF11EBB5512CFDA1251793', '48AB009E1DDB11EBB5512CFDA1251793', 'pelaksana10', '2', 'tesssss afaaa', '2020-11-03 16:16:08', 'kasubbag3'),
(35, 'E2CCB1DC2BAE11EBBEB92CFDA1251793', '02531C7D2BA511EBBEB92CFDA1251793', 'pelaksana_tik1', '-', 'SELESAI TIK\r\n', '2020-11-21 04:06:09', 'kaupttik'),
(47, 'E7018A212BDC11EBBEB92CFDA1251793', '5312EF742BDC11EBBEB92CFDA1251793', 'pelaksana10', '1', 'Revisi:\r\n\r\nSehubungan dengan akan diterapkannya aplikasi smartsam , maka diharapkan semua mengikuti sosialisasi tsb', '2020-11-21 09:35:24', 'kabag2'),
(6, 'E7438DEF1DE011EBB5512CFDA1251793', '48AB009E1DDB11EBB5512CFDA1251793', 'pelaksana10', '3', 'teess aaa', '2020-11-03 14:28:55', 'pelaksana10'),
(55, 'E9E59BF82F8E11EB93972CFDA1251793', '7D306A492E2F11EBB1F82CFDA1251793', 'anggota_sp', '5', 'LANJUT KE KABAG SP BY SEKRETARIS SP', '2020-11-26 02:27:14', 'sek_sp'),
(7, 'F407FF051DE011EBB5512CFDA1251793', '48AB009E1DDB11EBB5512CFDA1251793', 'pelaksana10', '3', 'teess aaa bbbb', '2020-11-03 14:29:16', 'pelaksana10'),
(8, 'FAB7C2771DE011EBB5512CFDA1251793', '48AB009E1DDB11EBB5512CFDA1251793', 'pelaksana10', '3', ' awd awdawdawda  awd awdawdawda  awd awdawdawda  awd awdawdawda  awd awdawdawda  awd awdawdawda  awd awdawdawda  awd awdawdawda  awd awdawdawda  awd awdawdawda  awd awdawdawda  awd awdawdawda  awd awdawdawda  awd awdawdawda  awd awdawdawda  awd awdawdawda  awd awdawdawda  awd awdawdawda  awd awdawdawda  awd awdawdawda  awd awdawdawda  awd awdawdawda  awd awdawdawda  awd awdawdawda  awd awdawdawda  awd awdawdawda  awd awdawdawda  awd awdawdawda  awd awdawdawda  awd awdawdawda  awd awdawdawda  awd awdawdawda  awd awdawdawda  awd awdawdawda  awd awdawdawda  awd awdawdawda  awd awdawdawda  awd awdawdawda ', '2020-11-03 14:29:28', 'pelaksana10'),
(43, 'FEFA3AE02BC411EBBEB92CFDA1251793', 'FEF9F97F2BC411EBBEB92CFDA1251793', 'teknisi_labor', '12', 'tes', '2020-11-21 06:44:21', 'teknisi_labor');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_login`
--

CREATE TABLE `tbl_login` (
  `username` varchar(16) NOT NULL,
  `stts` varchar(20) NOT NULL,
  `password` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tbl_login`
--

INSERT INTO `tbl_login` (`username`, `stts`, `password`) VALUES
('1111111111', 'kabag', 'e11170b8cbd2d74102651cb967fa28e5'),
('1234', 'pelaksana', '81dc9bdb52d04dc20036dbd8313ed055'),
('2222', 'rektor', '934b535800b1cba8f96a5d72f72f1611'),
('23323', 'kabag', 'b35b31a24acc2da3bd9e3feb30fc7e79'),
('244', 'biro', '9188905e74c28e489b44e954ec0b9bca'),
('admin', 'admin surat', '21232f297a57a5a743894a0e4a801fc3'),
('anggota_sp', 'pelaksana', '48dbf2b9ac3bc6653ea1ff029d01a791'),
('biro111', 'biro', '8817d2a146d928c46a339195b9e2797c'),
('biro_akpk', 'biro', '0959631689d0a2a92a8c3ee6722e02f6'),
('biro_umum', 'biro', '28abd1f04d287eeb1321dd6332b0ee11'),
('kabag1', 'kabag', '485ec531822b216a859ebe9151b715f6'),
('kabag1000', 'kabag', 'e0d4766a4ac3f6cfe64426a800718f83'),
('kabag121', 'kabag', '9a46637919a60d352ed42f3f8b4e5dad'),
('kabag1211', 'kabag', 'adab0f8eabbb1120177a2e8cce2b1876'),
('kabag2', 'kabag', '81d868eb7a14df728173e1ee147ea5fd'),
('kabag222', 'kabag', '40c71b93c63e33b9a429874bb9458ff5'),
('kabag2222', 'kabag', 'e28ffeb34b5290fc7f89fc0c06004d1c'),
('kabag_sp', 'kabag', '26624757725d7021f33ea11fd9e024cb'),
('kalabbhs211', 'kasubbag', '69f5abcc933746c40fb5ff8b508fbf23'),
('kalabdas', 'kasubbag', 'ab2067ebdd6d7dae30f69b17b37842dc'),
('kalabdas21', 'kasubbag', '55eabd11660677be66a392a9f2052ae8'),
('kasp2323', 'kabag', '10540eeaace1fa83dae47f75c3a63a20'),
('kasubbag1', 'kasubbag', '1a27f515cd7cd58d5b5248ab66a295e8'),
('kasubbag10', 'kasubbag', 'f583ea4ad4a11eaebcb2d9531bd8b92d'),
('kasubbag2', 'kasubbag', '4480b2610a01e277f947c6da1ea6588d'),
('kasubbag211', 'kasubbag', '3a8f2c4921cee0e214a35ce05cc11a75'),
('kasubbag223', 'kasubbag', '014b702ff66b00757523b20fff23dc71'),
('kasubbag3', 'kasubbag', '5b18490a44d521fb9b12de44cd2decff'),
('kasubbag4', 'kasubbag', '326da7a9f89af02866f6206a7c03d61b'),
('kasubbag5', 'kasubbag', '809e9fa9442bf6eb359a51121d3b1b13'),
('kasubbag6', 'kasubbag', '813b5fbcac1a39f730bf9f1549712760'),
('kasubbag7', 'kasubbag', 'ee2d2c1e6badd2c269260b582f5458c1'),
('kasubbag8', 'kasubbag', '5bd668424efba8a862d36b31aa6a9d70'),
('kasubbag9', 'kasubbag', '9ed6283fe6df769b0e356bd1138fd680'),
('kasubbagtes22', 'kabag', 'c0c32b768d16861ea9a0688fffc52bf4'),
('kasubbagtes222', 'kabag', '2834537a0e38ea2bd7daa3e382cc8519'),
('kasubbagtes333', 'kasubbag', '02c97b671ea7bb70aaf0c89e47737c7d'),
('kaupttik', 'kasubbag', '03cbb345067c4d99beeb5348e5ad5c7a'),
('kaupttik222', 'kasubbag', '3c5f056d5133f6a36eb58c82375f8af5'),
('labbahasa', 'admin surat', 'cff8beaf555b96ca5ea3e604a281d112'),
('labdas', 'admin surat', 'b99e60c3dfaa8ed812502bc49504d572'),
('pelaklabbhs222', 'pelaksana', 'bb9f39446659b0f7e123bb1024155f9d'),
('pelaklabdas12', 'pelaksana', '120ca495e6f5b032a3d737afba9ba0c6'),
('pelaksana', 'pelaksana', '6875ccc2c267a8c215afb1f25f81d7a0'),
('pelaksana1', 'pelaksana', 'e32daea4f91d2d6f69efa2da385e3725'),
('pelaksana10', 'pelaksana', '5223d74665420a8d4f0288ba67fc098e'),
('pelaksana1021', 'pelaksana', '140754743cfc3fd73ff66d459113ad46'),
('pelaksana1022', 'pelaksana', 'c1543531879e04c63dc9e51524c90586'),
('pelaksana11', 'pelaksana', '58e8c2b298a4af3ddcd9471c647e149e'),
('pelaksana12', 'pelaksana', '82c21ef79bf0d351e9bfc1cc32e84494'),
('pelaksana13', 'pelaksana', '6c67361dc3924d4a558adbc47080422b'),
('pelaksana14', 'pelaksana', '3f6a48dc7ad496d7fcde260daca8bd50'),
('pelaksana15', 'pelaksana', 'bc8998908292bd2508f43e808ce3a772'),
('pelaksana16', 'pelaksana', '9ae9646ecb7588ca8820296d40aa2463'),
('pelaksana17', 'pelaksana', 'b5395bcfbced23528b410b02192e034b'),
('pelaksana18', 'pelaksana', '80df783e19e4d1ffc69840f69edaa099'),
('pelaksana19', 'pelaksana', 'bcdb21d4ecd652f48b40926e0ee55377'),
('pelaksana2', 'pelaksana', 'd4f803080f4699bddecf5f19c73ca333'),
('pelaksana20', 'pelaksana', 'b5f32940f6b88ec1986219b5ddb63e7e'),
('pelaksana21', 'pelaksana', '287f47c5b6cafead3b0cd47c41edb934'),
('pelaksana22', 'pelaksana', 'c7c0348ab82418ec1f349bfdff8868da'),
('pelaksana23', 'pelaksana', 'dde7b0feae3e725cdb8208eb420b1cae'),
('pelaksana24', 'pelaksana', '725d7020d50dffe0de99b983b518bde5'),
('pelaksana3', 'pelaksana', '1c97c30aaaab877a76c231d3d5326bf9'),
('pelaksana4', 'pelaksana', 'a7c1e10f9d32aa50fbd755cf1606f8be'),
('pelaksana5', 'pelaksana', '5072a44b03756a10b424e9287801ebfd'),
('pelaksana6', 'pelaksana', 'e3a3b65cde0283ab4a57082bfe10ea9d'),
('pelaksana7', 'pelaksana', '372428373e31bd9bb44789209bbed802'),
('pelaksana8', 'pelaksana', 'e547e873a416571bde449a192d2ebe3d'),
('pelaksana9', 'pelaksana', '6f146bed8fe5eb3dd4e7e4df7e3420dc'),
('pelaksanalabdas2', 'pelaksana', 'c96dfd6f06698ab2a6ec53ffc3064971'),
('pelaksana_tik1', 'pelaksana', '18448d59db2c7d636239f08605e65b7f'),
('rektor', 'rektor', '8f3bfbf778b9ad6c7306ad89a0fe8824'),
('rektor3', 'rektor', '27dad8272df43053643a89779ab2d17c'),
('seksp111', 'kasubbag', '3180bfa82e147e86f1db9b39f14f04a2'),
('sek_sp', 'kasubbag', '642425dae2e905cb1c5aaabc77015f99'),
('sp', 'admin surat', '1952a01898073d1e561b9b4f2e42cbd7'),
('superadmin', 'super admin', '17c4520f6cfd1ab53d8745e84681eb49'),
('teknisilabbahasa', 'pelaksana', 'fde93bfefc29d85e501dfdf042522598'),
('teknisi_labor', 'pelaksana', 'fa72df00ef97bc6d3e582a82d2c79741'),
('tes', 'biro', '28b662d883b6d76fd96e4ddc5e9ba780'),
('tes008', 'rektor', '204f39398c77c95231dd5b2db6a578ef'),
('tes111', 'pelaksana', 'bc7e7c618a9bb0beb5a4092ca281acd1'),
('tes11222', 'kasubbag', '748405dd6329089058fe29e820418d53'),
('tes21222', 'kasubbag', '03673a18f2b8a240f9297fbf910c61bf'),
('tes22', 'kabag', 'f9da714e5247d9642aeb4d088b4c1f64'),
('tes45', 'pelaksana', '201f7fc27c640ddf03a4122b0373a332'),
('tes666', 'pelaksana', '3b38c046dc084a57a296f6d0e1cb48f2'),
('teskabag', 'kabag', 'cde2404bf7789529e653ea9281a5351d'),
('teskabag1', 'kabag', 'f50d239b8ed8c8cce4e07481709c07d5'),
('tess', 'pelaksana', '8b8be2799a2796a36a02004608426bdb'),
('tesseksp', 'kasubbag', '62aa5cd19cf1fd3f03c27c554e238781'),
('tik', 'admin surat', 'a54eba296e7f21fabd54923d9dcbd101'),
('uptik', 'kabag', 'f251a2c273f38d8c0181ce97cff39bb7');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_rektor`
--

CREATE TABLE `tbl_rektor` (
  `rektor_no` int(11) NOT NULL,
  `rektor_uuid` varchar(32) NOT NULL,
  `rektor_username` varchar(10) NOT NULL,
  `rektor_nama` varchar(30) NOT NULL,
  `rektor_posisi` varchar(40) NOT NULL,
  `rektor_time` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tbl_rektor`
--

INSERT INTO `tbl_rektor` (`rektor_no`, `rektor_uuid`, `rektor_username`, `rektor_nama`, `rektor_posisi`, `rektor_time`) VALUES
(1, '1324234', 'rektor', 'Dr. Bachtiar Akob, M.Pd.', 'Rektor', '2020-12-12 14:34:24'),
(2, '2234234', 'rektor3', 'Dr. Saiman, M.Pd.', 'Wakil Rektor Bidang Akademik', '2020-12-12 14:34:24'),
(3, '788DC9C737DC11EB87212CFDA1251793', 'rektor23', 'Ilham Ramadhan, S.Tr.Kom', 'Wakil Rektor Bidang Kemahasiswaan', '2020-12-12 14:34:24');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_riwayat_disposisi`
--

CREATE TABLE `tbl_riwayat_disposisi` (
  `riw_no` int(11) NOT NULL,
  `riw_uuid` varchar(32) NOT NULL,
  `riw_dist_uuid` varchar(32) NOT NULL,
  `riw_asal` varchar(30) NOT NULL,
  `riw_disposisi` varchar(30) NOT NULL,
  `riw_update_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `riw_memo` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tbl_riwayat_disposisi`
--

INSERT INTO `tbl_riwayat_disposisi` (`riw_no`, `riw_uuid`, `riw_dist_uuid`, `riw_asal`, `riw_disposisi`, `riw_update_at`, `riw_memo`) VALUES
(50, '00DF030011D211EBA64D2CFDA1251793', '545335F011CD11EBA64D2CFDA1251793', 'rektor', 'biro_akpk', '2020-10-19 06:11:57', 'Mohon segera tindaklanjuti'),
(186, '01ED1B3245F211EBA1882CFDA1251793', 'D6CC6850141311EB9ACC2CFDA1251793', 'biro_umum', 'kabag1000', '2020-12-24 14:12:10', ''),
(185, '01ED969045F211EBA1882CFDA1251793', 'D6CC6850141311EB9ACC2CFDA1251793', 'biro_umum', 'kabag1000', '2020-12-24 14:12:10', 'Untuk Dimaklumi'),
(115, '02E8A253144611EBB8712CFDA1251793', 'A2C2F804144011EBB8712CFDA1251793', 'kabag2', 'rektor', '2020-10-22 09:07:29', 'Program sosialisasi Kartu Indonesia Pintar'),
(116, '02E8CE88144611EBB8712CFDA1251793', 'A2C2F804144011EBB8712CFDA1251793', 'kabag2', 'rektor', '2020-10-22 09:07:29', 'Teruskan'),
(8, '041EDD170DC311EB8C442CFDA1251793', '041EB1000DC311EB8C442CFDA1251793', 'staff', 'rektor2', '2020-10-14 02:13:20', 'tesss'),
(226, '047E189746AE11EBA1882CFDA1251793', '30D0FB32141A11EB9ACC2CFDA1251793', 'biro_umum', 'kabag1', '2020-12-25 12:37:53', 'tes'),
(225, '047E3CF446AE11EBA1882CFDA1251793', '30D0FB32141A11EB9ACC2CFDA1251793', 'biro_umum', 'kabag1', '2020-12-25 12:37:53', 'Proses Sesuai Ketentuan/Peraturan yang Berlaku'),
(11, '04D47A310E8C11EB8C442CFDA1251793', '24D42E110B9611EBBB0B2CFDA1251793', 'rektor', 'biro2', '2020-10-15 02:11:44', 'Tes Memo ke Biro\r\n'),
(90, '068798D0141811EB9ACC2CFDA1251793', '06874106141811EB9ACC2CFDA1251793', 'admin', 'kabag2', '2020-10-22 03:38:15', 'forward to kabag umum'),
(210, '074ECF13466A11EBA1882CFDA1251793', 'D6CC6850141311EB9ACC2CFDA1251793', 'biro_umum', 'kabag1', '2020-12-25 04:31:12', ''),
(209, '074EF3FD466A11EBA1882CFDA1251793', 'D6CC6850141311EB9ACC2CFDA1251793', 'biro_umum', 'kabag1', '2020-12-25 04:31:12', 'Agendakan/Persiapkan/Koordinasikan'),
(12, '0832200E0E9411EB8C442CFDA1251793', '24D42E110B9611EBBB0B2CFDA1251793', 'biro2', 'kabag', '2020-10-15 03:09:05', 'TEs Memo untuk Kabag'),
(23, '0CEEB9AD0E9E11EB8C442CFDA1251793', '0CEE81840E9E11EB8C442CFDA1251793', 'admin', 'rektor2', '2020-10-15 04:20:48', '-'),
(267, '0DD519CC477C11EB96FD2CFDA1251793', '9B729C1D477B11EB96FD2CFDA1251793', 'rektor', 'biro_umum', '2020-12-26 13:12:40', 'lanjut'),
(268, '0DD54E5F477C11EB96FD2CFDA1251793', '9B729C1D477B11EB96FD2CFDA1251793', 'rektor', 'biro_umum', '2020-12-26 13:12:40', 'Proses Sesuai Ketentuan/Peraturan yang Berlaku'),
(136, '10662BF42CDF11EBBEB92CFDA1251793', '5E1E8C952CDE11EBBEB92CFDA1251793', 'kalabahasa', 'teknisilabbahasa', '2020-11-22 16:23:24', ''),
(135, '106688042CDF11EBBEB92CFDA1251793', '5E1E8C952CDE11EBBEB92CFDA1251793', 'kalabahasa', 'teknisilabbahasa', '2020-11-22 16:23:24', 'Untuk Dimaklumi'),
(180, '1154850D45BB11EBA1882CFDA1251793', 'D6CC6850141311EB9ACC2CFDA1251793', 'biro_umum', 'kabag121', '2020-12-24 07:38:53', ''),
(179, '1154A7B045BB11EBA1882CFDA1251793', 'D6CC6850141311EB9ACC2CFDA1251793', 'biro_umum', 'kabag121', '2020-12-24 07:38:53', 'Proses Sesuai Ketentuan/Peraturan yang Berlaku'),
(42, '124E8946113C11EB846E2CFDA1251793', '124E6302113C11EB846E2CFDA1251793', 'admin', 'kabag2', '2020-10-18 12:18:48', 'Program sistem terpadu'),
(77, '13F929BE140E11EB9ACC2CFDA1251793', 'E9827AD0140B11EB9ACC2CFDA1251793', 'kasubbag3', 'pelaksana10', '2020-10-22 02:27:03', 'Persiapkan dengan matang, jangan sampai ada kesalahan'),
(81, '167922F5141411EB9ACC2CFDA1251793', 'D6CC6850141311EB9ACC2CFDA1251793', 'kabag2', 'rektor', '2020-10-22 03:10:04', 'Terkait permohonan sosialisasi di kampus Unsam tentang Kartu Indonesia Pintar'),
(82, '16794F2A141411EB9ACC2CFDA1251793', 'D6CC6850141311EB9ACC2CFDA1251793', 'kabag2', 'rektor', '2020-10-22 03:10:04', 'Teruskan'),
(110, '19AA5137141B11EB9ACC2CFDA1251793', '30D0FB32141A11EB9ACC2CFDA1251793', 'kabag2', 'kasubbag3', '2020-10-22 04:00:16', 'Umumkan kepada seluruh civitas akademika dan mahasiswa untuk ikut hadir dalam sosialisasi ini'),
(111, '19AA9104141B11EB9ACC2CFDA1251793', '30D0FB32141A11EB9ACC2CFDA1251793', 'kabag2', 'kasubbag3', '2020-10-22 04:00:16', 'Tindaklanjuti'),
(27, '1B74D9CA0E9F11EB8C442CFDA1251793', '0CEE81840E9E11EB8C442CFDA1251793', 'kabag', 'kasubbag', '2020-10-15 04:28:22', 'Tolong pastikan mahasiswa dapat hadir semua'),
(152, '1DC2B5683F5611EB87AA2CFDA1251793', 'A2C2F804144011EBB8712CFDA1251793', 'biro_umum', 'kabag1', '2020-12-16 04:21:03', 'lanjutkan'),
(151, '1DC322C43F5611EB87AA2CFDA1251793', 'A2C2F804144011EBB8712CFDA1251793', 'biro_umum', 'kabag1', '2020-12-16 04:21:03', 'Tindaklanjuti'),
(202, '1FD82F9E45F711EBA1882CFDA1251793', 'D6CC6850141311EB9ACC2CFDA1251793', 'biro_umum', 'kabag1', '2020-12-24 14:48:47', 'Tes Tes memo'),
(201, '1FD8B6EC45F711EBA1882CFDA1251793', 'D6CC6850141311EB9ACC2CFDA1251793', 'biro_umum', 'kabag1', '2020-12-24 14:48:47', 'Proses Sesuai Ketentuan/Peraturan yang Berlaku'),
(243, '1FE20C6046EB11EB86592CFDA1251793', '20D61C952DFD11EBB1F82CFDA1251793', 'kabag_sp', 'sek_sp', '2020-12-25 19:55:19', 'tes'),
(244, '1FE2ABDD46EB11EB86592CFDA1251793', '20D61C952DFD11EBB1F82CFDA1251793', 'kabag_sp', 'sek_sp', '2020-12-25 19:55:19', 'Pelajari/Tela\'ah dan Berikan Saran/Masukan'),
(137, '20D646972DFD11EBB1F82CFDA1251793', '20D61C952DFD11EBB1F82CFDA1251793', 'sp', 'kabag_sp', '2020-11-24 02:31:16', 'tes'),
(233, '211107C146BC11EB86592CFDA1251793', '06874106141811EB9ACC2CFDA1251793', 'kabag121', 'kasubbag4', '2020-12-25 14:18:58', 'TES MEMO TAMBAHAN'),
(234, '211198C346BC11EB86592CFDA1251793', '06874106141811EB9ACC2CFDA1251793', 'kabag121', 'kasubbag4', '2020-12-25 14:18:58', 'Proses Sesuai Ketentuan/Peraturan yang Berlaku'),
(91, '222E5F65141811EB9ACC2CFDA1251793', '06874106141811EB9ACC2CFDA1251793', 'kabag2', 'kasubbag3', '2020-10-22 03:39:02', 'tindak lanjuti pak herman'),
(92, '222EC2EC141811EB9ACC2CFDA1251793', '06874106141811EB9ACC2CFDA1251793', 'kabag2', 'kasubbag3', '2020-10-22 03:39:02', 'Tindaklanjuti'),
(212, '22764CD9466A11EBA1882CFDA1251793', 'D6CC6850141311EB9ACC2CFDA1251793', 'biro_umum', 'kabag1', '2020-12-25 04:31:57', ''),
(211, '227670FB466A11EBA1882CFDA1251793', 'D6CC6850141311EB9ACC2CFDA1251793', 'biro_umum', 'kabag1', '2020-12-25 04:31:57', 'Untuk Diketahui'),
(55, '2365AF6D135311EBA60A2CFDA1251793', '124E6302113C11EB846E2CFDA1251793', 'kabag2', 'kasubbag3', '2020-10-21 04:08:44', 'tes Koordinasi'),
(224, '23D6315A46A211EBA1882CFDA1251793', 'D6CC6850141311EB9ACC2CFDA1251793', 'biro_umum', 'kabag1', '2020-12-25 11:12:51', 'TES'),
(223, '23D6581D46A211EBA1882CFDA1251793', 'D6CC6850141311EB9ACC2CFDA1251793', 'biro_umum', 'kabag1', '2020-12-25 11:12:51', 'Proses Sesuai Ketentuan/Peraturan yang Berlaku'),
(6, '24D45E4D0B9611EBBB0B2CFDA1251793', '24D42E110B9611EBBB0B2CFDA1251793', 'staff', 'rektor', '2020-10-11 07:46:28', 'Tes Memo Lagi'),
(56, '2884BA99135311EBA60A2CFDA1251793', '124E6302113C11EB846E2CFDA1251793', 'kabag2', 'kasubbag3', '2020-10-21 04:08:52', 'tes Koordinasi'),
(182, '29339CF045BB11EBA1882CFDA1251793', 'D6CC6850141311EB9ACC2CFDA1251793', 'biro_umum', 'kabag1000', '2020-12-24 07:39:33', ''),
(181, '2933C65045BB11EBA1882CFDA1251793', 'D6CC6850141311EB9ACC2CFDA1251793', 'biro_umum', 'kabag1000', '2020-12-24 07:39:33', 'Proses Sesuai Ketentuan/Peraturan yang Berlaku'),
(69, '29C2E795140911EB9ACC2CFDA1251793', '545335F011CD11EBA64D2CFDA1251793', 'kabag2', 'kasubbag3', '2020-10-22 01:51:55', 'tes'),
(70, '29C346B5140911EB9ACC2CFDA1251793', '545335F011CD11EBA64D2CFDA1251793', 'kabag2', 'kasubbag3', '2020-10-22 01:51:55', 'Arsipkan'),
(163, '2A4628583F7211EBB5682CFDA1251793', '06874106141811EB9ACC2CFDA1251793', 'kabag1', 'kasubbag1', '2020-12-16 07:41:53', '-\r\n'),
(164, '2A464E0C3F7211EBB5682CFDA1251793', '06874106141811EB9ACC2CFDA1251793', 'kabag1', 'kasubbag1', '2020-12-16 07:41:53', 'Proses Sesuai Ketentuan/Peraturan yang Berlaku'),
(46, '2B36D7B0116C11EB846E2CFDA1251793', 'E9117FDF113B11EB846E2CFDA1251793', 'kabag3', 'kasubbag6', '2020-10-18 18:03:05', 'Lanjutkan Kasubbag Pendidikan'),
(24, '2D36E83A0E9E11EB8C442CFDA1251793', '0CEE81840E9E11EB8C442CFDA1251793', 'rektor2', 'biro2', '2020-10-15 04:21:42', 'Teruskan, pastikan semua berjalan lancar'),
(18, '2EB8F23C0E9A11EB8C442CFDA1251793', 'BBFDE6FE0E9411EB8C442CFDA1251793', 'kabag', 'kasubbag', '2020-10-15 03:53:07', 'Laksanakan sesuai perintah, Ksbg'),
(3, '3', '24D42E110B9611EBBB0B2CFDA1251793', 'wdw', 'wdw', '2020-10-14 01:13:53', 'tes\r\n'),
(103, '30D17E2D141A11EB9ACC2CFDA1251793', '30D0FB32141A11EB9ACC2CFDA1251793', 'admin', 'kabag2', '2020-10-22 03:53:45', 'surat permohonan dari dinas sosial terkait kartu indonesia pintar'),
(43, '315C2F93113C11EB846E2CFDA1251793', 'E9117FDF113B11EB846E2CFDA1251793', 'kabag2', 'rektor', '2020-10-18 12:19:40', '-'),
(184, '3236358045BB11EBA1882CFDA1251793', 'D6CC6850141311EB9ACC2CFDA1251793', 'biro_umum', 'kabag1000', '2020-12-24 07:39:48', ''),
(183, '323656DD45BB11EBA1882CFDA1251793', 'D6CC6850141311EB9ACC2CFDA1251793', 'biro_umum', 'kabag1000', '2020-12-24 07:39:48', 'Teruskan'),
(35, '37D26CF3110511EB846E2CFDA1251793', '24D42E110B9611EBBB0B2CFDA1251793', 'kabag', 'kasubbag2', '2020-10-18 05:46:08', 'tes'),
(188, '391232F545F211EBA1882CFDA1251793', 'D6CC6850141311EB9ACC2CFDA1251793', 'biro_umum', 'kabag1', '2020-12-24 14:13:42', ''),
(187, '3912AD7645F211EBA1882CFDA1251793', 'D6CC6850141311EB9ACC2CFDA1251793', 'biro_umum', 'kabag1', '2020-12-24 14:13:42', 'Tindaklanjuti'),
(166, '3AD3430E3F7311EBB5682CFDA1251793', '06874106141811EB9ACC2CFDA1251793', 'kasubbag1', 'pelaksana5', '2020-12-16 07:49:30', '-'),
(165, '3AD39E913F7311EBB5682CFDA1251793', '06874106141811EB9ACC2CFDA1251793', 'kasubbag1', 'pelaksana5', '2020-12-16 07:49:30', 'Pelajari/Tela\'ah dan Berikan Saran/Masukan'),
(51, '3B31F19111D211EBA64D2CFDA1251793', '545335F011CD11EBA64D2CFDA1251793', 'biro_akpk', 'kabag3', '2020-10-19 06:13:35', 'Inventarisir nama calon penerima'),
(25, '3CAE017B0E9E11EB8C442CFDA1251793', '0CEE81840E9E11EB8C442CFDA1251793', 'rektor2', 'biro2', '2020-10-15 04:22:08', 'Teruskan, pastikan semua berjalan lancar'),
(235, '3CD6360646BC11EB86592CFDA1251793', '06874106141811EB9ACC2CFDA1251793', 'kabag121', 'kasubbag4', '2020-12-25 14:19:45', 'TES MEMO TAMBAHAN'),
(236, '3CD6F79946BC11EB86592CFDA1251793', '06874106141811EB9ACC2CFDA1251793', 'kabag121', 'kasubbag4', '2020-12-25 14:19:45', 'Proses Sesuai Ketentuan/Peraturan yang Berlaku'),
(192, '3CEB0FA845F511EBA1882CFDA1251793', 'D6CC6850141311EB9ACC2CFDA1251793', 'biro_umum', 'kabag1000', '2020-12-24 14:35:17', ''),
(191, '3CEB33FD45F511EBA1882CFDA1251793', 'D6CC6850141311EB9ACC2CFDA1251793', 'biro_umum', 'kabag1000', '2020-12-24 14:35:17', 'Agendakan/Persiapkan/Koordinasikan'),
(254, '3E1068AC477211EB96FD2CFDA1251793', '3ED974142BC411EBBEB92CFDA1251793', 'kalabdas', 'teknisi_labor', '2020-12-26 12:02:26', 'tes'),
(253, '3E108B43477211EB96FD2CFDA1251793', '3ED974142BC411EBBEB92CFDA1251793', 'kalabdas', 'teknisi_labor', '2020-12-26 12:02:26', 'Koordinasi/Konfirmasi dengan Ybs/Unit Kerja Terkait'),
(131, '3ED9D12B2BC411EBBEB92CFDA1251793', '3ED974142BC411EBBEB92CFDA1251793', 'labdas', 'kalabdas', '2020-11-21 06:38:59', 'TES'),
(93, '42200996141811EB9ACC2CFDA1251793', '06874106141811EB9ACC2CFDA1251793', 'kabag2', 'rektor', '2020-10-22 03:39:55', 'diteruskan ke rektor'),
(94, '42204611141811EB9ACC2CFDA1251793', '06874106141811EB9ACC2CFDA1251793', 'kabag2', 'rektor', '2020-10-22 03:39:55', 'Teruskan'),
(57, '43E0FFB0135311EBA60A2CFDA1251793', '124E6302113C11EB846E2CFDA1251793', 'kabag2', '', '2020-10-21 04:09:38', ''),
(230, '4439F8AC46B911EB86592CFDA1251793', 'D6CC6850141311EB9ACC2CFDA1251793', 'biro_umum', 'kabag1000', '2020-12-25 13:58:29', 'TES '),
(229, '443A570946B911EB86592CFDA1251793', 'D6CC6850141311EB9ACC2CFDA1251793', 'biro_umum', 'kabag1000', '2020-12-25 13:58:29', 'Proses Sesuai Ketentuan/Peraturan yang Berlaku'),
(216, '446A2AC046A111EBA1882CFDA1251793', 'D6CC6850141311EB9ACC2CFDA1251793', 'biro_umum', 'kabag1', '2020-12-25 11:06:37', 'tes memo tambahan'),
(215, '446A4F4646A111EBA1882CFDA1251793', 'D6CC6850141311EB9ACC2CFDA1251793', 'biro_umum', 'kabag1', '2020-12-25 11:06:37', 'Pelajari/Tela\'ah dan Berikan Saran/Masukan'),
(36, '470B1FBB110511EB846E2CFDA1251793', '24D42E110B9611EBBB0B2CFDA1251793', 'kasubbag2', 'pelaksana', '2020-10-18 05:46:34', 'tes'),
(28, '47776E250E9F11EB8C442CFDA1251793', '0CEE81840E9E11EB8C442CFDA1251793', 'kasubbag', 'pelaksana', '2020-10-15 04:29:36', 'Laksanakan sesuai perintah dan memo terlampir ya'),
(159, '478B0A653F7011EBB5682CFDA1251793', 'D6CC6850141311EB9ACC2CFDA1251793', 'kabag1', 'kasubbag1', '2020-12-16 07:28:23', 'dan laporkan'),
(160, '478B2FD93F7011EBB5682CFDA1251793', 'D6CC6850141311EB9ACC2CFDA1251793', 'kabag1', 'kasubbag1', '2020-12-16 07:28:23', 'Agendakan/Persiapkan/Koordinasikan'),
(58, '49952B40135311EBA60A2CFDA1251793', '124E6302113C11EB846E2CFDA1251793', 'kabag2', 'kasubbag3', '2020-10-21 04:09:48', 'tes'),
(171, '49EC6F73404311EB97F42CFDA1251793', '06874106141811EB9ACC2CFDA1251793', 'kabag121', 'rektor', '2020-12-17 08:38:51', 'Tindak lanjuti'),
(172, '49ECAF27404311EB97F42CFDA1251793', '06874106141811EB9ACC2CFDA1251793', 'kabag121', 'rektor', '2020-12-17 08:38:51', 'Proses Sesuai Ketentuan/Peraturan yang Berlaku'),
(67, '4B2D3DFD140811EB9ACC2CFDA1251793', '545335F011CD11EBA64D2CFDA1251793', 'kabag2', 'kasubbag3', '2020-10-22 01:45:41', 'tes'),
(113, '4D255D62141B11EB9ACC2CFDA1251793', '30D0FB32141A11EB9ACC2CFDA1251793', 'kasubbag3', 'pelaksana10', '2020-10-22 04:01:42', 'Siapkan data calon peserta sosialisasi, umumkan dan kemudian arsipkan '),
(112, '4D259963141B11EB9ACC2CFDA1251793', '30D0FB32141A11EB9ACC2CFDA1251793', 'kasubbag3', 'pelaksana10', '2020-10-22 04:01:42', 'Arsipkan'),
(71, '4E984572140911EB9ACC2CFDA1251793', '545335F011CD11EBA64D2CFDA1251793', 'kabag2', '', '2020-10-22 01:52:57', 'Pelajari'),
(72, '4E98EF61140911EB9ACC2CFDA1251793', '545335F011CD11EBA64D2CFDA1251793', 'kabag2', '', '2020-10-22 01:52:57', 'Pelajari/Tela\'ah dan Berikan Saran/Masukan'),
(15, '4F4C850C0E9511EB8C442CFDA1251793', 'BBFDE6FE0E9411EB8C442CFDA1251793', 'rektor', 'biro2', '2020-10-15 03:18:14', 'Lakukan sesuai arahan'),
(127, '4FD0F0FE2B5311EBBEB92CFDA1251793', '4FD066302B5311EBBEB92CFDA1251793', 'tik', 'uptik', '2020-11-20 17:10:38', 'TES'),
(44, '50A2A89D116B11EB846E2CFDA1251793', 'E9117FDF113B11EB846E2CFDA1251793', 'rektor', 'biro_akpk', '2020-10-18 17:56:58', 'lanjutkan pak biro'),
(95, '53007F4A141811EB9ACC2CFDA1251793', '06874106141811EB9ACC2CFDA1251793', 'rektor', 'biro_umum', '2020-10-22 03:40:24', 'wakilkan pak herman'),
(96, '5300E984141811EB9ACC2CFDA1251793', '06874106141811EB9ACC2CFDA1251793', 'rektor', 'biro_umum', '2020-10-22 03:40:24', 'Wakili/Hadir/Terima/Laporkan Hasilnya'),
(66, '53E6BB9E135711EBA60A2CFDA1251793', '545335F011CD11EBA64D2CFDA1251793', 'kabag2', 'kasubbag3', '2020-10-21 04:38:43', 'tes'),
(48, '5453650811CD11EBA64D2CFDA1251793', '545335F011CD11EBA64D2CFDA1251793', 'admin', 'kabag2', '2020-10-19 05:38:34', 'Terkait Usulan Kartu Indonesia Pintar'),
(83, '54943C1F141411EB9ACC2CFDA1251793', 'D6CC6850141311EB9ACC2CFDA1251793', 'rektor', 'biro_umum', '2020-10-22 03:11:48', 'Persiapkan, untuk fasilitas gedung hubungi bapak herman bagian gudang'),
(47, '5696FABA116C11EB846E2CFDA1251793', 'E9117FDF113B11EB846E2CFDA1251793', 'kasubbag6', 'pelaksana16', '2020-10-18 18:04:18', 'Lanjutkan ke Pengelola Data Sarana Pendidikan'),
(256, '58525042477711EB96FD2CFDA1251793', '585226D8477711EB96FD2CFDA1251793', 'labbahasa', 'kalabbhs211', '2020-12-26 12:38:58', 'tes'),
(59, '5888C5F0135411EBA60A2CFDA1251793', '124E6302113C11EB846E2CFDA1251793', 'kabag2', 'kasubbag3', '2020-10-21 04:17:22', 'tes'),
(75, '596E98C3140C11EB9ACC2CFDA1251793', 'E9827AD0140B11EB9ACC2CFDA1251793', 'kabag2', 'kasubbag3', '2020-10-22 02:14:40', 'Koordinasikan dengan pihak terkait (Pak Fadil)'),
(76, '596ECDA6140C11EB9ACC2CFDA1251793', 'E9827AD0140B11EB9ACC2CFDA1251793', 'kabag2', 'kasubbag3', '2020-10-22 02:14:40', 'Koordinasi/Konfirmasi dengan Ybs/Unit Kerja Terkait'),
(125, '5B014D6C21E111EB8E842CFDA1251793', '545335F011CD11EBA64D2CFDA1251793', 'kabag2', 'rektor', '2020-11-08 16:42:00', ''),
(126, '5B0197D121E111EB8E842CFDA1251793', '545335F011CD11EBA64D2CFDA1251793', 'kabag2', 'rektor', '2020-11-08 16:42:00', 'Untuk Diketahui'),
(237, '5CA8A71C46BC11EB86592CFDA1251793', '06874106141811EB9ACC2CFDA1251793', 'kabag121', 'rektor3', '2020-12-25 14:20:38', 'Tes Tambahan memo'),
(238, '5CA903BF46BC11EB86592CFDA1251793', '06874106141811EB9ACC2CFDA1251793', 'kabag121', 'rektor3', '2020-12-25 14:20:38', 'Untuk Diketahui'),
(134, '5E1F0BA92CDE11EBBEB92CFDA1251793', '5E1E8C952CDE11EBBEB92CFDA1251793', 'labbahasa', 'kalabahasa', '2020-11-22 16:18:25', 'tess'),
(204, '60A152FF45F711EBA1882CFDA1251793', 'D6CC6850141311EB9ACC2CFDA1251793', 'biro_umum', 'kabag1', '2020-12-24 14:50:36', 'tes memo tambahan dari pak adnan'),
(203, '60A1A59645F711EBA1882CFDA1251793', 'D6CC6850141311EB9ACC2CFDA1251793', 'biro_umum', 'kabag1', '2020-12-24 14:50:36', 'Wakili/Hadir/Terima/Laporkan Hasilnya'),
(147, '60FBDDF63F4E11EB87AA2CFDA1251793', '6E6998203E8211EB83F62CFDA1251793', 'rektor', 'biro111', '2020-12-16 03:25:40', ''),
(148, '60FC07B13F4E11EB87AA2CFDA1251793', '6E6998203E8211EB83F62CFDA1251793', 'rektor', 'biro111', '2020-12-16 03:25:40', 'Tindaklanjuti'),
(200, '65ABEFBB45F611EBA1882CFDA1251793', 'D6CC6850141311EB9ACC2CFDA1251793', 'biro_umum', 'kabag1', '2020-12-24 14:43:35', ''),
(199, '65AC644045F611EBA1882CFDA1251793', 'D6CC6850141311EB9ACC2CFDA1251793', 'biro_umum', 'kabag1', '2020-12-24 14:43:35', 'Koordinasi/Konfirmasi dengan Ybs/Unit Kerja Terkait'),
(194, '65FF9C9745F511EBA1882CFDA1251793', 'D6CC6850141311EB9ACC2CFDA1251793', 'biro_umum', 'kabag1', '2020-12-24 14:36:26', ''),
(193, '65FFD29645F511EBA1882CFDA1251793', 'D6CC6850141311EB9ACC2CFDA1251793', 'biro_umum', 'kabag1', '2020-12-24 14:36:26', 'Wakili/Hadir/Terima/Laporkan Hasilnya'),
(30, '67DF8D76110111EB846E2CFDA1251793', 'C395F91C110011EB846E2CFDA1251793', 'kabag', 'rektor', '2020-10-18 05:18:50', 'Dari dinas kehutanan'),
(98, '6905D596141811EB9ACC2CFDA1251793', '06874106141811EB9ACC2CFDA1251793', 'biro_umum', 'kabag2', '2020-10-22 03:41:00', 'proses pak faisal'),
(97, '69064BD3141811EB9ACC2CFDA1251793', '06874106141811EB9ACC2CFDA1251793', 'biro_umum', 'kabag2', '2020-10-22 03:41:00', 'Proses Sesuai Ketentuan/Peraturan yang Berlaku'),
(142, '6E69EEA83E8211EB83F62CFDA1251793', '6E6998203E8211EB83F62CFDA1251793', 'admin', 'kabag121', '2020-12-15 03:05:37', '-'),
(52, '6ED1A9E411D211EBA64D2CFDA1251793', '545335F011CD11EBA64D2CFDA1251793', 'kabag3', 'kasubbag8', '2020-10-19 06:15:02', 'Mintakan data-data calon dari fakultas'),
(39, '72422AD1113A11EB846E2CFDA1251793', 'F3430781112D11EB846E2CFDA1251793', 'kasubbag3', 'pelaksana10', '2020-10-18 12:07:10', 'Lanjutkan ke bagian Pengolah Data'),
(117, '727E212C179511EB9FB22CFDA1251793', 'A2C2F804144011EBB8712CFDA1251793', 'rektor', 'biro_umum', '2020-10-26 14:13:40', 'Koordinasikan juga ke bapak Reza terkait penggunaan Gedung'),
(118, '727ED2AA179511EB9FB22CFDA1251793', 'A2C2F804144011EBB8712CFDA1251793', 'rektor', 'biro_umum', '2020-10-26 14:13:40', 'Koordinasi/Konfirmasi dengan Ybs/Unit Kerja Terkait'),
(128, '72C61BC32B5611EBBEB92CFDA1251793', '72C5E4362B5611EBBEB92CFDA1251793', 'tik', 'kaupttik', '2020-11-20 17:33:06', 'TES\r\n'),
(63, '72D593D5135511EBA60A2CFDA1251793', '545335F011CD11EBA64D2CFDA1251793', 'kabag2', 'kasubbag3', '2020-10-21 04:25:16', 'tes'),
(167, '737EE3D3403B11EB97F42CFDA1251793', '06874106141811EB9ACC2CFDA1251793', 'rektor', 'biro_umum', '2020-12-17 07:42:44', 'Dan proses sesuai ketentuan'),
(168, '737F1E44403B11EB97F42CFDA1251793', '06874106141811EB9ACC2CFDA1251793', 'rektor', 'biro_umum', '2020-12-17 07:42:44', 'Tindaklanjuti'),
(17, '73E7A5150E9611EB8C442CFDA1251793', 'BBFDE6FE0E9411EB8C442CFDA1251793', 'biro2', 'kabag', '2020-10-15 03:26:25', 'Tes Memo ke Kabag\r\n'),
(99, '78BB2CB8141811EB9ACC2CFDA1251793', '06874106141811EB9ACC2CFDA1251793', 'kabag2', 'kasubbag3', '2020-10-22 03:41:27', 'agendakan pak herman'),
(100, '78BBA620141811EB9ACC2CFDA1251793', '06874106141811EB9ACC2CFDA1251793', 'kabag2', 'kasubbag3', '2020-10-22 03:41:27', 'Agendakan/Persiapkan/Koordinasikan'),
(149, '78ED91913F4E11EB87AA2CFDA1251793', '545335F011CD11EBA64D2CFDA1251793', 'rektor', '', '2020-12-16 03:26:20', 'Hubungi pak doni untuk keperluan perlengkapan acara'),
(150, '78EDB6A23F4E11EB87AA2CFDA1251793', '545335F011CD11EBA64D2CFDA1251793', 'rektor', '', '2020-12-16 03:26:20', 'Proses Sesuai Ketentuan/Peraturan yang Berlaku'),
(54, '79FB9EF2135211EBA60A2CFDA1251793', '124E6302113C11EB846E2CFDA1251793', 'kabag2', 'kasubbag3', '2020-10-21 04:03:59', 'tes Koordinasi'),
(85, '7B02E3AD141511EB9ACC2CFDA1251793', 'D6CC6850141311EB9ACC2CFDA1251793', 'biro_umum', 'kabag2', '2020-10-22 03:20:02', 'Koordinasi'),
(84, '7B032AD0141511EB9ACC2CFDA1251793', 'D6CC6850141311EB9ACC2CFDA1251793', 'biro_umum', 'kabag2', '2020-10-22 03:20:02', 'Koordinasi/Konfirmasi dengan Ybs/Unit Kerja Terkait'),
(176, '7B58D83A45BA11EBA1882CFDA1251793', 'D6CC6850141311EB9ACC2CFDA1251793', 'biro_umum', 'kabag121', '2020-12-24 07:34:42', ''),
(175, '7B5904BA45BA11EBA1882CFDA1251793', 'D6CC6850141311EB9ACC2CFDA1251793', 'biro_umum', 'kabag121', '2020-12-24 07:34:42', 'Proses Sesuai Ketentuan/Peraturan yang Berlaku'),
(104, '7C84E709141A11EB9ACC2CFDA1251793', '30D0FB32141A11EB9ACC2CFDA1251793', 'kabag2', 'rektor', '2020-10-22 03:55:52', 'Untuk di ketahui dan di setujui permohonan pihak dinas sosial'),
(105, '7C856560141A11EB9ACC2CFDA1251793', '30D0FB32141A11EB9ACC2CFDA1251793', 'kabag2', 'rektor', '2020-10-22 03:55:52', 'Untuk Diketahui'),
(31, '7D338FB8110111EB846E2CFDA1251793', 'C395F91C110011EB846E2CFDA1251793', 'rektor', 'biro2', '2020-10-18 05:19:25', 'Lanjutkan pak biro'),
(232, '7F0046B246B911EB86592CFDA1251793', 'D6CC6850141311EB9ACC2CFDA1251793', 'biro_umum', 'kabag1', '2020-12-25 14:00:07', 'TES'),
(231, '7F00B9CB46B911EB86592CFDA1251793', 'D6CC6850141311EB9ACC2CFDA1251793', 'biro_umum', 'kabag1', '2020-12-25 14:00:07', 'Koordinasi/Konfirmasi dengan Ybs/Unit Kerja Terkait'),
(218, '808BC84C46A111EBA1882CFDA1251793', 'D6CC6850141311EB9ACC2CFDA1251793', 'biro_umum', 'kabag1000', '2020-12-25 11:08:18', 'tes'),
(217, '808BEF6B46A111EBA1882CFDA1251793', 'D6CC6850141311EB9ACC2CFDA1251793', 'biro_umum', 'kabag1000', '2020-12-25 11:08:18', 'Proses Sesuai Ketentuan/Peraturan yang Berlaku'),
(79, '81438697140E11EB9ACC2CFDA1251793', 'E9827AD0140B11EB9ACC2CFDA1251793', 'kasubbag3', 'pelaksana10', '2020-10-22 02:30:06', 'jangan ada kesalahan'),
(78, '8143B8F3140E11EB9ACC2CFDA1251793', 'E9827AD0140B11EB9ACC2CFDA1251793', 'kasubbag3', 'pelaksana10', '2020-10-22 02:30:06', 'Agendakan/Persiapkan/Koordinasikan'),
(173, '836E0C93404311EB97F42CFDA1251793', '06874106141811EB9ACC2CFDA1251793', 'rektor', 'biro_umum', '2020-12-17 08:40:27', 'Proses Sesuai Ketentuan/Peraturan yang Berlaku Proses Sesuai Ketentuan/Peraturan yang Berlaku Proses Sesuai Ketentuan/Peraturan yang Berlaku Proses Sesuai Ketentuan/Peraturan yang Berlaku Proses Sesuai Ketentuan/Peraturan yang Berlaku Proses Sesuai Ketentuan/Peraturan yang Berlaku Proses Sesuai Ketentuan/Peraturan yang Berlaku Proses Sesuai Ketentuan/Peraturan yang Berlaku Proses Sesuai Ketentuan/Peraturan yang Berlaku'),
(174, '836E32B3404311EB97F42CFDA1251793', '06874106141811EB9ACC2CFDA1251793', 'rektor', 'biro_umum', '2020-12-17 08:40:27', 'Tindaklanjuti'),
(138, '89FBDDC42E2D11EBB1F82CFDA1251793', '20D61C952DFD11EBB1F82CFDA1251793', 'kabag_sp', 'sek_sp', '2020-11-24 08:17:40', 'Tes Proses\r\n'),
(139, '89FC01732E2D11EBB1F82CFDA1251793', '20D61C952DFD11EBB1F82CFDA1251793', 'kabag_sp', 'sek_sp', '2020-11-24 08:17:40', 'Proses Sesuai Ketentuan/Peraturan yang Berlaku'),
(32, '8CD05077110111EB846E2CFDA1251793', 'C395F91C110011EB846E2CFDA1251793', 'biro2', 'kabag', '2020-10-18 05:19:51', 'Lanjutkan pak Kabag Umum'),
(102, '8E20A6AC141811EB9ACC2CFDA1251793', '06874106141811EB9ACC2CFDA1251793', 'kasubbag3', 'pelaksana9', '2020-10-22 03:42:03', 'jawab azizah'),
(101, '8E210B83141811EB9ACC2CFDA1251793', '06874106141811EB9ACC2CFDA1251793', 'kasubbag3', 'pelaksana9', '2020-10-22 03:42:03', 'Dijawab'),
(86, '8E465BA8141711EB9ACC2CFDA1251793', 'F3430781112D11EB846E2CFDA1251793', 'rektor', '', '2020-10-21 03:34:53', 'Telaah'),
(87, '8E46D709141711EB9ACC2CFDA1251793', 'F3430781112D11EB846E2CFDA1251793', 'rektor', '', '2020-10-22 03:34:53', 'Pelajari/Tela\'ah dan Berikan Saran/Masukan'),
(19, '91ADCE350E9B11EB8C442CFDA1251793', 'BBFDE6FE0E9411EB8C442CFDA1251793', 'kasubbag', 'staff', '2020-10-15 04:03:03', 'Laksanakan sesuai perintah, jangan ada yg miss komunikasi'),
(252, '936DC691472A11EB96FD2CFDA1251793', '124E6302113C11EB846E2CFDA1251793', 'kasubbag3', 'pelaksana10', '2020-12-26 03:29:35', 'tes'),
(251, '936DF342472A11EB96FD2CFDA1251793', '124E6302113C11EB846E2CFDA1251793', 'kasubbag3', 'pelaksana10', '2020-12-26 03:29:35', 'Proses Sesuai Ketentuan/Peraturan yang Berlaku'),
(20, '93A495160E9B11EB8C442CFDA1251793', 'BBFDE6FE0E9411EB8C442CFDA1251793', 'kasubbag', 'staff', '2020-10-15 04:03:06', 'Laksanakan sesuai perintah, jangan ada yg miss komunikasi'),
(60, '978BA6F7135411EBA60A2CFDA1251793', '124E6302113C11EB846E2CFDA1251793', 'kabag2', 'kasubbag3', '2020-10-21 04:19:08', 'tes'),
(257, '980871D5477711EB96FD2CFDA1251793', '98084E50477711EB96FD2CFDA1251793', 'labbahasa', 'kalabbhs211', '2020-12-26 12:40:45', '-'),
(38, '9AAC4C16113111EB846E2CFDA1251793', 'F3430781112D11EB846E2CFDA1251793', 'kabag2', 'kasubbag3', '2020-10-18 11:03:52', 'Lanjutkan'),
(264, '9B72DE44477B11EB96FD2CFDA1251793', '9B729C1D477B11EB96FD2CFDA1251793', 'admin', 'kabag1000', '2020-12-26 13:09:28', 'tess'),
(33, '9CE56C53110111EB846E2CFDA1251793', 'C395F91C110011EB846E2CFDA1251793', 'kabag', 'kasubbag', '2020-10-18 05:20:18', 'Lanjutkan Kasubbag TU'),
(260, '9D7ACF9B477811EB96FD2CFDA1251793', '6E6998203E8211EB83F62CFDA1251793', 'rektor', 'biro_umum', '2020-12-26 12:48:03', 'tes'),
(261, '9D7AF4CE477811EB96FD2CFDA1251793', '6E6998203E8211EB83F62CFDA1251793', 'rektor', 'biro_umum', '2020-12-26 12:48:03', 'Proses Sesuai Ketentuan/Peraturan yang Berlaku'),
(53, 'A118A63F11D211EBA64D2CFDA1251793', '545335F011CD11EBA64D2CFDA1251793', 'kasubbag8', 'pelaksana20', '2020-10-19 06:16:26', 'Persiapkan surat permintaan calon ke fakultas'),
(114, 'A2C32BA8144011EBB8712CFDA1251793', 'A2C2F804144011EBB8712CFDA1251793', 'admin', 'kabag2', '2020-10-22 08:29:00', 'Sosialisasi Kartu Indonesia Pintar'),
(196, 'A2EF3DB645F511EBA1882CFDA1251793', 'D6CC6850141311EB9ACC2CFDA1251793', 'biro_umum', 'kabag1', '2020-12-24 14:38:08', ''),
(195, 'A2EFC61145F511EBA1882CFDA1251793', 'D6CC6850141311EB9ACC2CFDA1251793', 'biro_umum', 'kabag1', '2020-12-24 14:38:08', 'Koordinasi/Konfirmasi dengan Ybs/Unit Kerja Terkait'),
(120, 'A7670FD21DEB11EBB5512CFDA1251793', '124E6302113C11EB846E2CFDA1251793', 'kasubbag3', '', '2020-11-03 15:45:52', 'ya'),
(119, 'A7673B141DEB11EBB5512CFDA1251793', '124E6302113C11EB846E2CFDA1251793', 'kasubbag3', '', '2020-11-03 15:45:52', 'Tindaklanjuti'),
(259, 'AAE15B78477711EB96FD2CFDA1251793', '98084E50477711EB96FD2CFDA1251793', 'kalabbhs211', 'pelaklabbhs222', '2020-12-26 12:41:16', 'segera'),
(258, 'AAE17DCA477711EB96FD2CFDA1251793', '98084E50477711EB96FD2CFDA1251793', 'kalabbhs211', 'pelaklabbhs222', '2020-12-26 12:41:16', 'Tindaklanjuti'),
(248, 'AB58A0EE46EC11EB86592CFDA1251793', 'D6CC6850141311EB9ACC2CFDA1251793', 'kasubbag1', 'pelaksana3', '2020-12-25 20:06:23', 'tes'),
(247, 'AB58F69846EC11EB86592CFDA1251793', 'D6CC6850141311EB9ACC2CFDA1251793', 'kasubbag1', 'pelaksana3', '2020-12-25 20:06:23', 'Koordinasi/Konfirmasi dengan Ybs/Unit Kerja Terkait'),
(49, 'ABE3B56B11CD11EBA64D2CFDA1251793', '545335F011CD11EBA64D2CFDA1251793', 'kabag2', 'rektor', '2020-10-19 05:41:01', 'Mohon disposisi'),
(61, 'AC420334135411EBA60A2CFDA1251793', '545335F011CD11EBA64D2CFDA1251793', 'kabag2', '', '2020-10-21 04:19:43', 'tes'),
(262, 'AD7BFC5D477A11EB96FD2CFDA1251793', '06874106141811EB9ACC2CFDA1251793', 'rektor', 'biro_umum', '2020-12-26 13:02:49', 'tes'),
(263, 'AD7C1E9E477A11EB96FD2CFDA1251793', '06874106141811EB9ACC2CFDA1251793', 'rektor', 'biro_umum', '2020-12-26 13:02:49', 'Koordinasi/Konfirmasi dengan Ybs/Unit Kerja Terkait'),
(122, 'AEB6C68D1DEB11EBB5512CFDA1251793', 'F3430781112D11EB846E2CFDA1251793', 'kasubbag3', 'pelaksana10', '2020-11-03 15:46:05', 'tes'),
(121, 'AEB6ED381DEB11EBB5512CFDA1251793', 'F3430781112D11EB846E2CFDA1251793', 'kasubbag3', 'pelaksana10', '2020-11-03 15:46:05', 'Tindaklanjuti'),
(228, 'AF2821ED46AF11EBA1882CFDA1251793', 'D6CC6850141311EB9ACC2CFDA1251793', 'biro_umum', 'kabag1', '2020-12-25 12:49:49', 'Hubungi bapak B untuk mendapatkan masukan terkait perihal tersebut'),
(227, 'AF2850FF46AF11EBA1882CFDA1251793', 'D6CC6850141311EB9ACC2CFDA1251793', 'biro_umum', 'kabag1', '2020-12-25 12:49:49', 'Pelajari/Tela\'ah dan Berikan Saran/Masukan'),
(133, 'AF67A30B2BC411EBBEB92CFDA1251793', '3ED974142BC411EBBEB92CFDA1251793', 'kalabdas', 'teknisi_labor', '2020-11-21 06:42:07', 'Tinjut'),
(132, 'AF67E7C82BC411EBBEB92CFDA1251793', '3ED974142BC411EBBEB92CFDA1251793', 'kalabdas', 'teknisi_labor', '2020-11-21 06:42:07', 'Tindaklanjuti'),
(10, 'B016D8EF0E8B11EB8C442CFDA1251793', '24D42E110B9611EBBB0B2CFDA1251793', 'rektor', 'biro2', '2020-10-15 02:09:22', 'TES Memo ke Biro\r\n'),
(34, 'B04DAD60110111EB846E2CFDA1251793', 'C395F91C110011EB846E2CFDA1251793', 'kasubbag', 'pelaksana', '2020-10-18 05:20:51', 'Laksanakan bapak pelaksana'),
(206, 'B068FEC545FA11EBA1882CFDA1251793', 'D6CC6850141311EB9ACC2CFDA1251793', 'biro_umum', 'kabag1', '2020-12-24 15:14:15', 'Silahkan hubungi pak ABC untuk proses tindaklanjut'),
(205, 'B06922A045FA11EBA1882CFDA1251793', 'D6CC6850141311EB9ACC2CFDA1251793', 'biro_umum', 'kabag1', '2020-12-24 15:14:15', 'Proses Sesuai Ketentuan/Peraturan yang Berlaku'),
(106, 'B15F80C3141A11EB9ACC2CFDA1251793', '30D0FB32141A11EB9ACC2CFDA1251793', 'rektor', 'biro_umum', '2020-10-22 03:57:21', 'Lanjutkan, dan pastikan tidak ada masalah sampai hari H'),
(107, 'B15FC8CD141A11EB9ACC2CFDA1251793', '30D0FB32141A11EB9ACC2CFDA1251793', 'rektor', 'biro_umum', '2020-10-22 03:57:21', 'Koordinasi/Konfirmasi dengan Ybs/Unit Kerja Terkait'),
(265, 'B41B09E2477B11EB96FD2CFDA1251793', '9B729C1D477B11EB96FD2CFDA1251793', 'kabag1000', 'rektor', '2020-12-26 13:10:10', 'tes'),
(266, 'B41B2C55477B11EB96FD2CFDA1251793', '9B729C1D477B11EB96FD2CFDA1251793', 'kabag1000', 'rektor', '2020-12-26 13:10:10', 'Tindaklanjuti'),
(250, 'B534270A46ED11EB86592CFDA1251793', 'D6CC6850141311EB9ACC2CFDA1251793', 'kasubbag1', 'pelaksana6', '2020-12-25 20:13:49', 'tes'),
(249, 'B5349E8346ED11EB86592CFDA1251793', 'D6CC6850141311EB9ACC2CFDA1251793', 'kasubbag1', 'pelaksana6', '2020-12-25 20:13:49', 'Pelajari/Tela\'ah dan Berikan Saran/Masukan'),
(190, 'B680516545F411EBA1882CFDA1251793', 'D6CC6850141311EB9ACC2CFDA1251793', 'biro_umum', 'kabag1', '2020-12-24 14:31:32', ''),
(189, 'B6808B6745F411EBA1882CFDA1251793', 'D6CC6850141311EB9ACC2CFDA1251793', 'biro_umum', 'kabag1', '2020-12-24 14:31:32', 'Tindaklanjuti'),
(88, 'B6E6E552141711EB9ACC2CFDA1251793', 'D6CC6850141311EB9ACC2CFDA1251793', 'rektor', 'biro_umum', '2020-12-15 17:00:00', 'telaah'),
(89, 'B6E7823A141711EB9ACC2CFDA1251793', 'D6CC6850141311EB9ACC2CFDA1251793', 'rektor', 'biro_umum', '2020-10-22 03:36:02', 'Pelajari/Tela\'ah dan Berikan Saran/Masukan'),
(62, 'B8231A25135411EBA60A2CFDA1251793', '545335F011CD11EBA64D2CFDA1251793', 'kabag2', 'kasubbag3', '2020-10-21 04:20:03', 'tes'),
(154, 'B841D2933F5611EB87AA2CFDA1251793', '06874106141811EB9ACC2CFDA1251793', 'biro_umum', 'kabag1', '2020-12-16 04:25:22', '-\r\n'),
(153, 'B842140A3F5611EB87AA2CFDA1251793', '06874106141811EB9ACC2CFDA1251793', 'biro_umum', 'kabag1', '2020-12-16 04:25:22', 'Tindaklanjuti'),
(21, 'BBA0A3FD0E9B11EB8C442CFDA1251793', 'BBFDE6FE0E9411EB8C442CFDA1251793', 'kasubbag', 'staff', '2020-10-15 04:04:13', 'Laksanakan'),
(13, 'BBFE0C0C0E9411EB8C442CFDA1251793', 'BBFDE6FE0E9411EB8C442CFDA1251793', 'staff', 'rektor', '2020-10-15 03:14:07', 'Ditujuan untuk seluruh ASN melakukan tes SWAB'),
(214, 'BBFF8ACF46A011EBA1882CFDA1251793', 'D6CC6850141311EB9ACC2CFDA1251793', 'biro_umum', 'kabag1', '2020-12-25 11:02:48', 'tes memo tambahan'),
(213, 'BBFFCE8746A011EBA1882CFDA1251793', 'D6CC6850141311EB9ACC2CFDA1251793', 'biro_umum', 'kabag1', '2020-12-25 11:02:48', 'Pelajari/Tela\'ah dan Berikan Saran/Masukan'),
(124, 'C08C59F51DEB11EBB5512CFDA1251793', 'F3430781112D11EB846E2CFDA1251793', 'kasubbag3', 'pelaksana10', '2020-11-03 15:46:34', 'yaaa'),
(123, 'C08C7EC81DEB11EBB5512CFDA1251793', 'F3430781112D11EB846E2CFDA1251793', 'kasubbag3', 'pelaksana10', '2020-11-03 15:46:34', 'Untuk Diketahui'),
(156, 'C330E7503F5611EB87AA2CFDA1251793', 'D6CC6850141311EB9ACC2CFDA1251793', 'biro_umum', 'kabag1', '2020-12-16 04:25:40', '-'),
(155, 'C3316CD03F5611EB87AA2CFDA1251793', 'D6CC6850141311EB9ACC2CFDA1251793', 'biro_umum', 'kabag1', '2020-12-16 04:25:40', 'Koordinasi/Konfirmasi dengan Ybs/Unit Kerja Terkait'),
(29, 'C3962731110011EB846E2CFDA1251793', 'C395F91C110011EB846E2CFDA1251793', 'admin', 'kabag', '2020-10-18 05:14:14', '-'),
(145, 'C4BF65B23F4511EB87AA2CFDA1251793', 'D6CC6850141311EB9ACC2CFDA1251793', 'rektor', 'biro_umum', '2020-12-16 02:24:02', '-'),
(146, 'C4BF8FA43F4511EB87AA2CFDA1251793', 'D6CC6850141311EB9ACC2CFDA1251793', 'rektor', 'biro_umum', '2020-12-16 02:24:02', 'Tindaklanjuti'),
(255, 'C4F35819477611EB96FD2CFDA1251793', 'C4F3318D477611EB96FD2CFDA1251793', 'labbahasa', '000011111', '2020-12-26 12:34:51', '-'),
(143, 'C88AD20B3E8211EB83F62CFDA1251793', '6E6998203E8211EB83F62CFDA1251793', 'kabag121', 'rektor', '2020-12-15 03:08:08', '-'),
(144, 'C88B29223E8211EB83F62CFDA1251793', '6E6998203E8211EB83F62CFDA1251793', 'kabag121', 'rektor', '2020-12-15 03:08:08', 'Untuk Diketahui'),
(130, 'C9A259DC2BA411EBBEB92CFDA1251793', '72C5E4362B5611EBBEB92CFDA1251793', 'kaupttik', 'pelaksana_tik1', '2020-11-21 02:53:52', 'Laksana di Lab TIK '),
(129, 'C9A289E92BA411EBBEB92CFDA1251793', '72C5E4362B5611EBBEB92CFDA1251793', 'kaupttik', 'pelaksana_tik1', '2020-11-21 02:53:52', 'Tindaklanjuti'),
(220, 'CA286A3446A111EBA1882CFDA1251793', 'D6CC6850141311EB9ACC2CFDA1251793', 'biro_umum', 'kabag1', '2020-12-25 11:10:21', 'tes'),
(219, 'CA28A08346A111EBA1882CFDA1251793', 'D6CC6850141311EB9ACC2CFDA1251793', 'biro_umum', 'kabag1', '2020-12-25 11:10:21', 'Koordinasi/Konfirmasi dengan Ybs/Unit Kerja Terkait'),
(208, 'CDBA00B445FB11EBA1882CFDA1251793', 'D6CC6850141311EB9ACC2CFDA1251793', 'biro_umum', 'kabag1', '2020-12-24 15:22:14', 'Tanya kepada pak si B untuk saran dan masukan terkait masalah ini'),
(207, 'CDBA4DF745FB11EBA1882CFDA1251793', 'D6CC6850141311EB9ACC2CFDA1251793', 'biro_umum', 'kabag1', '2020-12-24 15:22:14', 'Pelajari/Tela\'ah dan Berikan Saran/Masukan'),
(141, 'D00A70972E2D11EBB1F82CFDA1251793', '20D61C952DFD11EBB1F82CFDA1251793', 'sek_sp', 'anggota_sp', '2020-11-24 08:19:37', 'Gas'),
(140, 'D00A96A62E2D11EBB1F82CFDA1251793', '20D61C952DFD11EBB1F82CFDA1251793', 'sek_sp', 'anggota_sp', '2020-11-24 08:19:37', 'Tindaklanjuti'),
(170, 'D0809F28403B11EB97F42CFDA1251793', '06874106141811EB9ACC2CFDA1251793', 'biro_umum', 'kabag121', '2020-12-17 07:45:21', 'Dipersiapkan SPT / SPPD untuk 1 orang'),
(169, 'D080C307403B11EB97F42CFDA1251793', '06874106141811EB9ACC2CFDA1251793', 'biro_umum', 'kabag121', '2020-12-17 07:45:21', 'Pelajari/Tela\'ah dan Berikan Saran/Masukan'),
(65, 'D3D635F6135611EBA60A2CFDA1251793', '545335F011CD11EBA64D2CFDA1251793', 'kabag2', '', '2020-10-21 04:35:08', 'TES'),
(162, 'D3F7173D3F7111EBB5682CFDA1251793', 'A2C2F804144011EBB8712CFDA1251793', 'kasubbag1', 'pelaksana5', '2020-12-16 07:39:28', 'Persiapkan pembayaran terkait sosialisasi kartu indonesia pintar'),
(161, 'D3F7409F3F7111EBB5682CFDA1251793', 'A2C2F804144011EBB8712CFDA1251793', 'kasubbag1', 'pelaksana5', '2020-12-16 07:39:28', 'Agendakan/Persiapkan/Koordinasikan'),
(157, 'D43198EA3F6F11EBB5682CFDA1251793', 'A2C2F804144011EBB8712CFDA1251793', 'kabag1', 'kasubbag1', '2020-12-16 07:25:09', '-'),
(158, 'D431DA2E3F6F11EBB5682CFDA1251793', 'A2C2F804144011EBB8712CFDA1251793', 'kabag1', 'kasubbag1', '2020-12-16 07:25:09', 'Teruskan'),
(272, 'D460AA61486811EB8C842CFDA1251793', '72C5E4362B5611EBBEB92CFDA1251793', 'kaupttik', 'tik', '2020-12-27 17:27:44', 'tes'),
(271, 'D460CE01486811EB8C842CFDA1251793', '72C5E4362B5611EBBEB92CFDA1251793', 'kaupttik', 'tik', '2020-12-27 17:27:44', 'Proses Sesuai Ketentuan/Peraturan yang Berlaku'),
(245, 'D5AD7D9246EB11EB86592CFDA1251793', '06874106141811EB9ACC2CFDA1251793', 'kabag1', 'kasubbag1', '2020-12-25 20:00:24', 'tes'),
(246, 'D5ADD79146EB11EB86592CFDA1251793', '06874106141811EB9ACC2CFDA1251793', 'kabag1', 'kasubbag1', '2020-12-25 20:00:24', 'Pelajari/Tela\'ah dan Berikan Saran/Masukan'),
(80, 'D6CC9655141311EB9ACC2CFDA1251793', 'D6CC6850141311EB9ACC2CFDA1251793', 'admin', 'kabag2', '2020-10-22 03:08:17', 'From Dinas Sosial Kota Langsa'),
(16, 'D7B78AC40E9511EB8C442CFDA1251793', 'BBFDE6FE0E9411EB8C442CFDA1251793', 'biro2', 'kabag', '2020-10-15 03:22:03', 'Lanjutkan, lakukan dengan penerapan Protokol Kesehatan '),
(73, 'D9A97CD7140B11EB9ACC2CFDA1251793', 'D9A94C15140B11EB9ACC2CFDA1251793', 'admin', 'Pilih Disposisi', '2020-10-22 02:11:06', '-'),
(109, 'DB763F91141A11EB9ACC2CFDA1251793', '30D0FB32141A11EB9ACC2CFDA1251793', 'biro_umum', 'kabag2', '2020-10-22 03:58:31', 'Koordinasikan dengan pak herman untuk izin penggunaan gedung'),
(108, 'DB76898F141A11EB9ACC2CFDA1251793', '30D0FB32141A11EB9ACC2CFDA1251793', 'biro_umum', 'kabag2', '2020-10-22 03:58:31', 'Agendakan/Persiapkan/Koordinasikan'),
(41, 'E911A135113B11EB846E2CFDA1251793', 'E9117FDF113B11EB846E2CFDA1251793', 'admin', 'kabag2', '2020-10-18 12:17:38', 'Arahan untuk melakukan tes SWAB'),
(68, 'E95CFFE2140811EB9ACC2CFDA1251793', '545335F011CD11EBA64D2CFDA1251793', 'kabag2', 'kasubbag3', '2020-10-22 01:50:07', 'agendakan'),
(74, 'E982AD6B140B11EB9ACC2CFDA1251793', 'E9827AD0140B11EB9ACC2CFDA1251793', 'admin', 'kabag2', '2020-10-22 02:11:33', '-'),
(45, 'E9C6395B116B11EB846E2CFDA1251793', 'E9117FDF113B11EB846E2CFDA1251793', 'biro_akpk', 'kabag3', '2020-10-18 18:01:15', 'Lanjutkan Kabag Akademik'),
(40, 'ECFA93A1113A11EB846E2CFDA1251793', 'F3430781112D11EB846E2CFDA1251793', 'kabag2', 'rektor', '2020-10-18 12:10:35', 'teruskan'),
(178, 'F1773AB045BA11EBA1882CFDA1251793', 'D6CC6850141311EB9ACC2CFDA1251793', 'biro_umum', 'kabag121', '2020-12-24 07:38:00', ''),
(177, 'F1775F3D45BA11EBA1882CFDA1251793', 'D6CC6850141311EB9ACC2CFDA1251793', 'biro_umum', 'kabag121', '2020-12-24 07:38:00', 'Tindaklanjuti'),
(14, 'F2A0F2F60E9411EB8C442CFDA1251793', 'BBFDE6FE0E9411EB8C442CFDA1251793', 'rektor', 'biro', '2020-10-15 03:15:39', 'Lakukan sesuai arahan dalam surat terlampir, dukung sepenuhnya fasilitas untuk mensukseskan agenda tersebut'),
(37, 'F3432A4B112D11EB846E2CFDA1251793', 'F3430781112D11EB846E2CFDA1251793', 'admin', 'kabag2', '2020-10-18 10:37:42', '-'),
(270, 'F5E14460484F11EB8C842CFDA1251793', '06874106141811EB9ACC2CFDA1251793', 'kasubbag3', 'admin', '2020-12-27 14:29:42', 'tes'),
(269, 'F5E1BF6A484F11EB8C842CFDA1251793', '06874106141811EB9ACC2CFDA1251793', 'kasubbag3', 'admin', '2020-12-27 14:29:42', 'Tindaklanjuti'),
(64, 'F752110F135511EBA60A2CFDA1251793', '545335F011CD11EBA64D2CFDA1251793', 'kabag2', 'kasubbag3', '2020-10-21 04:28:58', 'tes'),
(26, 'F84E9DD30E9E11EB8C442CFDA1251793', '0CEE81840E9E11EB8C442CFDA1251793', 'biro2', 'kabag', '2020-10-15 04:27:23', 'Lanjutkan pak, tolong pastikan fasilitas di sediakan'),
(198, 'F87F1F2F45F511EBA1882CFDA1251793', 'D6CC6850141311EB9ACC2CFDA1251793', 'biro_umum', 'kabag1000', '2020-12-24 14:40:32', ''),
(197, 'F87FD2E545F511EBA1882CFDA1251793', 'D6CC6850141311EB9ACC2CFDA1251793', 'biro_umum', 'kabag1000', '2020-12-24 14:40:32', 'Pelajari/Tela\'ah dan Berikan Saran/Masukan'),
(222, 'FB2D5D1846A111EBA1882CFDA1251793', 'D6CC6850141311EB9ACC2CFDA1251793', 'biro_umum', 'kabag1', '2020-12-25 11:11:43', 'tes'),
(221, 'FB2D803946A111EBA1882CFDA1251793', 'D6CC6850141311EB9ACC2CFDA1251793', 'biro_umum', 'kabag1', '2020-12-25 11:11:43', 'Dijawab'),
(22, 'FB3B9F0F0E9C11EB8C442CFDA1251793', 'BBFDE6FE0E9411EB8C442CFDA1251793', 'kasubbag', 'pelaksana', '2020-10-15 04:13:09', 'Tes memo Pelaksana'),
(241, 'FEA7E6E246C011EB86592CFDA1251793', 'D6CC6850141311EB9ACC2CFDA1251793', 'kabag121', 'kasubbagtes22', '2020-12-25 14:53:48', 'TES'),
(242, 'FEA8AED046C011EB86592CFDA1251793', 'D6CC6850141311EB9ACC2CFDA1251793', 'kabag121', 'kasubbagtes22', '2020-12-25 14:53:48', 'Koordinasi/Konfirmasi dengan Ybs/Unit Kerja Terkait'),
(240, 'FFB2A82546BC11EB86592CFDA1251793', '06874106141811EB9ACC2CFDA1251793', 'biro_umum', 'kabag121', '2020-12-25 14:25:12', 'TES'),
(239, 'FFB3464346BC11EB86592CFDA1251793', '06874106141811EB9ACC2CFDA1251793', 'biro_umum', 'kabag121', '2020-12-25 14:25:12', 'Proses Sesuai Ketentuan/Peraturan yang Berlaku'),
(7, 'wdw', '24D42E110B9611EBBB0B2CFDA1251793', 'staff3e', 'rektor', '2020-10-13 14:34:20', 'TES');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_staff`
--

CREATE TABLE `tbl_staff` (
  `staff_no` int(11) NOT NULL,
  `staff_uuid` varchar(32) NOT NULL,
  `staff_username` varchar(20) NOT NULL,
  `staff_nama` varchar(30) NOT NULL,
  `staff_posisi` varchar(55) NOT NULL,
  `staff_uuid_kasubbag` varchar(32) NOT NULL,
  `staff_time` timestamp NOT NULL DEFAULT current_timestamp(),
  `staff_kategori` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tbl_staff`
--

INSERT INTO `tbl_staff` (`staff_no`, `staff_uuid`, `staff_username`, `staff_nama`, `staff_posisi`, `staff_uuid_kasubbag`, `staff_time`, `staff_kategori`) VALUES
(28, '28', 'anggota_sp', 'Tuti Meutia, S.E., M.Si.', 'Anggota SP', '14', '2020-12-10 05:01:42', 'spi'),
(40, 'D01A962F3B8F11EBA5442CFDA1251793', 'pelaklabbhs222', 'Ilham Ramadha, S.Tr.Kom A', 'Pelaksana Lab Bahasa', '0EBCAC743B9011EBA5442CFDA1251793', '2020-12-11 09:03:54', 'upt lab bahasa'),
(39, '125934643B5F11EBA5442CFDA1251793', 'pelaklabdas12', 'Ilham Ramadha, S.Tr.Kom 2', 'Teknisi Laboratorium', '12', '2020-12-11 03:14:58', 'upt lab dasar'),
(1, '1', 'pelaksana1', 'Nurhayati, S.Pd., M.Pd.', 'Bendahara Penerimaan', '1', '2020-12-10 05:01:42', 'biro'),
(10, '10', 'pelaksana10', 'Alfadhil, S.H.', 'Pengolah Data', '3', '2020-12-10 05:01:42', 'biro'),
(32, '46D3E8333AC711EBA5442CFDA1251793', 'pelaksana1021', 'Ilham Ramadha, S.Tr.Kom', 'Pengelola Sistem dan Jaringan', '11', '2020-12-10 09:08:27', 'upt tik'),
(11, '11', 'pelaksana11', 'Rahmad Fadela Syahputra, S.E.', 'Pengelola Barang Milik Negara', '4', '2020-12-10 05:01:42', 'biro'),
(12, '12', 'pelaksana12', 'T. Muhammad Juanda, S.Pd.', 'Pengelola Data BMN', '4', '2020-12-10 05:01:42', 'biro'),
(13, '13', 'pelaksana13', 'Zulkhalik, S.Pd.', 'Pengelola Kepegawaian', '5', '2020-12-10 05:01:42', 'biro'),
(14, '14', 'pelaksana14', 'Hari Suryono, S.E.', 'Pengadministrasi Kepegawaian', '5', '2020-12-10 05:01:42', 'biro'),
(15, '15', 'pelaksana15', 'Hidayati, S.E.', 'Pengolah Data Akademik', '6', '2020-12-10 05:01:42', 'biro'),
(16, '16', 'pelaksana16', 'Mardiana Susanti, S.E.', 'Pengelola Data Sarana Pendidikan', '6', '2020-12-10 05:01:42', 'biro'),
(17, '17', 'pelaksana17', 'R. Aja Dwi Syukrillah, S.Kom.', 'Pengelola Informasi Akademik', '7', '2020-12-10 05:01:42', 'biro'),
(18, '18', 'pelaksana18', 'Suryanto, S.H.', 'Pengelola Data Registrasi', '7', '2020-12-10 05:01:42', 'biro'),
(19, '19', 'pelaksana19', 'Nurhayati, S.E.', 'Pengelola Program Minat Bakat & Penalaran Mahasiswa', '8', '2020-12-10 05:01:42', 'biro'),
(2, '2', 'pelaksana2', 'Wardiana, S.Hut.', 'Bendahara Pengeluaran', '1', '2020-12-10 05:01:42', 'biro'),
(20, '20', 'pelaksana20', 'Aza Mariani Ulpa, S.Pd.', 'Pengadministrasian Minat Bakat & Penalaran Mahasiswa', '8', '2020-12-10 05:01:42', 'biro'),
(21, '21', 'pelaksana21', 'Adi Musfadry, S.P.', 'Pengelola Data Pelaksanaan Program dan Anggaran', '9', '2020-12-10 05:01:42', 'biro'),
(22, '22', 'pelaksana22', 'Sasi Eka Putra, S.H.', 'Pengadministrasi Umum', '9', '2020-12-10 05:01:42', 'biro'),
(23, '23', 'pelaksana23', 'Badriah, S.P.', 'Pengelola Data Kerjasama', '10', '2020-12-10 05:01:42', 'biro'),
(24, '24', 'pelaksana24', 'Dailamy, S.H.I.', 'Pengadministrasi Umum', '10', '2020-12-10 05:01:42', 'biro'),
(3, '3', 'pelaksana3', 'Meilin', 'Pengelola Gaji', '1', '2020-12-10 05:01:42', 'biro'),
(4, '4', 'pelaksana4', 'Desi Maulida, S.Pd.', 'Pengolah Data Keuangan', '1', '2020-12-10 05:01:42', 'biro'),
(5, '5', 'pelaksana5', 'Muhammad Asri, S.T.', 'Pengelola Surat Perintah Membayar', '1', '2020-12-10 05:01:42', 'biro'),
(6, '6', 'pelaksana6', 'Munar, S.E.', 'Penata Dokumen Keuangan', '1', '2020-12-10 05:01:42', 'biro'),
(7, '7', 'pelaksana7', '-', 'Penyusun Laporan Keuangan', '2', '2020-12-10 05:01:42', 'biro'),
(8, '8', 'pelaksana8', 'Nuraini, S.Pd.', 'Pengelola Keuangan', '2', '2020-12-10 05:01:42', 'biro'),
(9, '9', 'pelaksana9', 'Hendra, S.H.', 'Pengadministrasi Data & Peraturan UU', '3', '2020-12-10 05:01:42', 'biro'),
(38, 'F011BC8B3B5E11EBA5442CFDA1251793', 'pelaksanalabdas22', 'Ilham Ramadha, S.Tr.Kom', 'Teknisi Laboratorium', '12', '2020-12-11 03:14:00', 'upt lab dasar'),
(25, '25', 'pelaksana_tik1', 'Sa\'adah, S.P. A', 'Bendahara Pengeluaran Pembantu', '6C6E0A603AC811EBA5442CFDA1251793', '2020-12-10 05:01:42', 'upt tik'),
(29, 'C6053E39392611EBB57E2CFDA1251793', 'swqsws', 'Ilham Ramadha, S.Tr.Kom', 'Anggota SP', '14', '2020-12-10 05:01:42', 'spi'),
(27, '27', 'teknisilabbahasa', 'Istiandari', 'Teknisi Laboratorium Bahasa', '13', '2020-12-10 05:01:42', 'upt lab bahasa'),
(26, '26', 'teknisi_labor', 'Bayu Nur Cahyo, A.Md.', 'Teknisi Laboratorium', '12', '2020-12-10 05:01:42', 'upt lab dasar'),
(34, '12BAC39B3AFE11EBA5442CFDA1251793', 'tes111', 'Ilham Ramadha, S.Tr.Kom', 'Anggota SP', 'CC1AC29F392D11EBB57E2CFDA1251793', '2020-12-10 15:40:42', ''),
(35, '4E0222453AFE11EBA5442CFDA1251793', 'tes45', 'Ilham Ramadha, S.Tr.Kom', 'Anggota SP', '1F7D0C2A3A9811EBA5442CFDA1251793', '2020-12-10 15:42:22', 'spi'),
(36, 'BDB6AB2D3B0711EBA5442CFDA1251793', 'tes666', 'Ilham Ramadha, S.Tr.Kom', 'Pengelola Barang Milik Negara', '2', '2020-12-10 16:49:55', ''),
(33, '07B89C153AFE11EBA5442CFDA1251793', 'tess', 'Ilham Ramadha, S.Tr.Kom', 'Pengelola Sistem dan Jaringan 2', '11', '2020-12-10 15:40:24', 'upt tik');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_super_admin`
--

CREATE TABLE `tbl_super_admin` (
  `username` varchar(10) NOT NULL,
  `nama_lengkap` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tbl_super_admin`
--

INSERT INTO `tbl_super_admin` (`username`, `nama_lengkap`) VALUES
('superadmin', 'Ilham Super Admin');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_surat_keluar`
--

CREATE TABLE `tbl_surat_keluar` (
  `out_no` int(11) NOT NULL,
  `out_uuid` varchar(32) NOT NULL,
  `out_urut` int(11) NOT NULL,
  `out_kode_hal` varchar(5) NOT NULL,
  `out_kode_instansi` varchar(30) NOT NULL,
  `out_tujuan` varchar(30) NOT NULL,
  `out_lampiran` varchar(20) NOT NULL,
  `out_perihal` varchar(40) NOT NULL,
  `out_penerbit` varchar(40) NOT NULL,
  `out_dokumen` varchar(255) NOT NULL,
  `out_status` varchar(30) NOT NULL,
  `out_created_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tbl_surat_keluar`
--

INSERT INTO `tbl_surat_keluar` (`out_no`, `out_uuid`, `out_urut`, `out_kode_hal`, `out_kode_instansi`, `out_tujuan`, `out_lampiran`, `out_perihal`, `out_penerbit`, `out_dokumen`, `out_status`, `out_created_at`) VALUES
(14, '0B00E2661ED711EBB5512CFDA1251793', 1000, 'BP', 'UN45.10', 'Ristekdikti', '1', 'Kunjungan kerja', 'pelaksana10', '', 'Menunggu Upload', '2020-11-04 19:50:46'),
(15, '1616200B1ED711EBB5512CFDA1251793', 1000, 'BP', 'UN45', 'Ristekdikti', '1', 'Kunjungan kerja', 'pelaksana10', '', 'Menunggu Upload', '2020-11-04 19:51:05'),
(16, '444AC42C1ED711EBB5512CFDA1251793', 1000, 'AK', 'UN45', 'Ristekdikti', '1', 'Kunjungan kerja', 'pelaksana10', '', 'Menunggu Upload', '2020-11-04 19:52:22'),
(17, '523583911ED711EBB5512CFDA1251793', 1001, 'AK', 'UN45', 'Ristekdikti', '1', 'Kunjungan kerja', 'pelaksana10', '523583911ED711EBB5512CFDA1251793.pdf', 'Done', '2020-11-04 19:52:45'),
(9, '83FF6C7A1ED611EBB5512CFDA1251793', 1, 'AK', 'UN45', 'Ristekdikti', '1', 'Kunjungan kerja', 'pelaksana10', '', 'Menunggu Upload', '2020-11-04 19:46:59'),
(10, '8A9F29F41ED611EBB5512CFDA1251793', 2, 'AK', 'UN45.10', 'Ristekdikti', '1', 'Kunjungan kerja', 'pelaksana10', '', 'Menunggu Upload', '2020-11-04 19:47:11'),
(11, '9610589B1ED611EBB5512CFDA1251793', 3, 'AK', 'UN45', 'Ristekdikti', '1', 'TES COBA', 'pelaksana10', '', 'Menunggu Upload', '2020-11-04 19:47:30'),
(12, 'F1E0A0441ED611EBB5512CFDA1251793', 4, 'AK', 'UN45', 'Ristekdikti', '1', 'TES', 'pelaksana10', '', 'Menunggu Upload', '2020-11-04 19:50:04'),
(13, 'F907B1081ED611EBB5512CFDA1251793', 999, 'AK', 'UN45', 'Ristekdikti', '1', 'TES', 'pelaksana10', 'F907B1081ED611EBB5512CFDA1251793.pdf', 'Done', '2020-11-04 19:50:16');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_unit`
--

CREATE TABLE `tbl_unit` (
  `ka_upt_no` int(11) NOT NULL,
  `ka_upt_uuid` varchar(32) NOT NULL,
  `ka_upt_username` varchar(10) NOT NULL,
  `ka_upt_nama` varchar(30) NOT NULL,
  `ka_upt_posisi` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tbl_unit`
--

INSERT INTO `tbl_unit` (`ka_upt_no`, `ka_upt_uuid`, `ka_upt_username`, `ka_upt_nama`, `ka_upt_posisi`) VALUES
(1, '1', 'ka_upt_tik', 'Bapak Ka UPT TIK', 'Ka. UPT TIK');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_users`
--

CREATE TABLE `tbl_users` (
  `username` varchar(16) NOT NULL,
  `nama_lengkap` varchar(30) NOT NULL,
  `jabatan` varchar(30) NOT NULL,
  `status` varchar(20) NOT NULL,
  `admin_time` timestamp NOT NULL DEFAULT current_timestamp(),
  `uuid_atasan` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tbl_users`
--

INSERT INTO `tbl_users` (`username`, `nama_lengkap`, `jabatan`, `status`, `admin_time`, `uuid_atasan`) VALUES
('admin', 'Ilham Ramadhan', 'Admin Surat Subbag. TU', 'admin surat', '2020-12-15 02:59:06', '3'),
('labbahasa', 'Ilham Ramadhan', 'Admin Surat Lab. Bahasa', 'admin surat', '2020-12-15 02:59:06', ''),
('labdas', 'Ilham Ramadhan', 'Admin Surat Lab. Dasar', 'admin surat', '2020-12-15 02:59:06', ''),
('sp', 'Ilham Ramadhan', 'Admin Surat SP', 'admin surat', '2020-12-15 02:59:06', ''),
('tik', 'Ilham Ramadhan', 'Admin Surat UPT TIK', 'admin surat', '2020-12-15 02:59:06', '11');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `tbl_biro`
--
ALTER TABLE `tbl_biro`
  ADD PRIMARY KEY (`biro_username`),
  ADD UNIQUE KEY `biro_no` (`biro_no`),
  ADD UNIQUE KEY `biro_uuid` (`biro_uuid`);

--
-- Indeks untuk tabel `tbl_distribusi_surat`
--
ALTER TABLE `tbl_distribusi_surat`
  ADD PRIMARY KEY (`dist_uuid`),
  ADD UNIQUE KEY `dist_no` (`dist_no`);

--
-- Indeks untuk tabel `tbl_draft_surat`
--
ALTER TABLE `tbl_draft_surat`
  ADD PRIMARY KEY (`draft_uuid`),
  ADD UNIQUE KEY `draft_no` (`draft_no`);

--
-- Indeks untuk tabel `tbl_kabag`
--
ALTER TABLE `tbl_kabag`
  ADD PRIMARY KEY (`kabag_username`),
  ADD UNIQUE KEY `kabag_no` (`kabag_no`),
  ADD UNIQUE KEY `kabag_uuid` (`kabag_uuid`);

--
-- Indeks untuk tabel `tbl_kasubbag`
--
ALTER TABLE `tbl_kasubbag`
  ADD PRIMARY KEY (`kasubbag_username`),
  ADD UNIQUE KEY `kasubbag_no` (`kasubbag_no`),
  ADD UNIQUE KEY `kasubbag_uuid` (`kasubbag_uuid`);

--
-- Indeks untuk tabel `tbl_kode_hal`
--
ALTER TABLE `tbl_kode_hal`
  ADD PRIMARY KEY (`hal_no`);

--
-- Indeks untuk tabel `tbl_kode_instansi`
--
ALTER TABLE `tbl_kode_instansi`
  ADD PRIMARY KEY (`instansi_no`);

--
-- Indeks untuk tabel `tbl_konsep_draft`
--
ALTER TABLE `tbl_konsep_draft`
  ADD PRIMARY KEY (`konsep_uuid`),
  ADD UNIQUE KEY `konsep_no` (`konsep_no`);

--
-- Indeks untuk tabel `tbl_login`
--
ALTER TABLE `tbl_login`
  ADD PRIMARY KEY (`username`);

--
-- Indeks untuk tabel `tbl_rektor`
--
ALTER TABLE `tbl_rektor`
  ADD PRIMARY KEY (`rektor_no`),
  ADD UNIQUE KEY `rektor_username` (`rektor_username`);

--
-- Indeks untuk tabel `tbl_riwayat_disposisi`
--
ALTER TABLE `tbl_riwayat_disposisi`
  ADD PRIMARY KEY (`riw_uuid`),
  ADD UNIQUE KEY `riw_no` (`riw_no`),
  ADD KEY `riw_dist_uuid` (`riw_dist_uuid`),
  ADD KEY `riw_asal` (`riw_asal`);

--
-- Indeks untuk tabel `tbl_staff`
--
ALTER TABLE `tbl_staff`
  ADD PRIMARY KEY (`staff_username`),
  ADD UNIQUE KEY `staff_no` (`staff_no`),
  ADD UNIQUE KEY `staff_uuid` (`staff_uuid`);

--
-- Indeks untuk tabel `tbl_super_admin`
--
ALTER TABLE `tbl_super_admin`
  ADD PRIMARY KEY (`username`);

--
-- Indeks untuk tabel `tbl_surat_keluar`
--
ALTER TABLE `tbl_surat_keluar`
  ADD PRIMARY KEY (`out_uuid`),
  ADD UNIQUE KEY `out_no` (`out_no`);

--
-- Indeks untuk tabel `tbl_unit`
--
ALTER TABLE `tbl_unit`
  ADD PRIMARY KEY (`ka_upt_username`),
  ADD UNIQUE KEY `ka_upt_no` (`ka_upt_no`),
  ADD UNIQUE KEY `ka_upt_uuid` (`ka_upt_uuid`);

--
-- Indeks untuk tabel `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`username`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `tbl_biro`
--
ALTER TABLE `tbl_biro`
  MODIFY `biro_no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT untuk tabel `tbl_distribusi_surat`
--
ALTER TABLE `tbl_distribusi_surat`
  MODIFY `dist_no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT untuk tabel `tbl_draft_surat`
--
ALTER TABLE `tbl_draft_surat`
  MODIFY `draft_no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT untuk tabel `tbl_kabag`
--
ALTER TABLE `tbl_kabag`
  MODIFY `kabag_no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT untuk tabel `tbl_kasubbag`
--
ALTER TABLE `tbl_kasubbag`
  MODIFY `kasubbag_no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT untuk tabel `tbl_kode_hal`
--
ALTER TABLE `tbl_kode_hal`
  MODIFY `hal_no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `tbl_kode_instansi`
--
ALTER TABLE `tbl_kode_instansi`
  MODIFY `instansi_no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `tbl_konsep_draft`
--
ALTER TABLE `tbl_konsep_draft`
  MODIFY `konsep_no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=61;

--
-- AUTO_INCREMENT untuk tabel `tbl_rektor`
--
ALTER TABLE `tbl_rektor`
  MODIFY `rektor_no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT untuk tabel `tbl_riwayat_disposisi`
--
ALTER TABLE `tbl_riwayat_disposisi`
  MODIFY `riw_no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=273;

--
-- AUTO_INCREMENT untuk tabel `tbl_staff`
--
ALTER TABLE `tbl_staff`
  MODIFY `staff_no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- AUTO_INCREMENT untuk tabel `tbl_surat_keluar`
--
ALTER TABLE `tbl_surat_keluar`
  MODIFY `out_no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT untuk tabel `tbl_unit`
--
ALTER TABLE `tbl_unit`
  MODIFY `ka_upt_no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
